import threading
import Queue
import traceback
import time
import uuid
import datetime

from dateutil.parser import parse as parse_timestamp

from splunktalib.common import log

import taaws.ta_aws_consts as tac
import taaws.ta_aws_common as tacommon
import s3_mod.aws_s3_consts as asc
import s3_mod.aws_s3_common as s3common
import s3_mod.aws_s3_checkpointer as s3ckpt
import s3_mod.s3_key_processer as skp

logger = log.Logs().get_logger(asc.s3_log)

from base import aws_credentials
aws_credentials.logger = logger

CREDENTIAL_THRESHOLD = datetime.timedelta(minutes=20)


class DummyKey(object):

    class DummyBucket(object):

        def __init__(self, name):
            self.name = name

    def __init__(self, bucket_name, key_name):
        self.bucket = DummyKey.DummyBucket(bucket_name)
        self.name = key_name


class S3DataLoader(object):

    def __init__(self, config):
        """
        :task_config: dict
        {
           bucket_name: xxx,
           host: xxx,
           prefix: xxx,
           after: xxx,
           key_character_set: xxx,
           secret_key: xxx,
           checkpoint_dir: xxx,
           server_uri: xxx,
           session_key: xxx,
           use_kv_store: xxx,
           is_secure: xxx,
           proxy_hostname: xxx,
           proxy_port: xxx,
           proxy_username: xxx,
           proxy_password: xxx,
           data_loader: xxx,
        }
        """

        self._config = config
        self._lock = threading.Lock()
        self._config[asc.bucket_name] = str(self._config[asc.bucket_name])
        self._stopped = False
        self._credentials_service = tacommon.create_credentials_service(
            self._config[tac.server_uri], self._config[tac.session_key])
        # Set proxy before getting credential by boto3
        tacommon.set_proxy_env(self._config)

    def get_interval(self):
        return self._config[tac.interval]

    def get_props(self):
        return self._config

    def stop(self):
        self._stopped = True

    def __call__(self):
        self.index_data()

    def index_data(self):
        try:
            self._do_index_data()
        except Exception:
            logger.error(
                "Failed to collect S3 data from datainput=%s, bucket_name=%s, "
                "error=%s", self._config[asc.data_input],
                self._config[asc.bucket_name], traceback.format_exc())

    def _do_index_data(self):
        if self._lock.locked():
            logger.info(
                "Previous run is not done yet for datainput=%s, bucket_name=%s",
                self._config[asc.data_input], self._config[asc.bucket_name])
            return

        start = time.time()
        self._config[asc.job_uid] = str(uuid.uuid4())
        logger.info(
            "Start processing datainput=%s, bucket_name=%s, job_uid=%s",
            self._config[asc.data_input], self._config[asc.bucket_name],
            self._config[asc.job_uid])

        with self._lock:
            self.collect_data()
            if not self._stopped:
                logger.info(
                    "Sweep checkpoint file while discovering done, "
                    "datainput=%s, bucket_name=%s, job_uid=%s",
                    self._config[asc.data_input],
                    self._config[asc.bucket_name],
                    self._config[asc.job_uid])
                s3ckpt.S3CkptPool.sweep_all()

        cost = time.time() - start
        logger.info(
            "End of processing datainput=%s, bucket_name=%s, job_uid=%s, "
            "time_cost=%s", self._config[asc.data_input],
            self._config[asc.bucket_name], self._config[asc.job_uid], cost)

    def _cleanup_index_ckpt(self, index_store):
        last_modified = self._normalize_last_modified(index_store.last_modified())
        if (not last_modified or
                last_modified == self._config.get(asc.last_modified)):
            return

        initial_size = 0
        reaped = 0
        for key_name in index_store.keys():
            initial_size += 1
            if not index_store.get_state(key_name):
                # We are done with this key
                idx_ckpt = index_store.get(key_name)
                key_last_modified = self._normalize_last_modified(idx_ckpt[asc.last_modified])
                if key_last_modified < last_modified:
                    index_store.delete_item(key_name, commit=False)
                    reaped += 1
        index_store.save()
        remaining = initial_size - reaped
        logger.info("Cleaned total=%s ckpt items for bucket_name=%s, "
                    "datainput=%s, remaining=%s",
                    reaped, self._config[asc.bucket_name],
                    self._config[asc.data_input], remaining)

    def _list_dir(self, bucket, prefix):
        pass

    def collect_data(self):
        index_store = s3ckpt.S3IndexCheckpointer(self._config)
        logger.info(
            "Start from last_modified=%s for datainput=%s, bucket_name=%s, "
            "job_uid=%s", index_store.last_modified(),
            self._config[asc.data_input], self._config[asc.bucket_name],
            self._config[asc.job_uid])

        self._discover_keys(index_store)
        self._fetch_keys(index_store)

    def _poll_progress(self, index_store):
        logger.info("Poll data collection progress for bucket_name=%s, "
                    "datainput=%s", index_store.bucket_name(),
                    self._config[asc.data_input])
        sleep_time = min(20, self._config[asc.polling_interval])
        while 1:
            if tacommon.sleep_until(sleep_time, self.stopped):
                return

            done, errors, total = 0, 0, 0
            for key_name in index_store.keys():
                total += 1
                key_ckpt = index_store.get_state(key_name)
                # Note when finished data collection, the data collection
                # thread deletes the key ckpt
                if key_ckpt is None:
                    done += 1
                elif key_ckpt[asc.state] == asc.failed:
                    errors += 1

            if done + errors >= total:
                break
            else:
                logger.info("There are still %s data collection(s) going on for "
                            "bucket_name=%s, datainput=%s",
                            total - done - errors, index_store.bucket_name(),
                            self._config[asc.data_input])

    def _create_ckpts_for_key(self, key, index_store):
        """
        :return: key_store if doing data collection for this key,
        """

        key_store = s3ckpt.S3KeyCheckpointer(self._config, key)
        if key_store.is_new:
            key_store.set_state(asc.started, flush=False)

        # Register this key in the index ckpt
        index_store.add(key.name, key.last_modified, flush=False)
        return key_store

    def _do_collect_data(self, loader_service, key):
        index_func = skp.create_s3_key_processer(
            self._config, loader_service, key, logger)

        while 1:
            try:
                return loader_service.run_io_jobs((index_func,), block=False)
            except Queue.Full:
                logger.debug(
                    "Job Queue is full for datainput=%s, bucket_name=%s, "
                    "job_uid=%s", self._config[asc.data_input],
                    self._config[asc.bucket_name], self._config[asc.job_uid])
                if key.size < 1024 * 1024 * 8:
                    # Do data collection in dispatching thread only if
                    # the key is not too big
                    logger.debug("Dispatch function pigback")
                    return index_func()
                else:
                    time.sleep(2)
            except Exception:
                logger.error(
                    "Failed to run io jobs for datainput=%s, bucket_name=%s, "
                    "job_uid=%s, error=%s", self._config[asc.data_input],
                    self._config[asc.bucket_name], self._config[asc.job_uid],
                    traceback.format_exc())
                time.sleep(2)

    def stopped(self):
        return self._stopped

    def _discover_keys(self, index_store):
        logger.info(
            "Discovering S3 keys started for bucket_name=%s, datainput=%s, "
            "job_uid=%s", self._config[asc.bucket_name],
            self._config[asc.data_input], self._config[asc.job_uid])

        credentials = self._generate_credentials()
        bucket = self._get_bucket(credentials)
        last_modified = self._normalize_last_modified(index_store.last_modified())
        keys = s3common.get_keys(
            bucket, self._config.get(asc.prefix, ""),
            self._config.get(asc.whitelist), self._config.get(asc.blacklist),
            last_modified, None)

        # Loop all keys from S3 bucket
        found = 0
        for key in keys:
            if self._stopped:
                break
            if self._is_key_indexed(key, index_store, last_modified):
                continue
            key_ckpt = self._create_ckpts_for_key(key, index_store)
            if not key_ckpt.is_new:
                continue
            found += 1
            if found % 100 == 0:
                index_store.flush()
            # Check and refresh credential of S3 connection
            credentials = self._check_and_refresh_bucket(bucket, credentials)
            if found % 10000 == 0:
                logger.info(
                    "Discovering S3 keys processing for bucket_name=%s, "
                    "datainput=%s, job_uid=%s, new_key_count=%s",
                    self._config[asc.bucket_name],
                    self._config[asc.data_input],
                    self._config[asc.job_uid], found)
        index_store.flush()

        logger.info(
            "Discovering S3 keys finished for bucket_name=%s, datainput=%s, "
            "job_uid=%s, new_key_total=%s", self._config[asc.bucket_name],
            self._config[asc.data_input], self._config[asc.job_uid], found)

    def _is_key_indexed(self, key, index_store, normalized_last_modified):
        if key.last_modified < normalized_last_modified:
            return True
        try:
            ckpt_idx = index_store.get(key.name)
        except KeyError:
            return False

        # Last modified time is not changed
        prev_last_modified = self._normalize_last_modified(ckpt_idx[asc.last_modified])
        if prev_last_modified >= key.last_modified:
            return True

    def _fetch_keys(self, index_store):
        logger.info(
            "Fetching S3 keys started for bucket_name=%s, datainput=%s, "
            "job_uid=%s", self._config[asc.bucket_name],
            self._config[asc.data_input], self._config[asc.job_uid])

        loader_service = self._config[tac.data_loader_mgr]
        credentials = self._generate_credentials()
        bucket = self._get_bucket(credentials)

        # Loop all existing keys
        min_last_modified = "z"
        key_count = 0
        for key_name in index_store.keys():
            if self._stopped:
                break
            if not index_store.get_state(key_name):
                # Already done with this key
                continue
            key_last_modified = self._fetch_key(bucket, key_name, loader_service)
            if key_last_modified:
                key_count += 1
                if key_last_modified < min_last_modified:
                    min_last_modified = key_last_modified

            # Check credential and create new S3 connection
            if credentials.need_retire(CREDENTIAL_THRESHOLD):
                credentials = self._generate_credentials()
                bucket = self._get_bucket(credentials)

        logger.info(
            "Fetching S3 keys finished for bucket_name=%s, "
            "datainput=%s, job_uid=%s, pending_key_total=%s",
            self._config[asc.bucket_name], self._config[asc.data_input],
            self._config[asc.job_uid], key_count)

        if not self._stopped and key_count > 0:
            logger.info(
                "Update minimum last modified time for bucket_name=%s, "
                "datainput=%s, job_uid=%s, minimum_last_modified=\"%s\"",
                self._config[asc.bucket_name], self._config[asc.data_input],
                self._config[asc.job_uid], min_last_modified)
            index_store.set_last_modified(min_last_modified)
            self._cleanup_index_ckpt(index_store)
            self._poll_progress(index_store)

    def _normalize_last_modified(self, last_modified):
        """
        Normalize timestamp of S3 key which is from bucket.get_key method.
        :param last_modified:
        :return:
        """
        try:
            mtime = parse_timestamp(last_modified)
            return mtime.strftime('%Y-%m-%dT%H:%M:%S.000Z')
        except Exception:
            logger.info(
                "Failed to normalize last modified time for bucket_name=%s, "
                "datainput=%s, job_uid=%s, minimum_last_modified=\"%s\","
                " call_stack=%s", self._config[asc.bucket_name],
                self._config[asc.data_input], self._config[asc.job_uid],
                last_modified, ''.join(traceback.format_stack()))
            raise

    def _fetch_key(self, bucket, key_name, loader_service):
        try:
            key = bucket.get_key(key_name)
            if key is None:
                raise Exception(
                    "{} has been deleted from bucket_name={}".format(
                        key_name, self._config[asc.bucket_name]))
        except Exception:
            logger.error(
                "Failed to get key=%s in bucket_name=%s, error=%s",
                key_name, self._config[asc.bucket_name],
                traceback.format_exc())

            # For deleted S3 key etc
            key = DummyKey(self._config[asc.bucket_name], key_name)
            key_store = s3ckpt.S3KeyCheckpointer(self._config, key)
            skp.increase_error_count(
                key_store, self._config[asc.max_retries], key, logger,
                count=self._config[asc.max_retries])
            return None

        key.last_modified = self._normalize_last_modified(key.last_modified)
        self._do_collect_data(loader_service, key)
        return key.last_modified

    def _get_bucket(self, credentials):
        logger.info(
            "Create new S3 connection for bucket_name=%s, datainput=%s, "
            "job_uid=%s", self._config[asc.bucket_name],
            self._config[asc.data_input], self._config[asc.job_uid])
        self._config[tac.key_id] = credentials.aws_access_key_id
        self._config[tac.secret_key] = credentials.aws_secret_access_key
        self._config['aws_session_token'] = credentials.aws_session_token
        conn = s3common.create_s3_connection(self._config)
        bucket = conn.get_bucket(self._config[asc.bucket_name])
        return bucket

    def _check_and_refresh_bucket(self, bucket, credentials):
        if not credentials.need_retire(CREDENTIAL_THRESHOLD):
            return credentials
        logger.info(
            "Refresh credentials of S3 connection for bucket_name=%s, "
            "datainput=%s, job_uid=%s", self._config[asc.bucket_name],
            self._config[asc.data_input], self._config[asc.job_uid])
        credentials = self._generate_credentials()
        tacommon.update_boto2_connection(bucket.connection, credentials)
        return credentials

    def _generate_credentials(self):
        return self._credentials_service.load(
            self._config[tac.aws_account],
            self._config.get(tac.aws_iam_role),
        )
