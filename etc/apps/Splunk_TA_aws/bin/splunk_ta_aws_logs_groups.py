"""
Get sourcetype list in Splunk server.
"""

import splunk_ta_aws_import_lib_declare

import json
import logging

import boto.logs

from splunk import admin, rest


from splunktalib.rest_manager import util, error_ctl
import taaws.ta_aws_common as tacommon
import taaws.ta_aws_consts as tac
import splunktalib.common.util as scutil


class LogsGroupHandler(admin.MConfigHandler):

    def setup(self):
        self.supportedArgs.addReqArg('aws_region')
        self.supportedArgs.addReqArg('aws_account')

    def user_app(self):
        app = self.context != admin.CONTEXT_NONE and self.appName or "-"
        user = self.context == admin.CONTEXT_APP_AND_USER and self.userName or "nobody"
        return user, app

    def handleList(self, confInfo):
        awsAccountName = self.callerArgs['aws_account'][0] or "default"
        awsRegion = self.callerArgs['aws_region'][0]

        user, app = self.user_app()

        config = {tac.region: awsRegion}

        # get AWS account
        try:
            url = '{uri}/servicesNS/{user}/{app}/splunk_ta_aws/settings/account/{account}?output_mode=json'.format(uri=rest.makeSplunkdUri(), user=user, app=app, account=awsAccountName)
            response, content = rest.simpleRequest(url, sessionKey=self.getSessionKey(), method='GET', raiseAllErrors=True)
            res = json.loads(content)
            awsAccount = res['entry'][0]['content']
        except Exception as exc:
            error_ctl.RestHandlerError.ctl(400, msgx='Fail to get AWS account: {}'.format(awsAccountName), logLevel=logging.INFO)

        # get proxy info
        try:
            url = '{uri}/servicesNS/{user}/{app}/splunk_ta_aws/settings/proxy/aws_proxy?output_mode=json'.format(uri=rest.makeSplunkdUri(), user=user, app=app)
            response, content = rest.simpleRequest(url, sessionKey=self.getSessionKey(), method='GET', raiseAllErrors=True)
            res = json.loads(content)
            ent = res.get('entry')
            if ent and ent[0].get('content'):
                cnt = ent[0].get('content')
                if scutil.is_false(cnt.get('disabled')):
                    config[tac.proxy_host] = cnt["host"]
                    config[tac.proxy_port] = cnt["port"]
                    config[tac.proxy_username] = cnt["username"]
                    config[tac.proxy_password] = cnt["password"]
        except Exception as exc:
            error_ctl.RestHandlerError.ctl(-1, msgx=exc, logLevel=logging.INFO)

        # get AWS Connection
        try:
            url = '{uri}/servicesNS/{user}/{app}/splunk_ta_aws/settings/global/aws_connection?output_mode=json'.format(uri=rest.makeSplunkdUri(), user=user, app=app)
            response, content = rest.simpleRequest(url, sessionKey=self.getSessionKey(), method='GET', raiseAllErrors=True)
            res = json.loads(content)
            awsConnection = res['entry'][0]['content']
        except Exception as exc:
            error_ctl.RestHandlerError.ctl(-1, msgx=exc, logLevel=logging.INFO)

        # connect to AWS Logs
        config[tac.key_id] = awsAccount[tac.key_id]
        config[tac.secret_key] = awsAccount[tac.secret_key]

        conn = tacommon.connect_service_to_region(
            boto.logs.connect_to_region, self._task_config)

        if conn is None:
            error_ctl.RestHandlerError.ctl(400, msgx='Fail to connect to AWS region "{region}" with account "{account}" - Exception: {exc}'.format(region=awsRegion, account=awsAccountName, exc=exc), logLevel=logging.INFO,  shouldPrint=False, shouldRaise=False)
        ent = {'logs_groups': ([] if conn is None else [logGroup['logGroupName'] for logGroup in conn.describe_log_groups()['logGroups']])}

        util.makeConfItem('splunk_ta_aws_logs_groups', ent, confInfo)


if __name__ == '__main__':
    admin.init(LogsGroupHandler, admin.CONTEXT_APP_AND_USER)
