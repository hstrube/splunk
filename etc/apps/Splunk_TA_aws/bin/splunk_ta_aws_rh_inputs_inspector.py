import splunk_ta_aws_import_lib_declare
import splunk.admin as admin

from splunktalib.rest_manager import base, normaliser


class Inspector(base.BaseModel):

    """ AWS Inspector Inputs Model. """

    endpoint = "configs/conf-aws_inspector_tasks"

    requiredArgs = ['aws_account', 'aws_regions']

    optionalArgs = ['interval', 'sourcetype', 'index']

    defaultVals = {
        'interval': '600',
        'sourcetype': 'aws:inspector',
        'index': 'default',
    }

    normalisers = {
        'disabled': normaliser.Boolean(),
    }

    keyMap = {
        'aws_account': 'account',
        'aws_regions': 'regions',
        'interval': 'polling_interval'
    }

    cap4disable = 'ta_aws_custom_action_disable'
    cap4enable = 'ta_aws_custom_action_enable'


if __name__ == "__main__":
    admin.init(base.ResourceHandler(Inspector), admin.CONTEXT_APP_AND_USER)
