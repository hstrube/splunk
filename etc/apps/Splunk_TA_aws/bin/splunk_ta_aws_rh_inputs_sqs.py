
import splunk_ta_aws_import_lib_declare

import splunk.admin as admin

from splunktalib.rest_manager import base, normaliser


class SQS(base.BaseModel):
    """
    AWS Config Inputs Model.
    """
    endpoint = 'configs/conf-aws_sqs_tasks'
    requiredArgs = [
        'aws_account',
        'aws_region',
        'sqs_queues',
    ]
    optionalArgs = [
        'interval',
        'sourcetype',
        'index',
        'disabled',
    ]

    defaultVals = {
        'sourcetype': 'aws:sqs',
        'interval': '30',
    }
    validators = {}
    normalisers = {
        'disabled': normaliser.Boolean(),
    }
    cap4disable = 'ta_aws_custom_action_disable'
    cap4enable = 'ta_aws_custom_action_enable'


if __name__ == '__main__':
    admin.init(
        base.ResourceHandler(SQS),
        admin.CONTEXT_APP_AND_USER
    )
