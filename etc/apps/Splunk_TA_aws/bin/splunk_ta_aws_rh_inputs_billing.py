
import splunk_ta_aws_import_lib_declare

import urllib
from datetime import datetime
from dateutil.relativedelta import relativedelta

import splunk.admin as admin

from solnlib.splunkenv import get_splunkd_uri
from splunktalib.rest import splunkd_request
from splunktalib.rest_manager import base, datainput, validator, normaliser
from splunktalib.rest_manager.error_ctl import RestHandlerError as RH_Err


def months_ago(months):
    utcnow = datetime.utcnow()
    months_later = utcnow.today() + relativedelta(months=months)
    return months_later.strftime('%Y-%m-%dT%H:%M:%SZ')


class BillingInputHandler(datainput.DataInputHandler):

    def handleCreate(self, confInfo):
        self.validate_datetime()
        datainput.DataInputHandler.handleCreate(self, confInfo)

    def handleEdit(self, confInfo):
        self.validate_datetime()
        datainput.DataInputHandler.handleEdit(self, confInfo)

    def validate_datetime(self):
        datetime_str = self.callerArgs.data.get('initial_scan_datetime')
        if not datetime_str or not datetime_str[0] or \
                datetime_str[0] == 'default':
            return
        datetime = urllib.quote(
            self.callerArgs.data['initial_scan_datetime'][0], safe='')
        endpoint = '/services/search/timeparser'
        url = get_splunkd_uri() + endpoint + '?time=' + datetime
        resp, cont = splunkd_request(url, self.getSessionKey())
        if resp and resp.status in (400, '400'):
            RH_Err.ctl(400, msgx='Invalid Start Date/Time')
        elif resp is None or resp.status not in (200, '200'):
            RH_Err.ctl(500, msgx=cont)


class BillingInput(datainput.DataInputModel):
    """
    AWS Billing Inputs Model.
    """
    dataInputName = "aws_billing"
    requiredArgs = ['aws_account', 'bucket_name']
    optionalArgs = [
        'aws_iam_role',
        'host_name',
        'monthly_report_type',
        'initial_scan_datetime',
        'detail_report_type',
        'report_file_match_reg',
        'interval',
        'sourcetype',
        'host',
        'index',
    ]
    defaultVals = {
        'host_name': 's3.amazonaws.com',
        'monthly_report_type': '2',
        'detail_report_type': '2',
        'initial_scan_datetime': months_ago(-3),
        'interval': '86400',
        'sourcetype': 'aws:billing',
    }
    normalisers = {
        'disabled': normaliser.Boolean(),
    }
    validators = {
        'monthly_report_type': validator.Enum(('0', '1', '2')),
        'detail_report_type': validator.Enum(('0', '1', '2')),
    }
    valMap = {
        'monthly_report_type': {
            '0': 'None',
            '1': 'Monthly report',
            '2': 'Monthly cost allocation report'
        },
        'detail_report_type': {
            '0': 'None',
            '1': 'Detailed billing report',
            '2': 'Detailed billing report with resources and tags'
        }
    }
    cap4disable = 'ta_aws_custom_action_disable'
    cap4enable = 'ta_aws_custom_action_enable'

if __name__ == "__main__":
    admin.init(
        base.ResourceHandler(BillingInput, handler=BillingInputHandler),
        admin.CONTEXT_APP_AND_USER,
    )
