"""
Custom REST Endpoint for enumerating AWS S3 Bucket.
"""

import splunk_ta_aws_import_lib_declare

from splunktalib.common import log
logger = log.Logs("splunk_ta_aws").get_logger("rest", level="DEBUG")
log_enter_exit = log.log_enter_exit(logger)


import splunk
import splunk.admin

import taaws.s3util
import taaws.ta_aws_common as tacommon
import taaws.ta_aws_consts as tac
from taaws.s3util import connect_s3
from splunk_ta_aws_s3buckets_handler import timed


def lv1_keys(s3_conn, bucket_name):
    bucket = s3_conn.get_bucket(bucket_name)

    rlist = []
    for key in taaws.s3util.get_keys(bucket, recursion_depth=1):
        rlist.append(key.name)
    return rlist


class ConfigHandler(splunk.admin.MConfigHandler):

    @log_enter_exit
    def setup(self):
        self.supportedArgs.addReqArg('bucket_name')
        self.supportedArgs.addReqArg('aws_account')
        self.supportedArgs.addReqArg('host_name')

    @log_enter_exit
    def handleList(self, confInfo):
        key_id, secret_key = tacommon.assert_creds(
            self.callerArgs[tac.aws_account][0], self.getSessionKey(), logger)

        host_name = ""
        if self.callerArgs['host_name'] is not None:
            host_name = self.callerArgs['host_name'][0]

        connection = connect_s3(
            key_id, secret_key, self.getSessionKey(), host_name)

        rlist = timed(25, lv1_keys, [],
                      (connection, self.callerArgs['bucket_name'][0]))
        confInfo['S3KeyNamesResult'].append('key_names', rlist)


@log_enter_exit
def main():
    splunk.admin.init(ConfigHandler, splunk.admin.CONTEXT_NONE)


if __name__ == '__main__':
    main()
