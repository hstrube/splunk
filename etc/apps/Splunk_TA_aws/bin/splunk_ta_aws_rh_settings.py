
import splunk_ta_aws_import_lib_declare

import splunk.admin as admin

from splunktalib.rest_manager import multimodel

import splunk_ta_aws_rh_settings_base


class AWSLogsSettings(splunk_ta_aws_rh_settings_base.AWSLogging):
    keyMap = {
        'level': 'log_level'
    }


class InspectorSettings(multimodel.MultiModel):
    endpoint = 'configs/conf-splunk_ta_aws_settings'
    modelMap = {
        'splunk_ta_aws_logs': AWSLogsSettings,
    }


if __name__ == '__main__':
    admin.init(
        multimodel.ResourceHandler(InspectorSettings),
        admin.CONTEXT_APP_AND_USER,
    )
