"""
Rest Handler
"""

import traceback

import splunk_ta_aws_import_lib_declare

from splunktalib.common import log
logger = log.Logs("splunk_ta_aws").get_logger("rest", level="DEBUG")
log_enter_exit = log.log_enter_exit(logger)

import splunk
import splunk.admin

from taaws.proxy_conf import ProxyManager


class ConfigHandler(splunk.admin.MConfigHandler):

    @log_enter_exit
    def setup(self):

        if self.requestedAction == splunk.admin.ACTION_EDIT:
            for arg in ['enable', 'proxy']:
                self.supportedArgs.addOptArg(arg)

    @log_enter_exit
    def handleList(self, confInfo):
        pm = ProxyManager(self.getSessionKey())
        proxy = pm.get_proxy()
        if proxy is None:
            confInfo['proxy'].append('enable', 0)
            confInfo['proxy'].append('proxy', '')
        else:
            confInfo['proxy'].append('enable', 1 if proxy.get_enable() else 0)
            confInfo['proxy'].append('proxy', proxy.get_proxy())

    @log_enter_exit
    def handleEdit(self, confInfo):
        try:
            pm = ProxyManager(self.getSessionKey())
            enable = self.callerArgs.data['enable'][0]
            enable = enable and enable.strip() or False
            proxy = self.callerArgs.data['proxy'][0]
            proxy = proxy and proxy.strip() or ''
            pm.set(proxy, enable)
        except Exception:
            logger.error("Failed to edit proxy, error=%s",
                         traceback.format_exc())


@log_enter_exit
def main():
    splunk.admin.init(ConfigHandler, splunk.admin.CONTEXT_NONE)


if __name__ == '__main__':
    main()
