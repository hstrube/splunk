import splunk_ta_aws_import_lib_declare
import os
import sys
import re


import splunk.admin as admin

from splunktalib.rest_manager import multimodel

import splunk_ta_aws_rh_settings_base


class CloudWatchLogsLogging(splunk_ta_aws_rh_settings_base.AWSLogging):
    keyMap          = {
                      'level': 'log_level'
                      }


class CloudWatchLogsSettings(multimodel.MultiModel):
    endpoint    = "configs/conf-aws_cloudwatch_logs"
    modelMap    = {
                  'logging': CloudWatchLogsLogging,
                  }


if __name__ == "__main__":
    admin.init(multimodel.ResourceHandler(CloudWatchLogsSettings), admin.CONTEXT_APP_AND_USER)
