import time
import traceback

import splunktalib.common.util as scutil
import description_mod.aws_description_consts as adc
import taaws.ta_aws_consts as tac
from splunktalib.common import log

logger = log.Logs().get_logger(adc.description_log)

from base import aws_credentials
aws_credentials.logger = logger


def get_supported_description_apis():
    import description_mod.ec2_description as ade
    import description_mod.elb_description as aed
    import description_mod.vpc_description as avd
    import description_mod.cloudfront_description as acd
    import description_mod.rds_description as ard
    import description_mod.lambda_description as ald

    return {
        "ec2_instances": ade.ec2_instances,
        "ec2_reserved_instances": ade.ec2_reserved_instances,
        "ebs_snapshots": ade.ec2_ebs_snapshots,
        "ec2_volumes": ade.ec2_volumes,
        "ec2_security_groups": ade.ec2_security_groups,
        "ec2_key_pairs": ade.ec2_key_pairs,
        "ec2_images": ade.ec2_images,
        "ec2_addresses": ade.ec2_addresses,
        "elastic_load_balancers": aed.load_balancers,
        "vpcs": avd.vpcs,
        "vpc_subnets": avd.vpc_subnets,
        "vpc_network_acls": avd.vpc_network_acls,
        "cloudfront_distributions": acd.cloudfront_distributions,
        "rds_instances": ard.rds_instances,
        "lambda_functions": ald.lambda_functions,
    }


class DescriptionDataLoader(object):

    def __init__(self, task_config):
        """
        :task_config: dict object
        {
        "interval": 30,
        "api": "ec2_instances" etc,
        "source": xxx,
        "sourcetype": yyy,
        "index": zzz,
        }
        """

        self._task_config = task_config
        self._supported_desc_apis = get_supported_description_apis()
        self._api = self._supported_desc_apis.get(task_config[adc.api], None)
        if self._api is None:
            logger.error("Unsupported service=%s", task_config[adc.api])

    def __call__(self):
        self.index_data()

    def index_data(self):
        logger.info("Start collecting description for service=%s, region=%s",
                    self._task_config[adc.api], self._task_config[tac.region])
        try:
            self._do_index_data()
        except Exception:
            logger.error("Failed to collect description data for %s, error=%s",
                         self._task_config[adc.api], traceback.format_exc())
        logger.info("End of collecting description for service=%s, region=%s",
                    self._task_config[adc.api], self._task_config[tac.region])

    def _do_index_data(self):
        if self._api is None:
            return

        evt_fmt = ("<stream><event>"
                   "<time>{time}</time>"
                   "<source>{source}</source>"
                   "<sourcetype>{sourcetype}</sourcetype>"
                   "<index>{index}</index>"
                   "<data>{data}</data>"
                   "</event></stream>")

        task = self._task_config
        results = self._api(task)

        events = []
        for result in results:
            event = evt_fmt.format(source=task[tac.source],
                                   sourcetype=task[tac.sourcetype],
                                   index=task[tac.index],
                                   data=scutil.escape_cdata(result),
                                   time=time.time())
            events.append(event)
        task["writer"].write_events("".join(events))

    def get_interval(self):
        return self._task_config[tac.interval]

    def stop(self):
        pass

    def get_props(self):
        return self._task_config
