import json
import types
import copy

import taaws.ta_aws_consts as tac
from taaws.ta_aws_common import load_credentials_from_cache


_BUILT_IN_TYPES = (
    types.NoneType,
    types.BooleanType,
    types.IntType,
    types.LongType,
    types.FloatType,
    types.StringType,
    types.UnicodeType,
    types.ListType,
    types.DictType,
)


class _ExtendedEncoder(json.JSONEncoder):
    def default(self, obj):
        if not isinstance(obj, _BUILT_IN_TYPES):
            return str(obj)
        return json.JSONEncoder.default(self, obj)


def serialize(value):
    return json.dumps(value, cls=_ExtendedEncoder)


def pop_description_result(item, keys, init_result,
                           pop_region_name=True, raw_event=False):
    if item is None:
        return {}

    result = dict(init_result)
    if pop_region_name:
        try:
            region_name = item.region.name  # item.region is an object
        except Exception:
            region_name = None
        result[tac.region] = region_name

    for key in keys:
        result[key] = getattr(item, key, None)

    if not raw_event:
        result = json.dumps(result)

    return result


def pop_description_results(items, keys, init_result,
                            pop_region_name=True, raw_event=False):
    results = []
    if not items:
        return results

    for item in items:
        if item is None:
            continue

        result = pop_description_result(
            item, keys, init_result, pop_region_name, raw_event)

        results.append(result)
    return results


def refresh_credentials(func):
    """
    Decorator for refreshing credentials.

    :param func:
    :return:
    """

    def load_credentials(config):
        credentials = load_credentials_from_cache(
            config[tac.server_uri],
            config[tac.session_key],
            config[tac.aws_account],
            config.get(tac.aws_iam_role),
        )
        config[tac.key_id] = credentials.aws_access_key_id
        config[tac.secret_key] = credentials.aws_secret_access_key
        config['aws_session_token'] = credentials.aws_session_token
        config[tac.account_id] = credentials.account_id

    def wrapper(config, *args, **kwargs):
        load_credentials(config)
        return func(config, *args, **kwargs)

    return wrapper


def describe_pagination(func):
    """
    Describe all metadata which based on pagination.

    :return:
    """

    def wrapper(*args, **kwargs):
        pagination = {}
        ret_all = []
        while True:
            kwargs_new = copy.copy(kwargs)
            kwargs_new.update(pagination)
            ret, pagination = func(*args, **kwargs_new)
            ret_all.extend(ret)
            pagination = {key: val for key, val in pagination.iteritems()
                          if val is not None}
            if not pagination:
                break
        return ret_all

    return wrapper
