import boto.ec2.elb

from splunksdc import logging
import traceback
from splunktalib.common import log
import taaws.ta_aws_consts as tac
import taaws.ta_aws_common as tacommon
import description_mod.aws_description_consts as adc
import description_mod.description as desc
from solnlib.utils import retry


logger = log.Logs().get_logger(adc.description_log)


def connect_elb_to_region(config):
    return tacommon.connect_service_to_region(
        boto.ec2.elb.connect_to_region, config)


def _dup_list(origins):
    if origins is None:
        return []

    return [origin for origin in origins]


@retry(retries=5)
def describe_instance_health(elb_connection, elb_name):
    return elb_connection.describe_instance_health(elb_name)


@desc.describe_pagination
@desc.refresh_credentials
def load_balancers(config, **kwargs):
    listener_keys = ["load_balancer_port", "instance_port", "protocol",
                     "instance_protocol", "ssl_certificate_id"]
    health_keys = ["interval", "target", "healthy_threshold", "timeout",
                   "unhealthy_threshold"]
    instance_keys = ["instance_id", "state"]
    backend_keys = ["instance_port", "policies"]

    elb_conn = connect_elb_to_region(config)
    all_elbs = elb_conn.get_all_load_balancers(**kwargs)
    pagination = {'marker': all_elbs.next_marker}

    results = []
    for elb in all_elbs:
        listeners = desc.pop_description_results(
            elb.listeners, listener_keys, {},
            pop_region_name=False, raw_event=True)

        health_check = desc.pop_description_result(
            elb.health_check, health_keys, {},
            pop_region_name=False, raw_event=True)

        availability_zones = _dup_list(elb.availability_zones)
        subnets = _dup_list(elb.subnets)
        security_groups = _dup_list(elb.security_groups)

        source_security_group = desc.pop_description_result(
            elb.source_security_group, ["name"], {},
            pop_region_name=False, raw_event=True)

        backends = desc.pop_description_results(
            elb.backends, backend_keys, {},
            pop_region_name=False, raw_event=True)

        try:
            instances = describe_instance_health(elb_conn, elb.name)
        except Exception:
            logger.error(
                'Ignore ELB due to exception',
                ELB=elb.name,
                error=traceback.format_exc(),
            )
            continue
        instances = desc.pop_description_results(
            instances, instance_keys, {},
            pop_region_name=False, raw_event=True)

        result = {
            tac.account_id: config[tac.account_id],
            "availability_zones": availability_zones,
            "backends": backends,
            "created_time": getattr(elb, "created_time", None),
            "dns_name": getattr(elb, "dns_name", None),
            "name": getattr(elb, "name", None),
            "health_check": health_check,
            "instances": instances,
            "listeners": listeners,
            "source_security_group": source_security_group,
            "subnets": subnets,
            "security_groups": security_groups,
            "vpc_id": getattr(elb, "vpc_id", None),
            tac.region: elb_conn.region.name,
        }
        results.append(desc.serialize(result))
    return results, pagination
