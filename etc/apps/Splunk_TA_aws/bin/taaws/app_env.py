import boto
import boto.iam
import boto.utils
import threading
import re
import taaws.ta_aws_consts as tac

from splunktalib.common import log
logger = log.Logs("splunk_ta_aws").get_logger("app_env")


class AppEnv(object):
    _env = None
    _rest_endpoint = None
    _session_key = None
    _app_name = None
    _proxy = None
    _lock = threading.Lock()

    @classmethod
    def setup(cls, rest_endpoint, session_key):
        app_name = tac.splunk_ta_aws
        proxy = _load_proxy(session_key)
        cls._rest_endpoint = rest_endpoint
        cls._session_key = session_key
        cls._app_name = app_name
        cls._proxy = proxy

    @classmethod
    def get(cls):
        if cls._env is None:
            with cls._lock:
                if cls._env is None:
                    access_keys = _load_access_keys(
                        cls._rest_endpoint,
                        cls._session_key,
                        cls._app_name,
                        cls._proxy
                    )
                    cls._env = cls(access_keys)
        assert cls._env is not None
        return cls._env

    def __init__(self, access_keys):
        self._access_keys = access_keys

    def get_account_id(self, **kwargs):
        key_id = kwargs.pop('key_id')
        if key_id is None:
            for key in self._access_keys:
                if key.iam:
                    return key.account_id
        else:
            for key in self._access_keys:
                if key.key_id == key_id:
                    return key.account_id
        return tac.DEFAULT_ACCOUNT_ID


def _load_proxy(session_key):
    from taaws.proxy_conf import get_proxy_info
    return get_proxy_info(session_key)


def _load_access_keys(rest_endpoint, session_key, app_name, proxy):
    from taaws.aws_accesskeys import AwsAccessKeyManager
    from splunktalib.conf_manager.conf_manager import ConfManager

    key_manager = AwsAccessKeyManager(app_name, '-', session_key)
    conf_manager = ConfManager(rest_endpoint, session_key, 'nobody', app_name)

    access_keys = [item for item in key_manager.all_accesskeys() if item.name != '_aws_proxy']

    # load category and account ID
    for key in access_keys:
        if key.iam:
            continue
        logger.info("get account id for %s", key.name)
        ext = conf_manager.get_stanza("aws_account_ext", key.name)
        key.category = int(ext['category'])
        key.account_id = _get_account_id(
            key.key_id,
            key.secret_key,
            key.category,
            proxy
        )
    return access_keys


def _create_conn_of_category(key_id, secret_key, category, proxy):
    region = None
    params = {
        'aws_access_key_id': key_id,
        'aws_secret_access_key': secret_key,
        'proxy': proxy.get(tac.proxy_hostname),
        'proxy_port': proxy.get(tac.proxy_port),
        'proxy_user': proxy.get(tac.proxy_username),
        'proxy_pass': proxy.get(tac.proxy_password),
        'is_secure': True
    }
    if category == tac.RegionCategory.COMMERCIAL:
        return boto.connect_iam(**params)
    elif category == tac.RegionCategory.USGOV:
        region = 'us-gov-west-1'
    elif category == tac.RegionCategory.CHINA:
        region = 'cn-north-1'
    if region is None:
        raise Exception('invalid region category')

    return boto.iam.connect_to_region(region, **params)


def _get_account_id(key_id, secret_key, category, proxy):
    
    if category not in tac.RegionCategory.VALID:
        return tac.DEFAULT_ACCOUNT_ID

    conn = _create_conn_of_category(key_id, secret_key, category, proxy)
    try:
        user = conn.get_user()
    except Exception as e:
        m = re.search("(?<=:)\d{12}(?=:)", e.message)
        if m is not None:
            return m.group(0)
        else:
            return tac.DEFAULT_ACCOUNT_ID

    return user.arn.split(":")[4]
