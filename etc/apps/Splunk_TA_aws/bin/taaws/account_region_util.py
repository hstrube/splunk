import boto
import boto.ec2

from splunktalib.common import log
logger = log.Logs("splunk_ta_aws").get_logger("region", level="DEBUG")

from proxy_conf import ProxyManager


def validate_region_access(session_key, region_name, aws_access_key_id, aws_secret_access_key):
    try:
        pm = ProxyManager(session_key)
        proxy = pm.get_proxy()
        if proxy is None or not proxy.get_enable():
            conn = boto.ec2.connect_to_region(region_name,
                                              is_secure=True,
                                              validate_certs=True,
                                              aws_access_key_id=aws_access_key_id,
                                              aws_secret_access_key=aws_secret_access_key)
            return conn.get_all_regions()
        else:
            proxy_info = pm.get_proxy_info()
            proxy_host = proxy_info['host']
            proxy_port = proxy_info['port']
            proxy_username = proxy_info['user']
            proxy_password = proxy_info['pass']
            logger.debug("use proxy")
            if proxy_host is None:
                logger.error("Proxy host must be set!")
                return None
            conn = boto.ec2.connect_to_region(region_name,
                                              is_secure=True,
                                              validate_certs=True,
                                              aws_access_key_id=aws_access_key_id,
                                              aws_secret_access_key=aws_secret_access_key,
                                              proxy=proxy_host,
                                              proxy_port=proxy_port,
                                              proxy_user=proxy_username,
                                              proxy_pass=proxy_password)
            return conn.get_all_regions()
    except boto.exception.BotoServerError as e:
        logger.exception("validate_region_access BotoServerError, region_name={}".format(region_name))
        return []
    except Exception as e:
        logger.exception("validate_region_access Exception, region_name={}".format(region_name))
        raise e


def get_commercial_regions(session_key, aws_access_key_id, aws_secret_access_key):
    try:
        pm = ProxyManager(session_key)
        proxy = pm.get_proxy()
        if proxy is None or not proxy.get_enable():
            conn = boto.ec2.connection.EC2Connection(is_secure=True,
                                                     validate_certs=True,
                                                     aws_access_key_id=aws_access_key_id,
                                                     aws_secret_access_key=aws_secret_access_key)
            return conn.get_all_regions()
        else:
            proxy_info = pm.get_proxy_info()
            proxy_host = proxy_info['host']
            proxy_port = proxy_info['port']
            proxy_username = proxy_info['user']
            proxy_password = proxy_info['pass']
            if proxy_host is None:
                logger.error("Proxy host must be set!")
                return None
            logger.debug("use proxy")
            conn = boto.ec2.connection.EC2Connection(is_secure=True,
                                                     validate_certs=True,
                                                     aws_access_key_id=aws_access_key_id,
                                                     aws_secret_access_key=aws_secret_access_key,
                                                     proxy=proxy_host,
                                                     proxy_port=proxy_port,
                                                     proxy_user=proxy_username,
                                                     proxy_pass=proxy_password)
            return conn.get_all_regions()
    except boto.exception.BotoServerError as e:
        logger.exception("get_commercial_regions BotoServerError")
        return []
    except Exception as e:
        logger.exception("get_commercial_regions Exception")
        raise e
