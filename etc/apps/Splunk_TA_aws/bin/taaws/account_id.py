import re
import boto
import logging
import traceback

DEFAULT_ID = '000000000000'


def get_account_id(key_id, secret_key, proxy=None, is_secure=True):
    try:
        if not proxy:
            conn = boto.connect_iam(
                aws_access_key_id=key_id,
                aws_secret_access_key=secret_key,
                is_secure=is_secure)
        else:
            proxy_host = proxy['host']
            proxy_port = proxy['port']
            proxy_username = proxy['user']
            proxy_password = proxy['pass']

            if proxy_host is None:
                return (
                    'Proxy host is not set and can not connect to aws IAM.',
                    logging.ERROR, DEFAULT_ID)
            conn = boto.connect_iam(
                aws_access_key_id=key_id,
                aws_secret_access_key=secret_key,
                proxy=proxy_host,
                proxy_port=proxy_port,
                proxy_user=proxy_username,
                proxy_pass=proxy_password,
                is_secure=is_secure)
    except Exception as e:
        return (
            'Failed to connect to aws IAM. Traceback: %s' % traceback.format_exc(),
            logging.ERROR, DEFAULT_ID)

    try:
        user = conn.get_user()
    except Exception as e0:
        try:
            m = re.search('(?<=\:)\d{12}(?=\:)', e0.message)
            return ("not authorized to perform: iam:GetUser, "
                    "try to get account ID from error message.",
                    logging.WARNING, m.group(0))
        except Exception as e1:
            return (
                "can't perform iam:GetUser and can't get account ID "
                "from error message. use 000000000000. "
                "Traceback: %s" % traceback.format_exc(),
                logging.ERROR, DEFAULT_ID)
    else:
        try:
            return (
                'Getting account ID from user arn splitted by ":".',
                logging.INFO,
                user.arn.split(':')[4])
        except Exception as e:
            return ("can perform iam:GetUser but can't get account ID. "
                    "use 000000000000. Traceback:%s" % traceback.format_exc(),
                    logging.ERROR, DEFAULT_ID)