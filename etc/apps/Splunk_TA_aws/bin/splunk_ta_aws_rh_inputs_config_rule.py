
import splunk_ta_aws_import_lib_declare

import splunk.admin as admin

from splunktalib.rest_manager import base,normaliser


class ConfigRule(base.BaseModel):
    """ AWS Config Rule Inputs Model. """

    endpoint = "configs/conf-aws_config_rule_tasks"

    requiredArgs = {'aws_account', 'aws_region', 'interval'}

    optionalArgs = {'sourcetype', 'host', 'index', 'rule_names'}

    defaultVals = {
        'sourcetype': 'aws:config:rule',
        'index': 'default',
    }

    normalisers = {
        'disabled': normaliser.Boolean(),
    }

    keyMap = {
        'aws_account': 'account',
        'aws_region': 'region',
        'rule_names': 'rule_names',
        'interval': 'polling_interval'
    }

    cap4disable = 'ta_aws_custom_action_disable'
    cap4enable = 'ta_aws_custom_action_enable'


if __name__ == "__main__":
    admin.init(base.ResourceHandler(ConfigRule), admin.CONTEXT_APP_AND_USER)
