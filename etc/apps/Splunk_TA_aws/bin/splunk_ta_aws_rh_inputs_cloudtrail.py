
import splunk_ta_aws_import_lib_declare

import splunk.admin as admin

from splunktalib.rest_manager.error_ctl import RestHandlerError as RH_Err
from splunktalib.rest_manager import base, datainput, normaliser
from aws_cloudtrail import delete_ckpt

 
class CloudTrail(datainput.DataInputModel):
    """
    AWS CloudTrail Inputs Model.
    """
    dataInputName = "aws_cloudtrail"
    requiredArgs = ['aws_account', 'aws_region', 'sqs_queue']
    optionalArgs = [
        'exclude_describe_events',
        'remove_files_when_done',
        'blacklist',
        'excluded_events_index',
        'host_name',
        'monthly_report_type',
        'detail_report_type',
        'report_file_match_reg',
        'interval',
        'sourcetype',
        'host',
        'index',
    ]
    defaultVals = {
        'exclude_describe_events': 'True',
        'remove_files_when_done': 'False',
        'interval': '30',
        'sourcetype': 'aws:cloudtrail',
    }
    normalisers = {
        'disabled': normaliser.Boolean(),
    }
    cap4disable = 'ta_aws_custom_action_disable'
    cap4enable = 'ta_aws_custom_action_enable'


class CloudTrailHandler(datainput.DataInputHandler):

    def _delete_ckpt(self):
        try:
            delete_ckpt(self.callerArgs.id)
        except Exception as exc:
            if (isinstance(exc, IOError) and
                    'No such file or directory' in str(exc)):
                return
            RH_Err.ctl(500, 'Failed to delete checkpoint')

    def handleRemove(self, confInfo):
        datainput.DataInputHandler.handleRemove(self, confInfo)
        self._delete_ckpt()

    def handleCreate(self, confInfo):
        self._delete_ckpt()
        datainput.DataInputHandler.handleCreate(self, confInfo)


if __name__ == '__main__':
    admin.init(
        base.ResourceHandler(CloudTrail, handler=CloudTrailHandler),
        admin.CONTEXT_APP_AND_USER,
    )
