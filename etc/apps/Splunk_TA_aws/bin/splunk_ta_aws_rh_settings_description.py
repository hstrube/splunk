import splunk_ta_aws_import_lib_declare

import splunk.admin as admin

from splunktalib.rest_manager import multimodel

import splunk_ta_aws_rh_settings_base


class DescriptionLogging(splunk_ta_aws_rh_settings_base.AWSLogging):
    keyMap = {
        "level": "log_level"
    }


class DescriptionSettings(multimodel.MultiModel):
    endpoint = "configs/conf-aws_description"
    modelMap = {
        "logging": DescriptionLogging,
    }


if __name__ == "__main__":
    admin.init(multimodel.ResourceHandler(DescriptionSettings),
               admin.CONTEXT_APP_AND_USER)
