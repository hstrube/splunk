"""
Custom REST Endpoint for enumerating AWS regions.
"""

import splunk_ta_aws_import_lib_declare

from splunktalib.common import log

from splunktalib.rest_manager.error_ctl import RestHandlerError as RH_Err

logger = log.Logs("splunk_ta_aws").get_logger("rest", level="DEBUG")
log_enter_exit = log.log_enter_exit(logger)


import splunk
import splunk.admin


class DummyRegion(object):
    def __init__(self, name):
        self.name = name

    @staticmethod
    def from_names(names):
        return [DummyRegion(name) for name in names]


class ConfigHandler(splunk.admin.MConfigHandler):

    @log_enter_exit
    def setup(self):
        self.supportedArgs.addReqArg('aws_service')

    @log_enter_exit
    def handleList(self, confInfo):
        service = self.callerArgs.data['aws_service'][0]
        if service == 'cloudwatch':
            import boto.ec2.cloudwatch
            regions = boto.ec2.cloudwatch.regions()
        elif service == 'cloudtrail':
            import boto.cloudtrail
            regions = boto.cloudtrail.regions()
        elif service in ('awsconfig', "config"):
            import boto.sqs
            regions = boto.sqs.regions()
        elif service in ("configrule", ):
            import boto.configservice
            # FIXME, hard code for now
            regions = DummyRegion.from_names([
                'us-east-1',
                'us-east-2',
                'us-west-2',
                'ap-southeast-1',
                'ap-southeast-2',
                'ap-northeast-1',
                'eu-central-1',
                'eu-west-1',
            ])
        elif service in ('cloudwatch-logs', 'cloudwatchlog', 'cloudwatchlogs'):
            import boto.logs
            regions = boto.logs.regions()
        elif service == "description":
            import boto.ec2
            regions = boto.ec2.regions()
        elif service == "inspector":
            regions = DummyRegion.from_names([
                'us-east-1',
                'us-west-2',
                'ap-northeast-2',
                'ap-south-1',
                'ap-southeast-2',
                'ap-northeast-1',
                'eu-west-1',
            ])
        elif service == "kinesis":
            import boto.kinesis
            regions = boto.kinesis.regions()
        elif service == 'sqs':
            import boto.sqs
            regions = boto.sqs.regions()
        elif service == "s3":
            import boto.s3
            regions = boto.s3.regions()
        else:
            msg = "Unsupported aws_service={} specified.".format(service)
            RH_Err.ctl(400, msgx=msg)

        rlist = [r.name for r in regions]
        confInfo['RegionsResult'].append('regions', rlist)


@log_enter_exit
def main():
    splunk.admin.init(ConfigHandler, splunk.admin.CONTEXT_NONE)


if __name__ == '__main__':
    main()
