
import splunk_ta_aws_import_lib_declare

import splunk.admin as admin

from splunktalib.common import log
import taaws.ta_aws_consts as tac

logger = log.Logs(tac.splunk_ta_aws.lower()).get_logger("custom_rest")


import splunktalib.common.pattern as scp
import taaws.ta_aws_common as tacommon
import taaws.proxy_conf as pc
import boto3


class ConfigRules(admin.MConfigHandler):
    valid_params = [tac.aws_region, tac.aws_account]

    def setup(self):
        for param in self.valid_params:
            self.supportedArgs.addOptArg(param)

    @scp.catch_all(logger)
    def handleList(self, conf_info):
        logger.info("start listing config rules")
        for required in self.valid_params:
            if not self.callerArgs or not self.callerArgs.get(required):
                logger.error('Missing "%s"', required)
                raise Exception('Missing "{}"'.format(required))

        key_id, secret_key = tacommon.assert_creds(
            self.callerArgs[tac.aws_account][0], self.getSessionKey(), logger)

        proxy = pc.get_proxy_info(self.getSessionKey())
        proxy[tac.region] = self.callerArgs[tac.aws_region][0]
        proxy[tac.key_id] = key_id
        proxy[tac.secret_key] = secret_key
        tacommon.set_proxy_env(proxy)

        client = boto3.client(
            "config",
            region_name=proxy[tac.region],
            aws_access_key_id=proxy[tac.key_id],
            aws_secret_access_key=proxy[tac.secret_key])

        all_rules = []
        next_token = ""
        while 1:
            response = client.describe_config_rules(NextToken=next_token)
            if not tacommon.is_http_ok(response):
                logger.error("Failed to describe config rules, errorcode=%s",
                             tacommon.http_code(response))
                return

            rules = response.get("ConfigRules")
            if not rules:
                break

            all_rules.extend(rule["ConfigRuleName"] for rule in rules)

            next_token = response.get("NextToken")
            if not next_token:
                break

        conf_info["config_rules"].append("config_rules", all_rules)
        logger.info("end of listing config rules")


def main():
    admin.init(ConfigRules, admin.CONTEXT_NONE)


if __name__ == "__main__":
    main()
