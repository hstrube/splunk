import splunk_ta_aws_import_lib_declare
import os
import sys
import re

import logging

import splunk.admin as admin

from splunktalib.rest_manager import util,error_ctl

from taaws.proxy_conf import ProxyManager

KEY_NAMESPACE = util.getBaseAppName()
KEY_OWNER = '-'

AWS_PROXY = 'aws_proxy'

POSSIBLE_KEYS=('host', 'port', 'username', 'password')

CRED_KEYS = ('password', )

class ProxyRestHandler(admin.MConfigHandler):
    """Manage AWS Accounts in Splunk_TA_aws add-on.
    """
   
    def __init__(self, scriptMode, ctxInfo):
        admin.MConfigHandler.__init__(self, scriptMode, ctxInfo)
        
        if self.callerArgs.id and self.callerArgs.id!='aws_proxy':
            error_ctl.RestHandlerError.ctl(404, msgx='name={}'.format(self.callerArgs.id), logLevel=logging.INFO)

    def setup(self):
        if self.requestedAction in (admin.ACTION_CREATE, admin.ACTION_EDIT):
            for arg in ('disabled', 'host', 'port', 'username', 'password'):
                self.supportedArgs.addOptArg(arg)
        return
    
    def handleRemove(self, confInfo):
        try:
            self.update('', False)
        except Exception as exc:
            error_ctl.RestHandlerError.ctl(400, msgx=exc, logLevel=logging.INFO)

    def handleEdit(self, confInfo):
        try:
            proxy = self.validate(self.callerArgs.data)
            proxyStr = '%s:%s@%s:%s' % (proxy['username'], proxy['password'], proxy['host'], proxy['port'])
            enable = True if proxy['disabled']=='0' else False
            self.update(proxyStr, enable)
        except Exception as exc:
            error_ctl.RestHandlerError.ctl(400, msgx=exc, logLevel=logging.INFO)
    
    def handleList(self, confInfo):
        try:
            user, app = self.user_app()
            proxy = self.get()
            del proxy['password']
            util.makeConfItem('aws_proxy', proxy, confInfo, user=user, app=app)
        except Exception as exc:
            error_ctl.RestHandlerError.ctl(400, msgx=exc, logLevel=logging.INFO)
        return
    
    def handleCustom(self, confInfo, **params):
        if self.customAction in ['acl']:
            return self.handleACL(confInfo)

        if self.customAction == 'disable':
            self.handleDisable(confInfo)
        elif self.customAction == 'enable':
            self.handleEnable(confInfo)
        else:
            error_ctl.RestHandlerError.ctl(1101, msgx='action=%s' % self.customAction, logLevel=logging.INFO)
    
    def handleDisable(self, confInfo):
        try:
            proxy  = ProxyManager(self.getSessionKey()).get_proxy()
            return None if proxy is None else self.update(proxy.get_proxy(), False)
        except Exception as exc:
            error_ctl.RestHandlerError.ctl(400, msgx=exc, logLevel=logging.INFO)
        return
    
    def handleEnable(self, confInfo):
        try:
            proxy = self.get()
            if not proxy.get('host'):
                return
            proxy  = ProxyManager(self.getSessionKey()).get_proxy()
            return None if proxy is None else self.update(proxy.get_proxy(), True)
        except Exception as exc:
            error_ctl.RestHandlerError.ctl(400, msgx=exc, logLevel=logging.INFO)
        return
    
    def validate(self, args):
        host = args['host'][0] if args.get('host') and args['host'][0] else ''
        port = args['port'][0] if args.get('port') and args['port'][0] else ''
        username = args['username'][0] if args.get('username') and args['username'][0] else ''
        password = args['password'][0] if args.get('password') and args['password'][0] else ''
        if args.get('disabled') and args['disabled'][0] not in ('0', '1'):
            error_ctl.RestHandlerError.ctl(1100, msgx='disabled="{}"'.format(args['disabled'][0]), logLevel=logging.INFO)
        try:
            if port:
                assert 0<int(args['port'][0])<=65535
        except:
            error_ctl.RestHandlerError.ctl(1100, msgx='port={}'.format(port), logLevel=logging.INFO)
        
        
        if host and not port:
            error_ctl.RestHandlerError.ctl(1002, msgx='Port is required for non-empty host.', logLevel=logging.INFO)
        if port and not host:
            error_ctl.RestHandlerError.ctl(1002, msgx='Host is required for non-empty port.', logLevel=logging.INFO)
        if username and not host:
            error_ctl.RestHandlerError.ctl(1002, msgx='Host is required for non-empty username.', logLevel=logging.INFO)
        if password and not username:
            error_ctl.RestHandlerError.ctl(1002, msgx='Username is required for non-empty password.', logLevel=logging.INFO)
        
        proxy = self.get()
        disabled = ('1' if args['disabled'][0].lower() in ('true', 't', 'yes', 'y', '1') else '0') if args.get('disabled') else proxy['disabled']
        
        if disabled=='0' and not host:
            error_ctl.RestHandlerError.ctl(1002, msgx='Host is required for a enabled proxy.'.format(args['username'][0]), logLevel=logging.INFO)
        if disabled=='0' and not port:
            error_ctl.RestHandlerError.ctl(1002, msgx='Port is required for a enabled proxy.'.format(args['username'][0]), logLevel=logging.INFO)
        
        if username and username!=proxy['username'] and not password:
            error_ctl.RestHandlerError.ctl(1002, msgx='Password is required for a non-empty modified/new username "{}".'.format(args['username'][0]), logLevel=logging.INFO)
        
        password = password if password else (proxy['password'] if username else '')
        return {'host':host, 'port':port, 'username':username, 'password':password, 'disabled':disabled}
    
    def get(self):
        pm  = ProxyManager(self.getSessionKey())
        proxy = pm.get_proxy()
        if proxy is None:
            return {'host':'', 'port':'', 'username':'', 'password':'', 'disabled':'1'}
        
        m = re.match('^(?P<username>\S{,100}):(?P<password>\S{,100})@(?P<host>\S{1,100}):(?P<port>\d+$)',proxy.get_proxy())
        if not m:
            error_ctl.RestHandlerError.ctl(1000, msgx='Fail to load AWS Proxy Information', logLevel=logging.ERROR, shouldPrint=False, shouldRaise=False)
        groupDict = m.groupdict() if m else {}
        
        host = groupDict['host'] if groupDict.get('host')  else ''
        port = groupDict['port'] if groupDict.get('port')  else ''
        username = groupDict['username'] if groupDict.get('username')  else ''
        password = groupDict['password'] if groupDict.get('password')  else ''
        disabled = '0' if proxy.get_enable() else '1'
        return {'host':host, 'port':port, 'username':username, 'password':password, 'disabled':disabled}
    
    def update(self, proxy, enable):
        try:
            pm = ProxyManager(self.getSessionKey())
            pm.set(proxy,str(enable).lower())
        except Exception as exc:
            error_ctl.RestHandlerError.ctl(400, msgx=exc, logLevel=logging.INFO)
            
    def user_app(self):
        app  = self.context != admin.CONTEXT_NONE           and self.appName or "-"
        user = self.context == admin.CONTEXT_APP_AND_USER   and self.userName or "nobody"
        return user, app
    
if __name__ == "__main__":
    admin.init(ProxyRestHandler, admin.CONTEXT_APP_AND_USER)
