
import splunk_ta_aws_import_lib_declare

import splunk.admin as admin
from splunktalib.rest_manager import base, validator


class IAMRole(base.BaseModel):
    endpoint = "configs/conf-splunk_ta_aws_iam_roles"
    requiredArgs = {'arn'}
    encryptedArgs = {'arn'}
    validators = {
        'arn': validator.Pattern(
            '^arn:[^\s:]+:iam::\d+:role(:|/)[^/:\s]+$')
    }


if __name__ == "__main__":
    admin.init(
        base.ResourceHandler(IAMRole),
        admin.CONTEXT_APP_AND_USER,
    )
