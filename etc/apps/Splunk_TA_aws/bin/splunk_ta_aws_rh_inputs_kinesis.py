import splunk_ta_aws_import_lib_declare
import splunk.admin as admin

from splunktalib.rest_manager import base, normaliser


class Kinesis(base.BaseModel):

    """ AWS Inspector Inputs Model. """

    endpoint = 'configs/conf-aws_kinesis_tasks'

    requiredArgs = ['aws_account', 'aws_region']

    optionalArgs = [
        'aws_iam_role',
        'stream_names',
        'encoding',
        'format',
        'init_stream_position',
        'sourcetype',
        'index'
    ]

    defaultVals = {
        'stream_names': '',
        'encoding': '',
        'format': '',
        'init_stream_position': 'LATEST',
        'sourcetype': 'aws:kinesis',
        'index': 'default',
    }

    normalisers = {
        'disabled': normaliser.Boolean(),
    }

    keyMap = {
        'aws_account': 'account',
        'aws_region': 'region',
    }

    cap4disable = 'ta_aws_custom_action_disable'
    cap4enable = 'ta_aws_custom_action_enable'


if __name__ == '__main__':
    admin.init(base.ResourceHandler(Kinesis), admin.CONTEXT_APP_AND_USER)
