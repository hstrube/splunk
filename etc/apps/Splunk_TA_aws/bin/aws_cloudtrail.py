"""
Modular Input for AWS CloudTrail
"""

import splunk_ta_aws_import_lib_declare

import sys
import os
import os.path as op

from splunklib import modularinput as smi

import taaws.ta_aws_consts as taconsts

from cloudtrail_mod import modular_args
from cloudtrail_mod.aws_cloudtrail_data_loader import Input
import solnlib.modular_input.event_writer as cew
from cloudtrail_mod import logger


class CloudTrailInput(smi.Script):

    def __init__(self):
        super(CloudTrailInput, self).__init__()
        self._canceled = False
        self._ew = None

    def get_scheme(self):
        """overloaded splunklib modularinput method"""

        scheme = smi.Scheme('AWS CloudTrail')
        scheme.description = (
            'Collect and index log files produced by AWS CloudTrail. '
            'CloudTrail logging must be enabled and published to SNS topics '
            'and an SQS queue.'
        )
        scheme.use_external_validation = True
        scheme.streaming_mode_xml = True
        scheme.use_single_instance = False

        # Add arguments
        for name, arg in modular_args.iteritems():
            scheme.add_argument(
                smi.Argument(
                    name,
                    title=arg.get('title'),
                    validation=arg.get('validation'),
                    data_type=arg.get(
                        'data_type',
                        smi.Argument.data_type_string,
                    ),
                    description=arg.get('description'),
                    required_on_create=arg.get('required_on_create', False),
                    required_on_edit=arg.get('required_on_edit', False),
                )
            )
        return scheme

    def validate_input(self, definition):
        """overloaded splunklib modularinput method"""
        pass

    def stream_events(self, inputs, ew):
        ew = cew.ClassicEventWriter()
        """overloaded splunklib modularinput method"""
        input_name, input_items = inputs.inputs.popitem()
        Input(
            self._input_definition.metadata[taconsts.server_uri],
            self._input_definition.metadata[taconsts.session_key],
            self._input_definition.metadata[taconsts.checkpoint_dir],
            input_name, input_items, ew
        ).run()


def delete_ckpt(input_name):
    from splunksdc.utils import get_checkpoint_folder
    ckpt_dir = get_checkpoint_folder('aws_cloudtrail')
    ckpt_file = op.join(ckpt_dir, input_name + '.v3.ckpt')
    if op.isfile(ckpt_file):
        os.remove(ckpt_file)
        logger.info('Checkpoint file is deleted: %s' % ckpt_file)


if __name__ == '__main__':
    # Force Enable SIGV4 for S3
    if "S3_USE_SIGV4" not in os.environ:
        os.environ["S3_USE_SIGV4"] = "True"

    exitcode = CloudTrailInput().run(sys.argv)
    sys.exit(exitcode)
