"""
Custom REST Endpoint for enumerating AWS S3 Bucket.
"""

import splunk_ta_aws_import_lib_declare

from splunktalib.common import log
logger = log.Logs("splunk_ta_aws").get_logger("rest", level="DEBUG")
log_enter_exit = log.log_enter_exit(logger)


import splunk
import splunk.admin

from taaws.s3util import connect_s3

import threading

from splunktalib.rest_manager.error_ctl import RestHandlerError as RH_Err
from solnlib.splunkenv import get_splunkd_uri
import taaws.ta_aws_common as tacommon
import taaws.proxy_conf as pc


def timed(timeout, func, default=None, args=(), kwargs={}):
    """
    Run func with the given timeout. If func didn't finish running
    within the timeout, raise TimeLimitExpired
    """

    class FuncThread(threading.Thread):
        def __init__(self):
            threading.Thread.__init__(self)
            self.result = default
            self.error = None

        def run(self):
            try:
                self.result = func(*args, **kwargs)
            except Exception, exc:
                self.error = exc

    it = FuncThread()
    it.start()
    it.join(timeout)
    if it.error:
        RH_Err.ctl(400, msgx=it.error)
    return it.result


def all_buckets(s3_conn):
    return s3_conn.get_all_buckets()


class ConfigHandler(splunk.admin.MConfigHandler):

    @log_enter_exit
    def setup(self):
        self.supportedArgs.addReqArg('host_name')
        self.supportedArgs.addReqArg('aws_account')
        self.supportedArgs.addOptArg('aws_iam_role')

    @log_enter_exit
    def handleList(self, confInfo):
        aws_account = ""
        if self.callerArgs['aws_account'] is not None:
            aws_account = self.callerArgs['aws_account'][0]

        aws_iam_role = None
        if self.callerArgs.get('aws_iam_role') is not None:
            aws_iam_role = self.callerArgs['aws_iam_role'][0]

        if not aws_account:
            confInfo['S3BucketsResult'].append('buckets', [])
            return

        host_name = ""
        if self.callerArgs['host_name'] is not None:
            host_name = self.callerArgs['host_name'][0]

        # Set proxy for boto3
        proxy = pc.get_proxy_info(self.getSessionKey())
        tacommon.set_proxy_env(proxy)

        cred_service = tacommon.create_credentials_service(
            get_splunkd_uri(), self.getSessionKey())
        cred = cred_service.load(aws_account, aws_iam_role)

        connection = connect_s3(
            cred.aws_access_key_id, cred.aws_secret_access_key,
            self.getSessionKey(), host_name,
            security_token=cred.aws_session_token,
        )

        rs = timed(25, all_buckets, [], (connection,))
        rlist = []
        for r in rs:
            rlist.append(r.name)

        confInfo['S3BucketsResult'].append('buckets', rlist)


@log_enter_exit
def main():
    splunk.admin.init(ConfigHandler, splunk.admin.CONTEXT_NONE)


if __name__ == '__main__':
    main()
