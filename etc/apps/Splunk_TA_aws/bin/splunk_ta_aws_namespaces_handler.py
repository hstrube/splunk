"""
Custom REST Endpoint for enumerating AWS cloudwatch namepaces.
"""

import splunk_ta_aws_import_lib_declare


from splunktalib.common import log
logger = log.Logs("splunk_ta_aws").get_logger("rest", level="DEBUG")
log_enter_exit = log.log_enter_exit(logger)


import splunk
import splunk.admin

import taaws.s3util
import taaws.ta_aws_common as tacommon


class ConfigHandler(splunk.admin.MConfigHandler):

    @log_enter_exit
    def setup(self):
        self.supportedArgs.addReqArg('aws_region')
        self.supportedArgs.addReqArg('aws_account')

    @log_enter_exit
    def handleList(self, confInfo):
        try:
            key_id, secret_key = tacommon.assert_creds(
                self.callerArgs["aws_account"][0], self.getSessionKey(), logger)
            namespaces = taaws.s3util.list_cloudwatch_namespaces(
                self.callerArgs['aws_region'][0], key_id, secret_key,
                self.getSessionKey())
            confInfo['NameSpacesResult'].append('metric_namespace', namespaces)
        except Exception, exc:
            err = "Error while loading Metric Namespace: type=%s, content=%s" \
                  "" % (type(exc), exc)
            print err
            raise BaseException()


@log_enter_exit
def main():
    splunk.admin.init(ConfigHandler, splunk.admin.CONTEXT_NONE)


if __name__ == '__main__':
    main()
