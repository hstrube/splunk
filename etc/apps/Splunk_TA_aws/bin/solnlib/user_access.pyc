ó
o¨>Yc           @   s*  d  Z  d d l Z d d l Z d d l m Z d d l m Z d d l j Z	 d d d d d	 d
 d d d d d d d d g Z
 d e f d     YZ d e f d     YZ e d e j g  d    Z d e f d     YZ d e f d     YZ d e f d     YZ d	 e f d     YZ d e f d     YZ d
 e f d     YZ d e f d     YZ e e e d   Z d e f d!     YZ e d e j g  e e e d"   Z d e f d#     YZ e d e j g  e e e d$   Z e e e d%  Z e d e j g  e e e d&   Z d S('   s/   
Splunk user access control related utilities.
iÿÿÿÿN(   t   binding(   t   retryt   ObjectACLExceptiont	   ObjectACLt   ObjectACLManagerExceptiont   ObjectACLManagert   AppCapabilityManagerExceptiont   AppCapabilityManagert   UserAccessExceptiont   check_user_accesst   InvalidSessionKeyExceptiont   get_current_usernamet   UserNotExistExceptiont   get_user_capabilitiest   user_is_capablet   get_user_rolesc           B   s   e  Z RS(    (   t   __name__t
   __module__(    (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyR   +   s   c           B   sÈ   e  Z d  Z d Z d Z d Z d Z d Z d Z d Z	 d Z
 d	 Z d
 Z d Z d   Z e d    Z e d    Z e j d    Z e d    Z e d    Z e d    Z d   Z d   Z RS(   sÕ  Object ACL record.

    :param obj_collection: Collection where object currently stored.
    :type obj_collection: ``string``
    :param obj_id: ID of this object.
    :type obj_id: ``string``
    :param obj_app: App of this object.
    :param obj_type: ``string``
    :param obj_owner: Owner of this object.
    :param obj_owner: ``string``
    :param obj_perms: Object perms, like: {
        'read': ['*'],
        'write': ['admin'],
        'delete': ['admin']}.
    :type obj_perms: ``dict``
    :param obj_shared_by_inclusion: Flag of object is shared by inclusion.
    :type obj_shared_by_inclusion: ``bool``

    Usage::

       >>> from solnlib import user_access
       >>> obj_acl = user_access.ObjectACL(
       >>>    'test_collection',
       >>>    '9defa6f510d711e6be16a45e60e34295',
       >>>    'test_object',
       >>>    'Splunk_TA_test',
       >>>    'admin',
       >>>    {'read': ['*'], 'write': ['admin'], 'delete': ['admin']},
       >>>    False)
    t   obj_collectiont   obj_idt   obj_typet   obj_appt	   obj_ownert	   obj_permst   readt   writet   deletet   *t   obj_shared_by_inclusionc         C   sP   | |  _  | |  _ | |  _ | |  _ | |  _ |  j |  | |  _ | |  _ d  S(   N(   R   R   R   R   R   t   _check_permst
   _obj_permsR   (   t   selfR   R   R   R   R   R   R   (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyt   __init__[   s    						c         C   sl   t  | t  s( t d t |    n  |  j | k oR |  j | k oR |  j | k sh t d |   n  d  S(   Ns4   Invalid object acl perms type: %s, should be a dict.sJ   Invalid object acl perms: %s, should include read, write and delete perms.(   t
   isinstancet   dictR   t   typet   OBJ_PERMS_READ_KEYt   OBJ_PERMS_WRITE_KEYt   OBJ_PERMS_DELETE_KEY(   t   clsR   (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyR   f   s    c         C   s   |  j  S(   N(   R   (   R   (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyR   t   s    c         C   s   |  j  |  | |  _ d  S(   N(   R   R   (   R   R   (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyR   x   s    c         C   sx   i |  j  |  j |  j  d 6|  j |  j 6|  j |  j 6|  j |  j 6|  j |  j 6|  j	 |  j
 6|  j |  j 6|  j |  j 6S(   sÓ  Get object acl record.

        :returns: Object acl record, like: {
            '_key': 'test_collection-1234',
            'obj_collection': 'test_collection',
            'obj_id': '1234',
            'obj_type': 'test_object',
            'obj_app': 'Splunk_TA_test',
            'obj_owner': 'admin',
            'obj_perms': {'read': ['*'], 'write': ['admin'], 'delete': ['admin']},
            'obj_shared_by_inclusion': True}
        :rtype: ``dict``
        t   _key(   t   generate_keyR   R   t   OBJ_COLLECTION_KEYt
   OBJ_ID_KEYR   t   OBJ_TYPE_KEYR   t   OBJ_APP_KEYR   t   OBJ_OWNER_KEYR   t   OBJ_PERMS_KEYR   t   OBJ_SHARED_BY_INCLUSION_KEY(   R   (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyt   record}   s    c         C   s   d j  d |  d |  S(   s*  Generate object acl record key.

        :param obj_collection: Collection where object currently stored.
        :type obj_collection: ``string``
        :param obj_id: ID of this object.
        :type obj_id: ``string``
        :returns: Object acl record key.
        :rtype: ``string``
        s   {obj_collection}_{obj_id}R   R   (   t   format(   R   R   (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyR)      s    	c      	   C   sM   t  |  t  j |  t  j |  t  j |  t  j |  t  j |  t  j |  t  j  S(   sé   Parse object acl record and construct a new `ObjectACL` object from it.

        :param obj_acl_record: Object acl record.
        :type obj_acl: ``dict``
        :returns: New `ObjectACL` object.
        :rtype: `ObjectACL`
        (   R   R*   R+   R,   R-   R.   R/   R0   (   t   obj_acl_record(    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyt   parse¦   s    






c         C   s}   xv |  j  D]k } t t j t |  j  |  t | j  |    |  j  | <|  j |  j  | k r
 |  j g |  j  | <q
 q
 Wd S(   s   Merge current object perms with perms of `obj_acl`.

        :param obj_acl: Object acl to merge.
        :type obj_acl: ``ObjectACL``
        N(   R   t   listt   sett   uniont   OBJ_PERMS_ALLOW_ALL(   R   t   obj_aclt   perm_key(    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyt   merge¹   s     c         C   s   t  j |  j  S(   N(   t   jsont   dumpsR1   (   R   (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyt   __str__È   s    (   R   R   t   __doc__R*   R+   R,   R-   R.   R/   R$   R%   R&   R8   R0   R    t   classmethodR   t   propertyR   t   setterR1   t   staticmethodR)   R4   R;   R>   (    (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyR   /   s*   		t
   exceptionsc         K   sá   t  j | | d | d | d | d | | j } t j d d |   }  y | j d |   Wn8 t j k
 r }	 |	 j d k r   n  | j	 |   n X| j
 d	 |   }
 x4 |
 D] } | j |  k r­ | j Sq­ Wt d
 |    d  S(   Nt   ownert   schemet   hostt   ports   [^\w]+t   _t   namei  t   searchs   Get collection data: %s failed.(   t   rest_clientt   SplunkRestClientt   kvstoret   ret   subt   getR    t	   HTTPErrort   statust   createR5   RJ   t   datat   KeyError(   t   collection_namet   session_keyt   appRE   RF   RG   RH   t   contextRN   t   et   collectionst
   collection(    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyt   _get_collection_dataÌ   s&    	c           B   s   e  Z RS(    (   R   R   (    (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyR   è   s   t   ObjectACLNotExistExceptionc           B   s   e  Z RS(    (   R   R   (    (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyR_   ì   s   c           B   s  e  Z d  Z d d d d d  Z e d e j g  e e d   Z	 e d e j g  e e d   Z
 e d e j g  d    Z e d e j g  d    Z e d e j g  d    Z e d e j g  d	    Z e d e j g  d
    Z RS(   så  Object ACL manager.

    :param collection_name: Collection name to store object ACL info.
    :type collection_name: ``string``
    :param session_key: Splunk access token.
    :type session_key: ``string``
    :param app: App name of namespace.
    :type app: ``string``
    :param owner: (optional) Owner of namespace, default is `nobody`.
    :type owner: ``string``
    :param scheme: (optional) The access scheme, default is None.
    :type scheme: ``string``
    :param host: (optional) The host name, default is None.
    :type host: ``string``
    :param port: (optional) The port number, default is None.
    :type port: ``integer``
    :param context: Other configurations for Splunk rest client.
    :type context: ``dict``

    :raises ObjectACLManagerException: If init ObjectACLManager failed.

    Usage::

       >>> from solnlib import user_access
       >>> oaclm = user_access.ObjectACLManager(session_key,
                                                'Splunk_TA_test')
    t   nobodyc   	   	   K   sh   d j  d | d |  } y( t | | | | | | | |  |  _ Wn! t k
 rc t d |   n Xd  S(   Ns   {app}_{collection_name}RY   RW   s#   Get object acl collection: %s fail.(   R2   R^   t   _collection_dataRV   R   (	   R   RW   RX   RY   RE   RF   RG   RH   RZ   (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyR      s    	RD   c	         C   s   t  | | | | | | |  }	 | sm y |  j | |  }
 Wn t k
 rS d }
 n X|
 rm |	 j |
  qm n  |  j j |	 j  d S(   s©  Update acl info of object.

        Construct a new object acl info first, if `replace_existing` is True
        then replace existing acl info else merge new object acl info with the
        old one and replace the old acl info with merged acl info.

        :param obj_collection: Collection where object currently stored.
        :type obj_collection: ``string``
        :param obj_id: ID of this object.
        :type obj_id: ``string``
        :param obj_app: App of this object.
        :param obj_type: ``string``
        :param obj_owner: Owner of this object.
        :param obj_owner: ``string``
        :param obj_perms: Object perms, like: {
            'read': ['*'],
            'write': ['admin'],
            'delete': ['admin']}.
        :type obj_perms: ``dict``
        :param obj_shared_by_inclusion: (optional) Flag of object is shared by
            inclusion, default is True.
        :type obj_shared_by_inclusion: ``bool``
        :param replace_existing: (optional) Replace existing acl info flag, True
            indicates replace old acl info with new one else merge with old acl
            info, default is True.
        :type replace_existing: ``bool``
        N(   R   t   get_aclR_   t   NoneR;   Ra   t
   batch_saveR1   (   R   R   R   R   R   R   R   R   t   replace_existingR9   t   old_obj_acl(    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyt
   update_acl  s    	
c	      	   C   s¨   g  }	 x | D] }
 t  | |
 | | | | |  } | s y |  j | |
  } Wn t k
 rf d } n X| r | j |  q n  |	 j | j  q W|  j j |	   d S(   sç  Batch update object acl info to all provided `obj_ids`.

        :param obj_collection: Collection where objects currently stored.
        :type obj_collection: ``string``
        :param obj_id: IDs list of objects.
        :type obj_id: ``list``
        :param obj_app: App of this object.
        :param obj_type: ``string``
        :param obj_owner: Owner of this object.
        :param obj_owner: ``string``
        :param obj_perms: Object perms, like: {
            'read': ['*'],
            'write': ['admin'],
            'delete': ['admin']}.
        :type obj_perms: ``dict``
        :param obj_shared_by_inclusion: (optional) Flag of object is shared by
            inclusion, default is True.
        :type obj_shared_by_inclusion: ``bool``
        :param replace_existing: (optional) Replace existing acl info flag, True
            indicates replace old acl info with new one else merge with old acl
            info, default is True.
        :type replace_existing: ``bool``
        N(	   R   Rb   R_   Rc   R;   t   appendR1   Ra   Rd   (   R   R   t   obj_idsR   R   R   R   R   Re   t   obj_acl_recordsR   R9   Rf   (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyt   update_aclsG  s    	
c         C   sy   t  j | |  } y |  j j |  } WnA t j k
 rk } | j d k rR   n  t d | | f   n Xt  j |  S(   s  Get acl info.

        Query object acl info with parameter of the combination of
        `obj_collection` and `obj_id` from `self.collection_name` and
        return it.

        :param obj_collection: Collection where object currently stored.
        :type obj_collection: ``string``
        :param obj_id: ID of this object.
        :type obj_id: ``string``
        :returns: Object acl info if success else None.
        :rtype: ``ObjectACL``

        :raises ObjectACLNotExistException: If object ACL info does not exist.
        i  s(   Object ACL info of %s_%s does not exist.(	   R   R)   Ra   t   query_by_idR    RR   RS   R_   R4   (   R   R   R   t   keyR9   R[   (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyRb   u  s    c         C   sq   t  j i g  | D] } i t j | |  d 6^ q d 6 } |  j j d |  } g  | D] } t j |  ^ qX S(   s«  Batch get acl info.

        Query objects acl info with parameter of the combination of
        `obj_collection` and `obj_ids` from KVStore and return them.

        :param obj_collection: Collection where object currently stored.
        :type obj_collection: ``string``
        :param obj_ids: IDs of objects.
        :type obj_ids: ``list``
        :returns: List of `ObjectACL` instances.
        :rtype: ``list``
        R(   s   $ort   query(   R<   R=   R   R)   Ra   Rn   R4   (   R   R   Ri   R   Rn   t   obj_aclsR9   (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyt   get_acls  s
    0c         C   sn   t  j | |  } y |  j j |  WnA t j k
 ri } | j d k rP   n  t d | | f   n Xd S(   s®  Delete acl info.

        Query object acl info with parameter of the combination of
        `obj_collection` and `obj_ids` from KVStore and delete it.

        :param obj_collection: Collection where object currently stored.
        :type obj_collection: ``string``
        :param obj_id: ID of this object.
        :type obj_id: ``string``

        :raises ObjectACLNotExistException: If object ACL info does not exist.
        i  s(   Object ACL info of %s_%s does not exist.N(   R   R)   Ra   t   delete_by_idR    RR   RS   R_   (   R   R   R   Rm   R[   (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyt
   delete_aclª  s    c         C   sS   t  j i g  | D] } i t j | |  d 6^ q d 6 } |  j j d |  d S(   sc  Batch delete acl info.

        Query objects acl info with parameter of the combination of
        `obj_collection` and `obj_ids` from KVStore and delete them.

        :param obj_collection: Collection where object currently stored.
        :type obj_collection: ``string``
        :param obj_ids: IDs of objects.
        :type obj_id: ``list``
        R(   s   $orRn   N(   R<   R=   R   R)   Ra   R   (   R   R   Ri   R   Rn   (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyt   delete_aclsÄ  s    0c   	      C   sh   |  j  | |  } g  } xI | D]A } | j | } t j | k sM | | k r | j | j  q q W| S(   s	  Get accessible IDs of objects from `obj_acls`.

        :param user: User name of current `operation`.
        :type user: ``string``
        :param operation: User operation, possible option: (read/write/delete).
        :type operation: ``string``
        :param obj_collection: Collection where object currently stored.
        :type obj_collection: ``string``
        :param obj_ids: IDs of objects.
        :type obj_id: ``list``
        :returns: List of IDs of accessible objects.
        :rtype: ``list``
        (   Rp   R   R   R8   Rh   R   (	   R   t   usert	   operationR   Ri   Ro   t   accessible_obj_idsR9   t   perms(    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyt   get_accessible_object_idsÖ  s    N(   R   R   R?   Rc   R    R   R    RR   t   TrueRg   Rk   Rb   Rp   Rr   Rs   Rx   (    (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyR   ð   s   ,,c           B   s   e  Z RS(    (   R   R   (    (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyR   ð  s   t   AppCapabilityNotExistExceptionc           B   s   e  Z RS(    (   R   R   (    (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyRz   ô  s   c           B   s   e  Z d  Z d d d d d  Z e d e j g  d    Z e d e j g  d    Z	 e d e j g  d    Z
 e d e j g  d    Z RS(	   s  App capability manager.

    :param collection_name: Collection name to store capabilities.
    :type collection_name: ``string``
    :param session_key: Splunk access token.
    :type session_key: ``string``
    :param app: App name of namespace.
    :type app: ``string``
    :param owner: (optional) Owner of namespace, default is `nobody`.
    :type owner: ``string``
    :param scheme: (optional) The access scheme, default is None.
    :type scheme: ``string``
    :param host: (optional) The host name, default is None.
    :type host: ``string``
    :param port: (optional) The port number, default is None.
    :type port: ``integer``
    :param context: Other configurations for Splunk rest client.
    :type context: ``dict``

    :raises AppCapabilityManagerException: If init AppCapabilityManager failed.

    Usage::

       >>> from solnlib import user_access
       >>> acm = user_access.AppCapabilityManager('test_collection',
                                                  session_key,
                                                  'Splunk_TA_test')
       >>> acm.register_capabilities(...)
       >>> acm.unregister_capabilities(...)
    R`   c   	   	   K   sq   | |  _  d j d | d |  } y( t | | | | | | | |  |  _ Wn! t k
 rl t d |   n Xd  S(   Ns   {app}_{collection_name}RY   RW   s+   Get app capabilities collection: %s failed.(   t   _appR2   R^   Ra   RV   R   (	   R   RW   RX   RY   RE   RF   RG   RH   RZ   (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyR      s    		RD   c         C   s+   i |  j  d 6| d 6} |  j j |  d S(   sì  Register app capabilities.

        :param capabilities: App capabilities, example: {
            'object_type1': {
            'read': 'read_app_object_type1',
            'write': 'write_app_object_type1',
            'delete': 'delete_app_object_type1'},
            'object_type2': {
            'read': 'read_app_object_type2',
            'write': 'write_app_object_type2',
            'delete': 'delete_app_object_type2'},
            ...}
        :type capabilities: ``dict``
        R(   t   capabilitiesN(   R{   Ra   Rd   (   R   R|   R1   (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyt   register_capabilities'  s    c         C   s\   y |  j  j |  j  Wn> t j k
 rW } | j d k rA   n  t d |  j   n Xd S(   s   Unregister app capabilities.

        :raises AppCapabilityNotExistException: If app capabilities are
            not registered.
        i  s1   App capabilities for %s have not been registered.N(   Ra   Rq   R{   R    RR   RS   Rz   (   R   R[   (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyt   unregister_capabilities;  s    c         C   sJ   y |  j  j |  j  Wn, t j k
 rE } | j d k rA   n  t SXt S(   s   Check if app capabilities are registered.

        :returns: True if app capabilities are registered else
            False.
        :rtype: ``bool``
        i  (   Ra   Rl   R{   R    RR   RS   t   FalseRy   (   R   R[   (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyt   capabilities_are_registeredL  s    	c         C   sb   y |  j  j |  j  } Wn> t j k
 rY } | j d k rC   n  t d |  j   n X| d S(   sÁ   Get app capabilities.

        :returns: App capabilities.
        :rtype: ``dict``

        :raises AppCapabilityNotExistException: If app capabilities are
            not registered.
        i  s1   App capabilities for %s have not been registered.R|   (   Ra   Rl   R{   R    RR   RS   Rz   (   R   R1   R[   (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyt   get_capabilities_  s    N(   R   R   R?   Rc   R    R   R    RR   R}   R~   R   R   (    (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyR   ø  s   c           B   s   e  Z RS(    (   R   R   (    (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyR   v  s   c   
      K   ss   t  |  d | d | d | | } | | | }	 t |  | |	 d | d | d | | so t d | |	 f   n  d S(   sA  User access checker.

    It will fetch user capabilities from given `session_key` and check if
    the capability extracted from `capabilities`, `obj_type` and `operation`
    is contained, if user capabilities include the extracted capability user
    access is ok else fail.

    :param session_key: Splunk access token.
    :type session_key: ``string``
    :param capabilities: App capabilities, example: {
        'object_type1': {
        'read': 'read_app_object_type1',
        'write': 'write_app_object_type1',
        'delete': 'delete_app_object_type1'},
        'object_type2': {
        'read': 'read_app_object_type2',
        'write': 'write_app_object_type2',
        'delete': 'delete_app_object_type2'},
        ...}
    :type capabilities: ``dict``
    :param obj_type: Object type.
    :type obj_type: ``string``
    :param operation: User operation, possible option: (read/write/delete).
    :type operation: ``string``
    :param scheme: (optional) The access scheme, default is None.
    :type scheme: ``string``
    :param host: (optional) The host name, default is None.
    :type host: ``string``
    :param port: (optional) The port number, default is None.
    :type port: ``integer``
    :param context: Other configurations for Splunk rest client.
    :type context: ``dict``

    :raises UserAccessException: If user access permission is denied.

    Usage::
       >>> from solnlib.user_access import check_user_access
       >>> def fun():
       >>>     check_user_access(
       >>>         session_key, capabilities, 'test_object', 'read')
       >>>     ...
    RF   RG   RH   s7   Permission denied, %s does not have the capability: %s.N(   R   R   R   (
   RX   R|   R   Ru   RF   RG   RH   RZ   t   usernamet
   capability(    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyR	   z  s    -c           B   s   e  Z RS(    (   R   R   (    (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyR
   ±  s   c      
   K   s    t  j |  d d | d | d | | } y" | j d d d j j   } Wn7 t j k
 r } | j d k rs   n  t d	   n Xt	 j
 |  d
 d d d S(   sç  Get current user name from `session_key`.

    :param session_key: Splunk access token.
    :type session_key: ``string``
    :param scheme: (optional) The access scheme, default is None.
    :type scheme: ``string``
    :param host: (optional) The host name, default is None.
    :type host: ``string``
    :param port: (optional) The port number, default is None.
    :type port: ``integer``
    :param context: Other configurations for Splunk rest client.
    :type context: ``dict``
    :returns: Current user name.
    :rtype: ``string``

    :raises InvalidSessionKeyException: If `session_key` is invalid.

    Usage::

       >>> from solnlib import user_access
       >>> user_name = user_access.get_current_username(session_key)
    t   -RF   RG   RH   s(   /services/authentication/current-contextt   output_modeR<   i  s   Invalid session key.t   entryi    t   contentR   (   RL   RM   RQ   t   bodyR   R    RR   RS   R
   R<   t   loads(   RX   RF   RG   RH   RZ   t   _rest_clientt   responseR[   (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyR   µ  s    	c           B   s   e  Z RS(    (   R   R   (    (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyR   â  s   c   
   
   K   s¶   t  j |  d d | d | d | | } d j d |  } y" | j | d d j j   } Wn; t j k
 r }	 |	 j d	 k r   n  t	 d
 |   n Xt
 j |  d d d d S(   sJ  Get user capabilities.

    :param session_key: Splunk access token.
    :type session_key: ``string``
    :param username: User name of capabilities to get.
    :type username: ``string``
    :param scheme: (optional) The access scheme, default is None.
    :type scheme: ``string``
    :param host: (optional) The host name, default is None.
    :type host: ``string``
    :param port: (optional) The port number, default is None.
    :type port: ``integer``
    :param context: Other configurations for Splunk rest client.
    :type context: ``dict``
    :returns: User capabilities.
    :rtype: ``list``

    :raises UserNotExistException: If `username` does not exist.

    Usage::

       >>> from solnlib import user_access
       >>> user_capabilities = user_access.get_user_capabilities(
       >>>     session_key, 'test_user')
    R   RF   RG   RH   s)   /services/authentication/users/{username}R   R   R<   i  s   User: %s does not exist.R   i    R   R|   (   RL   RM   R2   RQ   R   R   R    RR   RS   R   R<   R   (
   RX   R   RF   RG   RH   RZ   R   t   urlR   R[   (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyR   æ  s    	"c      
   K   s.   t  |  | d | d | d | | } | | k S(   sã  Check if user is capable for given `capability`.

    :param session_key: Splunk access token.
    :type session_key: ``string``
    :param username: (optional) User name of roles to get.
    :type username: ``string``
    :param capability: The capability we wish to check for.
    :type capability: ``string``
    :param scheme: (optional) The access scheme, default is None.
    :type scheme: ``string``
    :param host: (optional) The host name, default is None.
    :type host: ``string``
    :param port: (optional) The port number, default is None.
    :type port: ``integer``
    :param context: Other configurations for Splunk rest client.
    :type context: ``dict``
    :returns: True if user is capable else False.
    :rtype: ``bool``

    :raises UserNotExistException: If `username` does not exist.

    Usage::

       >>> from solnlib import user_access
       >>> is_capable = user_access.user_is_capable(
       >>>     session_key, 'test_user', 'object_read_capability')
    RF   RG   RH   (   R   (   RX   R   R   RF   RG   RH   RZ   R|   (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyR     s    !c   
   
   K   s¶   t  j |  d d | d | d | | } d j d |  } y" | j | d d j j   } Wn; t j k
 r }	 |	 j d	 k r   n  t	 d
 |   n Xt
 j |  d d d d S(   s"  Get user roles.

    :param session_key: Splunk access token.
    :type session_key: ``string``
    :param username: (optional) User name of roles to get.
    :type username: ``string``
    :param scheme: (optional) The access scheme, default is None.
    :type scheme: ``string``
    :param host: (optional) The host name, default is None.
    :type host: ``string``
    :param port: (optional) The port number, default is None.
    :type port: ``integer``
    :param context: Other configurations for Splunk rest client.
    :type context: ``dict``
    :returns: User roles.
    :rtype: ``list``

    :raises UserNotExistException: If `username` does not exist.

    Usage::

       >>> from solnlib import user_access
       >>> user_roles = user_access.get_user_roles(session_key, 'test_user')
    R   RF   RG   RH   s)   /services/authentication/users/{username}R   R   R<   i  s   User: %s does not exist.R   i    R   t   roles(   RL   RM   R2   RQ   R   R   R    RR   RS   R   R<   R   (
   RX   R   RF   RG   RH   RZ   R   R   R   R[   (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyR   9  s    	"(    R?   RO   R<   t	   splunklibR    t   solnlib.utilsR   t   solnlib.splunk_rest_clientt   splunk_rest_clientRL   t   __all__t	   ExceptionR   t   objectR   RR   R^   R   R_   R   R   Rz   R   R   Rc   R	   R
   R   R   R   R   R   (    (    (    s=   /opt/splunk/etc/apps/Splunk_TA_aws/bin/solnlib/user_access.pyt   <module>   sP   	ÿ ~6+/"