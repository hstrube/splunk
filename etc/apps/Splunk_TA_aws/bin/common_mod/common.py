import re

import taaws.proxy_conf as pc
import taaws.ta_aws_consts as tac

import s3_mod.aws_s3_consts as asc
import s3_mod.aws_s3_common as s3common


def get_queue(sqs_conn, queue_name):
    """
    Workaround boto bugs
    """

    queue = sqs_conn.get_queue(queue_name)
    if queue is not None:
        return queue

    if sqs_conn.region.name == "cn-north-1":
        queues = sqs_conn.get_all_queues(prefix=queue_name)
        for queue in queues:
            if queue.name == queue_name:
                return queue
        return None


def extract_region_from_key_name(key_name, region_rex):
    m = re.search(region_rex, key_name)
    if m:
        return m.group(1)
    return None


def create_s3_connection_from_keyname(key_id, secret_key, session_key,
                                      bucket_name, key_name, region_rex):
    region = extract_region_from_key_name(key_name, region_rex)
    if region and (region.startswith('us-gov-') or region.startswith('cn-')):
        return create_s3_connection(
            key_id, secret_key, session_key, bucket_name, region)
    return create_s3_connection(key_id, secret_key, session_key, bucket_name)


def create_s3_connection(key_id, secret_key, session_key, bucket_name=None,
                         region=None, host_name=asc.default_host,
                         aws_session_token=None):
    proxy_info = pc.get_proxy_info(session_key)
    proxy_info[tac.key_id] = key_id
    proxy_info[tac.secret_key] = secret_key
    proxy_info['aws_session_token'] = aws_session_token
    proxy_info[asc.host_name] = host_name
    proxy_info[asc.bucket_name] = bucket_name
    proxy_info[tac.region] = region
    return s3common.create_s3_connection(proxy_info)
