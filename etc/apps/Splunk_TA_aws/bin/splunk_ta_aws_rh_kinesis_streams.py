
import splunk_ta_aws_import_lib_declare

import splunk.admin as admin

from splunktalib.common import log
import taaws.ta_aws_consts as tac

logger = log.Logs(tac.splunk_ta_aws.lower()).get_logger("custom_rest")


import splunktalib.common.pattern as scp
import taaws.ta_aws_common as tacommon
import taaws.proxy_conf as pc
import kinesis_mod.aws_kinesis_common as akc
from solnlib.splunkenv import get_splunkd_uri


class KinesisStreams(admin.MConfigHandler):
    valid_params = [tac.aws_region, tac.aws_account]

    def setup(self):
        for param in self.valid_params:
            self.supportedArgs.addOptArg(param)
        self.supportedArgs.addOptArg(tac.aws_iam_role)

    @scp.catch_all(logger)
    def handleList(self, conf_info):
        logger.info("start listing kinesis streams")
        for required in self.valid_params:
            if not self.callerArgs or not self.callerArgs.get(required):
                logger.error('Missing "%s"', required)
                raise Exception('Missing "{}"'.format(required))

        aws_account = ""
        if self.callerArgs['aws_account'] is not None:
            aws_account = self.callerArgs['aws_account'][0]

        aws_iam_role = None
        if self.callerArgs.get('aws_iam_role') is not None:
            aws_iam_role = self.callerArgs['aws_iam_role'][0]

        # Set proxy for boto3
        proxy = pc.get_proxy_info(self.getSessionKey())
        tacommon.set_proxy_env(proxy)

        cred_service = tacommon.create_credentials_service(
            get_splunkd_uri(), self.getSessionKey())
        cred = cred_service.load(aws_account, aws_iam_role)

        proxy[tac.server_uri] = get_splunkd_uri()
        proxy[tac.session_key] = self.getSessionKey()
        proxy[tac.aws_account] = aws_account
        proxy[tac.aws_iam_role] = aws_iam_role
        proxy[tac.region] = self.callerArgs[tac.aws_region][0]
        proxy[tac.key_id] = cred.aws_access_key_id
        proxy[tac.secret_key] = cred.aws_secret_access_key
        proxy['aws_session_token'] = cred.aws_session_token
        client = akc.KinesisClient(proxy, logger)
        streams = client.list_streams()
        conf_info["streams"].append("streams", streams)
        logger.info("end of listing kinesis streams")


def main():
    admin.init(KinesisStreams, admin.CONTEXT_NONE)


if __name__ == "__main__":
    main()
