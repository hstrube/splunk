import cStringIO as StringIO
from datetime import datetime
import gzip
from splunksdc import logging
from s3logs.handler import AWSLogsTask


logger = logging.getLogger('splunk_aws_logs')


class CloudFrontAccessLogsDelegate(object):
    @classmethod
    def build(cls, args):
        prefix = args.get('log_file_prefix', '')
        start_date = args.get('log_start_date', datetime(1970, 1, 1))

        if isinstance(start_date, str):
            start_date = datetime.strptime(start_date, "%Y-%m-%d")

        name_format = args.get('log_name_format', '')

        if isinstance(start_date, str):
            start_date = datetime.strptime(start_date, "%Y-%m-%d")

        s1 = datetime(1970, 1, 1).strftime(name_format)
        s2 = datetime(2010, 10, 10).strftime(name_format)
        filename = ''
        for x, y in zip(s1, s2):
            if x != y:
                break
            filename += x

        return cls(prefix, start_date, filename)

    def __init__(self, prefix, start_date, filename):
        self._prefix = prefix
        self._start_date = start_date
        self._filename = filename

    def create_tasks(self, client, metadata):
        name = metadata.name
        return [AWSLogsTask(name, None)]

    def create_prefix(self, name, params):
        prefix = self._prefix + self._filename
        return prefix

    def create_initial_marker(self, name, params):
        prefix = self.create_prefix(name, params)
        marker = prefix + self._start_date.strftime('%Y-%m-%d-')
        return marker

    def create_filter(self):
        return self._filter

    def create_decoder(self):
        return self._decode

    @classmethod
    def _filter(cls, files):
        return [item for item in files if item['Key'].endswith('.gz')]

    @classmethod
    def _decode(cls, content):
        compressed = StringIO.StringIO()
        compressed.write(content)
        compressed.seek(0)

        decompressed = gzip.GzipFile(fileobj=compressed, mode='rb')
        content = decompressed.read()
        decompressed.close()
        compressed.close()

        return [content]
