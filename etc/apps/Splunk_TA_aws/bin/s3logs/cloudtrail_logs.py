import cStringIO as StringIO
from datetime import datetime
import re
import gzip
import json
import os
from splunksdc import logging
from s3logs.handler import AWSLogsTask


logger = logging.getLogger('splunk_aws_logs')


class CloudTrailLogsDelegate(object):
    @classmethod
    def build(cls, args):
        prefix = args.get('log_file_prefix', '')
        start_date = args.get('log_start_date', datetime(1970, 1, 1))
        partitions = args.get('log_partitions', '')

        if isinstance(start_date, str):
            start_date = datetime.strptime(start_date, "%Y-%m-%d")

        if len(prefix) > 0 and not prefix.endswith('/'):
            prefix += '/'

        partitions = re.compile(partitions)

        return cls(prefix, start_date, partitions)

    def __init__(self, prefix, start_date, partitions):
        self._prefix = prefix
        self._start_date = start_date
        self._partitions = partitions

    def create_tasks(self, client, metadata):
        partitions = self._enumerate_partitions(client)
        logger.info("discover partitions done", partitions=partitions)

        return [self._make_task(metadata, partition) for partition in partitions]

    def _enumerate_partitions(self, client):
        s3 = client.create_s3_client()
        partitions = list()
        prefix = self._prefix + 'AWSLogs/'
        for prefix in client.list_folders(s3, prefix):
            prefix += 'CloudTrail/'
            regions = client.list_folders(s3, prefix)
            regions = filter(self._interested, regions)
            partitions.extend(regions)
        return partitions

    def _interested(self, prefix):
        if not self._partitions:
            return True
        if self._partitions.match(prefix):
            return True
        return False

    def create_prefix(self, name, params):
        return params

    def create_initial_marker(self, name, params):
        return params + self._start_date.strftime('%Y/%m/%d/')

    def create_filter(self):
        return self._filter

    def create_decoder(self):
        return self._decode

    @classmethod
    def _filter(cls, files):
        return [item for item in files if item['Key'].endswith('.json.gz')]

    @classmethod
    def _decode(cls, content):
        compressed = StringIO.StringIO()
        compressed.write(content)
        compressed.seek(0)

        decompressed = gzip.GzipFile(fileobj=compressed, mode='rb')
        content = json.load(decompressed)
        decompressed.close()
        compressed.close()
        records = '\n'.join(json.dumps(item) for item in content.get('Records', []))
        return [records]

    @classmethod
    def _make_task(cls, metadata, partition):
        suffix = partition
        suffix = suffix.lower().replace('/', '_')
        suffix = suffix[:-1]
        name = os.path.join(metadata.name, suffix)
        return AWSLogsTask(name, partition)
