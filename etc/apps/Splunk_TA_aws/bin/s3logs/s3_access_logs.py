from datetime import datetime
from splunksdc import logging
from s3logs.handler import AWSLogsTask


logger = logging.getLogger('splunk_aws_logs')


class S3AccessLogsDelegate(object):
    @classmethod
    def build(cls, args):
        prefix = args.get('log_file_prefix', '')
        start_date = args.get('log_start_date', datetime(1970, 1, 1))

        if isinstance(start_date, str):
            start_date = datetime.strptime(start_date, "%Y-%m-%d")

        return cls(prefix, start_date)

    def __init__(self, prefix, start_date):
        self._prefix = prefix
        self._start_date = start_date

    def create_tasks(self, client, metadata):
        name = metadata.name
        return [AWSLogsTask(name, None)]

    def create_prefix(self, name, params):
        return self._prefix

    def create_initial_marker(self, name, params):
        marker = self._prefix + self._start_date.strftime('%Y-%m-%d-')
        return marker

    def create_filter(self):
        return self._filter

    def create_decoder(self):
        return self._decode

    @classmethod
    def _filter(cls, files):
        return files

    @classmethod
    def _decode(cls, content):
        return [content]
