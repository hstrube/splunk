#!/usr/bin/python

"""
This is the main entry point for AWS Config Rules Modinput
"""

import splunk_ta_aws_import_lib_declare

import time

import config_mod.aws_config_consts as acc
from splunktalib.common import log

# logger should be init at the very begging of everything
logger = log.Logs(acc.config_log_rule_ns).get_logger(acc.config_log)


import splunktalib.common.util as scutil
import splunktalib.data_loader_mgr as dlm
import taaws.ta_aws_consts as tac
import taaws.ta_aws_common as tacommon
import config_mod.aws_config_rule_conf as arc
import config_mod.aws_config_rule_data_loader as ardl


def print_scheme():
    title = "AWS Config Rules"
    description = "Collect and index Config Rules for AWS services"
    tacommon.print_scheme(title, description)


def _do_run():
    meta_configs, _, tasks = tacommon.get_configs(
        arc.AWSConfigRuleConf, "aws_config_rule", logger)

    if not tasks:
        return

    meta_configs[tac.log_file] = acc.config_log
    loader_mgr = dlm.create_data_loader_mgr(meta_configs)
    tacommon.setup_signal_handler(loader_mgr, logger)
    conf_change_handler = tacommon.get_file_change_handler(loader_mgr, logger)
    conf_monitor = arc.create_conf_monitor(conf_change_handler)
    loader_mgr.add_timer(conf_monitor, time.time(), 10)

    jobs = [ardl.ConfigRuleDataLoader(task) for task in tasks]
    loader_mgr.run(jobs)


@scutil.catch_all(logger, False)
def run():
    """
    Main loop. Run this TA forever
    """

    logger.info("Start aws_config_rule")
    _do_run()
    logger.info("End aws_config_rule")


def main():
    """
    Main entry point
    """

    tacommon.main(print_scheme, run)


if __name__ == "__main__":
    main()
