
import splunk_ta_aws_import_lib_declare

import splunk.admin as admin

from splunktalib.rest_manager import base, datainput, normaliser, validator


class Config(datainput.DataInputModel):
    """
    AWS Config Inputs Model.
    """
    dataInputName = "aws_config"
    requiredArgs = [
        'aws_account',
        'aws_region',
        'sqs_queue',
    ]
    optionalArgs = [
        'enable_additional_notifications',
        'interval',
        'sourcetype',
        'host',
        'index',
    ]
    keyMap = {
        'interval': 'polling_interval',
    }

    defaultVals = {
        'enable_additional_notifications': 'False',
        'sourcetype': 'aws:config',
        'interval': '30',
    }
    normalisers = {
        'disabled': normaliser.Boolean(),
    }
    cap4disable = 'ta_aws_custom_action_disable'
    cap4enable = 'ta_aws_custom_action_enable'


if __name__ == "__main__":
    admin.init(
        base.ResourceHandler(Config, handler=datainput.DataInputHandler),
        admin.CONTEXT_APP_AND_USER
    )
