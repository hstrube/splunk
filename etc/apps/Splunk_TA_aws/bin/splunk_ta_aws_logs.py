from __future__ import absolute_import
from datetime import timedelta
import re
import os
import shutil
import json
import urllib2
import boto3
import botocore.exceptions
import botocore.endpoint
import base.aws_credentials
from base.errors import AWSCredentialsError
from splunksdc import logging
from splunksdc import SimpleCollectorV1
from s3logs.handler import AWSLogsHandler
from s3logs.cloudtrail_logs import CloudTrailLogsDelegate
from s3logs.elb_access_logs import ELBAccessLogsDelegate
from s3logs.s3_access_logs import S3AccessLogsDelegate
from s3logs.cloudfront_access_logs import CloudFrontAccessLogsDelegate


logger = logging.getLogger('splunk_aws_logs')


class UnsupportedLogType(Exception):
    def __init__(self, typename):
        super(UnsupportedLogType, self).__init__('unknown log type')
        self.typename = typename


class ProxySettingsDecoder(object):
    def decode(self, service, content):
        response = service.get('splunk_ta_aws/settings/proxy', output_mode='json')
        response = response.body.read()
        response = json.loads(response)
        entry = response['entry'][0]
        content.update(entry['content'])
        return content


class ProxySettings(object):
    @classmethod
    def load(cls, config):
        endpoint = ('splunk_ta_aws_settings', 'proxy')
        config.set_hook(endpoint, ProxySettingsDecoder())
        content = config.load(endpoint)
        config.watch(endpoint, content)
        proxy = cls()
        proxy.parse(content)
        return proxy

    def __init__(self):
        self.enabled = None
        self.host = None
        self.port = None
        self.username = None
        self.password = None

    def parse(self, content):
        self.enabled = not content.get('disabled')
        self.host = content.get('host')
        self.port = content.get('port')
        self.username = content.get('username')
        self.password = content.get('password')

    def make_url(self, scheme):
        endpoint = '{host}:{port}'.format(
            host=self.host,
            port=self.port
        )
        auth = None
        if self.username and len(self.username) > 0:
            auth = urllib2.quote(self.username.encode(), safe='')
            if self.password and len(self.password) > 0:
                auth += ':'
                auth += urllib2.quote(self.password.encode(), safe='')

        if auth:
            endpoint = auth + '@' + endpoint

        url = scheme + '://' + endpoint
        return url


class AWSLogsSettings(object):
    @classmethod
    def load(cls, config):
        endpoint = ('splunk_ta_aws_settings', 'splunk_ta_aws_logs')
        content = config.load(endpoint)
        config.watch(endpoint, content)
        settings = cls()
        settings.parse(content)
        return settings

    def __init__(self):
        self.log_level = logging.INFO

    def parse(self, content):
        value = content.get('log_level')
        if value == 'DEBUG':
            self.log_level = logging.DEBUG
        elif value == 'ERROR':
            self.log_level = logging.ERROR


class AWSLogsClientBuilder(object):
    def __init__(
            self,
            credentials,
            aws_account,
            aws_iam_role,
            s3_bucket_name,
            s3_bucket_region
    ):
        self._credentials = credentials
        self._aws_account = aws_account
        self._aws_iam_roel = aws_iam_role
        self._s3_bucket_name = s3_bucket_name
        self._s3_bucket_region = s3_bucket_region
        self._cached_client = self._refresh()

    def _refresh(self):
        aws_credentials = self._credentials.load(
            self._aws_account,
            self._aws_iam_roel,
        )
        return AWSLogsClient(
            aws_credentials=aws_credentials,
            s3_bucket_name=self._s3_bucket_name,
            s3_bucket_region=self._s3_bucket_region
        )

    def __call__(self):
        if self._cached_client.need_retire():
            self._cached_client = self._refresh()
        return self._cached_client


class AWSLogsClient(object):
    _MIN_TTL = timedelta(minutes=15)

    def __init__(self, **kwargs):
        self._aws_credentials = kwargs.pop('aws_credentials')
        self._s3_bucket_name = kwargs.pop('s3_bucket_name')
        self._s3_bucket_region = kwargs.pop('s3_bucket_region')

    def create_s3_client(self):
        credentials = self._aws_credentials
        session = boto3.session.Session()
        client = session.client(
            's3',
            region_name=self._s3_bucket_region,
            aws_access_key_id=credentials.aws_access_key_id,
            aws_secret_access_key=credentials.aws_secret_access_key,
            aws_session_token=credentials.aws_session_token,
        )
        return client

    def need_retire(self):
        return self._aws_credentials.need_retire(self._MIN_TTL)

    def list_folders(self, s3, prefix):
        logger.info("s3 list folders start", prefix=prefix)
        response = s3.list_objects(
            Bucket=self._s3_bucket_name,
            Delimiter='/',
            Prefix=prefix
        )
        prefixes = response.get('CommonPrefixes', [])
        prefixes = [item['Prefix'] for item in prefixes]
        return prefixes

    def list_files(self, s3, prefix, marker):
        logger.info("s3 list files start", prefix=prefix, marker=marker)
        response = s3.list_objects(
            Bucket=self._s3_bucket_name,
            Prefix=prefix,
            Marker=marker
        )
        items = response.get('Contents', [])
        return [item for item in items]

    @property
    def bucket_name(self):
        return self._s3_bucket_name


class AWSLogsMetadata(object):
    @classmethod
    def build(cls, stanza):

        kind = stanza.kind
        name = stanza.name
        params = stanza.content

        max_retries = params.get('max_retries', '-1')
        max_retries = int(max_retries)
        max_retries = max(-1, max_retries)
        max_retries = min(1000, max_retries)

        max_fails = params.get('max_fails', '10000')
        max_fails = int(max_fails)
        max_fails = max(0, max_fails)
        max_fails = min(10000, max_fails)

        metadata = cls()
        metadata.kind = kind
        metadata.name = name
        metadata.index = params.get('index')
        metadata.host = params.get('host')
        metadata.sourcetype = params.get('sourcetype')
        metadata.max_retries = max_retries
        metadata.max_fails = max_fails
        return metadata

    def __init__(self):
        self.kind = None
        self.name = None
        self.index = None
        self.host = None
        self.sourcetype = None
        self.max_retries = None
        self.max_fails = None

    @property
    def stanza(self):
        return self.kind + '://' + self.name

    def should_retry(self, retried):
        if self.max_retries == -1:
            return True
        return retried <= self.max_retries


class LogType(object):
    def __init__(self, sourcetype, delegate):
        self.sourcetype = sourcetype
        self.delegate = delegate


class AWSLogsInput(object):
    _LOG_TYPES = {
        'cloudtrail': LogType(
            'aws:cloudtrail',
            CloudTrailLogsDelegate
        ),
        'elb:accesslogs': LogType(
            'aws:elb:accesslogs',
            ELBAccessLogsDelegate
        ),
        'cloudfront:accesslogs': LogType(
            'aws:cloudfront:accesslogs',
            CloudFrontAccessLogsDelegate
        ),
        's3:accesslogs': LogType(
            'aws:s3:accesslogs',
            S3AccessLogsDelegate
        )
    }

    @classmethod
    def load(cls, app, config):
        inputs = app.inputs()
        stanza = inputs[0]
        kind = stanza.kind
        name = stanza.name
        params = stanza.content

        logger.info("start parse stanza", kind=kind, name=name, params=params)

        family = params.get('log_type', '')
        family = family.lower()
        log_type = cls._get_log_type(family)
        if log_type is None:
            raise UnsupportedLogType(family)

        aws_account = params.get('aws_account')
        aws_iam_role = params.get('aws_iam_role')
        s3_bucket_name = params.get('bucket_name')
        s3_bucket_region = params.get('bucket_region')

        credentials = base.aws_credentials.create_service(config)
        client_builder = AWSLogsClientBuilder(
            credentials,
            aws_account,
            aws_iam_role,
            s3_bucket_name,
            s3_bucket_region,
        )

        metadata = AWSLogsMetadata.build(stanza)
        if not metadata.sourcetype:
            metadata.sourcetype = log_type.sourcetype

        extra = {k: v for k, v in params.items() if v is not None}
        delegate = log_type.delegate.build(extra)
        return cls(client_builder, metadata, delegate)

    def __init__(self, client_builder, metadata, delegate):
        self._client_builder = client_builder
        self._metadata = metadata
        self._delegate = delegate

    def create_handler(self):
        return AWSLogsHandler.build(
            self._client_builder,
            self._metadata,
            self._delegate
        )

    @classmethod
    def _get_log_type(cls, family):
        return cls._LOG_TYPES.get(family)


class AWSLogsDelegate(object):

    _EXCEPTIONS = (
        IOError,
        re.error,
        botocore.exceptions.BotoCoreError,
        botocore.exceptions.ClientError,
        AWSCredentialsError,
        UnsupportedLogType,
    )

    def __init__(self):
        self._proxy = None
        self._handler = None

    def prepare(self, app, config, scheduler):
        try:
            settings = AWSLogsSettings.load(config)
            app.set_log_level(settings.log_level)
            self._proxy = ProxySettings.load(config)
            self._setup_proxy_for_boto3()
            inputs = AWSLogsInput.load(app, config)
            self._handler = inputs.create_handler()
            self._handler.prepare(scheduler)
        except self._EXCEPTIONS:
            logger.exception('prepare error')

    def perform(self, app, portal, checkpoint, name, params):
        self._setup_proxy_for_boto3()
        self._handler.perform(app, portal, checkpoint, name, params)

    def _setup_proxy_for_boto3(self):
        proxy = self._proxy
        http_url = proxy.make_url('http')
        https_url = proxy.make_url('https')

        def _get_proxies(creator, url):
            return {"http": http_url, "https": https_url}

        logger.info(
            'proxy settings',
            enabled=proxy.enabled,
            host=proxy.host,
            port=proxy.port,
            username=proxy.username
        )

        if self._proxy.enabled:
            creator = botocore.endpoint.EndpointCreator
            creator._get_proxies = _get_proxies


def main():
    arguments = {
        'aws_account': {
            'title': 'The AWS account name.'
        },
        'aws_iam_role': {
            'title': 'Assume Role.',
            'required_on_create': False
        },
        'log_type': {
            'title': 'What is kind of log.'
        },
        'host_name': {
            'title': 'S3 host name for bucket.'
        },
        'bucket_name': {
            'title': 'Where are the logs located.'
        },
        'bucket_region': {
            'title': 'Where is the bucket located.'
        },
        'log_file_prefix': {
            'title': 'Please Read document for details.'
        },
        'log_start_date': {
            'title': 'The logs earlier than this date would not be ingested.'
        },
        'log_name_format': {
            'title': 'Please Read document for details.'
        },
        'max_retries': {
            'title': 'Max Retries',
            'required_on_create': False
        },
        'max_fails': {
            'title': 'Max Fails',
            'required_on_create': False
        }
    }

    SimpleCollectorV1.main(
        AWSLogsDelegate(),
        use_single_instance=False,
        arguments=arguments,
    )


def create_data_input(name, *args, **kwargs):
    pass


def delete_data_input(name, *args, **kwargs):
    from splunksdc.utils import get_checkpoint_folder
    root = get_checkpoint_folder('splunk_ta_aws_logs')
    path = os.path.join(root, name)

    # try remove files for cloudtrail and elb
    if os.path.isdir(path):
        shutil.rmtree(path, ignore_errors=True)

    # try remove files for s3 and cloudfront
    path += '.ckpt'
    if os.path.isfile(path):
        os.remove(path)


if __name__ == '__main__':
    main()
