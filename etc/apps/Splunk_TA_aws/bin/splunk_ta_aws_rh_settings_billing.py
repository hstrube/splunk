import splunk_ta_aws_import_lib_declare
import os
import sys
import re

import splunk.admin as admin

from splunktalib.rest_manager import multimodel

import splunk_ta_aws_rh_settings_base


class BillingSettingHandler(splunk_ta_aws_rh_settings_base.AWSSettingHandler):
    stanzaName = 'aws_billing'
    

if __name__ == "__main__":
    admin.init(multimodel.ResourceHandler(splunk_ta_aws_rh_settings_base.AWSSettings, handler=BillingSettingHandler), admin.CONTEXT_APP_AND_USER)
