import splunk_ta_aws_import_lib_declare
import re

import logging

import splunk.admin as admin
import splunk.clilib.cli_common as scc
from splunktalib.conf_manager.conf_manager import ConfManager
from taaws.aws_accesskeys import AwsAccessKeyManager

import splunktalib.common.util as scutil
from splunktalib.rest_manager import util, error_ctl

import taaws.ta_aws_consts as tac

KEY_NAMESPACE = util.getBaseAppName()
KEY_OWNER = '-'

AWS_PROXY_PREFIX = "_aws_"

POSSIBLE_KEYS = ('secret_key', 'key_id')
OPTIONAL_KEYS = ('category', 'iam')
CONF_FOR_ACCOUNT_EXT_FIELDS = "aws_account_ext"


class AccountRestHandler(admin.MConfigHandler):
    """
    Manage AWS Accounts in Splunk_TA_aws add-on.
    """

    def setup(self):
        if self.requestedAction in (admin.ACTION_CREATE, admin.ACTION_EDIT):
            for arg in POSSIBLE_KEYS:
                self.supportedArgs.addReqArg(arg)
            for arg in OPTIONAL_KEYS:
                self.supportedArgs.addOptArg(arg)
        return

    def _getConfManager(self):
        sessionKey = self.getSessionKey()
        server_uri = scc.getMgmtUri()
        return ConfManager(server_uri, sessionKey, "nobody", KEY_NAMESPACE)

    def _getAccountConfig(self, key):
        result = {
            'name': key.name,
            'key_id': key.key_id,
            'secret_key': key.secret_key,
            'category': key.category,
            'iam': key.iam,
            'token': key.token,
            'account_id': key.account_id,
        }
        cm = self._getConfManager()
        if cm.stanza_exist(CONF_FOR_ACCOUNT_EXT_FIELDS, key.name):
            temp = cm.get_stanza(CONF_FOR_ACCOUNT_EXT_FIELDS, key.name)
            for key in temp:
                if key in OPTIONAL_KEYS:
                    result[key] = temp[key]
            result['category'] = int(result['category'])
            if result['category'] not in tac.RegionCategory.VALID:
                result['category'] = tac.RegionCategory.DEFAULT
        elif result["iam"]:
            # Commit IAM in aws_account_ext.conf
            account_ext = {
                "category": result["category"],
                "iam": 1,
            }
            cm.create_stanza(CONF_FOR_ACCOUNT_EXT_FIELDS, key.name, account_ext)

        return result

    def handleCreate(self, confInfo):
        try:
            self.callerArgs.id = self.callerArgs.id.strip()
            if self.callerArgs.id.lower() in ('default', ):
                error_ctl.RestHandlerError.ctl(
                    400,
                    msgx='Name "%s" for AWS account is not allowed.' % self.callerArgs.id,
                    logLevel=logging.INFO,
                )
            if not re.match(r'[a-zA-Z0-9_\-,\s]{1,255}', self.callerArgs.id):
                error_ctl.RestHandlerError.ctl(
                    400,
                    msgx='Please enter a valid name containing only: '
                         'alphanumeric, space, dash, underscore and '
                         'comma characters',
                    logLevel=logging.INFO,
                )

            accs = self.all()
            keys = {key.lower() for key in accs}
            if self.callerArgs.id.lower() in keys:
                raise Exception('An AWS account named \"%s\" already exists. Note: it is not case-sensitive.' % self.callerArgs.id)

            args = self.validate(self.callerArgs.data)
            km = AwsAccessKeyManager(KEY_NAMESPACE, KEY_OWNER, self.getSessionKey())
            km.set_accesskey(key_id=args['key_id'][0], secret_key=args['secret_key'][0], name=self.callerArgs.id)

            cm = self._getConfManager()
            cate = args['category'][0]

            if cm.stanza_exist(CONF_FOR_ACCOUNT_EXT_FIELDS, self.callerArgs.id):
                cm.delete_stanza(CONF_FOR_ACCOUNT_EXT_FIELDS, self.callerArgs.id)

            cm.create_stanza(CONF_FOR_ACCOUNT_EXT_FIELDS, self.callerArgs.id, {"category": cate})
        except Exception as exc:
            error_ctl.RestHandlerError.ctl(400, msgx=exc, logLevel=logging.INFO)

    def handleRemove(self, confInfo):
        try:
            km = AwsAccessKeyManager(KEY_NAMESPACE, KEY_OWNER, self.getSessionKey())
            km.delete_accesskey(self.callerArgs.id)

            cm = self._getConfManager()
            if cm.stanza_exist(CONF_FOR_ACCOUNT_EXT_FIELDS, self.callerArgs.id):
                cm.delete_stanza(CONF_FOR_ACCOUNT_EXT_FIELDS, self.callerArgs.id)
        except Exception as exc:
            error_ctl.RestHandlerError.ctl(400, msgx=exc, logLevel=logging.INFO)

    def handleEdit(self, confInfo):
        try:
            self.get(self.callerArgs.id)
            args = self.validate(self.callerArgs.data)
            if not args.get("iam") or scutil.is_false(args["iam"][0]):
                self.get(self.callerArgs.id)
                km = AwsAccessKeyManager(KEY_NAMESPACE, KEY_OWNER, self.getSessionKey())
                km.set_accesskey(key_id=args['key_id'][0], secret_key=args['secret_key'][0], name=self.callerArgs.id)

            km = AwsAccessKeyManager(KEY_NAMESPACE, KEY_OWNER, self.getSessionKey())
            km.set_accesskey(key_id=args['key_id'][0], secret_key=args['secret_key'][0], name=self.callerArgs.id)

            cm = self._getConfManager()
            cate = args['category'][0]
            if cm.stanza_exist(CONF_FOR_ACCOUNT_EXT_FIELDS, self.callerArgs.id):
                cm.update_stanza(CONF_FOR_ACCOUNT_EXT_FIELDS, self.callerArgs.id, {"category": cate})
            else:
                cm.create_stanza(CONF_FOR_ACCOUNT_EXT_FIELDS, self.callerArgs.id, {"category": cate})
        except Exception as exc:
            error_ctl.RestHandlerError.ctl(400, msgx=exc, logLevel=logging.INFO)

    def handleList(self, confInfo):
        try:
            if self.callerArgs.id is None:
                accs = self.all()
                for name, ent in accs.items():
                    self.makeConfItem(name, ent, confInfo)
            else:
                self.makeConfItem(self.callerArgs.id, self.get(self.callerArgs.id), confInfo)
        except Exception as exc:
            error_ctl.RestHandlerError.ctl(400, msgx=exc, logLevel=logging.INFO)

    def validate(self, args):
        name = self.callerArgs.id
        m = re.match('^[a-zA-Z0-9_\-\, ]+$', name)
        if not m:
            error_ctl.RestHandlerError.ctl(1100, msgx='name=%s' % name, logLevel=logging.INFO)

        try:
            args['key_id'][0] = args['key_id'][0].strip()
            if len(args['key_id'][0]) <= 0:
                raise Exception('')
        except:
            error_ctl.RestHandlerError.ctl(1100, msgx='key_id', logLevel=logging.INFO)

        try:
            args['secret_key'][0] = args['secret_key'][0].strip()
            if len(args['secret_key'][0]) <= 0:
                raise Exception('')
        except:
            error_ctl.RestHandlerError.ctl(1100, msgx='secret_key', logLevel=logging.INFO)

        try:
            cate = args['category'][0] = int(args['category'][0])
            if cate not in tac.RegionCategory.VALID:
                raise Exception('')
        except:
            error_ctl.RestHandlerError.ctl(1100, msgx='category', logLevel=logging.INFO)

        return args

    def all(self):
        km = AwsAccessKeyManager(KEY_NAMESPACE, KEY_OWNER, self.getSessionKey())
        keys = km.all_accesskeys()
        accs = {}
        for key in keys:
            if str(key.name).lower().startswith(AWS_PROXY_PREFIX):
                continue
            accs[key.name] = self._getAccountConfig(key)
        return accs

    def get(self, name):
        name = name.strip()
        km = AwsAccessKeyManager(KEY_NAMESPACE, KEY_OWNER, self.getSessionKey())
        key = km.get_accesskey(name)
        if key is None:
            raise Exception('No AWS account named \"%s\" exists.' % (name))
        return self._getAccountConfig(key)

    def makeConfItem(self, name, entity, confInfo):
        confItem = confInfo[name]
        for key, val in entity.items():
                confItem[key] = str(val)


if __name__ == "__main__":
    admin.init(AccountRestHandler, admin.CONTEXT_APP_AND_USER)
