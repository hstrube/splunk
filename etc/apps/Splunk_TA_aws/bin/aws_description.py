#!/usr/bin/python

"""
This is the main entry point for AWS Description TA
"""

import splunk_ta_aws_import_lib_declare

import time
import traceback

import description_mod.aws_description_consts as adcon
from splunktalib.common import log

# logger should be init at the very begging of everything
logger = log.Logs(adcon.description_log_ns).get_logger(adcon.description_log)


import splunktalib.data_loader_mgr as dlm
import taaws.ta_aws_consts as tac
import taaws.ta_aws_common as tacommon
import description_mod.aws_description_conf as adc
import description_mod.aws_description_data_loader as addl


def print_scheme():
    title = "AWS Description Metadata"
    description = "Collect and index descriptions for AWS services"
    tacommon.print_scheme(title, description)


def _do_run():
    meta_configs, _, tasks = tacommon.get_configs(
        adc.AWSDescribeConf, "aws_description", logger)

    if not tasks:
        logger.info("No data input has been configured, exiting...")
        return

    meta_configs[tac.log_file] = adcon.description_log
    loader_mgr = dlm.create_data_loader_mgr(meta_configs)
    tacommon.setup_signal_handler(loader_mgr, logger)
    conf_change_handler = tacommon.get_file_change_handler(loader_mgr, logger)
    conf_monitor = adc.create_conf_monitor(conf_change_handler)
    loader_mgr.add_timer(conf_monitor, time.time(), 10)

    jobs = [addl.DescriptionDataLoader(task) for task in tasks]
    loader_mgr.run(jobs)


def run():
    """
    Main loop. Run this TA forever
    """

    logger.info("Start aws_description")
    try:
        _do_run()
    except Exception:
        logger.error("Failed to collect description data, error=%s",
                     traceback.format_exc())
    logger.info("End aws_description")


def main():
    """
    Main entry point
    """

    tacommon.main(print_scheme, run)


if __name__ == "__main__":
    main()
