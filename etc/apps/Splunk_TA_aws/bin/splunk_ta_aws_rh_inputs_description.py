import splunk_ta_aws_import_lib_declare

import splunk.admin as admin

from splunktalib.rest_manager import base,normaliser


class Description(base.BaseModel):
    """
    AWS Description Inputs Model.
    """
    endpoint = "configs/conf-aws_description_tasks"
    requiredArgs = {'aws_account', 'aws_regions', 'apis'}
    optionalArgs = {'aws_iam_role', 'sourcetype', 'host', 'index'}
    defaultVals = {
        'sourcetype': 'aws:description',
        'index': 'default',
    }
    normalisers = {
        'disabled': normaliser.Boolean(),
    }
    keyMap = {
        'aws_account': 'account',
        'aws_regions': 'regions',
    }
    cap4disable = 'ta_aws_custom_action_disable'
    cap4enable = 'ta_aws_custom_action_enable'


if __name__ == "__main__":
    admin.init(base.ResourceHandler(Description), admin.CONTEXT_APP_AND_USER)
