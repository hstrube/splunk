
class AWSCredentialsError(Exception):
    pass


class AWSAccountError(AWSCredentialsError):
    def __init__(self, message, aws_account):
        super(AWSAccountError, self).__init__(message)
        self.aws_account = aws_account


class AWSIAMRoleError(AWSCredentialsError):
    def __init__(self, message, aws_iam_role):
        super(AWSIAMRoleError, self).__init__(message)
        self.aws_iam_role = aws_iam_role
