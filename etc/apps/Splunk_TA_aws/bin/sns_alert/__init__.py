"""
Sends alerting messages to AWS SNS topic.
"""

# This import is required to using k-v logging
from splunksdc import logging

from splunktalib.common import log as sclog

from taaws.ta_aws_consts import splunk_ta_aws

alert_name = 'splunk_ta_aws_sns_alert'

logger = sclog.Logs(splunk_ta_aws).get_logger('sns_alert')
