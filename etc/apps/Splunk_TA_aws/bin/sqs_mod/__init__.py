"""
AWS SQS Modular Input
"""

from splunksdc import logging

modular_name = 'splunk_ta_aws_sqs'

logger = logging.getLogger(modular_name)
