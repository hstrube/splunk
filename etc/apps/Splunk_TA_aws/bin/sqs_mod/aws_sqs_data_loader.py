import re
import copy
import json
import traceback

from splunksdc import logging
from splunksdc import EventStream
from splunktalib.common import util as scutil

from taaws.ta_aws_common import make_splunkd_uri, make_splunk_endpoint
from taaws.ta_aws_common import load_config
import taaws.ta_aws_common as tacommon
import taaws.proxy_conf as pc

from sqs_mod import logger
from sqs_mod.aws_sqs_collector import SQSCollector, get_sqs_queue_url
from sqs_mod.aws_sqs_collector import get_sqs_client


class Configs(object):

    INPUTS = 'aws_sqs_inputs'
    ACCOUNTS = 'aws_accounts'
    SETTINGS_LOGGING = 'aws_settings_sqs_logging'
    SETTINGS_PROXY = 'aws_settings_proxy'

    ENDPOINTS = {
        INPUTS: {
            'endpoint': 'splunk_ta_aws/inputs/sqs',
            'label': 'AWS SQS Inputs',
        },
        ACCOUNTS: {
            'endpoint': 'splunk_ta_aws/settings/account',
            'label': 'AWS Accounts',
        },
        SETTINGS_LOGGING: {
            'endpoint': 'splunk_ta_aws/settings/sqs_setting/logging',
            'label': 'AWS SQS Logging Setting',
        },
    }

    def decode(self, service, stanzas):
        logger.debug('AWS SQS Input Configs')
        splunkd_uri = \
            make_splunkd_uri(service.scheme, service.host, service.port)

        user, app = 'nobody', 'Splunk_TA_aws'
        configs = {
            key: load_config(
                make_splunk_endpoint(splunkd_uri, ep['endpoint'], user, app),
                service.token, ep['label'])
            for key, ep in Configs.ENDPOINTS.iteritems()
        }
        configs[Configs.SETTINGS_PROXY] = pc.get_proxy_info(service.token)
        return configs


def ingest(messages, portal, index, sourcetype, source):

    stream = EventStream(
        index=index,
        sourcetype=sourcetype,
        source=source,
    )

    for message in messages:
        data = json.dumps(message)
        stream.append(data)
    portal.write(stream)


class Input(object):

    _SQS_TASKS = 'aws_sqs_tasks'
    _SEP = '``splunk_ta_aws_sqs_sep``'

    def prepare(self, app, config, scheduler):
        config.set_hook(Input._SQS_TASKS, Configs())
        configs = config.load(Input._SQS_TASKS)
        config.watch(Input._SQS_TASKS, configs)

        # If config is empty, do nothing and return.
        if not configs:
            logger.info('No Task Configured')
            return

        # Set Logging
        logger.setLevel(
            configs[Configs.SETTINGS_LOGGING]['logging']['level'])
        logger.debug('AWS SQS Input Discover')

        # Set Proxy
        tacommon.set_proxy_env(configs[Configs.SETTINGS_PROXY])

        # Generate Tasks
        for name, item in configs[Configs.INPUTS].iteritems():
            if scutil.is_true(item.get('disabled', '0')):
                continue
            try:
                aws_account = configs[Configs.ACCOUNTS][item['aws_account']]
            except KeyError:
                logger.error(
                    'AWS account not found for SQS input',
                    datainput=name,
                    aws_account=item['aws_account'],
                )
                continue
            item['datainput'] = name
            self.generate_tasks(name, item, aws_account, scheduler)

    def generate_tasks(self, input_name, input_item, aws_account, scheduler):
        sqs_queues = re.split('\s*,\s*', input_item.get('sqs_queues', ''))
        for sqs_queue in sqs_queues:
            item_new = copy.deepcopy(input_item)
            del item_new['sqs_queues']
            item_new['key_id'] = aws_account.get('key_id')
            item_new['secret_key'] = aws_account.get('secret_key')
            item_new['token'] = aws_account.get('token')
            item_new['sqs_queue'] = sqs_queue
            task_name = input_name + Input._SEP + sqs_queue
            interval = int(item_new.get('interval', '30'))
            scheduler.add_task(task_name, item_new, interval)

    @staticmethod
    def log(params, msg, level=logging.DEBUG, **kwargs):
        logger.log(
            level=level,
            msg=msg,
            data_input=params['datainput'],
            aws_account=params['aws_account'],
            aws_region=params['aws_region'],
            sqs_queue=params['sqs_queue'],
            **kwargs
        )

    def perform(self, app, portal, checkpoint, name, params):
        Input.log(params, 'AWS SQS Input Collect')
        client = get_sqs_client(params['aws_region'],
                                params['key_id'],
                                params['secret_key'],
                                params['token'])
        params['client'] = client
        try:
            queue_url = get_sqs_queue_url(params['client'],
                                          params['sqs_queue'])
            Input.log(params, 'AWS SQS Input Collect TEST')
        except Exception:
            Input.log(params, 'Failed to get SQS queue url',
                      logging.ERROR, error=traceback.format_exc())
            return

        sourcetype = params.get('sourcetype', 'aws:sqs')
        index = params.get('index', 'default')
        source = queue_url

        collector = SQSCollector(
            params['client'], queue_url, logger,
            handler=ingest, portal=portal,
            index=index, sourcetype=sourcetype, source=source,

        )
        result = collector.run()
        if result is not True:
            Input.log(params, 'SQS queue fetching failed', logging.ERROR)
        Input.log(params, 'SQS Fetching Finished')
