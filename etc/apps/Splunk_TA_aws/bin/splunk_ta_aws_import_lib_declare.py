import os
import sys
import re

ta_name = os.path.basename(os.path.dirname(os.path.dirname(__file__)))
pattern = re.compile(r"[\\/]etc[\\/]apps[\\/][^\\/]+[\\/]bin[\\/]?$")
new_paths = [path for path in sys.path if not pattern.search(path) or ta_name in path]
new_paths.insert(0, os.path.dirname(__file__))
sys.path = new_paths
