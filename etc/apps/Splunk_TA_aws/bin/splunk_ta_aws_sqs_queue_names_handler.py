"""
Custom REST Endpoint for enumerating AWS SQS queue names.
"""

import splunk_ta_aws_import_lib_declare

from splunktalib.common import log
from splunktalib.rest_manager.error_ctl import RestHandlerError as RH_Err
logger = log.Logs("splunk_ta_aws").get_logger("rest", level="DEBUG")
log_enter_exit = log.log_enter_exit(logger)


import traceback
import splunk
import splunk.admin

import taaws.ta_aws_common as tacommon
import taaws.ta_aws_consts as tac
from taaws.s3util import connect_sqs


class ConfigHandler(splunk.admin.MConfigHandler):

    @log_enter_exit
    def setup(self):

        self.supportedArgs.addReqArg('aws_region')
        self.supportedArgs.addReqArg('aws_account')

    @log_enter_exit
    def handleList(self, confInfo):
        import boto.sqs
        import boto.exception

        key_id, secret_key = tacommon.assert_creds(
            self.callerArgs[tac.aws_account][0], self.getSessionKey(), logger)

        sqs_conn = connect_sqs(
            self.callerArgs['aws_region'][0], key_id, secret_key,
            self.getSessionKey())

        if sqs_conn is None:
            msg = "Invalid SQS Queue Region: {}".format(
                self.callerArgs['aws_region'][0])
            RH_Err.ctl(400, msgx=msg)

        q_names = []
        try:
            q_list = sqs_conn.get_all_queues()
            if q_list:
                for q in q_list:
                    q_names.append(q.name)
        except boto.exception.SQSError as e:
            if e.status == 403 and e.reason == 'Forbidden':
                if e.error_code == 'InvalidClientTokenId':
                    msg = ("Authentication Key ID is invalid or lack of "
                           "permission. Check configurations.")
                    logger.error(msg)
                    raise Exception(msg)
                elif e.error_code == 'SignatureDoesNotMatch':
                    msg = ("Authentication Secret Key is invalid. "
                           "Check configurations.")
                    logger.error(msg)
                    raise Exception(msg)
        except Exception as e:
            msg = "Failed AWS Authentication: error={}".format(traceback.format_exc())
            logger.error(msg)
            raise Exception(msg)

        confInfo['SqsQueueNamesResult'].append('sqs_queue_names', q_names)


@log_enter_exit
def main():
    splunk.admin.init(ConfigHandler, splunk.admin.CONTEXT_NONE)


if __name__ == '__main__':
    main()
