import common_mod.common as cc


def create_s3_connection(key_id, secret_key, session_key,
                         bucket_name, key_name):
    # 063605715280_Config_ap-southeast-1_ConfigHistory_AWS::EC2::EIP_20151218T090657Z_20151218T090657Z_1.json.gz
    region_rex = r"AWSLogs\/\d+\/Config\/([^_\/]+)\/"
    return cc.create_s3_connection_from_keyname(
        key_id, secret_key, session_key, bucket_name, key_name, region_rex)
