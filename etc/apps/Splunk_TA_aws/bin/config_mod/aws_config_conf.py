import os.path as op

# FIXME Legacy code started
import taaws.proxy_conf as tpc
# Legacy code done

import taaws.ta_aws_consts as tac
import config_mod.aws_config_consts as acc

import splunktalib.file_monitor as fm
import splunktalib.conf_manager.conf_manager as cm

from splunktalib.common import log
import taaws.ta_aws_common as tacommon


logger = log.Logs().get_logger(acc.config_log)


def create_conf_monitor(callback):
    files = (AWSConfigConf.app_file,
             AWSConfigConf.task_file_w_path,
             AWSConfigConf.passwords_file_w_path,
             AWSConfigConf.log_info_w_path)

    return fm.FileMonitor(callback, files)


class AWSConfigConf(object):

    app_dir = op.dirname(op.dirname(op.dirname(op.abspath(__file__))))
    app_file = op.join(app_dir, "local", "app.conf")
    task_file = "inputs"
    task_file_w_path = op.join(app_dir, "local", task_file + ".conf")
    passwords = "passwords"
    passwords_file_w_path = op.join(app_dir, "local", passwords + ".conf")
    log_info = "log_info"
    log_info_w_path = op.join(app_dir, "local", log_info + ".conf")

    def __init__(self):
        self.metas, self.stanza_configs = tacommon.get_modinput_configs()
        self.metas[tac.app_name] = tac.splunk_ta_aws

    def get_tasks(self):
        if not self.stanza_configs:
            return None

        conf_mgr = cm.ConfManager(self.metas[tac.server_uri],
                                  self.metas[tac.session_key])
        logging = conf_mgr.get_stanza(
            self.log_info, acc.log_stanza, do_reload=True)

        proxy_info = tpc.get_proxy_info(self.metas[tac.session_key])
        tasks, creds = [], {}
        for stanza in self.stanza_configs:
            task = {}
            task.update(stanza)
            task.update(self.metas)
            task.update(proxy_info)

            # Normalize aws_region to region
            task[tac.region] = stanza.get(tac.aws_region)

            key_id, secret_key = tacommon.get_aws_creds(
                stanza, self.metas, creds)
            task[tac.log_level] = logging[acc.log_level]
            task[tac.key_id] = key_id
            task[tac.secret_key] = secret_key
            task[tac.interval] = tacommon.get_interval(task, 3600)
            task[tac.sourcetype] = task.get(tac.sourcetype, "aws:config")
            tasks.append(task)
        return tasks
