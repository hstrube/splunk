import time
import traceback
import json
import re
import calendar

import boto.sqs

import splunktalib.common.util as scutil
import config_mod.aws_config_consts as acc
import taaws.ta_aws_consts as tac
import taaws.ta_aws_common as tacommon
import s3_mod.s3_key_reader as skr
import s3_mod.aws_s3_consts as asc
from splunktalib.common import log

logger = log.Logs().get_logger(acc.config_log)


def is_configuration_change_notification(json_msg):
    return json_msg["messageType"] == "ConfigurationItemChangeNotification"


def is_configuration_history_delivery_completed_message(json_msg):
    return (json_msg["messageType"] == "ConfigurationHistoryDeliveryCompleted"
            and json_msg.get("s3ObjectKey")
            and json_msg.get("s3Bucket"))


def is_configuration_snaphost_delivery_completed_message(json_msg):
    return (json_msg["messageType"] == "ConfigurationSnapshotDeliveryCompleted"
            and json_msg.get("s3ObjectKey")
            and json_msg.get("s3Bucket"))


def delete_message_batch(sqs_queue, messages):
    if not messages:
        return

    logger.info("Delete {0} messages from SQS".format(len(messages)))
    res = sqs_queue.delete_message_batch(messages)
    if res.errors:
        logger.error("Failed to delete messages, errors=%s", res.errors)
    del messages[:]


def get_event_time(config_item):
    cap_time = config_item["configurationItemCaptureTime"]
    cap_time = cap_time.replace("Z", "GMT")
    cap_time = time.strptime(cap_time, "%Y-%m-%dT%H:%M:%S.%f%Z")
    event_time = int(calendar.timegm(cap_time))
    return event_time


class ConfigDataLoader(object):
    _key_pattern = re.compile(r'\/AWSLogs\/\d+\/Config\/(?P<region>[^_\/]+)\/')

    _evt_fmt = ("<stream><event>"
                "<time>{time}</time>"
                "<source>{source}</source>"
                "<sourcetype>{sourcetype}</sourcetype>"
                "<index>{index}</index>"
                "<data>{data}</data>"
                "</event></stream>")

    @classmethod
    def _extract_region_from_key(cls, key):
        match = cls._key_pattern.search(key)
        if match:
            return match.group('region')
        return None

    def __init__(self, config):
        """
        :config: dict object
        {
        "interval": 30,
        "source": xxx,
        "sourcetype": yyy,
        "index": zzz,
        }
        """

        self._config = config
        self._stopped = False

    def __call__(self):
        self.index_data()

    def index_data(self):
        logger.info("Start collecting config from sqs=%s, region=%s",
                    self._config[acc.sqs_queue], self._config[tac.aws_region])
        try:
            self._do_index_data()
        except Exception:
            logger.error("Failed to collect config from sqs=%s, region=%s, "
                         "error=%s", self._config[acc.sqs_queue],
                         self._config[tac.aws_region], traceback.format_exc())
        logger.info("End of collecting config from sqs=%s, region=%s",
                    self._config[acc.sqs_queue], self._config[tac.aws_region])

    def _do_index_data(self):
        sqs_queue = self._get_sqs_queue()
        if sqs_queue is None:
            return

        sqs_queue.set_message_class(boto.sqs.message.RawMessage)
        processed_messages = []
        while not self.stopped():
            # num_messages=10 was chosen based on aws pricing faq.
            # see request batch pricing: http://aws.amazon.com/sqs/pricing/
            messages = sqs_queue.get_messages(
                num_messages=10, visibility_timeout=20, wait_time_seconds=20)
            if not messages:
                break

            for message in messages:
                if self.stopped():
                    break

                valid, json_msg = self._unpack_config_message(message)
                if not valid:
                    logger.error("Invalid Config message in sqs queue")
                    processed_messages.append(message)
                    continue

                try:
                    self._handle_message(message, json_msg)
                except Exception:
                    logger.error("Failed to process message")
                processed_messages.append(message)

            delete_message_batch(sqs_queue, processed_messages)

    def _unpack_config_message(self, message):
        try:
            envelope = json.loads(message.get_body())
        except Exception:
            logger.error("Invalid Config message in sqs queue")
            return False, message

        if not isinstance(envelope, dict):
            return False, envelope

        required_keys = ("Type", "MessageId", "TopicArn", "Message")
        if not all(key in envelope for key in required_keys):
            return False, envelope

        if not isinstance(envelope["Message"], basestring):
            return False, envelope

        try:
            msg = json.loads(envelope["Message"])
            if not isinstance(msg, dict):
                return False, envelope
        except ValueError:
            return False, envelope

        if "messageType" not in msg or not msg["messageType"]:
            return False, envelope

        return True, msg

    def _handle_message(self, message, json_msg):
        if is_configuration_change_notification(json_msg):
            logger.debug("Consuming Config change data")
            event_time = get_event_time(json_msg["configurationItem"])
            sourcetype = "{}:notification".format(self._config[tac.sourcetype])
            event = self._evt_fmt.format(
                time=event_time, source=self._config[acc.sqs_queue],
                sourcetype=sourcetype,
                index=self._config[tac.index],
                data=scutil.escape_cdata(json.dumps(json_msg)),
            )
            self._config[tac.data_loader_mgr].write_events(event)
        elif (is_configuration_history_delivery_completed_message(json_msg) or
              is_configuration_snaphost_delivery_completed_message(json_msg)):
            self._process_config_in_s3(message, json_msg)

    def _read_s3_key(self, json_msg):
        key = json_msg["s3ObjectKey"]
        region = self._extract_region_from_key(key)
        config = dict(self._config)
        config[asc.bucket_name] = json_msg["s3Bucket"]
        config[asc.key] = key
        config[tac.region] = region
        key_reader = skr.create_s3_key_reader(config, logger)
        return key_reader.read_all()

    def _process_config_in_s3(self, message, json_msg):
        """Extract events from AWS Config S3 logs referenced in
        SNS notifications."""

        logger.info("Consuming Config history change data in S3 bucket with \
                    s3Bucket {0} s3ObjectKey {1}.".format(json_msg["s3Bucket"],
                                                          json_msg["s3ObjectKey"]))
        data = self._read_s3_key(json_msg)
        json_payload = json.loads(data)
        items = json_payload.get("configurationItems", [])
        source = "s3://{bucket_name}/{key_name}".format(
            bucket_name=json_msg["s3Bucket"], key_name=json_msg["s3ObjectKey"])
        logger.info("Processing %s configurationItems in %s",
                    len(items), source)

        events = []
        for item in items:
            event_time = get_event_time(item)
            event = self._evt_fmt.format(
                time=event_time, source=source,
                sourcetype=self._config[tac.sourcetype],
                index=self._config[tac.index],
                data=json.dumps(item))
            events.append(event)
        self._config[tac.data_loader_mgr].write_events("".join(events))

    def _get_sqs_queue(self):
        sqs_conn = tacommon.connect_service_to_region(
            boto.sqs.connect_to_region, self._config)

        sqs_queue = sqs_conn.get_queue(self._config[acc.sqs_queue])
        if sqs_queue is None:
            logger.error("Queue name=%s is invalid or AWS credential=%s is "
                         "invalid", self._config[acc.sqs_queue],
                         self._config[tac.aws_account])
        return sqs_queue

    def get_interval(self):
        return self._config[tac.interval]

    def stop(self):
        self._stopped = True

    def stopped(self):
        return self._stopped or self._config[tac.data_loader_mgr].stopped()

    def get_props(self):
        return self._config
