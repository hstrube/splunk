#!/usr/bin/python

"""
This is the main entry point for AWS Config TA
"""

import time
import traceback

import config_mod.aws_config_consts as accon
from splunktalib.common import log

# logger should be init at the very begging of everything
logger = log.Logs(accon.config_log_ns).get_logger(accon.config_log)


import splunktalib.data_loader_mgr as dlm
import taaws.ta_aws_consts as tac
import taaws.ta_aws_common as tacommon
import config_mod.aws_config_conf as acc
import config_mod.aws_config_data_loader as acdl


def print_scheme():
    import sys

    scheme = """<scheme>
    <title>AWS Config</title>
    <description>Collect notifications produced by AWS Config.The feature must be enabled and its SNS topic must be subscribed to an SQS queue.</description>
    <use_external_validation>true</use_external_validation>
    <use_single_instance>true</use_single_instance>
    <streaming_mode>xml</streaming_mode>
    <endpoint>
        <args>
            <arg name="name">
                <title>Name</title>
                <description>Choose an ID or nickname for this configuration</description>
                <required_on_edit>false</required_on_edit>
                <required_on_create>true</required_on_create>
            </arg>
            <arg name="aws_account">
                <title>AWS Account</title>
                <description>AWS account</description>
                <required_on_edit>true</required_on_edit>
                <required_on_create>true</required_on_create>
            </arg>
            <arg name="aws_region">
                <title>SQS Queue Region</title>
                <description>Name of the AWS region in which the notification queue is located. Regions should be entered as e.g., us-east-1, us-west-2, eu-west-1, ap-southeast-1, etc.</description>
                <required_on_edit>true</required_on_edit>
                <required_on_create>true</required_on_create>
            </arg>
            <arg name="sqs_queue">
                <title>SQS Queue Name</title>
                <description>Name of queue to which notifications of AWS Config are sent. The queue should be subscribed to the AWS Config SNS topic.</description>
                <required_on_edit>true</required_on_edit>
                <required_on_create>true</required_on_create>
            </arg>
            <arg name="polling_interval">
                <title>Interval</title>
                <description>Data collection interval</description>
                <data_type>number</data_type>
                <required_on_edit>false</required_on_edit>
                <required_on_create>false</required_on_create>
            </arg>
            <arg name="enable_additional_notifications">
                <title>Enable Debug</title>
                <description>Index additional SNS/SQS events to help with troubleshooting.</description>
                <data_type>boolean</data_type>
                <required_on_edit>false</required_on_edit>
                <required_on_create>false</required_on_create>
            </arg>
        </args>
    </endpoint>
    </scheme>"""
    sys.stdout.write(scheme)


def _do_run():
    meta_configs, _, tasks = tacommon.get_configs(
        acc.AWSConfigConf, "aws_config", logger)

    if not tasks:
        logger.info("No data input has been configured, exiting...")
        return

    meta_configs[tac.log_file] = accon.config_log
    loader_mgr = dlm.create_data_loader_mgr(meta_configs)
    tacommon.setup_signal_handler(loader_mgr, logger)
    conf_change_handler = tacommon.get_file_change_handler(loader_mgr, logger)
    conf_monitor = acc.create_conf_monitor(conf_change_handler)
    loader_mgr.add_timer(conf_monitor, time.time(), 10)

    jobs = [acdl.ConfigDataLoader(task) for task in tasks]
    loader_mgr.run(jobs)


def run():
    """
    Main loop. Run this TA forever
    """

    logger.info("Start aws_config")
    try:
        _do_run()
    except Exception:
        logger.error("Failed to collect config data, error=%s",
                     traceback.format_exc())
    logger.info("End aws_config")


def main():
    """
    Main entry point
    """

    tacommon.main(print_scheme, run)


if __name__ == "__main__":
    main()
