import splunk_ta_aws_import_lib_declare
import splunk.admin as admin

from splunktalib.rest_manager import multimodel

import splunk_ta_aws_rh_settings_base


class ConfigRuleLogging(splunk_ta_aws_rh_settings_base.AWSLogging):
    keyMap = {
        'level': 'log_level'
    }


class ConfigRuleSettings(multimodel.MultiModel):
    endpoint = "configs/conf-aws_config_rule"
    modelMap = {
        'logging': ConfigRuleLogging,
    }


if __name__ == "__main__":
    admin.init(multimodel.ResourceHandler(ConfigRuleSettings), admin.CONTEXT_APP_AND_USER)
