import copy
from splunklib.modularinput.event import ET


class SimpleEventWriter(object):
    def __init__(self, lock, dev):
        self._lock = lock
        self._dev = dev

    def write(self, stream):
        text = stream.render()
        with self._lock:
            self._dev.write(text)
            self._dev.flush()


class EventStream(object):
    _VALID_STREAM_KEYS_ = {
        'source',
        'sourcetype',
        'host',
        'index',
        'stanza',
        'unbroken',
    }

    def __init__(self, **kwargs):
        for key in kwargs.iterkeys():
            if key not in self._VALID_STREAM_KEYS_:
                raise ValueError('{} is an invalid key'.format(key))

        self._root = ET.Element('stream')
        self._prototype = ET.Element('event')

        stanza = kwargs.pop('stanza', None)
        if stanza:
            self._prototype.set("stanza", stanza)

        unbroken = kwargs.pop('unbroken', None)
        if unbroken:
            self._prototype.set('unbroken', str(unbroken))

        for key, value in kwargs.iteritems():
            if value is not None:
                element = ET.SubElement(self._prototype, key)
                element.text = value

    def append(self, data, **kwargs):
        event = copy.copy(self._prototype)
        element = ET.SubElement(event, 'data')
        if isinstance(data, str):
            data = data.decode('utf-8', errors='replace')
        element.text = data
        time = kwargs.pop('time', None)
        if time:
            element = ET.SubElement(event, "time")
            element.text = str(time)

        if kwargs.pop('done', None):
            ET.SubElement(event, 'done')

        self._root.append(event)

    def render(self):
        return ET.tostring(self._root)

