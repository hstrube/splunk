from __future__ import absolute_import
import sys
import os
import json
from logging import *
from logging import _srcfile
from logging.handlers import RotatingFileHandler


class RotatingFileHandlerWithLock(RotatingFileHandler):
    def __init__(self, lock, filename, mode='a', max_bytes=0, backup_count=0, encoding=None):
        super(RotatingFileHandlerWithLock, self).__init__(filename, mode, max_bytes, backup_count, encoding, True)
        self.lock = lock

    def emit(self, record):
        with self.lock:
            super(RotatingFileHandlerWithLock, self).emit(record)
            if os.name == 'nt':
                if self.stream:
                    self.stream.close()
                    self.stream = None

    def createLock(self):
        pass

    def acquire(self):
        pass

    def release(self):
        pass


def _log(logger, level, msg, args, exc_info=None, extra=None, **kwargs):
    if _srcfile:
        #IronPython doesn't track Python frames, so findCaller raises an
        #exception on some versions of IronPython. We trap it here so that
        #IronPython can use logging.
        try:
            fn, lno, func = logger.findCaller()
        except ValueError:
            fn, lno, func = "(unknown file)", 0, "(unknown function)"
    else:
        fn, lno, func = "(unknown file)", 0, "(unknown function)"
    if exc_info:
        if not isinstance(exc_info, tuple):
            exc_info = sys.exc_info()
    kwargs['message'] = msg
    msg = dict2str(kwargs)
    record = logger.makeRecord(logger.name, level, fn, lno, msg, args, exc_info, func, extra)
    logger.handle(record)

Logger._log = _log


def dict2str(kv):
    return u', '.join([u'{0}={1}'.format(key, json.dumps(value)) for key, value in sorted(kv.items())])


def setup_null_handler():
    root = getLogger()
    root.addHandler(NullHandler())
