from __future__ import absolute_import
import argparse
import signal
import time
import os
import os.path
import sys
import multiprocessing
import random
from splunksdc import logging
from splunksdc.event import SimpleEventWriter
from splunksdc.checkpoint import LocalKVStore
from splunksdc.utils import FSLock, ParentWatcher
from splunksdc.multitasking import TimerTaskScheduler
from splunksdc.config import ConfigManager

logger = logging.getLogger('splunksdc')


def _parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--scheme", action='store_true')
    parser.add_argument("--validate-arguments", action='store_true')
    parser.add_argument("--server_uri", metavar="SPLUNK_URI")
    parser.add_argument("--stanza", metavar="STANZA")
    parser.add_argument("--app_name", metavar="APP_NAME")
    return parser.parse_args()


def _build_scheme(title, use_single_instance, **kwargs):
    from splunklib.modularinput.scheme import Scheme
    from splunklib.modularinput.argument import Argument

    scheme = Scheme(title)
    scheme.description = kwargs.pop('description', None)
    scheme.use_external_validation = True
    scheme.streaming_mode = Scheme.streaming_mode_xml
    scheme.use_single_instance = use_single_instance
    for name, options in kwargs.pop('arguments', {}).items():
        description = options.pop('description', None)
        validation = options.pop('validation', None)
        data_type = options.pop('data_type', Argument.data_type_string)
        required_on_edit = options.pop('required_on_edit', False)
        required_on_create = options.pop('required_on_create', False)
        title = options.pop('title', None)
        argument = Argument(
            name, description, validation, data_type,
            required_on_edit, required_on_create, title
        )
        scheme.add_argument(argument)

    return scheme


def _render_scheme(stream, scheme):
    from splunklib.modularinput.scheme import ET
    data = ET.tostring(scheme.to_xml())
    stream.write(data)
    stream.flush()
    return 0


def _validate_definition(stream, scheme):
    from splunklib.modularinput.validation_definition import ValidationDefinition
    definition = ValidationDefinition.parse(stream)
    return 0


def _reset_sig_interrupt():
    if os.name == 'posix':
        signal.siginterrupt(signal.SIGINT, False)
        signal.siginterrupt(signal.SIGTERM, False)
        signal.siginterrupt(signal.SIGHUP, False)


def _register_teardown_handler(callback):
    signal.signal(signal.SIGINT, callback)
    signal.signal(signal.SIGTERM, callback)
    if os.name == 'posix':
        signal.signal(signal.SIGHUP, callback)
    if os.name == 'nt':
        signal.signal(signal.SIGBREAK, callback)
    _reset_sig_interrupt()


def _exit(code):
    sys.exit(code)


def _extract_app_name_from_path(path):
    path, name = os.path.split(path)
    path, name = os.path.split(path)
    path, name = os.path.split(path)
    return name


def _extract_modular_name_from_path(path):
    _, name = os.path.split(path)
    return name[:-3]


def run_modular_input(modular_input_factory, **kwargs):
    from splunksdc import context

    modular_input_path = sys.argv[0]
    args = _parse_args()
    app_name = args.app_name
    if not app_name:
        app_name = _extract_app_name_from_path(modular_input_path)

    modular_name = _extract_modular_name_from_path(modular_input_path)

    use_single_instance = kwargs.pop('use_single_instance', True)

    scheme = _build_scheme(app_name, use_single_instance, **kwargs)

    if args.scheme:
        _exit(_render_scheme(sys.stdout, scheme))
    elif args.validate_arguments:
        _exit(_validate_definition(sys.stdin, scheme))

    if args.server_uri:
        context = context.build_context_from_url(args.server_uri, args.stanza)
    else:
        context = context.build_context_from_stream(sys.stdin)

    # To avoid too many data inputs run at the very same time which
    # cause lots resource usage like REST API calls.
    wait_time = random.uniform(0, 5)
    time.sleep(wait_time)

    modular_input = modular_input_factory(app_name, modular_name, context, use_single_instance)

    def _signal_handler(signum, frame):
        modular_input.abort()

    _register_teardown_handler(_signal_handler)
    _exit(modular_input.run())


def _collector_work_procedure(collector, name, params):
    collector.perform(name, params)


class SimpleCollectorV1(object):
    @classmethod
    def main(cls, delegate, **kwargs):
        def factory(app_name, modular_name, context, use_single_instance):
            return cls(app_name, modular_name, context, use_single_instance, delegate)

        run_modular_input(factory, **kwargs)

    def __init__(self, app_name, modular_name, context, use_single_instance, delegate):
        self._app_name = app_name
        self._modular_name = modular_name
        self._context = context
        self._delegate = delegate
        self._stdout_lock = multiprocessing.Lock()
        self._logging_lock = multiprocessing.Lock()
        self._logging_level = logging.INFO
        self._use_single_instance = use_single_instance
        self._aborted = multiprocessing.Event()
        self._parent_watcher = ParentWatcher()

    def _setup_root_logger(self):
        app_name = self._app_name
        modular_name = self._modular_name
        app_name = app_name.replace('-', '_')
        app_name = app_name.lower()

        if modular_name.startswith(app_name):
            filename = modular_name
        else:
            filename = app_name + '_' + modular_name

        if self._use_single_instance is False:
            stanza = self.inputs()[0]
            filename += '_' + stanza.name

        filename += '.log'
        filename = os.path.join(self._context.log_dir, filename)
        file_handler = logging.RotatingFileHandlerWithLock(
            self._logging_lock, filename, mode="a",
            max_bytes=1024 * 1024 * 25, backup_count=5
        )
        formatter = logging.Formatter(
            '%(asctime)s %(levelname)s pid=%(process)d tid=%(threadName)s '
            'logger=%(name)s caller=%(filename)s:%(funcName)s:%(lineno)d '
            '%(message)s'
        )
        file_handler.setFormatter(formatter)
        root = self.get_root_logger()
        root.addHandler(file_handler)
        root.setLevel(self._logging_level)
        return

    def _sylock(self):
        filename = ''
        if self._use_single_instance is False:
            stanza = self.inputs()[0]
            filename += stanza.name

        filename += '.lock'
        filename = os.path.join(self._context.checkpoint_dir, filename)
        return FSLock.open(filename)

    def abort(self):
        # logger.info('modular input aborted')
        self._aborted.set()

    def run(self):
        check_interval = random.randint(300, 600)
        with self._sylock():
            self._setup_root_logger()
            self.get_root_logger().setLevel(logging.INFO)

            logger.info('modular input started')
            scheduler = self._create_scheduler()
            config = self.create_config_service()
            self._delegate.prepare(self, config, scheduler)
            while not self.is_aborted():
                scheduler.run()
                if config.expired(check_interval):
                    logger.info('config changed')
                    break
                if scheduler.idle():
                    logger.info('task not found.')
                    break

            logger.info('modular input exited')
            return 0

    def perform(self, name, params):
        if os.name == 'nt':
            self._setup_root_logger()

        logger.info('task started', name=name)
        workspace = self._context.checkpoint_dir
        fullname = os.path.join(workspace, name + '.ckpt')
        checkpoint = LocalKVStore.open_always(fullname)
        portal = SimpleEventWriter(self._stdout_lock, sys.stdout)
        self._delegate.perform(self, portal, checkpoint, name, params)
        checkpoint.close(True)

        logger.info('task finished', name=name)

    def create_splunk_service(self):
        from splunklib.client import Service

        context = self._context
        service = Service(
            scheme=context.server_scheme,
            host=context.server_host,
            port=context.server_port,
            token=context.token,
            owner='nobody',
            app=self._app_name,
        )
        return service

    def create_config_service(self):
        splunk = self.create_splunk_service()
        config = ConfigManager(splunk)
        return config

    def _create_worker(self, name, params):
        fullname = os.path.join(self.workspace(), name)
        folder = os.path.dirname(fullname)
        if not os.path.exists(folder):
            os.makedirs(folder)

        args = (
            self,
            name,
            params
        )
        worker = multiprocessing.Process(
            target=_collector_work_procedure,
            args=args
        )
        worker.daemon = True
        worker.start()
        return worker

    def _worker_has_done(self, worker):
        worker.join(0)
        return not worker.is_alive()

    def _destroy_worker(self, worker):
        worker.terminate()

    def _create_scheduler(self):
        return TimerTaskScheduler(
            self._create_worker,
            self._worker_has_done,
            self._destroy_worker
        )

    def workspace(self):
        return self._context.checkpoint_dir

    def set_log_level(self, level):
        self._logging_level = level
        root = self.get_root_logger()
        root.setLevel(self._logging_level)

    def inputs(self):
        return self._context.inputs

    def is_aborted(self):
        if not self._parent_watcher.is_alive():
            return True
        return self._aborted.is_set()

    @classmethod
    def get_root_logger(cls):
        return logging.getLogger()
