import hashlib
import json
import time
from splunksdc import logging


logger = logging.getLogger('splunksdc')


class ConfigManager(object):
    @classmethod
    def digest(cls, stanzas):
        text = json.dumps(
            stanzas,
            separators=(',', ': '),
            sort_keys=True
        )
        sha256 = hashlib.sha256()
        sha256.update(text)
        return sha256.digest()

    def __init__(self, service):
        self._service = service
        self._confs = service.confs
        self._revisions = dict()
        self._hooks = dict()
        self._last_check = time.time()

    def reload(self, name):
        endpoint = 'configs/conf-{}/_reload'.format(name)
        self._service.get(endpoint)

    def load(self, endpoint):
        endpoint = self._normalize(endpoint)
        name, stanza = endpoint
        self.reload(name)
        content = dict()
        if name in self._confs:
            conf = self._confs[name]
            if stanza is None:
                content.update({item.name: item.content for item in conf})
            elif stanza in conf:
                content.update(conf[stanza].content)
        hook = self._hooks.get(endpoint)
        if hook:
            content = hook.decode(self._service, content)
        return content

    def expired(self, interval=30):
        now = time.time()
        if now - self._last_check < interval:
            return False

        self._last_check = now
        for endpoint, revision in self._revisions.items():
            content = self.load(endpoint)
            digest = self.digest(content)
            if digest != revision:
                name, stanza = self._normalize(endpoint)
                logger.info('config changed', name=name, stanza=stanza)
                return True
        return False

    def watch(self, endpoint, content):
        endpoint = self._normalize(endpoint)
        self._revisions[endpoint] = self.digest(content)

    def set_hook(self, endpoint, hook):
        endpoint = self._normalize(endpoint)
        self._hooks[endpoint] = hook

    @classmethod
    def _normalize(cls, endpoint):
        if not isinstance(endpoint, tuple):
            return endpoint, None
        return endpoint
