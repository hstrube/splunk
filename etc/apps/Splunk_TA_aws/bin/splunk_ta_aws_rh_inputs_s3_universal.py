
import splunk_ta_aws_import_lib_declare


import re
import json
import logging
import collections
from urllib import quote

import splunk.admin as admin

from solnlib.splunkenv import get_splunkd_uri

from splunktalib.common import util as scutil
from splunktalib.rest import splunkd_request
from splunktalib.rest_manager import base, util
from splunktalib.rest_manager.error_ctl import RestHandlerError as RH_Err

from splunk_ta_aws_rh_inputs_s3 import S3Input
from splunk_ta_aws_rh_inputs_logs import LogsInput


class S3UniversalInputHandler(base.BaseRestHandler):

    entry_postfix = ' (Incremental)'

    endpoints_incremental = {
        False: 'splunk_ta_aws/inputs/s3',
        True: 'splunk_ta_aws/inputs/splunk_ta_aws_inputs_logs',
    }

    def __init__(self, *args, **kwargs):
        base.BaseRestHandler.__init__(self, *args, **kwargs)
        self.is_incremental = False
        self.parse()

    def handleList(self, confInfo):
        if self.callerArgs.id:
            self.list_one(confInfo)
        else:
            self.list_all(confInfo)

    def handleCreate(self, confInfo):
        cont = self.handle_incremental('POST')
        self.export(cont, self.is_incremental, confInfo)

    def handleEdit(self, confInfo):
        if 'incremental' in self.callerArgs.data.keys():
            incremental = self.callerArgs.data['incremental'][0]
            if scutil.is_true(incremental) != self.is_incremental:
                RH_Err.ctl(
                    400,
                    msgx='Field "incremental" is not allowed to change',
                )
        self.handle_incremental('POST')

    def handleRemove(self, confInfo):
        self.handle_incremental('DELETE')

    def handleCustom(self, confInfo, **params):
        if self.customAction not in ['enable', 'disable']:
            RH_Err.ctl(1101, 'action=%s' % self.customAction,
                       logLevel=logging.INFO)
        self.handle_incremental('POST')

    def handle_incremental(self, method):
        url = self.make_url(self.is_incremental)
        return self.request(url, method=method,
                            data=self.filter_data(self.callerArgs.data))

    def request(self, splunkd_uri, method="GET", data=None):
        resp, cont = splunkd_request(
            splunkd_uri, self.getSessionKey(), method=method, data=data)

        if resp is None:
            raise Exception('Failed to request to url=%s' % splunkd_uri)
        if resp.status not in (200, 201, '200', '201'):
            pattern = r'[\s\S]+output:\s+\'(?P<error>REST\sERROR\[\d+\]:' \
                      r'[\s\S]+)\'\.\s+See splunkd\.log for stderr output\."\}'
            match = re.match(pattern, cont)
            error = match.groupdict()['error'] if match else cont
            print error
            raise BaseException()
        return cont

    def make_entry_name(self, name, increment):
        postfix = self.entry_postfix if scutil.is_true(increment) else ''
        return name + postfix

    def list_one(self, conf_info):
        url = self.make_url(self.is_incremental)
        try:
            cont = self.request(url)
        except Exception as exc:
            RH_Err.ctl(500, msgx=exc)
            return
        find = self.export(cont, self.is_incremental, conf_info)
        if not find:
            RH_Err.ctl(404)

    def list_all(self, conf_info):
        for incremental in self.endpoints_incremental:
            url = self.make_url(incremental)
            try:
                cont = self.request(url)
            except Exception:
                continue
            self.export(cont, incremental, conf_info)

    def parse(self):
        if not self.callerArgs.id:
            return
        if self.requestedAction == admin.ACTION_CREATE:
            if self.callerArgs.id.find('(') > -1 or \
                            self.callerArgs.id.find(')') > -1:
                RH_Err.ctl(400,
                           msgx='"(" and ")" are not allowed in S3 input name')
            self.is_incremental = \
                scutil.is_true(self.callerArgs.data['incremental'][0])
            return

        self.is_incremental = False
        if self.callerArgs.id.endswith(self.entry_postfix):
            self.is_incremental = True
            self.callerArgs.id = self.callerArgs.id[:-len(self.entry_postfix)]

    def export(self, content, incremental, conf_info):
        user, app = self.user_app()
        res = json.loads(content)
        for entry in res['entry']:
            ent = entry['content']
            ent['name'] = self.make_entry_name(entry['name'], incremental)
            ent['incremental'] = 1 if incremental else 0
            ent[admin.EAI_ENTRY_ACL] = entry['acl']
            if incremental:
                ent['logs_sourcetype'] = ent.get('sourcetype', '')
            util.makeConfItem(ent['name'], ent, conf_info, user=user, app=app)
        return len(res)

    def filter_data(self, data):
        if self.is_incremental:
            data['sourcetype'] = data.get('logs_sourcetype', '')
            fields = set(LogsInput.requiredArgs) | set(LogsInput.optionalArgs)
        else:
            fields = set(S3Input.requiredArgs) | set(S3Input.optionalArgs)
        data = {key: val[0] or '' for key, val in data.iteritems()
                if key in fields and val}
        if self.requestedAction == admin.ACTION_CREATE:
            data['name'] = self.callerArgs.id
        return data

    def make_url(self, incremental):
        user, app = self.user_app()
        entry = '' if self.requestedAction == admin.ACTION_CREATE or \
                self.callerArgs.id is None else \
            '/' + quote(self.callerArgs.id.encode('utf-8'), safe='')
        action = '' if self.customAction is None else \
            '/' + quote(self.customAction.encode('utf-8'), safe='')
        url_temp = '{mgmt_uri}/servicesNS/{user}/{app}/' \
                   '{endpoint}{entry}{action}?output_mode=json&count=0'
        return url_temp.format(
            mgmt_uri=get_splunkd_uri(), user=user, app=app,
            endpoint=self.endpoints_incremental[incremental],
            entry=entry, action=action
        )


class S3UniversalInput(base.BaseModel):
    """
    AWS S3 Universal Inputs Model.
    """
    endpoint = 's3_universal'
    requiredArgs = {'incremental'}
    optionalArgs = set(S3Input.requiredArgs) | set(S3Input.optionalArgs) | \
        set(LogsInput.requiredArgs) | set(LogsInput.optionalArgs) | \
        {'logs_sourcetype', }

    cap4disable = 'ta_aws_custom_action_disable'
    cap4enable = 'ta_aws_custom_action_enable'


if __name__ == "__main__":
    admin.init(
        base.ResourceHandler(
            S3UniversalInput,
            handler=S3UniversalInputHandler
        ),
        admin.CONTEXT_APP_AND_USER,
    )
