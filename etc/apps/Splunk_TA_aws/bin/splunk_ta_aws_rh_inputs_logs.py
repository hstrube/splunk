
import splunk_ta_aws_import_lib_declare

import re
import datetime
from urllib import quote

from splunk import admin

from solnlib.splunkenv import get_splunkd_uri

from splunktalib.common.util import is_true

from splunktalib.rest_manager import base, datainput, normaliser, validator
from splunktalib.rest_manager.error_ctl import RestHandlerError as RH_Err

from taaws.ta_aws_common import load_config, make_splunk_endpoint
import s3_mod.aws_s3_consts as asc
from s3_mod.aws_s3_common import get_region_for_bucketname

from splunk_ta_aws_logs import delete_data_input


def validate_log_name_format(data):
    if data['log_type'][0] == 'cloudfront:accesslogs' and \
            not data.get('log_name_format'):
        raise validator.ValidationFailed(
            'Field "log_name_format" is required for input log_type')


def get_7_days_ago():
    today = datetime.date.today()
    return str(today - datetime.timedelta(days=7))


class LogsInputHandler(datainput.DataInputHandler):

    def handleCreate(self, confInfo):
        if self.callerArgs.data['sourcetype'][0] is None:
            RH_Err.ctl(400, '"sourcetype" is required.')
        validate_log_name_format(self.callerArgs.data)
        datainput.DataInputHandler.handleCreate(self, confInfo)

    def handleEdit(self, confInfo):
        if self.callerArgs.data['sourcetype'][0] is None:
            RH_Err.ctl(400, '"sourcetype" is required.')
        datainput.DataInputHandler.handleEdit(self, confInfo)

    def handleRemove(self, confInfo):
        datainput.DataInputHandler.handleRemove(self, confInfo)
        try:
            delete_data_input(self.callerArgs.id)
        except Exception:
            RH_Err.ctl(500, 'Failed to delete checkpoint')

    def encode(self, args, setDefault=True):
        args = datainput.DataInputHandler.encode(self, args, setDefault)
        if 'log_name_format' in args and args['log_name_format'][0]:
            args['log_name_format'][0] += '.%Y-%m-%d-'
        if 'bucket_name' in args and args['bucket_name'][0]:
            args['bucket_region'] = self._get_bucket(args)
        return args

    def decode(self, name, ent):
        ent = datainput.DataInputHandler.decode(self, name, ent)
        if ent.get('log_name_format', '').endswith('.%Y-%m-%d-'):
            ent['log_name_format'] = ent['log_name_format'][:-len('.%Y-%m-%d-')]
        return ent

    def _get_bucket(self, data):
        config = self._load_config(data)
        host_name = data[asc.host_name][0]
        config[asc.host_name] = host_name
        config['bucket_name'] = data['bucket_name'][0]
        if host_name == asc.default_host:
            return get_region_for_bucketname(config)
        else:
            pattern = r's3[.-]([\w-]+)\.amazonaws.com'
            m = re.search(pattern, config[asc.host_name])
            return m.group(1) if m else 'us-east-1'

    def _load_config(self, data):
        user, app = self.user_app()
        account_name = data['aws_account'][0]
        account_name = quote(account_name.encode('utf-8'), safe='')
        account_endpoint = 'splunk_ta_aws/settings/account/' + account_name
        endpoints = [
            {
                'endpoint': account_endpoint,
                'label': 'AWS Account',
            },
            {
                'endpoint': 'splunk_ta_aws/settings/proxy',
                'label': 'Proxy Setting',
            },
        ]
        config = {}
        splunkd_uri = get_splunkd_uri()
        for ep in endpoints:
            url = make_splunk_endpoint(splunkd_uri, ep['endpoint'], user, app)
            res = load_config(url, self.getSessionKey(), ep['label'])
            config.update(res.popitem()[1])
        if is_true(config['disabled']) or config.get('host') is None:
            config['proxy_hostname'] = None
            config['proxy_port'] = None
            config['proxy_username'] = None
            config['proxy_password'] = None
        else:
            config['proxy_hostname'] = config.get('host')
            config['proxy_port'] = config.get('port')
            config['proxy_username'] = config.get('username')
            config['proxy_password'] = config.get('password')
        return config


class LogsInput(datainput.DataInputModel):
    """
    AWS CloudTrail Inputs Model Based on S3.
    """
    dataInputName = 'splunk_ta_aws_logs'
    requiredArgs = {
        'aws_account',
        'log_type',
        'sourcetype',
        'host_name',
        'bucket_name',
    }
    optionalArgs = {
        'aws_iam_role',
        'bucket_region',
        'log_file_prefix',
        'log_start_date',
        'log_name_format',
        'partitions',
        'index',
        'interval',
    }
    defaultVals = {
        'log_start_date': get_7_days_ago(),
        'index': 'default',
        'interval': '1800',
        'disabled': '0',
    }
    validators = {
        'log_type': validator.Enum(
            (
                'cloudtrail',
                's3:accesslogs',
                'cloudfront:accesslogs',
                'elb:accesslogs',
             )
        ),
        'log_start_date': validator.AllOf(
            validator.Pattern(r'\d{4}-\d{2}-\d{2}'),
            validator.Datetime('%Y-%m-%d'),
        ),
        'bucket_name': validator.RequiresIf(
            fields=('aws_account', 'host_name')),
    }
    normalisers = {
       'disabled': normaliser.Boolean(),
    }
    cap4disable = 'ta_aws_custom_action_disable'
    cap4enable = 'ta_aws_custom_action_enable'


if __name__ == '__main__':
    admin.init(
        base.ResourceHandler(LogsInput, handler=LogsInputHandler),
        admin.CONTEXT_APP_AND_USER,
    )
