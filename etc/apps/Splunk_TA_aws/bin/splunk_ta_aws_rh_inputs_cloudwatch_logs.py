import splunk_ta_aws_import_lib_declare
import os
import sys
import re


import splunk.admin as admin

from splunktalib.rest_manager import base,normaliser


class CloudWatchLogs(base.BaseModel):
    '''AWS CloudWatch Logs Inputs Model.
    '''
    endpoint        = "configs/conf-aws_cloudwatch_logs_tasks"
    requiredArgs   = ['aws_account', 'aws_region', 'groups']
    optionalArgs   = ['delay', 'only_after', 'stream_matcher', 'interval', 'sourcetype', 'host', 'index']
    defaultVals    = {
                       'only_after': '1970-01-01T00:00:00',
                       'stream_matcher': '.*',
                       'delay': '1800',
                       'interval': '600',
                       'sourcetype': 'aws:cloudwatchlogs',
                       'index': 'default',
                       }
    normalisers     = {
                       'disabled': normaliser.Boolean(),
                       }
    keyMap          = {
                      'aws_account': 'account',
                      'aws_region': 'region'
                      }
    cap4disable = 'ta_aws_custom_action_disable'
    cap4enable = 'ta_aws_custom_action_enable'


if __name__ == "__main__":
    admin.init(base.ResourceHandler(CloudWatchLogs), admin.CONTEXT_APP_AND_USER)
