
import splunk_ta_aws_import_lib_declare

import splunk.admin as admin

from splunktalib.rest_manager import base, datainput, normaliser, validator


class MetricsValidator(validator.JsonString):

    def validate(self, value, data):
        if value == '.*':
            return True
        else:
            return super(MetricsValidator, self).validate(value, data)


class CloudWatch(datainput.DataInputModel):
    """
    AWS CloudWatch Inputs Model.
    """
    dataInputName = "aws_cloudwatch"
    requiredArgs = [
        'aws_account',
        'aws_regions',
        'metric_namespace',
        'metric_names',
        'metric_dimensions',
        'statistics',
        'period',
        'polling_interval',
    ]
    optionalArgs = ['aws_iam_role', 'sourcetype', 'host', 'index']
    defaultVals = {
        'sourcetype': 'aws:cloudwatch',
    }
    validators = {
        'metric_dimensions': validator.JsonString(),
        'metric_names': MetricsValidator(),
    }
    normalisers = {
        'disabled': normaliser.Boolean(),
    }
    keyMap = {
        'aws_regions': 'aws_region',
    }
    cap4disable = 'ta_aws_custom_action_disable'
    cap4enable = 'ta_aws_custom_action_enable'


if __name__ == "__main__":
    admin.init(
        base.ResourceHandler(CloudWatch, handler=datainput.DataInputHandler),
        admin.CONTEXT_APP_AND_USER,
    )
