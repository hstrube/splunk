
import splunk_ta_aws_import_lib_declare

import splunk.admin as admin
from splunktalib.rest_manager import multimodel
import splunk_ta_aws_rh_settings_base


class KinesisLogging(splunk_ta_aws_rh_settings_base.AWSLogging):
    keyMap = {
        'level': 'log_level'
    }


class KinesisSettings(multimodel.MultiModel):
    endpoint = "configs/conf-aws_kinesis"
    modelMap = {
        'logging': KinesisLogging,
    }


if __name__ == "__main__":
    admin.init(multimodel.ResourceHandler(KinesisSettings), admin.CONTEXT_APP_AND_USER)
