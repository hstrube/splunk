import splunk_ta_aws_import_lib_declare
import os
import sys
import re

import logging

import splunk.admin as admin

from splunktalib.rest_manager import util,error_ctl

from taaws.proxy_conf import ProxyManager

KEY_NAMESPACE = util.getBaseAppName()
KEY_OWNER = '-'

AWS_PROXY = 'aws_proxy'

POSSIBLE_KEYS=('host', 'port', 'username', 'password')

class ProxyRestHandler(admin.MConfigHandler):
    """Manage AWS Accounts in Splunk_TA_aws add-on.
    """
   
    def __init__(self, scriptMode, ctxInfo):
        admin.MConfigHandler.__init__(self, scriptMode, ctxInfo)
        
        if self.callerArgs.id and self.callerArgs.id!='aws_proxy':
            error_ctl.RestHandlerError.ctl(1202, msgx='aws_proxy', logLevel=logging.INFO)

    def setup(self):
        if self.requestedAction in (admin.ACTION_CREATE, admin.ACTION_EDIT):
            for arg in POSSIBLE_KEYS:
                self.supportedArgs.addReqArg(arg)
            self.supportedArgs.addOptArg('disabled')
        return
    
    def handleRemove(self, confInfo):
        try:
            self.update('', False)
        except Exception as exc:
            error_ctl.RestHandlerError.ctl(400, msgx=exc, logLevel=logging.INFO)

    def handleEdit(self, confInfo):
        try:
            args = self.validate(self.callerArgs.data)
            proxyStr = '%s:%s@%s:%s' % (args['username'][0], args['password'][0], args['host'][0], args['port'][0])
            if 'disabled' in args:
                enable = True if args['disabled'][0]=='0' else False
            else:
                proxy = self.get()
                enable = True if (proxy and proxy.get_enable()) else False
            self.update(proxyStr, enable)
        except Exception as exc:
            error_ctl.RestHandlerError.ctl(400, msgx=exc, logLevel=logging.INFO)
    
    def handleList(self, confInfo):
        try:
            proxy = self.get()
            if not proxy:
                confInfo[AWS_PROXY].append('disabled','1')
                return
            m = re.match('^(?P<username>\S*):(?P<password>\S*)@(?P<host>\S+):(?P<port>\d+$)',proxy.get_proxy())
            if not m:
                confInfo[AWS_PROXY].append('disabled','1')
                return
            
            groupDict=m.groupdict()
            confInfo[AWS_PROXY].append('username', groupDict['username'])
            confInfo[AWS_PROXY].append('password', groupDict['password'])
            confInfo[AWS_PROXY].append('host', groupDict['host'])
            confInfo[AWS_PROXY].append('port', groupDict['port'])
            confInfo[AWS_PROXY].append('disabled', proxy.get_enable() and '0' or '1')
        except Exception as exc:
            error_ctl.RestHandlerError.ctl(400, msgx=exc, logLevel=logging.INFO)
        return
    
    def handleCustom(self, confInfo, **params):
        if self.customAction in ['acl']:
            return self.handleACL(confInfo)

        if self.customAction == 'disable':
            self.handleDisable(confInfo)
        elif self.customAction == 'enable':
            self.handleEnable(confInfo)
        else:
            error_ctl.RestHandlerError.ctl(1101, msgx='action=%s' % self.customAction, logLevel=logging.INFO)
    
    def handleDisable(self, confInfo):
        try:
            proxy = self.get()
            self.update(proxy.get_proxy(), False)
        except Exception as exc:
            error_ctl.RestHandlerError.ctl(400, msgx=exc, logLevel=logging.INFO)
        return
    
    def handleEnable(self, confInfo):
        try:
            proxy = self.get()
            self.update(proxy.get_proxy(), True)
        except Exception as exc:
            error_ctl.RestHandlerError.ctl(400, msgx=exc, logLevel=logging.INFO)
        return
    
    def validate(self, args):
        if 'disabled' in args and args['disabled'][0] not in ('0', '1'):
            error_ctl.RestHandlerError.ctl(1100, msgx='disabled={}'.format(args['disabled'][0]), logLevel=logging.INFO)
        return args
    
    def get(self):
        pm  = ProxyManager(self.getSessionKey())
        proxy = pm.get_proxy()
        return proxy
    
    def update(self, proxy, enable):
        try:
            pm = ProxyManager(self.getSessionKey())
            pm.set(proxy,str(enable).lower())
        except Exception as exc:
            error_ctl.RestHandlerError.ctl(400, msgx=exc, logLevel=logging.INFO)
    
if __name__ == "__main__":
    admin.init(ProxyRestHandler, admin.CONTEXT_NONE)
