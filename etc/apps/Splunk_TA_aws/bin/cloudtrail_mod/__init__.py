
# This import is required to using k-v logging
from splunksdc import logging

from splunklib import modularinput as smi

from splunktalib.common import log as sclog

from taaws.ta_aws_consts import splunk_ta_aws

modular_name = 'splunk_ta_aws_cloudtrail'

logger = sclog.Logs(splunk_ta_aws).get_logger('cloudtrail')

modular_args = {
    'name': {
        'title': 'Name',
        'description': 'Choose an ID or nickname for this configuration',
        'required_on_create': True
    },
    'aws_account': {
        'title': 'AWS Account',
        'description': 'AWS account',
        'required_on_create': True,
        'required_on_edit': True
    },
    'aws_region': {
        'title': 'SQS Queue Region',
        'description':
            'Name of the AWS region in which the notification queue is '
            'located. Regions should be entered as e.g., us-east-1, '
            'us-west-2, eu-west-1, ap-southeast-1, etc.',
        'required_on_create': True,
        'required_on_edit': True
    },
    'sqs_queue': {
        'title': 'SQS Queue Name',
        'description':
            'Name of queue to which notifications of new CloudTrail logs are '
            'sent. CloudTrail logging should be configured to publish to an '
            'SNS topic. The queue should be subscribed to the topics that '
            'notify of the desired logs. Note that multiple topics from '
            'different regions can publish to a single queue if desired.',
        'required_on_create': True,
        'required_on_edit': True
    },
    'exclude_describe_events': {
        'title': 'Exclude \'Describe*\' events',
        'description':
            'Do not index \'Describe\' events. These events typically '
            'constitute a high volume of calls, and indicate read-only '
            'requests for information.',
        'data_type': smi.Argument.data_type_boolean,
        'required_on_create': False
    },
    'remove_files_when_done': {
        'title': 'Remove log files when done',
        'description':
            'Delete log files from the S3 bucket once they have been read '
            'and sent to the Splunk index',
        'data_type': smi.Argument.data_type_boolean,
        'required_on_create': False
    },
    'blacklist': {
        'title': 'Blacklist for Describe events',
        'description':
            'PCRE regex for specifying event names to be excluded. '
            'Leave blank to use the default set of read-only event names',
        'required_on_create': False
    },
    'excluded_events_index': {
        'title': 'Excluded events index',
        'description':
            'Optional index in which to write the excluded events. '
            'Leave blank to use the default of simply deleting events. '
            'Specified indexes must be created in Splunk for this to '
            'be effective.',
        'required_on_create': False
    }
}
