
import splunk_ta_aws_import_lib_declare

import splunk.admin as admin

from splunktalib.rest_manager import base, datainput, normaliser
from splunktalib.rest_manager.error_ctl import RestHandlerError as RH_Err

from aws_s3 import delete_ckpt


class S3Input(datainput.DataInputModel):
    """
    AWS S3 Inputs Model.
    """
    dataInputName = "aws_s3"
    requiredArgs = ['aws_account', 'bucket_name']
    optionalArgs = [
      'aws_iam_role',
      'host_name',
      'key_name',
      'recursion_depth',
      'initial_scan_datetime',
      'max_items',
      'max_retries',
      'blacklist',
      'whitelist',
      'character_set',
      'ct_blacklist',
      'ct_excluded_events_index',
      'interval',
      'sourcetype',
      'host',
      'index'
    ]

    keyMap = {
      'interval': 'polling_interval'
    }

    defaultVals = {
      'host_name': 's3.amazonaws.com',
      'recursion_depth': '-1',
      'max_items': '100000',
      'max_retries': '3',
      'character_set': 'auto',
      'ct_blacklist': '^$',  # ADDON-5300, ADDON-4684
      'polling_interval': '1800',
      'sourcetype': 'aws:s3',
    }
    normalisers = {
      'disabled': normaliser.Boolean(),
    }
    cap4disable = 'ta_aws_custom_action_disable'
    cap4enable = 'ta_aws_custom_action_enable'


class S3Handler(datainput.DataInputHandler):

    def decode(self, name, ent):
        """
        Override this function for ADDON-7399:
        using interval if polling_interval is empty in conf file.
        :param name:
        :param ent:
        :return:
        """
        # decrypt
        ent = self._credMgmt.decrypt(name, ent)

        # Adverse Key Mapping
        ent = {k: v for k, v in ent.iteritems()}
        keyMapAdv = {v: k for k, v in self.keyMap.items()}
        ent_new = {keyMapAdv[k]: vs for k, vs in ent.items()
                   if k in keyMapAdv and vs}
        ent.update(ent_new)

        # Adverse Value Mapping
        valMapAdv = {k: {y: x for x, y in m.items()}
                     for k, m in self.valMap.items()}
        ent = {k: (k in valMapAdv and
                   (isinstance(vs, list) and
                    [(v in valMapAdv[k] and
                     valMapAdv[k][v] or v) for v in vs] or
                    (vs in valMapAdv[k] and
                    valMapAdv[k][vs] or vs)) or vs)
               for k, vs in ent.items()}

        # normalize
        ent = self.normalize(ent)

        # filter undesired arguments & handle none value
        return {k: (str(v).lower() if isinstance(v, bool) else v)
                if (v is not None and str(v).strip()) else ''
                for k, v in ent.iteritems()
                if k not in self.transientArgs and
                (self.allowExtra or
                 k in self.requiredArgs or
                 k in self.optionalArgs or
                 k in self.outputExtraFields)
                }

    def _delete_ckpt(self):
        try:
            delete_ckpt(self.callerArgs.id)
        except Exception as exc:
            if (isinstance(exc, IOError) and
                    'No such file or directory' in str(exc)):
                return
            RH_Err.ctl(500, 'Failed to delete checkpoint')

    def handleRemove(self, confInfo):
        datainput.DataInputHandler.handleRemove(self, confInfo)
        self._delete_ckpt()

    def handleCreate(self, confInfo):
        self._delete_ckpt()
        datainput.DataInputHandler.handleCreate(self, confInfo)


if __name__ == "__main__":
    admin.init(base.ResourceHandler(S3Input, handler=S3Handler),
               admin.CONTEXT_APP_AND_USER)
