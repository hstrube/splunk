
from splunksdc import SimpleCollectorV1

from sqs_mod.aws_sqs_data_loader import Input


if __name__ == '__main__':
    SimpleCollectorV1.main(
        Input(),
        description='Collect and index AWS SQS messages',
        arguments={
            'placeholder': {'title': 'placeholder'}
        }
    )
