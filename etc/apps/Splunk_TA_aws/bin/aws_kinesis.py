#!/usr/bin/python

"""
This is the main entry point for My TA
"""

import splunk_ta_aws_import_lib_declare

import time

from splunktalib.common import log
import kinesis_mod.aws_kinesis_consts as akc
logger = log.Logs(akc.kinesis_log_ns).get_logger(akc.kinesis_log)

import splunktalib.common.util as utils
import splunktalib.orphan_process_monitor as opm

import aws_concurrent_data_loader as acdl
import taaws.ta_aws_common as tacommon
import kinesis_mod.aws_kinesis_conf as akconf


def print_scheme():
    title = "Splunk AddOn for AWS"
    description = "Collect and index Kinesis data for AWS"
    tacommon.print_scheme(title, description)


@utils.catch_all(logger, False)
def run():
    """
    Main loop. Run this TA forever
    """

    logger.info("Start Kinesis TA")
    metas, _, tasks = tacommon.get_configs(
        akconf.AWSKinesisConf, "aws_kinesis", logger)

    if not tasks:
        return

    loader = acdl.AwsDataLoaderManager(tasks)

    conf_change_handler = tacommon.get_file_change_handler(loader, logger)
    conf_monitor = akconf.create_conf_monitor(conf_change_handler)
    loader.add_timer(conf_monitor, time.time(), 10)

    orphan_checker = opm.OrphanProcessChecker(loader.stop)
    loader.add_timer(orphan_checker.check_orphan, time.time(), 1)

    # monitor shard changes
    # Disable it for ADDON-9537
    # streams = list(set([t[akc.stream_name] for t in tasks]))
    # shard_checker = akconf.ShardChangesChecker(tasks[0], streams, loader.stop)
    # loader.add_timer(shard_checker, time.time(), 120)

    loader.start()
    logger.info("End Kinesis TA")


def main():
    """
    Main entry point
    """

    tacommon.main(print_scheme, run)


if __name__ == "__main__":
    main()
