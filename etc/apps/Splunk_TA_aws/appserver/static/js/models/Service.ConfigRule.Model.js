define([
    'jquery',
    'underscore',
    'backbone',
    'app/models/Service.Base.Model'
], function(
    $,
    _,
    Backbone,
    BaseModel
) {
    var Identity = BaseModel.extend({
        url: [
            "splunk_ta_aws",
            "inputs",
            "config-rule"
        ].join('/'),
        parse: function(resp, options){
            delete resp.entry[0].content.password;
            BaseModel.prototype.parse.call(this, resp, options);
        },
        initialize: function(attributes, options){
            options = options || {};
            this.collection = attributes.collection;
            BaseModel.prototype.initialize.call(this, attributes, options);
            this.addValidation('aws_region', this.nonEmptyString);
            this.removeValidation("interval");

            this.attr_labels = _.extend(this.attr_labels, {
                aws_region: "AWS Region",
                rule_names: "Rule Names"
            });

        }

    });

    return Identity;
});
