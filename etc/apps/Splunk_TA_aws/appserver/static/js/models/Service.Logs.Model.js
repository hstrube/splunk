define([
    'jquery',
    'underscore',
    'backbone',
    'app/models/Service.Base.Model'
], function(
    $,
    _,
    Backbone,
    BaseModel
) {
    var Identity = BaseModel.extend({
        url: [
            "splunk_ta_aws",
            "inputs",
            "splunk_ta_aws_inputs_logs"
        ].join('/'),

        /*
        We never show the password for an edit operation
        This avoids weird things with asterisks and stuff
        */
        parse: function(resp, options){
            delete resp.entry[0].content.password;
            BaseModel.prototype.parse.call(this, resp, options);
        },
        /**
        attrs: {
            appData: {
                owner,
                appName
            }
        }
        **/
        initialize: function(attributes, options){
            options = options || {};
            this.collection = attributes.collection;
            BaseModel.prototype.initialize.call(this, attributes, options);

            this.addValidation('log_type', this.nonEmptyString);
            this.addValidation('s3_bucket_region', this.nonEmptyString);
            this.addValidation('s3_bucket_name', this.nonEmptyString);
            this.removeValidation("sourcetype");

            this.attr_labels = _.extend(this.attr_labels, {
                log_type: "AWS Log Type",
                s3_bucket_region: "S3 Bucket Region",
                s3_bucket_name: "S3 Bucket Name",
                s3_bucket_prefix: "Log File Prefix",
                log_start_date: "Log Start Date",
                interval: "Interval",
                index: "Index"
            });

        }

    });

    return Identity;
});
