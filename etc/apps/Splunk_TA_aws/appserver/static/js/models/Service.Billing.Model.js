define([
    'jquery',
    'underscore',
    'backbone',
    'app/models/Service.Base.Model',
    "util/splunkd_utils"
], function(
    $,
    _,
    Backbone,
    ServiceBaseModel,
    splunkdUtils
) {
    var Identity = ServiceBaseModel.extend({
        url: [
            "splunk_ta_aws",
            "inputs",
            "billing"
        ].join('/'),

        /*
        We never show the password for an edit operation
        This avoids weird things with asterisks and stuff
        */
        parse: function(resp, options){
            ServiceBaseModel.prototype.parse.call(this, resp, options);
        },

        initialize: function(attributes, options){
            options = options || {};
            this.collection = attributes.collection;
            ServiceBaseModel.prototype.initialize.call(this, attributes, options);

            this.addValidation('host_name', this.nonEmptyString);
            this.addValidation('bucket_name', this.validateBucketName);
            this.addValidation('report_file_match_reg', this.emptyOr(this.validRegexString));

            this.attr_labels = _.extend(this.attr_labels, {
                host_name: "S3 Host Name",
                bucket_name: "S3 Bucket",
                report_file_match_reg: "Regex for Report Selection"
            });
        }

    });

    return Identity;
});
