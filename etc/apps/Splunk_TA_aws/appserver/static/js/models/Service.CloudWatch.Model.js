define([
    'jquery',
    'underscore',
    'backbone',
    'app/models/Service.Base.Model'
], function(
    $,
    _,
    Backbone,
    BaseModel
) {
    var Identity = BaseModel.extend({
        url: [
            "splunk_ta_aws",
            "inputs",
            "cloudwatch"
        ].join('/'),

        // statistics: '["Sum", "Average"....]'
        // statistics_lst: 'Sum, Average...'
        save: function(attributes, options) {
            var statistics_lst = this.entry.content.get("statistics_lst");
            if (statistics_lst == null)
                statistics_lst = "";

            this.entry.content.set({statistics: JSON.stringify(String(statistics_lst).split(","))});
            return BaseModel.prototype.save.call(this, attributes, options);
        },

        parse: function(resp, options){
            BaseModel.prototype.parse.call(this, resp, options);

            var statistics = this.entry.content.get("statistics");
            if (statistics) {
                this.entry.content.set({statistics_lst: JSON.parse(statistics).join(",")});
            }

            var aws_regions = this.entry.content.get("aws_regions");
            if (aws_regions)
                this.entry.content.set({aws_region: aws_regions});
        },
        /**
        attrs: {
            appData: {
                owner,
                appName
            }
        }
        **/
        initialize: function(attributes, options){
            options = options || {};
            this.collection = attributes.collection;
            BaseModel.prototype.initialize.call(this, attributes, options);
            this.addValidation('aws_region', this.nonEmptyString);
            this.addValidation('metric_namespace', this.nonEmptyString);
            this.addValidation('metric_names', this.nonEmptyString);
            this.addValidation('metric_dimensions', this.nonEmptyString);
            this.addValidation('period', this.validNumberInRange(60, 86400));
            this.addValidation('polling_interval', this.validNumberInRange(60, 86400));
            this.removeValidation("interval");

            // either one should be not be empty
            this.addValidation('statistics_lst', this.nonEmptyString);
            this.addValidation('statistics', this.nonEmptyString);

            this.entry.content.toJSON=function(options){
                var json = BaseModel.prototype.toJSON.call(this, options);
                json.aws_regions = json.aws_region;
                delete json.aws_region;

                return json;
            };

            this.attr_labels = _.extend(this.attr_labels, {
                aws_region: "AWS Region",
                metric_namespace: "Metric Namespace",
                metric_names: "Metric Names",
                metric_dimensions: "Dimension Names",
                period: "Metric Granularity",
                polling_interval: "Minimum Polling Interval",
                statistics_lst: "Metric Statistics",
                statistics: "Metric Statistics"
            });

        }

    });

    return Identity;
});
