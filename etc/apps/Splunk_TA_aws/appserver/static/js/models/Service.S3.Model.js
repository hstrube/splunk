define([
    'jquery',
    'underscore',
    'backbone',
    'app/models/Service.Base.Model',
    "util/splunkd_utils"
], function(
    $,
    _,
    Backbone,
    BaseModel,
    splunkdUtils
) {
    var Identity = BaseModel.extend({
        url: [
            "splunk_ta_aws",
            "inputs",
            "s3"
        ].join('/'),

        /*
        We never show the password for an edit operation
        This avoids weird things with asterisks and stuff
        */
        parse: function(resp, options){
            delete resp.entry[0].content.password;
            BaseModel.prototype.parse.call(this, resp, options);
        },
        /**
        attrs: {
            appData: {
                owner,
                appName
            }
        }
        **/
        initialize: function(attributes, options){
            options = options || {};
            this.collection = attributes.collection;
            BaseModel.prototype.initialize.call(this, attributes, options);

            this.addValidation('host_name', this.nonEmptyString);
            this.addValidation('bucket_name', this.nonEmptyString);
            this.addValidation('initial_scan_datetime', this.nonEmptyString);
            this.addValidation('blacklist', this.emptyOr(this.validRegexString));
            this.addValidation('whitelist', this.emptyOr(this.validRegexString));
            this.addValidation('ct_blacklist', this.emptyOr(this.validRegexString));

            this.attr_labels = _.extend(this.attr_labels, {
                host_name: "S3 Host Name",
                bucket_name: "S3 Bucket",
                blacklist: "Blacklist",
                whitelist: "Whitelist",
                ct_blacklist: "Blacklist for Excluded Cloudtrail Events",
                initial_scan_datetime: "Start Date/Time"
            });

        }

    });

    return Identity;
});
