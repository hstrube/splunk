define([
    'jquery',
    'underscore',
    'backbone',
    'app/models/Base.Model'
], function(
    $,
    _,
    Backbone,
    BaseModel
) {
    return BaseModel.extend({
        url: [
            "splunk_ta_aws",
            "settings",
            "splunk_ta_aws_iam_role"
        ].join("/"),
        nameValidator: function(attr) {
            var collection = this.get('collection'),
                regex = /^[^/]+$/;

            if (this.isNew() === true) {
                var val = this.entry.content.get(attr) || '';
                val = val.trim();
                if(val === ''){
                    return _('Name is required.').t();
                }

                if(!regex.test(val)) {
                    return _('Name can not contain "/".').t();
                }
                var matches = collection.models.some(function(model) {
                    return model.entry.content.get('name') === val;
                });
                if (matches) {
                    return _('Name is already in use.').t();
                }
            }
        },
        initialize: function(attributes, options){
            options = options || {};
            this.collection = attributes.collection;
            BaseModel.prototype.initialize.call(this, attributes, options);
            this.entry.content.validation = {
                'name': this.nameValidator.bind(this)
            };

            this.entry.content.toJSON=function(options){
                return BaseModel.prototype.toJSON.call(this, options);
            };
        }

    });
});
