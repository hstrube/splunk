define([
    'jquery',
    'underscore',
    'backbone',
    'app/models/Base.Model',
    "util/splunkd_utils"
], function(
    $,
    _,
    Backbone,
    BaseModel,
    splunkdUtils
) {
    var Identity = BaseModel.extend({

        parse: function(resp, options){
            delete resp.entry[0].content.password;
            BaseModel.prototype.parse.call(this, resp, options);
        },
        /**
        attrs: {
            appData: {
                owner,
                appName
            }
        }
        **/
        initialize: function(attributes, options){
            options = options || {};
            this.collection = attributes.collection;
            BaseModel.prototype.initialize.call(this, attributes, options);
            this.entry.content.validation = {
                'name': this.nameValidator.bind(this),
//                'account_password': this.passwordValidator.bind(this),
//                'confirmPassword': this.passwordValidator.bind(this),
                'server_url': {
                    required: true,
                    minLength: 1,
                    msg: _("Server URL is required").t()
                }
            };

            var self = this;
            this.entry.content.toJSON=function(options){
                var json = BaseModel.prototype.toJSON.call(this, options);
                if(!self.isNew() && self.numPwChanges === 0){
                    delete json.account_password;
                }
                return json;
            };

            this.numPwChanges = 0;
            this.entry.content.on('change:account_password', function(){
               this.numPwChanges++;
            }, this);
        }

    });

    return Identity;
});
