define([
    'jquery',
    'underscore',
    'backbone',
    'app/models/Service.Base.Model'
], function(
    $,
    _,
    Backbone,
    BaseModel
) {
    var Identity = BaseModel.extend({
        url: [
            "splunk_ta_aws",
            "inputs",
            "description"
        ].join('/'),

        /*
        We never show the password for an edit operation
        This avoids weird things with asterisks and stuff
        */
        parse: function(resp, options){
            BaseModel.prototype.parse.call(this, resp, options);

            var aws_regions = this.entry.content.get("aws_regions");
            if (aws_regions){
                this.entry.content.set({aws_region: aws_regions});
            }
        },
        /**
        attrs: {
            appData: {
                owner,
                appName
            }
        }
        **/
        initialize: function(attributes, options){
            options = options || {};
            this.collection = attributes.collection;
            BaseModel.prototype.initialize.call(this, attributes, options);
            this.addValidation('aws_region', this.nonEmptyString);
            this.addValidation('apis', this.nonEmptyString);
            this.removeValidation("interval");

            this.attr_labels = _.extend(this.attr_labels, {
                aws_region: "AWS Region",
                apis: "APIs/Interval"
            });

            this.entry.content.toJSON=function(options){
                var json = BaseModel.prototype.toJSON.call(this, options);
                json.aws_regions = json.aws_region;
                delete json.aws_region;

                return json;
            };


        }

    });

    return Identity;
});
