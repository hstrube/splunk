define([
    'jquery',
    'underscore',
    'backbone',
    'app/models/Base.Model'
], function(
    $,
    _,
    Backbone,
    BaseModel
) {
    var Identity = BaseModel.extend({
        url: [
            "splunk_ta_aws",
            "settings",
            "account4ui"
        ].join("/"),
        nameValidator: function(attr) {
            var collection = this.get('collection'),
                regex = /^[^/]+$/;

            if (this.isNew() === true) {
                var val = this.entry.content.get(attr) || '';
                val = val.trim();
                if(val === ''){
                    return _('Name is required.').t();
                }

                if(!regex.test(val)) {
                    return _('Name can not contain "/".').t();
                }
                if (val === "EC2 with IAM Role"){
                    return _('Name "EC2 with IAM Role" is reserved.').t();
                }
                var matches = collection.models.some(function(model) {
                    return model.entry.content.get('name') === val;
                });

                if (matches) {
                    return _('Name is already in use.').t();
                }
            }
        },
        initialize: function(attributes, options){
            options = options || {};
            this.collection = attributes.collection;
            BaseModel.prototype.initialize.call(this, attributes, options);
            this.entry.content.validation = {
                'name': this.nameValidator.bind(this),
                'server_url': {
                    required: true,
                    minLength: 1,
                    msg: _("Server URL is required.").t()
                }
            };

            var self = this;
            this.entry.content.toJSON=function(options){
                var json = BaseModel.prototype.toJSON.call(this, options);
                if(!self.isNew() && self.numPwChanges === 0){
                    delete json.account_password;
                }
                return json;
            };

            this.numPwChanges = 0;
            this.entry.content.on('change:account_password', function(){
               this.numPwChanges++;
            }, this);
        }

    });

    return Identity;
});
