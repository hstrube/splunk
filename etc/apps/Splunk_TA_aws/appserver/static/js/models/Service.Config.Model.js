define([
    'jquery',
    'underscore',
    'backbone',
    'app/models/Service.Base.Model',
    "util/splunkd_utils"
], function(
    $,
    _,
    Backbone,
    BaseModel,
    splunkdUtils
) {
    var Identity = BaseModel.extend({
        url: [
            "splunk_ta_aws",
            "inputs",
            "config"
        ].join('/'),

        /*
        We never show the password for an edit operation
        This avoids weird things with asterisks and stuff
        */
        parse: function(resp, options){
            delete resp.entry[0].content.password;
            BaseModel.prototype.parse.call(this, resp, options);
        },
        /**
        attrs: {
            appData: {
                owner,
                appName
            }
        }
        **/
        initialize: function(attributes, options){
            options = options || {};
            this.collection = attributes.collection;
            BaseModel.prototype.initialize.call(this, attributes, options);
            this.addValidation('aws_region', this.nonEmptyString);
            this.addValidation('sqs_queue', this.nonEmptyString);

            this.attr_labels = _.extend(this.attr_labels, {
                aws_region: "AWS Region",
                sqs_queue: "SQS Queue Name"
            });
        }

    });

    return Identity;
});
