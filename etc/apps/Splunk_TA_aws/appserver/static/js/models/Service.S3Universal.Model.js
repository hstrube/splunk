define([
    'jquery',
    'underscore',
    'backbone',
    'app/models/Service.Base.Model',
    "util/splunkd_utils"
], function(
    $,
    _,
    Backbone,
    BaseModel,
    splunkdUtils
) {
    var Identity = BaseModel.extend({
        url: [
            "splunk_ta_aws",
            "inputs",
            "splunk_ta_aws_inputs_s3_universal"
        ].join('/'),

        /*
        We never show the password for an edit operation
        This avoids weird things with asterisks and stuff
        */
        parse: function(resp, options){
            delete resp.entry[0].content.password;
            BaseModel.prototype.parse.call(this, resp, options);
        },
        /**
        attrs: {
            appData: {
                owner,
                appName
            }
        }
        **/
        initialize: function(attributes, options){
            options = options || {};
            this.collection = attributes.collection;
            BaseModel.prototype.initialize.call(this, attributes, options);

            this.addValidation('host_name', this.nonEmptyString);
            this.addValidation('bucket_name', this.nonEmptyString);

            this.attr_labels = _.extend(this.attr_labels, {
                host_name: "S3 Host Name",
                bucket_name: "S3 Bucket"
            });

        }

    });

    return Identity;
});
