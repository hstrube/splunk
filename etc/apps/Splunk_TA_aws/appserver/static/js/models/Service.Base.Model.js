define([
    'jquery',
    'underscore',
    'backbone',
    'app/models/Base.Model',
    "util/splunkd_utils"
], function ($, _, Backbone, BaseModel, splunkdUtils) {
    var Identity = BaseModel.extend({
        nameValidator: function (attr) {
            var val = this.entry.content.get(attr);
            var collection = this.get('collection'),
                matches,
                // regex = XRegExp('^(\\p{L}|\\p{N}|[a-z-_.\\s])+$', 'i');
                // regex = /^([\x00-\x2E]|[\x30-\x7F])*$/;
                // regex = /^[\w\s-.]+$/g;
                regex = /^[^/]+$/;

            if (this.isNew() === true) {
                if (val === undefined || val === '') {
                    return _('Name is required').t();
                }

                val = String(this.entry.content.get(attr)).replace(/^\s+|\s+$/gm,'');

                if (val.indexOf("_") == 0) {
                    return _('Name starting with "_" underscore is not allowed.').t();
                }

                if (regex.test(val) === false) {
                    // return _('Name can only contain alpha, numeric, dash, dot, space and underscore characters').t();
                    return _('Name can not contain "/".').t();
                }

                matches = collection.filter(function (item) {
                    return item._url.indexOf(this.url) >= 0;
                }.bind(this));
                matches = _.find(matches, function (identity) {
                    return identity.entry.get('name') === val;
                });

                if (matches !== undefined) {
                    return _('Name is already in use').t();
                }
            }
        },

        /*
         We never show the password for an edit operation
         This avoids weird things with asterisks and stuff
         */
        parse: function (resp, options) {
            delete resp.entry[0].content.password;
            BaseModel.prototype.parse.call(this, resp, options);
        },

        initialize: function (attributes, options) {
            options = options || {};
            this.collection = attributes.collection;
            BaseModel.prototype.initialize.call(this, attributes, options);
            this.entry.content.validation = {
                'name': this.nameValidator.bind(this),
                'interval': this.nonEmptyString.bind(this),
                'index': this.nonEmptyString.bind(this),
                'sourcetype': this.nonEmptyString.bind(this),
                'aws_account': this.nonEmptyString.bind(this)
            };
        },

        attr_labels: {
            'name': "Name",
            'interval': 'Interval',
            'index': "Index",
            "sourcetype": "Sourcetype",
            "aws_account": "AWS Account"
        },

        _getAttrLabel: function(attr) {
            var val = this.attr_labels[attr];
            if (val) return val;
            return attr;
        },

        getAttrLabel: function(attr) {
            return this._getAttrLabel(attr);
        },

        _all_validator: function (validator, attr) {
            var ret;
            if (_.isUndefined(validator) || _.isNaN(validator)) {
                // do nothing
            } else if (_.isFunction(validator)) {
                ret = validator(attr);
                if (ret) return ret;
            } else if (_.isArray(validator)) {
                for (var fn in validator) {
                    if (!validator.hasOwnProperty(fn)) continue;
                    ret = this._all_validator(validator[fn], attr);
                    if (ret) return ret;
                }
            }
        },

        validate: function () {
            var validations = this.entry.content.validation;
            if (!validations) return;

            var ret;
            for (var name in validations) {
                if (!validations.hasOwnProperty(name)) continue;
                ret = this._all_validator(validations[name], name);
                if (ret) return ret;
            }
        },

        positiveNumberValidator: function (attr) {
            var ret = this.convertNumericAttr(attr);
            if (undefined === ret || isNaN(ret))
                return _("Option '" + this._getAttrLabel(attr) + "' should be positive value: ").t();
        },

        nonEmptyString: function (attr) {
            var val = this.entry.content.get(attr);
            if (!val || !String(val).trim())
                return _("Option '" + this._getAttrLabel(attr) + "' cannot be empty string").t();
        },

        addValidation: function (name, validator) {
            this.entry.content.validation[name] = validator.bind(this);
        },

        removeValidation: function (name) {
            delete this.entry.content.validation[name];
        },

        // TODO: check http://docs.aws.amazon.com/AmazonS3/latest/dev/BucketRestrictions.html
        validateBucketName: function (attr) {
            return this.nonEmptyString(attr);
        },

        validRegexString: function(attr) {
            var val = this.entry.content.get(attr);
            var isValid;
            try {
                new RegExp(val);
                isValid = true;
            }
            catch(e) {
                isValid = false;
            }

            if (!isValid) return "Option '" + this._getAttrLabel(attr) + "' is not a valid regex";
        },

        emptyOr: function(furtherValidator) {
            return function(attr) {
                if (!this.nonEmptyString(attr))
                    return furtherValidator.bind(this)(attr);
            }.bind(this);
        },

        validNumberInRange: function(start, end) {
            return function(attr) {
                var ret = this.positiveNumberValidator(attr);
                if (ret)
                    return ret;

                var val = this.convertNumericAttr(attr);
                if (_.isNumber(start) && _.isNumber(end)) {
                    if (val < start || val > end)
                        return "Option '" + this._getAttrLabel(attr) + "' is not in expected range (" + start + ", " + end + ")";
                } else if (_.isNumber(start)) {
                    if (val < start)
                        return "Option '" + this._getAttrLabel(attr) + "' should be >= " + start;
                } else if (_.isNumber(end)) {
                    if (val > end)
                        return "Option '" + this._getAttrLabel(attr) + "' should be <= " + end;
                }

            }.bind(this);
        }


    });

    return Identity;
});
