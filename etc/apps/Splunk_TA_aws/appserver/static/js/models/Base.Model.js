define([
    'jquery',
    'underscore',
    'backbone',
    'app/models/ProxyBase.Model',
    "app/util/mixin",
    'app/mixins/WithDeepClone.Mixin',
    'splunk.util'
], function(
    $,
    _,
    Backbone,
    ProxyBase,
    Util,
    WithDeepClone
) {
    var BaseModel =  ProxyBase.extend({
        initialize: function(attributes, options){
            ProxyBase.prototype.initialize.call(this, attributes, options);
            options = options || {};
            this.targetCollection = options.targetCollection || this.collection;

            var targetApp, appMoveTo;

            // Just in case the proxyBase didn't set appData already
            if(options !== undefined){
                if(options.appData !== undefined && !this.has('appData')){
                    this.set('appData', options.appData);
                }

                if(!this.has('targetOwner')){
                    if(options.targetOwner !== undefined){
                        this.set("targetOwner", options.targetOwner);
                    }
                }
            }

            if(this.has('targetApp')){
                targetApp = this.get('targetApp');
            } else {
                if(this.get('appData').targetApp !== undefined){
                    targetApp = this.get('appData').targetApp;
                }
            }


            if(targetApp === '-'){
                console.error("targetApp should not be '-'");
            } else if (targetApp === undefined){
                // Just in case proxyBase didnt do this
                targetApp = this.get('appData').app;
            }

            appMoveTo = targetApp;
            this.set("targetApp", targetApp);
            this.set("appMoveTo", appMoveTo);
        },
        clone: function() {
            var clone = ProxyBase.prototype.clone.apply(this, arguments),
                attrs = clone.entry.acl.attributes;

            // wah-wah.... rule#1 of backbone: model attributes should not be
            // mutables!
            attrs.perms = this.deepClone(attrs.perms);
            /*
            TODO: This is sort of a hack
            A better method would be to actually call the
            construtor when creating a new cloned object
            */
            clone.entry.content.validation = this.deepClone(this.entry.content.validation);


            // flag ourselves as a clone so our view might introspect.
            clone._isClone = true;

            // unset name and id attrs on entry... giving us an independent
            // clone
            clone.entry.unset("name",{silent: true});
            clone.entry.unset("id", {silent: true});
            clone.targetCollection = this.targetCollection;

            return clone;
        },

        isClone: function() {
            return !!this._isClone;
        },
        validate: function () {
            var validations = this.entry.content.validation;
            if (!validations) return;

            var ret;
            for (var name in validations) {
                if (!validations.hasOwnProperty(name)) continue;
                ret = this._all_validator(validations[name], name);
                if (ret) return ret;
            }
        },
        _all_validator: function (validator, attr) {
            var ret;
            if (_.isUndefined(validator) || _.isNaN(validator)) {
                // do nothing
            } else if (_.isFunction(validator)) {
                ret = validator(attr);
                if (ret) return ret;
            } else if (_.isArray(validator)) {
                for (var fn in validator) {
                    if (!validator.hasOwnProperty(fn)) continue;
                    ret = this._all_validator(validator[fn], attr);
                    if (ret) return ret;
                }
            }
        },
        nameValidator: function(value, attr, computed) {
            // name is immutable once created... at least as far as we're concerned
            if(!this.isNew()) return undefined;

            if(_.isUndefined(value)) return _("Name attribute is required");

            if(_.isEmpty(value)) return _("Name cannot be empty");

            if(!_.isString(value)) return _("Name must be a string");

            if(value.indexOf(" ") > -1){
                return _("Name cannot have spaces");
            }

            if(this.targetCollection !== undefined && this.targetCollection.findByName(value)){
                return _("Name: ").t() + value + _("already in use");
            }
        },

        getName: function(){
            return this.entry.get('name') || this.entry.content.get("name") ||  this.get('name');
        },
        jsonAttrs: [],
        nullableAttrs: [],
        nullStr: 'NULL',
        parse: function(resp, options){
            ProxyBase.prototype.parse.apply(this, arguments);

            var entry = this.entry,
                content = this.entry.content,
                raw,
                val;

            _.each(this.jsonAttrs, function(jsonAttr){
                raw = content.get(jsonAttr);

                /*
                Just in case something else already
                parsed the json.
                Splunkd base will turn "[]" into []
                */
                if(_.isString(raw)){
                    val = JSON.parse(raw);
                    content.set(jsonAttr, val);
                }

            }, this);

            _.each(this.nullableAttrs, function(attr){
                if(content.get(attr) === '' || !content.has(attr)){
                    content.set(attr, this.nullStr);
                }
            }, this);

            this.normalizeBooleanAttr('disabled');
        },
        sync: function(method, model, options){
            var entry = this.entry,
                content = this.entry.content,
                val,
                jsonStr;

            if(method === "update" || method === "create"){
                _.each(this.jsonAttrs, function(jsonAttr){
                    val = content.get(jsonAttr);
                    if(_.isArray(val) || _.isObject(val)){
                        jsonStr = JSON.stringify(val);
                        /*
                        TODO:
                        Another way of doing this would be to set the
                        json str directly on the data that backbone
                        sends to the server.
                        This would be neccesary if we ever needed
                        to save the model, but not re-fetch immedietly.
                        */
                        content.set(jsonAttr, jsonStr, {silent: true});
                    } else {
                        console.warn(jsonAttr, " is not a string:", val);
                    }
                }, this);

                _.each(this.nullableAttrs, function(attr){
                    if(content.get(attr) === this.nullStr){
                        content.set(attr, '', {silent: true});
            }
                }, this);
            }



            if(method==='update'){
                var targetApp, appMoveTo;
                targetApp = this.get('targetApp');
                appMoveTo = this.get('appMoveTo');

                if(appMoveTo !== undefined && appMoveTo !== targetApp){
                    var syncDef, moveDef;
                    syncDef = $.Deferred();
                    moveDef = this.move();

                    moveDef.done(function(){
                        // update the appName so we can fetch again
                        // otherwise it will point to the old app name
                        // (the one before the move)
                        this.entry.content.set('eai:appName', appMoveTo);
                        this.entry.acl.set('app', appMoveTo);
                        this.set('targetApp', appMoveTo);

                        var proxyDef = ProxyBase.prototype.sync.call(this, method, model, options);
                        proxyDef.done(function(){
                            this.set('targetApp', appMoveTo);
                            syncDef.resolve.apply(syncDef, arguments);
                        }.bind(this));
                        proxyDef.fail(function(){
                            syncDef.reject.apply(syncDef, arguments);
                        });
                    }.bind(this));

                    moveDef.fail(function(){
                        syncDef.reject.apply(syncDef, arguments);
                    });

                    return syncDef;
                } else {
                    return ProxyBase.prototype.sync.apply(this, arguments);
                }
            } else {
                return ProxyBase.prototype.sync.apply(this, arguments);
            }
        },
        getMoveUrl: function(){
            return this._getFullUrl() + "/move";
        },
        move: function(newApp){
            var app, user;

            app = this.get("appMoveTo") || this.get("targetApp") || this.get("appData").app;
            user = this.get("targetOwner") || "nobody";

            return $.ajax({
                type: "POST",
                data: {
                    app: app,
                    user: user
                },
                url: this.getMoveUrl()
            });
        },
        enable: function() {
            return $.ajax({
                type: "POST",
                url: this._getFullUrl() + "/enable?output_mode=json"
            });
        },
        disable: function() {
            return $.ajax({
                type: "POST",
                url: this._getFullUrl() + "/disable?output_mode=json"
            });
        },
        normalizeBooleanAttr: function(attr){
            var val = this.entry.content.get(attr);
            if(val !== undefined){
                val = Splunk.util.normalizeBoolean(val);
                this.entry.content.set(attr, val);
            }
            return val;
        },
        convertNumericAttr: function(attr){
            var val = this.entry.content.get(attr);
            if(val !== undefined){
                val = Number(val);
                if(!isNaN(val)){
                    this.entry.content.set(attr, val);
                }
            }
            return val;
        },
        toZeroBased: function(attr){
            var val = this.entry.content.get(attr);

            // negative numbers and zero are out of range. -1 is used
            // by the backend and has special meaning
            if(_.isNumber(val) && val > 0)  val-=1;
            this.entry.content.set(attr,val);

            return val;
        },
        toOneBased: function(attr){
            var val = this.entry.content.get(attr);

            // negative numbers are out of range. -1 is used
            // by the backend and has special meaning
            if(_.isNumber(val) && val >= 0)  val+=1;
            this.entry.content.set(attr,val);

            return val;
        },
        getNumericAttr: function(attr){
            var val = this.entry.content.get(attr);
            if(val !== undefined){
                val = Number(val);

                if(isNaN(val)){
                    return undefined;
                }
            }

            return val;
        },
        isDisabled: function() {
            // just in case, but it should be true|false
            var val = this.entry.content.get('disabled');
            return Splunk.util.normalizeBoolean(val);
        },
        //Make mixin an instance property so we can mixin dynamically
        mixin: Util.dynamicMixin
    },{
        //make mixin a Class property so we can mixin using ClassName.mixin
        //at def time
        mixin: Util.modelMixin
    });

    BaseModel.mixin(WithDeepClone);

    return BaseModel;
});
