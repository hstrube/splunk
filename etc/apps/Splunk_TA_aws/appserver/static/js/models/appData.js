define(
    [
        'jquery',
        'underscore',
        'splunk.config',
        'models/Base'
    ],
    function($, _, $C, BaseModel) {

        var app,
            // if we're using custom root, then app name is @ index 4
            appIdx = ($C.MRSPARKLE_ROOT_PATH) ? 4 : 3;

        // if we're running in a browser, just go ahead and grab our
        // app name. We can explicitly set/change it later.
        if(window && window.location && window.location.pathname) {
            app = window.location.pathname.split("/")[appIdx] || app;
        }

        var Model = BaseModel.extend({

            defaults: {
                owner: $C.USERNAME,
                //TODO: Change me
                // app: app,
                app: 'Splunk_TA_aws',
                custom_rest: 'splunk_ta_aws',
                nullStr: 'NULL',
                stanzaPrefix: 'splunk_ta_aws'
            },

            id: "appData",

            sync: function(method, model, options) {
                throw new Error('invalid method: ' + method);
            }
        });
        return new Model({});
    }
);
