define([
    'jquery',
    'underscore',
    'backbone',
    'app/models/Base.Model'
], function(
    $,
    _,
    Backbone,
    BaseModel
) {
    var Identity = BaseModel.extend({
        url: "server"
    });

    return Identity;
});