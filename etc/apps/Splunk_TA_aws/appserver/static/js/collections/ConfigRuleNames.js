define([
    'jquery',
    'underscore',
    'backbone',
    'app/collections/ProxyBase.Collection',
    'app/models/ConfigRuleName'
  ],
function(
    $,
    _,
    Backbone,
    BaseCollection,
    ConfigRuleName
) {
    return BaseCollection.extend({
        url: 'splunk_ta_aws/splunk_ta_aws_config_rules',
        model: ConfigRuleName,
        initialize: function(attributes, options){
            BaseCollection.prototype.initialize.call(this, attributes, options);
        }
    });
});
