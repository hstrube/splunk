define([
    'jquery',
    'underscore',
    'backbone',
    'app/collections/ProxyBase.Collection',
    'app/models/SQSQueue',
    'splunk.util'
  ],
function(
    $,
    _,
    Backbone,
    BaseCollection,
    SQSQueue,
    splunkdUtils
) {
    return BaseCollection.extend({
        url: 'splunk_ta_aws/splunk_ta_aws_sqs_queue_names',
        model: SQSQueue,
        initialize: function(attributes, options){
            BaseCollection.prototype.initialize.call(this, attributes, options);
        }
    });
});
