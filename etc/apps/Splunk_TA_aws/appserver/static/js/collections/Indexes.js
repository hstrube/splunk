define([
    'jquery',
    'underscore',
    'backbone',
    'app/collections/ProxyBase.Collection'
  ],
function(
    $,
    _,
    Backbone,
    BaseCollection
) {
    return BaseCollection.extend({
        url: 'splunk_ta_aws/splunk_ta_aws_indexes',
        model: Backbone.Model,
        initialize: function(attributes, options){
            BaseCollection.prototype.initialize.call(this, attributes, options);
        }
    });
});
