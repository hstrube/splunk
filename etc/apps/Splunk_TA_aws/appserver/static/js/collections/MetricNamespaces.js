define([
    'jquery',
    'underscore',
    'backbone',
    'app/collections/ProxyBase.Collection',
    'app/models/MetricNamespace.Model'
  ],
function(
    $,
    _,
    Backbone,
    BaseCollection,
    MetricNamespaceModel
) {
    return BaseCollection.extend({
        url: 'splunk_ta_aws/splunk_ta_aws_namespaces',
        model: MetricNamespaceModel,
        initialize: function(attributes, options){
            BaseCollection.prototype.initialize.call(this, attributes, options);
        }
    });
});
