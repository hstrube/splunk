define([
    'jquery',
    'underscore',
    'backbone',
    'app/views/Models/BaseSection',
    'views/shared/controls/TextControl',
    'app/views/Models/SingleInputControl',
    'app/views/Models/ControlWrapper'
],function(
    $,
    _,
    Backbone,
    BaseSection,
    TextControl,
    SingleInputControl,
    ControlWrapper
){
    return BaseSection.extend({
      initialize: function () {
          BaseSection.prototype.initialize.apply(this, arguments);

          this.control = new Backbone.Model();

          var monthly_report = [
              {value:'none',label:'None'},
              {value:'monthly_report',label:'Monthly Report',default:true},
              {value:'monthly_cost_allocation_report',label:'Monthly Cost Allocation Report'}
          ]

          this.children.monthlyReport = new ControlWrapper({
              label: _('Monthly Report').t(),
              controlType: SingleInputControl,
              wrapperClass: 'monthlyreport',
              controlOptions: {
                  model: this.control,
                  modelAttribute: 'monthlyreport',
                  disableSearch: true,
                  autoCompleteFields: monthly_report
              }
          });

          var detail_report = [
              {value:'none',label:'None'},
              {value:'detailed_billing_report',label:'Detailed Billing Report',default:true},
              {value:'detailed_billing_report_with_resrouces_and_tags',label:'Detailed Billing Report with Resources and Tags'}
          ]
          this.children.detailReport = new ControlWrapper({
              label: _('Detail Report').t(),
              controlType: SingleInputControl,
              wrapperClass: 'detailreport',
              controlOptions: {
                  model: this.control,
                  modelAttribute: 'detailreport',
                  disableSearch: true,
                  autoCompleteFields: detail_report
              }
          });

          this.children.regexForReport = new ControlWrapper({
              label: _('Regex for Report Selection').t(),
              controlType: TextControl,
              wrapperClass: 'regexforreport',
              controlOptions: {
                  model: this.control,
                  modelAttribute: 'regexforreport'
              }
          });
      },

      renderContent: function($body){
          $body.append(this.children.monthlyReport.render().$el);
          $body.append(this.children.detailReport.render().$el);
          $body.append(this.children.regexForReport.render().$el);
          return this;
      }
    });
});
