define([
    'jquery',
    'underscore',
    'backbone',
    'app/collections/ProxyBase.Collection',
    'app/models/Service.Billing.Model'
  ],
function(
    $,
    _,
    Backbone,
    BaseCollection,
    BillingInput
) {
    return BaseCollection.extend({
        url: 'splunk_ta_aws/inputs/billing',
        model: BillingInput,
        initialize: function(attributes, options){
            BaseCollection.prototype.initialize.call(this, attributes, options);
        }
    });
});
