define([
    'jquery',
    'underscore',
    'backbone',
    'app/collections/ProxyBase.Collection',
    'app/models/Cloudwatch.Settings.Model'
  ],
function(
    $,
    _,
    Backbone,
    BaseCollection,
    MetricNamespaceModel
) {
    return BaseCollection.extend({
        url: 'splunk_ta_aws/splunk_ta_aws_cloudwatch_default_settings',
        model: MetricNamespaceModel,
        initialize: function(attributes, options){
            BaseCollection.prototype.initialize.call(this, attributes, options);
        }
    });
});
