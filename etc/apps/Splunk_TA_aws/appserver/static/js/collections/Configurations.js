define([
    'jquery',
    'underscore',
    'backbone',
    'app/models/Configuration'],
function(
    $,
    _,
    Backbone,
    Configuration
) {
    return Backbone.Collection.extend({
        url: '/en-US/custom/Splunk_TA_aws/manage_configurations/configurations',
        model: Configuration

    });
});
