define([
    'jquery',
    'underscore',
    'backbone',
    'app/collections/ProxyBase.Collection',
    'app/models/Service.Kinesis.Model'
  ],
function(
    $,
    _,
    Backbone,
    BaseCollection,
    DescriptionInput
) {
    return BaseCollection.extend({
        url: 'splunk_ta_aws/inputs/kinesis',
        model: DescriptionInput,
        initialize: function(attributes, options){
            BaseCollection.prototype.initialize.call(this, attributes, options);
        }
    });
});
