define([
    'jquery',
    'underscore',
    'backbone',
    'app/collections/ProxyBase.Collection',
    'app/models/Account',
    'splunk.util'
  ],
function(
    $,
    _,
    Backbone,
    BaseCollection,
    Account,
    splunkdUtils
) {
    return BaseCollection.extend({
        url: 'splunk_ta_aws/settings/account4ui',
        model: Account,
        initialize: function(attributes, options){
            BaseCollection.prototype.initialize.call(this, attributes, options);
        }
    });
});
