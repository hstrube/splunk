define([
    'jquery',
    'underscore',
    'backbone',
    'app/collections/ProxyBase.Collection',
    'app/models/Service.Config.Model'
  ],
function(
    $,
    _,
    Backbone,
    BaseCollection,
    ConfigInput
) {
    return BaseCollection.extend({
        url: 'splunk_ta_aws/inputs/config',
        model: ConfigInput,
        initialize: function(attributes, options){
            BaseCollection.prototype.initialize.call(this, attributes, options);
        }
    });
});
