define([
    'jquery',
    'underscore',
    'backbone',
    'app/collections/ProxyBase.Collection',
    'app/models/Service.Logs.Model'
  ],
function(
    $,
    _,
    Backbone,
    BaseCollection,
    CloudTrailInput
) {
    return BaseCollection.extend({
        url: 'splunk_ta_aws/inputs/splunk_ta_aws_inputs_logs',
        model: CloudTrailInput,
        initialize: function(attributes, options){
            BaseCollection.prototype.initialize.call(this, attributes, options);
        }
    });
});
