define([
    'jquery',
    'underscore',
    'backbone',
    'app/collections/ProxyBase.Collection',
    'app/models/KinesisStream',
    'splunk.util'
  ],
function(
    $,
    _,
    Backbone,
    BaseCollection,
    KinesisStream,
    splunkdUtils
) {
    return BaseCollection.extend({
        url: 'splunk_ta_aws/splunk_ta_aws_kinesis_streams',
        model: KinesisStream,
        initialize: function(attributes, options){
            BaseCollection.prototype.initialize.call(this, attributes, options);
        }
    });
});
