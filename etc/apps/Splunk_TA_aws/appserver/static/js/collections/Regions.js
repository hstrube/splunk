define([
    'jquery',
    'underscore',
    'backbone',
    'app/collections/ProxyBase.Collection',
    'app/models/Region',
    'splunk.util'
  ],
function(
    $,
    _,
    Backbone,
    BaseCollection,
    Region,
    splunkdUtils
) {
    return BaseCollection.extend({
        url: 'splunk_ta_aws/splunk_ta_aws_regions',
        model: Region,
        initialize: function(attributes, options){
            BaseCollection.prototype.initialize.call(this, attributes, options);
        }
    });
});
