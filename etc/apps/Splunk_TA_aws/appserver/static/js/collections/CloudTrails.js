define([
    'jquery',
    'underscore',
    'backbone',
    'app/collections/ProxyBase.Collection',
    'app/models/Service.CloudTrail.Model'
  ],
function(
    $,
    _,
    Backbone,
    BaseCollection,
    CloudTrailInput
) {
    return BaseCollection.extend({
        url: 'splunk_ta_aws/inputs/cloudtrail',
        model: CloudTrailInput,
        initialize: function(attributes, options){
            BaseCollection.prototype.initialize.call(this, attributes, options);
        }
    });
});
