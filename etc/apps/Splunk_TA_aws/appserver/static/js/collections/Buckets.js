define([
    'jquery',
    'underscore',
    'backbone',
    'app/collections/ProxyBase.Collection',
    'app/models/Bucket',
    'splunk.util'
  ],
function(
    $,
    _,
    Backbone,
    BaseCollection,
    Bucket,
    splunkdUtils
) {
    return BaseCollection.extend({
        url: 'splunk_ta_aws/splunk_ta_aws_s3buckets',
        model: Bucket,
        initialize: function(attributes, options){
            BaseCollection.prototype.initialize.call(this, attributes, options);
        }
    });
});
