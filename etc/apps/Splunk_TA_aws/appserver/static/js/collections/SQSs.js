define([
    'jquery',
    'underscore',
    'backbone',
    'app/collections/ProxyBase.Collection',
    'app/models/Service.SQS.Model'
  ],
function(
    $,
    _,
    Backbone,
    BaseCollection,
    SQSInput
) {
    return BaseCollection.extend({
        url: 'splunk_ta_aws/inputs/sqs',
        model: SQSInput,
        initialize: function(attributes, options){
            BaseCollection.prototype.initialize.call(this, attributes, options);
        }
    });
});
