define([
    'jquery',
    'underscore',
    'backbone',
    'app/collections/ProxyBase.Collection',
    'app/models/IAMRole'
],
function(
    $,
    _,
    Backbone,
    BaseCollection,
    IAMRole
) {
    return BaseCollection.extend({
        url: 'splunk_ta_aws/settings/splunk_ta_aws_iam_role',
        model: IAMRole,
        initialize: function(attributes, options){
            BaseCollection.prototype.initialize.call(this, attributes, options);
        }
    });
});
