define([
    'jquery',
    'underscore',
    'backbone',
    'app/collections/ProxyBase.Collection',
    'app/models/Service.S3Universal.Model'
  ],
function(
    $,
    _,
    Backbone,
    BaseCollection,
    S3UniversalInput
) {
    return BaseCollection.extend({
        url: 'splunk_ta_aws/inputs/splunk_ta_aws_inputs_s3_universal',
        model: S3UniversalInput,
        initialize: function(attributes, options){
            BaseCollection.prototype.initialize.call(this, attributes, options);
        }
    });
});
