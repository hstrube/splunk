define([
    'jquery',
    'underscore',
    'backbone',
    'app/collections/ProxyBase.Collection',
    'app/models/Service.S3.Model'
  ],
function(
    $,
    _,
    Backbone,
    BaseCollection,
    S3Input
) {
    return BaseCollection.extend({
        url: 'splunk_ta_aws/inputs/s3',
        model: S3Input,
        initialize: function(attributes, options){
            BaseCollection.prototype.initialize.call(this, attributes, options);
        }
    });
});
