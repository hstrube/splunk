define([
    'jquery',
    'underscore',
    'backbone',
    'app/collections/ProxyBase.Collection',
    'app/models/Service.CloudWatch.Model'
  ],
function(
    $,
    _,
    Backbone,
    BaseCollection,
    CloudWatchInput
) {
    return BaseCollection.extend({
        url: 'splunk_ta_aws/inputs/cloudwatch',
        model: CloudWatchInput,
        initialize: function(attributes, options){
            BaseCollection.prototype.initialize.call(this, attributes, options);
        }
    });
});
