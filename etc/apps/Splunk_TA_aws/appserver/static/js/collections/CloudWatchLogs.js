define([
    'jquery',
    'underscore',
    'backbone',
    'app/collections/ProxyBase.Collection',
    'app/models/Service.CloudWatchLogs.Model'
  ],
function(
    $,
    _,
    Backbone,
    BaseCollection,
    CloudWatchLogInput
) {
    return BaseCollection.extend({
        url: 'splunk_ta_aws/inputs/cloudwatch-logs',
        model: CloudWatchLogInput,
        initialize: function(attributes, options){
            BaseCollection.prototype.initialize.call(this, attributes, options);
        }
    });
});
