define([
    'jquery',
    'underscore',
    'backbone',
    'app/collections/ProxyBase.Collection',
    'app/models/Service.Inspector.Model'
  ],
function(
    $,
    _,
    Backbone,
    BaseCollection,
    DescriptionInput
) {
    return BaseCollection.extend({
        url: 'splunk_ta_aws/inputs/inspector',
        model: DescriptionInput,
        initialize: function(attributes, options){
            BaseCollection.prototype.initialize.call(this, attributes, options);
        }
    });
});
