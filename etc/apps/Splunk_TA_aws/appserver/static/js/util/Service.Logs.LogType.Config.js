define([
    "app/views/ServiceSetting/SettingLogsCloudTrail",
    "app/views/ServiceSetting/SettingLogsS3Access"
], function(
    SettingLogsCloudTrail,
    SettingLogsS3Access
) {
    return {

        LOG_TYPE_LIST: [
            {
                value: "cloudtrail",
                label: "CloudTrail Logs"
            },
            {
                value: "s3:accesslogs",
                label: "S3 Access Logs"
            },
            {
                value: "cloudfront:accesslogs",
                label: "CloudFront Access Logs"
            },
            {
                value: "elb:accesslogs",
                label: "ELB Access Logs"
            }
        ],

        getSourceType: function (logType){
            var sourceTypeMapping = {
                "cloudtrail": "aws:cloudtrail",
                "s3:accesslogs": "aws:s3:accesslogs",
                "cloudfront:accesslogs": "aws:cloudfront:accesslogs",
                "elb:accesslogs": "aws:elb:accesslogs"
            }
            return sourceTypeMapping[logType] || "";
        },

        getSettings: function(logType) {
            var settingsMapping = {
                "cloudtrail": SettingLogsCloudTrail,
                "s3:accesslogs": SettingLogsCloudTrail,
                "cloudfront:accesslogs": SettingLogsS3Access,
                "elb:accesslogs": SettingLogsCloudTrail
            };
            return settingsMapping[logType] || SettingLogsCloudTrail;
        }
    };
});