define(function() {
    var config = {
        API_LIST: [
            "ec2_volumes",
            "ec2_instances",
            "ec2_reserved_instances",
            "ebs_snapshots",
            "elastic_load_balancers",
            "vpcs",
            "vpc_network_acls",
            "cloudfront_distributions",
            "vpc_subnets",
            "rds_instances",
            "ec2_key_pairs",
            "ec2_security_groups",
            "ec2_images",
            "ec2_addresses",
            "lambda_functions"
        ],
        INTERVAL_DEFAULT_VALUES: {
            "ec2_volumes": "3600",
            "ec2_instances": "3600",
            "ec2_reserved_instances": "3600",
            "ebs_snapshots": "3600",
            "elastic_load_balancers": "3600",
            "vpcs": "3600",
            "vpc_network_acls": "3600",
            "cloudfront_distributions": "3600",
            "vpc_subnets": "3600",
            "rds_instances": "3600",
            "ec2_key_pairs": "3600",
            "ec2_security_groups": "3600",
            "ec2_images": "3600",
            "ec2_addresses": "3600",
            "lambda_functions": "3600"
        },
        INTERVAL_DEFAULT_VALUE: "3600",
        API_TO_LABEL: {
            "ec2_volumes": "ec2_volumes",
            "ec2_instances": "ec2_instances",
            "ec2_reserved_instances": "ec2_reserved_instances",
            "ebs_snapshots": "ebs_snapshots",
            "elastic_load_balancers": "elastic_load_balancers",
            "vpcs": "vpcs",
            "vpc_network_acls": "vpc_network_acls",
            "cloudfront_distributions": "cloudfront_distributions",
            "vpc_subnets": "vpc_subnets",
            "rds_instances": "rds_instances",
            "ec2_key_pairs": "ec2_key_pairs",
            "ec2_security_groups": "ec2_security_groups",
            "ec2_images": "ec2_images",
            "ec2_addresses": "ec2_addresses",
            "lambda_functions": "lambda_functions"
        },
        API_INTERVAL_INPUT_DEFAULT_VALUE: "ec2_volumes/3600,ec2_instances/3600,ec2_reserved_instances/3600,ebs_snapshots/3600,elastic_load_balancers/3600,vpcs/3600,vpc_network_acls/3600,cloudfront_distributions/3600,vpc_subnets/3600,rds_instances/3600,ec2_key_pairs/3600,ec2_security_groups/3600,ec2_images/3600,ec2_addresses/3600,lambda_functions/3600",
    };

    return config;
});