define([
    "jquery"
], function(
    $
){
    var util = {};

    util.eventHandler = function(e) {
        var $el = $(e.currentTarget);
        if ($el.hasClass("disabled")){
            return;
        }
        var checkedStatus = $el.attr("checked");
        if (checkedStatus) {
            $el.removeAttr("checked");
            $el.find("i.icon-check").attr("style", "display:none;");
            $el.trigger("change", {checked: false});
        } else {
            $el.attr("checked", "checked");
            $el.find("i.icon-check").removeAttr("style");
            $el.trigger("change", {checked: true});
        }
    };

    return util;
});