define([
    'app/views/ServiceSetting/SettingS3',
    'app/views/ServiceSetting/SettingS3CloudTrail',
    'app/views/ServiceSetting/SettingS3Accesslogs'

], function(
    SettingS3,
    SettingS3CloudTrail,
    SettingS3Accesslogs

) {
    var config = {
        SOURCETYPE_LIST: [
            "aws:s3",
            "aws:cloudtrail",
            "aws:s3:accesslogs",
            "aws:cloudfront:accesslogs",
            "aws:elb:accesslogs"
        ],
        getSettingsBySourcetype: function(sourcetype) {
            var settingsMapping = {
                "aws:s3": SettingS3,
                "aws:cloudtrail": SettingS3CloudTrail,
                "aws:s3:accesslogs": SettingS3Accesslogs,
                "aws:cloudfront:accesslogs": SettingS3Accesslogs,
                "aws:elb:accesslogs": SettingS3Accesslogs
            };
            return settingsMapping[sourcetype] || SettingS3;
        }
    };

    return config;
});