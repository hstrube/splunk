define([
    'underscore'
], function(_) {
    var util = {};

    util.display_names = {
        "ap-northeast-1": "Asia Pacific (Tokyo)",
        "ap-northeast-2": "Asia Pacific (Seoul)",
        "ap-southeast-1": "Asia Pacific (Singapore)",
        "ap-southeast-2": "Asia Pacific (Sydney)",
        "ap-south-1": "Asia Pacific (Mumbai)",
        "cn-north-1": "China (Beijing)",
        "eu-central-1": "EU (Frankfurt)",
        "eu-west-1": "EU (Ireland)",
        "eu-west-2": "EU (London)",
        "sa-east-1": "South America (Sao Paulo)",
        "us-east-1": "US East (N. Virginia)",
        "us-east-2": "US East (Ohio)",
        "us-gov-west-1": "GovCloud (US)",
        "us-west-1": "US West (N. California)",
        "us-west-2": "US West (Oregon)",
        "ca-central-1": "Canada (Central)"
    };

    util.categories = {
        "ap-northeast-1": "commercial",
        "ap-northeast-2": "commercial",
        "ap-southeast-1": "commercial",
        "ap-southeast-2": "commercial",
        "ap-south-1": "commercial",
        "eu-central-1": "commercial",
        "eu-west-1": "commercial",
        "eu-west-2": "commercial",
        "sa-east-1": "commercial",
        "us-east-1": "commercial",
        "us-east-2": "commercial",
        "us-west-1": "commercial",
        "us-west-2": "commercial",
        "ca-central-1": "commercial",
        "cn-north-1": "china",
        "us-gov-west-1": "government"
    };
    util.default_category = "commercial";

    
    util.getDisplayName = function(region_name) {
        return this.display_names[region_name] || region_name;
    };
    
    util.getCategory = function (region_name) {
        return this.categories[region_name] || this.default_category;
    };

    return util;
});
