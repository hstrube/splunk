define([
    'underscore',
    "app/util/RegionUtil"
], function(_, RegionUtil) {
    var util = {};

    /**
     * bin 001 = dec 1 = cmc
     * bin 010 = dec 2 = gov
     * bin 011 = dec 3 = cmc,gov
     * bin 100 = dec 4 = chn
     * bin 101 = dec 5 = cmc,chn
     * bin 110 = dec 6 = gov,chn
     * bin 111 = dec 7 = cmc,gov,chn

     * Specially, bin 000 = dec 0 = all
     */

    util.getCategoryCode = function(bCmc, bGov, bChn) {
        var ret = 0;
        if (bCmc){
            ret = ret | 1;
        }
        if (bGov){
            ret = ret | 2;
        }
        if (bChn){
            ret = ret | 4;
        }
        return ret;
    };

    util.parseCategoryCode = function(code){
        if (code < 1 || code > 7){
            return {
                commercial: false,
                government: false,
                china: false
            };
        }
        var bCmc = !!(code & 1);
        var bGov = !!((code >> 1) & 1);
        var bChn = !!((code >> 2) & 1);
        return {
            commercial: bCmc,
            government: bGov,
            china: bChn
        };
    };

    util.getAccessableRegionList = function(code, oriList){
        var parseResult = util.parseCategoryCode(+code);
        return _.filter(oriList, function(region){
            return parseResult[RegionUtil.getCategory(region)];
        });
    };

    return util;
});
