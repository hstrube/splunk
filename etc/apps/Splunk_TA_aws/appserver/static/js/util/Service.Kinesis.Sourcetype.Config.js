define([
], function(
) {
    var config = {
        SOURCETYPE_LIST: [
            "aws:kinesis",
            "aws:cloudwatchlogs:vpcflow"
        ]
    };

    return config;
});
