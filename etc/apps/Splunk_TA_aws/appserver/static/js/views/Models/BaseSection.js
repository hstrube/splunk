define([
    'jquery',
    'underscore',
    'backbone',
    'views/Base'
], function ($, _, Backbone, BaseView) {


    return BaseView.extend({
        initialize: function (options) {
            BaseView.prototype.initialize.apply(this, arguments);
            this.container = options.container;
            this.real_model = options.real_model;

            if (!this.container.loadStatusStarted)
                this.container.loadStatusStarted = { showing: {}, jobs: {}};
            if (!this.container.loadStatusFailed)
                this.container.loadStatusFailed = { showing: {}, jobs: {}};
        },

        render: function () {
            this.$el.html(this.compiledTemplate({
                label: this.label,
                allowExpand: this.allowExpand
            }));
            this.renderContent(this.$('.section-body'));
            return this;
        },

        renderContent: function (/*$content*/) {
            // should be override by sub module
        },

        template: '<div class="section-body"><div/>',

        // call this when loading list for a model's attribute
        startLoadingForModel: function(model, attr) {
            console.log("startLoadingForModel: attr: " + attr);

            var msg = "Loading options for " + model.getAttrLabel(attr) + ".";
            return this._addStartJobMessage(attr, msg);
        },

        _addStartJobMessage: function(jobKey, msg) {
            // get a random job id
            var jobId = String(Math.random());
            while (this.container.loadStatusStarted.jobs[jobId])
                jobId = String(Math.random());

            // remove all previous shown or pending failure
            this._removePreviousFailure(jobKey);

            // add it into list
            var job = {jobId: jobId, jobKey: jobKey, msg: msg};
            this.container.loadStatusStarted.jobs[jobId] = job;

            // and show it
            this.container.loadStatusStarted.showing = job;
            this.container.addLoadingMsg(msg);

            return job;
        },

        // called when one task is finished
        _removeOngoingJob: function(job) {
            // delete it
            delete this.container.loadStatusStarted.jobs[job.jobId];

            // if it's just showing, remove it either and show next
            if (this.container.loadStatusStarted.showing.jobId) {
                var keys = _.keys(this.container.loadStatusStarted.jobs);
                if (keys.length >= 1) {
                    var newJob = this.container.loadStatusStarted.jobs[keys[0]];
                    this.container.loadStatusStarted.showing = newJob;
                    this.container.addLoadingMsg(newJob.msg);
                } else {
                    this.container.loadStatusStarted.showing = {};
                    this.container.removeLoadingMsg();
                }
            }
        },

        // called when a new job started
        _removePreviousFailure: function(newJobKey) {
            // it's a new job, remove all failed result with same jobKey
            var matchJobs = _.filter(_.values(this.container.loadStatusFailed.jobs),
                                     function(val) {return newJobKey == val.jobKey;});
            _.each(matchJobs, function(val) {
                delete this.container.loadStatusFailed.jobs[val.jobId];
            }.bind(this) );

            // if it's just showing, remove it either and show next
            if (this.container.loadStatusFailed.showing.jobKey == newJobKey) {
                var keys = _.keys(this.container.loadStatusFailed.jobs);
                if (keys.length >= 1) {
                    var newJob = this.container.loadStatusFailed.jobs[keys[0]];
                    this.container.loadStatusFailed.showing = newJob;
                    this.container.addErrorMsg(this.container.loadStatusFailed.jobs[newJob.msg]);
                } else {
                    this.container.loadStatusFailed.showing = {};
                    this.container.removeErrorMsg();
                }
            }
        },

        // directly put the failure message in job
        // suppose the failed message is in job.msg
        showJobFailure: function(job, model, attr, ajaxRet, errMsg) {
            console.log("showJobFailure: id: " + job.jobId + ", attr: " + attr);

            // if it's current loading task, finish it
            this._removeOngoingJob(job);

            // if current is showing some failed results. back-up it
            var showingJob = this.container.loadStatusFailed.showing;
            if (showingJob.jobId) {
                this.container.loadStatusFailed.jobs[showingJob.jobId] = showingJob;
            }

            // and show latest failure
            this.container.loadStatusFailed.showing = job;
            if (ajaxRet) {
                var firstLine = 'Failed to load options for ' + model.getAttrLabel(attr) +
                    '. Detailed Error: ' + this.container.parseAjaxError(ajaxRet);
                var msg = [
                    firstLine,
                    "",
                    "Possible reasons could be:",
                    "",
                    "1. Incorrect key ID and/or secret key. ",
                    "2. Internet connection is not stable or AWS server is down.",
                    "3. Your account do not have permission to access regions.",
                    "4. You may need to select a proper IAM role to assume.",
                    "",
                    "Please check above factors and try again."
                ];
                this.container.addErrorMsg(msg);
            }
            else {
                this.container.addErrorMsg(errMsg);
            }

        },

        showJobNoData: function(job, model, attr, reason) {
            console.log("showJobNoData: id: " + job.jobId + ", attr: " + attr);
            var msg = "No options found for " + model.getAttrLabel(attr) + ".";
            if (reason){
                msg = [
                    msg + " Possible reasons could be:",
                    ""
                ];
                var reasons;
                if (!_.isArray(reason)){
                    reasons = [reason];
                } else {
                    reasons = reason;
                }
                _(reasons).each(function(reason, index){
                    msg.push((index + 1) + ". " + reason);
                });
                msg.push("");
                msg.push("Please check the above factors and try again.");
            }
            this.showJobFailure(job, model, attr, null, msg);
        },

        //
        showJobSuccess: function(job, model, attr) {
            console.log("showJobSuccess: id: " + job.jobId + ", attr: " + attr);

            // if it's current loading task, finish it
            this._removeOngoingJob(job);
        },

        getDefaultDropdownlistItem: function(attr) {
            var selected_value = this.model.get(attr);
            var selected_value_item = [];
            if (selected_value)
                selected_value_item = [{label: selected_value, value: selected_value}];

            return selected_value_item;
        },

        isModelValueInDropdownList: function(attr, data) {
            var selected_value = this.model.get(attr);
            return _.find(data, function(item){ return item.value === selected_value; }) !== undefined;
        },

        ensureModelValueInDropdownList: function(attr, data) {
            var selected_value_item = this.getDefaultDropdownlistItem(attr);

            if (_.find(data, function(item){ return item.value === selected_value_item.value; }) === undefined)
                data = data.concat(selected_value_item);

            return data;
        }

    });
});
