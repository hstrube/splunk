define([
    'jquery',
    'underscore',
    'views/shared/controls/Control',
    'splunk.util',
    'select2/select2'
], function(
    $,
    _,
    Control,
    splunkUtils
) {
    /**
     * Radio button Group
     *
     * @param {Object} options
     *                        {Object} model The model to operate on
     *                        {String} modelAttribute The attribute on the model to observe and update on selection
     *                        {Object} items An array of one-level deep data structures:
     *                                      label (textual display),
     *                                      value (value to store in model)
     */
    var DELIMITER = '::::';

    var NO_MATCH_STRING = "No matches found.";

    return Control.extend({
        className: 'control multiselect-input-control splunk-multidropdown splunk-chioce-input',
        // moduleId: module.id,
        initialize: function() {
            if (this.options.modelAttribute) {
                this.$el.attr('data-name', this.options.modelAttribute);
            }
            this._noMatchString = NO_MATCH_STRING;
            Control.prototype.initialize.call(this, this.options);
        },
        render: function() {
            var _this = this;
            this.$el.html(this.compiledTemplate({
                items: this.options.items
            }));
            var $select = this.$('select');
            $select.select2({
                placeholder: this.options.placeholder,
                formatNoMatches: function() {
                    return _this._noMatchString;
                },
                value: this._value,
                dropdownCssClass: 'empty-results-allowed',
                separator: DELIMITER,
                // SPL-77050, this needs to be false for use inside popdowns/modals
                openOnEnter: false
            }).select2('val', splunkUtils.stringToFieldList(this._value || ''));
            if (_.isEqual($select.select2("val").sort(), this._fullList)) {
                this._noMatchString = "";
            }
            return this;
        },
        setItems: function(items) {
            this.options.items = items;
            this._fullList = _.map(items, function(item) {
                return item.value;
            }).sort();
            this.render();

            var items_selected = splunkUtils.stringToFieldList(this._value || '');
            var items_all = [];
            _.each(this.options.items, function(item) {
                items_all.push(item.value);
            }.bind(this));

            this.error_items = [];
            var items_new = [];
            _.each(items_selected, function(item) {
                if (items_all.indexOf(item)<0){
                    this.error_items.push(item);
                } else {
                    items_new.push(item);
                }
            }.bind(this));
            this._value = splunkUtils.fieldListToString(items_new);
        },
        remove: function() {
            this.$('select').select2('close').select2('destroy');
            return Control.prototype.remove.apply(this, arguments);
        },
        events: {
            'change select': function(e) {
                var $select = this.$('select');
                if (_.isEqual($select.select2("val").sort(), this._fullList)) {
                    this._noMatchString = "";
                } else {
                    this._noMatchString = NO_MATCH_STRING;
                }
                var values = e.val || [];
                this.setValue(splunkUtils.fieldListToString(values), false);
            }
        },
        template: [
            '<select multiple="multiple">',
            '<% _.each(items, function(item, index){ %>',
            '<option value="<%- item.value %>"><%- item.label %></option>',
            '<% }) %>',
            '</select>'
        ].join('')
    });
});
