define(
    [
        'underscore',
        'jquery',
        'views/shared/PopTart',
        'app/views/Dialogs/AddNewInput',
        'app/views/Dialogs/Error',
        'app/collections/Accounts',
        'app/models/appData'
    ],
    function(
        _,
        $,
        PopTartView,
        AddNewInput,
        ErrorDialog,
        Accounts,
        appData
    ){
        return PopTartView.extend({
            className: 'dropdown-menu',
            initialize: function(options) {
                _.bindAll(this, 'create');
                PopTartView.prototype.initialize.apply(this, arguments);
                this.collection = options.collection;
                this.dispatcher = options.dispatcher;
                this.accounts = new Accounts([],{
                    appData: { app: appData.get("app"), owner: appData.get("owner") },
                    targetApp: "Splunk_TA_aws",
                    targetOwner: "nobody"
                });
            },

            events: {
                'click a': 'create'
            },

            render: function(){
                var html = this.compiledTemplate({});
                this.el.innerHTML = PopTartView.prototype.template_menu;
                this.$el.append(html);
                this.$el.addClass('dropdown-menu-narrow');
                return this;
            },

            create: function(e){
                this.checkAccount().done(function(){
                    if(this.accounts.models.length == 0){
                        var errorDialog = new ErrorDialog({
                            el: $('#addon_task_error_dialog'),
                            msg: 'Please create a account first under configuration page.'
                        });
                        errorDialog.render().modal();
                    }else{
                        this.service_type = $(e.target).attr('class')
                        var dlg = new AddNewInput({
                            el: $("#addon_task_edit_dialog"),
                            collection: this.collection,
                            service_type: this.service_type,
                            dispatcher: this.dispatcher
                        }).render();
                        dlg.modal();
                    }
                }.bind(this));
                this.hide();
            },

            checkAccount: function(){
                return this.accounts.fetch();
            },

            template:'\
                <ul class="first-group">\
                    <li><a href="#" class="cloudtrail">CloudTrail</a></li>\
                    <li><a href="#" class="config">Config</a></li>\
                    <li><a href="#" class="cloudwatch">CloudWatch</a></li>\
                    <li><a href="#" class="cloudwatchlog">CloudWatch Logs</a></li>\
                    <li><a href="#" class="description">Description</a></li>\
                    <li><a href="#" class="s3">S3</a></li>\
                    <li><a href="#" class="billing">Billing</a></li>\
                    <li><a href="#" class="kinesis">Kinesis</a></li>\
                    <li><a href="#" class="configrule">Config Rules</a></li>\
                    <li><a href="#" class="inspector">Inspector</a></li>\
                    <li><a href="#" class="sqs">SQS</a></li>\
                </ul>\
            '
        });
    }
);
