define(
    [
        'underscore',
        'jquery',
        'views/shared/PopTart'
    ],
    function(
        _,
        $,
        PopTartView
    ){
        return PopTartView.extend({
            className: 'dropdown-menu',

            events: {
                'click a.all': 'all',
                'click a.billing': 'billing',
                'click a.config': 'config',
                'click a.cloudtrail': 'cloudtrail',
                'click a.cloudwatch': 'cloudwatch',
                'click a.s3': 's3',
                'click a.description': 'description',
                'click a.cloudwatchlog': 'cloudwatchlog',
                'click a.kinesis': 'kinesis',
                'click a.configrule': 'configrule',
                'click a.inspector': 'inspector',
                'click a.sqs': 'sqs',
                'click a.logs': 'logs'
            },

            initialize: function(options) {
                PopTartView.prototype.initialize.apply(this, arguments);
                this.dispatcher = options.dispatcher;
            },

            render: function(){
                var html = this.compiledTemplate({});
                this.el.innerHTML = PopTartView.prototype.template_menu;
                this.$el.append(html);
                this.$el.addClass('dropdown-menu-narrow');
                return this;
            },

            all: function(){
                this.hide();
                this.dispatcher.trigger('filter-change','all');
            },
            billing: function(){
                this.hide();
                this.dispatcher.trigger('filter-change','billing');
            },
            config: function(){
                this.hide();
                this.dispatcher.trigger('filter-change','config');
            },
            cloudtrail: function(){
                this.hide();
                this.dispatcher.trigger('filter-change','cloudtrail');
            },
            cloudwatch: function(){
                this.hide();
                this.dispatcher.trigger('filter-change','cloudwatch');
            },
            s3: function(){
                this.hide();
                this.dispatcher.trigger('filter-change','s3');
            },
            description: function(){
                this.hide();
                this.dispatcher.trigger('filter-change','description');
            },
            cloudwatchlog: function(){
                this.hide();
                this.dispatcher.trigger('filter-change','cloudwatchlog');
            },
            kinesis: function(){
                this.hide();
                this.dispatcher.trigger('filter-change','kinesis');
            },
            configrule: function(){
                this.hide();
                this.dispatcher.trigger('filter-change','configrule');
            },
            inspector: function(){
                this.hide();
                this.dispatcher.trigger('filter-change','inspector');
            },
            sqs: function(){
                this.hide();
                this.dispatcher.trigger('filter-change','sqs');
            },
	    
            logs: function(){
                this.hide();
                this.dispatcher.trigger('filter-change','logs');
            },

            template:'\
                <ul class="first-group">\
                    <li><a href="#" class="all"><%- _("All").t() %></a></li>\
                </ul>\
                <ul class="second-group">\
                    <li><a href="#" class="cloudtrail">CloudTrail</a></li>\
                    <li><a href="#" class="config">Config</a></li>\
                    <li><a href="#" class="cloudwatch">CloudWatch</a></li>\
                    <li><a href="#" class="cloudwatchlog">CloudWatch Logs</a></li>\
                    <li><a href="#" class="description">Description</a></li>\
                    <li><a href="#" class="s3">S3</a></li>\
                    <li><a href="#" class="billing">Billing</a></li>\
                    <li><a href="#" class="kinesis">Kinesis</a></li>\
                    <li><a href="#" class="configrule">Config Rules</a></li>\
                    <li><a href="#" class="inspector">Inspector</a></li>\
                    <li><a href="#" class="sqs">SQS</a></li>\
                </ul>\
            '
        });
    }
);
