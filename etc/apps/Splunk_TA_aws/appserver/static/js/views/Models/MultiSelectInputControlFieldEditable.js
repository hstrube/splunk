define([
    'jquery',
    'underscore',
    'views/shared/controls/Control',
    'splunk.util',
    'app/util/EditablePopoverUtil',
    'select2/select2'
], function(
    $,
    _,
    Control,
    splunkUtils,
    EditablePopoverUtil
) {
    /**
     * Multiple select control with editable fields
     *
     * @param {Object} options
     *                        {Object} model The model to operate on
     *                        {String} modelAttribute The attribute on the model to observe and update on selection
     *                        {Object} items The attribute set by setItems and following certain struture
     */
    var DELIMITER = '::::';

    var NO_MATCH_STRING = "No matches found.";

    function findItem(items, id) {
        return _.find(items, function(item) {
            return item.mainField.id === id;
        });
    }

    return Control.extend({
        className: 'control multiselect-input-control splunk-multidropdown splunk-chioce-input',
        // moduleId: module.id,
        initialize: function() {
            if (this.options.modelAttribute) {
                this.$el.attr('data-name', this.options.modelAttribute);
            }
            this._noMatchString = NO_MATCH_STRING;
            this.options.fieldDelimiter = "/";
            Control.prototype.initialize.call(this, this.options);
            this.compliedSelectContentTemplate = _.template(this._templateSelectContent);
            this.compliedDropdownContentTemplate = _.template(this._templateDropdownContent);
        },
        render: function() {
            var _this = this;
            this.$el.html(this.compiledTemplate({
                items: this.options.items,
            }));
            var compliedSelectContentTemplate = this.compliedSelectContentTemplate;
            var compliedDropdownContentTemplate = this.compliedDropdownContentTemplate;
            var fieldDelimiter = this.options.fieldDelimiter;
            var $popContainer = this.$("div.popover-container");
            var $select = this.$('select');
            $select.select2({
                placeholder: this.options.placeholder,
                formatNoMatches: function() {
                    return _this._noMatchString;
                },
                dropdownCssClass: 'empty-results-allowed',
                separator: DELIMITER,
                formatSelection: function(state) {
                    var item = findItem(_this.options.items, state.id);

                    _.each(item.editableFields, function(field) {
                        if (field.value == null) {
                            field.value = field.defaultValue;
                        }
                    });

                    return compliedSelectContentTemplate({
                        item: item,
                        delimiter: fieldDelimiter
                    });
                },
                formatResult: function(state) {
                    var item = findItem(_this.options.items, state.id);
                    return compliedDropdownContentTemplate({
                        item: item,
                        delimiter: fieldDelimiter
                    });
                },
                // SPL-77050, this needs to be false for use inside popdowns/modals
                openOnEnter: false
            });

            this._setInitValue();

            EditablePopoverUtil.addPopover(this.$(".editable-field"), {
                container: $popContainer,
                placement: "top",
                scope: _this,
                onConfirming: function($el, val){
                    var selectItemId = $el.parent().find(".select-item").data("id");
                    return this._validateFieldInput(selectItemId, $el.data("name"), val);
                },
                onConfirmed: function($el, val) {
                    var selectItemId = $el.parent().find(".select-item").data("id");
                    this._setFieldValue(selectItemId, $el.data("name"), val);
                }
            });

            return this;
        },
        setFieldDelimiter: function(fieldDelimiter) {
            this.options.fieldDelimiter = fieldDelimiter;
            return this;
        },
        _setInitValue: function() {
            var val = _.chain(this.options.items).filter(function(item) {
                return item.mainField.initialSelected;
            }).map(function(item) {
                return item.mainField.id;
            }).value();
            if (val && val.length) {
                this.$("select").select2("val", val);
                if (_.isEqual(val.sort(), this._fullList)) {
                    this._noMatchString = "";
                }
            }
        },
        _setFieldValue: function(itemId, fieldName, fieldValue) {
            var item = findItem(this.options.items, itemId);
            var field = _.find(item.editableFields, function(f) {
                return f.name === fieldName;
            });
            if (field.value + "" !== fieldValue + "") {
                field.value = fieldValue;
                var $select = this.$('select');
                // reset select2's value to refresh the result.
                var val = $select.select2("val");
                $select.select2("val", "").select2("val", val);
                this._setControlValue(val, this.options.items);
            }
        },
        _validateFieldInput: function(itemId, fieldName, fieldValue) {
            var item = findItem(this.options.items, itemId);
            var field = _.find(item.editableFields, function(f) {
                return f.name === fieldName;
            });
            if (_.isFunction(field.validator)){
                return field.validator(fieldValue);
            } else {
                return true;
            }
        },
        _setControlValue: function(values, items){
            // FIXME try better way to set control value.
            var delimiter = this.options.fieldDelimiter;
            var value = _.chain(values).map(function(val) {
                var item = findItem(items, val);
                return [val].concat(_.map(item.editableFields, function(f) {
                    return f.value || f.defaultValue;
                })).join(delimiter);
            }).value().join(", ");
            this.setValue(value, false, {
                silent: true
            });
        },
        setItems: function(items) {
            /**
             * @param items should be contructed in following format:
             *     [{
             *         mainField: {
             *             id: "...",
             *             label: "...",
             *             initialSelected: true||false // whether initial selected or not.
             *         },
             *         editableFields: [{
             *             name: "...", // would be filtered if null or empty
             *             defaultValue: "...", // could be plain object, would be " " if null or empty
             *             type: "...", // would be text if null or empty
             *             formatter: function(value){}, // format the default value before rendering
             *             unit: "...", // unit of the editable field
             *             title: "...", // title of the popover dialog
             *             value: "..." // initial value of the field if this item is initially seleted
             *             config: {
             *                 // would be passed in popover template.
             *             },
             *             validator: function(){},// the validator before onConfirmed is called.
             *             errormessage: "..."// the error message which shows when value is invalid.
             *         }, {
             *             // other editable fields.
             *         }]
             *     }, {
             *        // other items
             *     }]
             */
             this._fullList = _.map(items, function(item) {
                 return item.mainField.id;
             }).sort();

            this.options.items = items;
        },
        remove: function() {
            this.$('select').select2('close').select2('destroy');
            return Control.prototype.remove.apply(this, arguments);
        },
        events: {
            'change select': function(e) {
                this._setControlValue(e.val, this.options.items);
                // reset select2's value to keep the result in order.
                var $select = this.$('select');
                var val = $select.select2("val");
                if (_.isEqual(val.sort(), this._fullList)) {
                    this._noMatchString = "";
                } else {
                    this._noMatchString = NO_MATCH_STRING;
                }
                $select.select2("val", "").select2("val", val);
            },
            'select2-focus select': function(){
                EditablePopoverUtil.hideAll();
            },
            'select2-removing select': function(e){
                var item = findItem(this.options.items, e.val);
                _.each(item.editableFields, function(field) {
                    delete field.value;
                });
                EditablePopoverUtil.removePopover(this.$("[data-id='" + e.val + "']~a.editable-field"));
            }
        },
        template: [
            '<select multiple="multiple">',
            '<% _.each(items, function(item){ %>',
            '<option value="<%- item.mainField.id %>"><%- item.mainField.label %></option>',
            '<% }) %>',
            '</select>',
            '<div class="popover-container" style="position:fixed;z-index:1000;"></div>'
        ].join(''),
        _templateSelectContent: [
            '<span>',
            '<span class="select-item" data-id="<%- item.mainField.id %>"><%- item.mainField.label %></span>',
            '<% _.each(item.editableFields, function(field){ %>',
            '<%- delimiter %>',
            '<a class="editable-field" ',
            'data-name="<%- field.name %>" ',
            'data-value="<%- field.value %>" ',
            'data-type="<%- field.type %>" ',
            'data-title="<%- field.title? field.title:\"\" %>" ',
            'data-unit="<%- field.unit? field.unit:\"\" %>" ',
            'data-errormessage="<%- field.errormessage? field.errormessage:\"\" %>" ',
            'data-config="<%- field.config? JSON.stringify(field.config):\"{}\" %>">',
            '<%- _.isFunction(field.formatter) ? field.formatter(field.value) : field.value %>',
            '</a>',
            '<% }); %>',
            '</span>'
        ].join(''),
        _templateDropdownContent: [
            '<span>',
            '<span class="dropdown-item" data-id=<%- item.mainField.id %>><%- item.mainField.label %></span>',
            '<% _.each(item.editableFields, function(field){ %>',
            '<%- delimiter %>',
            '<span class="optional-field" >',
            '<%- _.isFunction(field.formatter) ? field.formatter(field.defaultValue) : (field.defaultValue) %>',
            '</span>',
            '<% }); %>',
            '</span>'
        ].join('')
    });
});
