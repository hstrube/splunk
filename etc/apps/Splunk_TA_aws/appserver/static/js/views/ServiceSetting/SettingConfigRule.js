define([
    'jquery',
    'underscore',
    'backbone',
    'app/models/appData',
    'app/views/Models/BaseSection',
    'views/shared/controls/TextControl',
    'app/views/Models/SingleInputControlEx',
    'app/views/Models/MultiSelectInputControl',
    'app/views/Models/ControlWrapper',
    'app/collections/ConfigRuleNames'

], function (
    $,
    _,
    Backbone,
    appData,
    BaseSection,
    TextControl,
    SingleInputControlEx,
    MultiSelectInputControl,
    ControlWrapper,
    ConfigRuleNames
) {
    return BaseSection.extend({
        initialize: function () {
            BaseSection.prototype.initialize.apply(this, arguments);

            this.children.rule_names = new ControlWrapper({
                label: _('Rule Names').t(),
                controlType: MultiSelectInputControl,
                wrapperClass: 'rule_names',
                controlOptions: {
                    model: this.model,
                    allowClear: true,
                    modelAttribute: 'rule_names',
                    placeholder: _("Leave blank to select all rules").t(),
                    disableSearch: false
                }
            });
            this.loadDataSyncUI();
            this.model.on("change:aws_account", this.loadDataSyncUI.bind(this));
            this.model.on("change:aws_region", this.loadDataSyncUI.bind(this));
        },


        // just called once
        loadDataSyncUI: function () {
            var aws_account = this.model.get("aws_account");
            var aws_region = this.model.get("aws_region");
            var attr = 'rule_names';
            if (!aws_account || !aws_region) {
                return;
            }

            var names = new ConfigRuleNames([], {
                appData: { app: appData.get("app"), owner: appData.get("owner") },
                targetApp: "-",
                targetOwner: "nobody"
            });
            names.deferred = names.fetch({
                data: {
                    aws_account: this.model.get("aws_account"),
                    aws_region: this.model.get("aws_region")
                }
            });
            var job = this.startLoadingForModel(this.real_model, attr);

            names.deferred.done(function () {
                // load AWS Account into list
                var data = _.map(names.models[0].entry.content.attributes.config_rules, function (stream) {
                    return {value: stream, label: stream};
                });

                if (data.length >= 1)
                    this.showJobSuccess(job, this.real_model, attr);
                else
                    this.showJobNoData(job, this.real_model, attr);

                this.children.rule_names.control.setItems(data, true);

            }.bind(this)).fail(function(ajaxRet) {
                this.showJobFailure(job, this.real_model, attr, ajaxRet);
            }.bind(this));

        },

        renderContent: function ($body) {
            $body.append(this.children.rule_names.render().$el);
            return this;
        }
    });
});
