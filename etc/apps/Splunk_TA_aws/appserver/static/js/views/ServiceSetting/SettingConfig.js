define([
    'jquery',
    'underscore',
    'backbone',
    'app/models/appData',
    'app/views/Models/BaseSection',
    'views/shared/controls/TextControl',
    'app/views/Models/SingleInputControl',
    'app/views/Models/SingleInputControlEx',
    'app/views/Models/ControlWrapper',
    'app/collections/SQSQueues',
    "contrib/text!app/templates/Models/LoadingMsg.html",
    "contrib/text!app/templates/Models/ErrorMsg.html"

], function (
    $,
    _,
    Backbone,
    appData,
    BaseSection,
    TextControl,
    SingleInputControl,
    SingleInputControlEx,
    ControlWrapper,
    SQSQueues,
    LoadingMsg,
    ErrorMsg
) {
    return BaseSection.extend({
        initialize: function () {
            BaseSection.prototype.initialize.apply(this, arguments);

            this.children.aws_sqs_queue = new ControlWrapper({
                label: _('SQS Queue Name').t(),
                controlType: SingleInputControlEx,
                wrapperClass: 'bucket_name',
                required: true,
                controlOptions: {
                    model: this.model,
                    allowClear: true,
                    modelAttribute: 'sqs_queue',
                    placeholder: _("Select a Queue").t(),
                    disableSearch: false,
                    autoCompleteFields: []
                }
            });

            this.loadDataSyncUI();
            this.model.on("change:aws_account", this.loadDataSyncUI.bind(this));
            this.model.on("change:aws_region", this.loadDataSyncUI.bind(this));
        },


        // just called once
        loadDataSyncUI: function () {
            var aws_account = this.model.get("aws_account");
            var aws_region = this.model.get("aws_region");
            var attr = 'sqs_queue';
            this.children.aws_sqs_queue.control.setAutoCompleteFields(this.getDefaultDropdownlistItem(attr), true);
            if (!aws_account || !aws_region) {
                return;
            }

            var sqsQueues = new SQSQueues([], {
                appData: { app: appData.get("app"), owner: appData.get("owner") },
                targetApp: "-",
                targetOwner: "nobody"
            });
            sqsQueues.deferred = sqsQueues.fetch({
                data: {
                    aws_account: this.model.get("aws_account"),
                    aws_region: this.model.get("aws_region")
                }
            });
            var job = this.startLoadingForModel(this.real_model, attr);

            sqsQueues.deferred.done(function () {
                // load AWS Account into list
                var data = _.map(sqsQueues.models[0].entry.content.attributes.sqs_queue_names, function (sqsQueue) {
                    return {value: sqsQueue, label: sqsQueue};
                });

                if (data.length >= 1)
                    this.showJobSuccess(job, this.real_model, attr);
                else
                    this.showJobNoData(job, this.real_model, attr);

                if (!this.isModelValueInDropdownList(attr, data)) {
                    this.model.unset(attr);
                }
                this.children.aws_sqs_queue.control.setAutoCompleteFields(data, true);

            }.bind(this)).fail(function(ajaxRet) {
                this.showJobFailure(job, this.real_model, attr, ajaxRet);
            }.bind(this));

        },

        renderContent: function ($body) {
            $body.append(this.children.aws_sqs_queue.render().$el);
            return this;
        }
    });
});
