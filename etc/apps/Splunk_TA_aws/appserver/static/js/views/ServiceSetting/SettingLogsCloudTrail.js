define([
    'jquery',
    'underscore',
    'backbone',
    'app/models/appData',
    'app/views/Models/BaseSection',
    'views/shared/controls/TextControl',
    'app/views/Models/SingleInputControl',
    'app/views/Models/SingleInputControlEx',
    'app/views/Models/ControlWrapper'
], function (
    $,
    _,
    Backbone,
    appData,
    BaseSection,
    TextControl,
    SingleInputControl,
    SingleInputControlEx,
    ControlWrapper
) {
    return BaseSection.extend({
        initialize: function () {
            BaseSection.prototype.initialize.apply(this, arguments);

            this.children.log_file_prefix = new ControlWrapper({
                label: _('Log File Prefix').t(),
                controlType: TextControl,
                wrapperClass: 'log_file_prefix',
                required: false,
                controlOptions: {
                    model: this.model,
                    modelAttribute: 'log_file_prefix',
                    placeholder: _("Input a Log File Prefix").t(),
                    autoCompleteFields: []
                }
            });

            this.children.log_start_date = new ControlWrapper({
                label: _('Log Start Date').t(),
                controlType: TextControl,
                wrapperClass: 'log_start_date',
                required: false,
                disabled: this.options.edit_mode,
                controlOptions: {
                    model: this.model,
                    modelAttribute: 'log_start_date',
                    placeholder: _("Input a Log Start UTC Date in Format \"YYYY-mm-dd\"").t(),
                    autoCompleteFields: []
                }
            });
        },

        renderContent: function ($body) {
            $body.append(this.children.log_file_prefix.render().$el);
            $body.append(this.children.log_start_date.render().$el);
            return this;
        }
    });
});
