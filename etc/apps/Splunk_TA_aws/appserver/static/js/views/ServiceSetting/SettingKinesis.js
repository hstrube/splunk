define([
    'jquery',
    'underscore',
    'backbone',
    'app/models/appData',
    'app/views/Models/BaseSection',
    'views/shared/controls/TextControl',
    'app/views/Models/SingleInputControl',
    'app/views/Models/SingleInputControlEx',
    'app/views/Models/MultiSelectInputControl',
    'app/views/Models/ControlWrapper',
    'app/collections/KinesisStreams'
], function (
    $,
    _,
    Backbone,
    appData,
    BaseSection,
    TextControl,
    SingleInputControl,
    SingleInputControlEx,
    MultiSelectInputControl,
    ControlWrapper,
    KinesisStreams
) {
    return BaseSection.extend({
        initialize: function () {
            BaseSection.prototype.initialize.apply(this, arguments);

            this.children.stream_names = new ControlWrapper({
                label: _('Stream Name').t(),
                controlType: SingleInputControl,
                wrapperClass: 'stream_names',
                required: true,
                controlOptions: {
                    model: this.model,
                    modelAttribute: 'stream_names',
                    placeholder: _("Add a Stream Name").t(),
                    disableSearch: true
                }
            });
            this.children.init_stream_position = new ControlWrapper({
                label: _('Initial Stream Position').t(),
                controlType: SingleInputControlEx,
                wrapperClass: 'init_stream_position',
                required: true,
                controlOptions: {
                    model: this.model,
                    modelAttribute: 'init_stream_position',
                    placeholder: _("Select a Stream Position").t(),
                    disableSearch: true,
                    autoCompleteFields: [{
                        label: "TRIM_HORIZON",
                        value: "TRIM_HORIZON"
                    }, {
                        label: "LATEST",
                        value: "LATEST"
                    }]
                }
            });
            this.children.encoding = new ControlWrapper({
                label: _('Encoding').t(),
                controlType: SingleInputControl,
                wrapperClass: 'encoding',
                controlOptions: {
                    model: this.model,
                    modelAttribute: 'encoding',
                    placeholder: _("(none)").t(),
                    disableSearch: true,
                    autoCompleteFields: [{
                        label: "(none)",
                        value: ""
                    }, {
                        label: "gzip",
                        value: "gzip"
                    }]
                }
            });
            this.children.format = new ControlWrapper({
                label: _('Record Format').t(),
                controlType: SingleInputControl,
                wrapperClass: 'format',
                controlOptions: {
                    model: this.model,
                    modelAttribute: 'format',
                    placeholder: _("(none)").t(),
                    disableSearch: true,
                    autoCompleteFields: [{
                        label: "(none)",
                        value: ""
                    }, {
                        label: "CloudWatchLogs",
                        value: "CloudWatchLogs"
                    }]
                }
            });

            this.loadDataSyncUI();
            this.model.on("change:aws_account", this.loadDataSyncUI.bind(this));
            this.model.on("change:aws_iam_role", this.loadDataSyncUI.bind(this));
            this.model.on("change:aws_region", this.loadDataSyncUI.bind(this));
        },


        // just called once
        loadDataSyncUI: function () {
            var aws_account = this.model.get("aws_account");
            var aws_iam_role = this.model.get("aws_iam_role");
            var aws_region = this.model.get("aws_region");
            var attr = 'stream_names';
            this.children.stream_names.control.setAutoCompleteFields(this.getDefaultDropdownlistItem(attr), true);
            if (!aws_account || !aws_region) {
                return;
            }

            var streams = new KinesisStreams([], {
                appData: { app: appData.get("app"), owner: appData.get("owner") },
                targetApp: "-",
                targetOwner: "nobody"
            });
            streams.deferred = streams.fetch({
                data: {
                    aws_account: this.model.get("aws_account"),
                    aws_region: this.model.get("aws_region"),
                    aws_iam_role: aws_iam_role
                }
            });
            var job = this.startLoadingForModel(this.real_model, attr);

            streams.deferred.done(function () {
                // load AWS Account into list
                var data = _.map(streams.models[0].entry.content.attributes.streams, function (stream) {
                    return {value: stream, label: stream};
                });

                if (data.length >= 1)
                    this.showJobSuccess(job, this.real_model, attr);
                else
                    this.showJobNoData(job, this.real_model, attr);

                this.children.stream_names.control.setAutoCompleteFields(data, true);

            }.bind(this)).fail(function(ajaxRet) {
                this.showJobFailure(job, this.real_model, attr, ajaxRet);
            }.bind(this));

        },

        renderContent: function ($body) {
            $body.append(this.children.stream_names.render().$el);
            $body.append(this.children.init_stream_position.render().$el);
            $body.append(this.children.encoding.render().$el);
            $body.append(this.children.format.render().$el);
            return this;
        }
    });
});
