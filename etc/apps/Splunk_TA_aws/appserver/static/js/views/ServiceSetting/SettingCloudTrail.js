define([
    'jquery',
    'underscore',
    'backbone',
    'app/models/appData',
    'app/views/Models/BaseSection',
    'views/shared/controls/TextControl',
    'app/views/Models/SingleInputControl',
    'app/views/Models/SingleInputControlEx',
    'app/views/Models/ControlWrapper',
    'app/collections/SQSQueues',
    'views/shared/controls/SyntheticCheckboxControl',
    'app/collections/Indexes'

], function ($, _, Backbone, appData, BaseSection, TextControl, SingleInputControl, SingleInputControlEx, ControlWrapper, SQSQueues, SyntheticCheckboxControl, Indexes) {
    return BaseSection.extend({
        initialize: function () {
            BaseSection.prototype.initialize.apply(this, arguments);

            this.children.aws_sqs_queue = new ControlWrapper({
                label: _('SQS Queue Name').t(),
                controlType: SingleInputControlEx,
                wrapperClass: 'sqs_queue',
                required: true,
                controlOptions: {
                    allowClear: true,
                    model: this.model,
                    modelAttribute: 'sqs_queue',
                    placeholder: _("Select a Queue").t(),
                    disableSearch: false,
                    autoCompleteFields: []
                }
            });

            this.children.exclude_describe_events = new ControlWrapper({
                label: _('Exclude Events').t(),
                controlType: SyntheticCheckboxControl,
                wrapperClass: 'exclude_describe_events',
                controlOptions: {
                    model: this.model,
                    modelAttribute: 'exclude_describe_events'

                }
            });

            this.children.remove_files_when_done = new ControlWrapper({
                label: _('Remove Logs When Done').t(),
                controlType: SyntheticCheckboxControl,
                wrapperClass: 'remove_files_when_done',
                controlOptions: {
                    model: this.model,
                    modelAttribute: 'remove_files_when_done'
                }
            });

            this.children.blacklist = new ControlWrapper({
                label: _('Blacklist for Exclusion').t(),
                controlType: TextControl,
                wrapperClass: 'blacklist',
                controlOptions: {
                    model: this.model,
                    modelAttribute: 'blacklist'
                }});

            this.children.excluded_events_indexcloudtrail = new ControlWrapper({
                label: _('Excluded Events Index').t(),
                controlType: SingleInputControlEx,
                wrapperClass: 'excluded_events_index',
                controlOptions: {
                    model: this.model,
                    modelAttribute: 'excluded_events_index',
                    placeholder: _("Select an index").t(),
                    allowClear: true,
                    disableSearch: false,
                    autoCompleteFields: [{label: "default", value: "default"}]
                }});

            this.init_index_lst();
            this.loadDataSyncUI();
            this.model.on("change:aws_account", this.loadDataSyncUI.bind(this));
            this.model.on("change:aws_region", this.loadDataSyncUI.bind(this));
        },

        init_index_lst: function () {
            var indexes = new Indexes([], {
                appData: { app: appData.get("app"), owner: appData.get("owner") },
                targetApp: "-",
                targetOwner: "nobody"
            });
            indexes.deferred = indexes.fetch();
            var attr = 'excluded_events_index';
            var job = this.startLoadingForModel(this.real_model, attr);

            indexes.deferred.done(function () {
                var id_lst = _.map(indexes.models[0].attributes.entry[0].content.indexes, function (index) {
                    return {
                        label: index,
                        value: index
                    };
                });

                id_lst = this.ensureModelValueInDropdownList("excluded_events_index", id_lst);
                this.children.excluded_events_indexcloudtrail.control.setAutoCompleteFields(id_lst, true);

                if (id_lst.length >= 1)
                    this.showJobSuccess(job, this.real_model, attr);
                else
                    this.showJobNoData(job, this.real_model, attr);

            }.bind(this)).fail(function(ajaxRet) {
                this.showJobFailure(job, this.real_model, attr, ajaxRet);
            }.bind(this));
        },

        // just called once
        loadDataSyncUI: function () {
            var aws_account = this.model.get("aws_account");
            var aws_region = this.model.get("aws_region");
            var attr = 'sqs_queue';
            this.children.aws_sqs_queue.control.setAutoCompleteFields(this.getDefaultDropdownlistItem(attr), true);

            if (!aws_account || !aws_region) {
                return;
            }

            var sqsQueues = new SQSQueues([], {
                appData: { app: appData.get("app"), owner: appData.get("owner") },
                targetApp: "-",
                targetOwner: "nobody"
            });

            sqsQueues.deferred = sqsQueues.fetch({
                data: {
                    aws_account: this.model.get("aws_account"),
                    aws_region: this.model.get("aws_region")
                }
            });

            var job = this.startLoadingForModel(this.real_model, attr);

            sqsQueues.deferred.done(function () {
                // load AWS Account into list
                var data = _.map(sqsQueues.models[0].entry.content.attributes.sqs_queue_names, function (sqsQueue) {
                    return {value: sqsQueue, label: sqsQueue};
                });

                if (data.length >= 1)
                    this.showJobSuccess(job, this.real_model, attr);
                else
                    this.showJobNoData(job, this.real_model, attr);

                if (!this.isModelValueInDropdownList(attr, data)) {
                    this.model.unset(attr);
                }
                this.children.aws_sqs_queue.control.setAutoCompleteFields(data, true);
            }.bind(this)).fail(function(ajaxRet) {
                this.showJobFailure(job, this.real_model, attr, ajaxRet);
            }.bind(this));

        },

        renderContent: function ($body) {
            $body.append(this.children.aws_sqs_queue.render().$el);
            $body.append(this.children.remove_files_when_done.render().$el);
            $body.append(this.children.exclude_describe_events.render().$el);
            $body.append(this.children.blacklist.render().$el);
            $body.append(this.children.excluded_events_indexcloudtrail.render().$el);
            return this;
        }
    });
});
