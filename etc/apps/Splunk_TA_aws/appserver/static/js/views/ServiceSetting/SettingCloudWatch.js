define([
    'jquery',
    'underscore',
    'backbone',
    'app/models/appData',
    'app/views/Models/BaseSection',
    'views/shared/controls/TextControl',
    'app/views/Models/SingleInputControl',
    'app/views/Models/SingleInputControlEx',
    'app/views/Models/MultiSelectInputControl',
    'app/views/Models/ControlWrapper',
    'app/collections/MetricNamespaces',
    'app/collections/Cloudwatch.Settings'
], function (
    $,
    _,
    Backbone,
    appData,
    BaseSection,
    TextControl,
    SingleInputControl,
    SingleInputControlEx,
    MultiSelectInputControl,
    ControlWrapper,
    MetricNamespaces,
    CloudwatchSettings
) {
    return BaseSection.extend({
        initialize: function () {
            BaseSection.prototype.initialize.apply(this, arguments);

            this.children.aws_metric_namespace = new ControlWrapper({
                label: _('Metric Namespace').t(),
                controlType: SingleInputControlEx,
                wrapperClass: 'metric_namespace',
                required: true,
                controlOptions: {
                    model: this.model,
                    allowClear: true,
                    modelAttribute: 'metric_namespace',
                    placeholder: _("Select a Metric Namespace").t(),
                    disableSearch: false,
                    autoCompleteFields: []
                }
            });

            this.children.metric_names = new ControlWrapper({
                label: _('Metric Names').t(),
                controlType: TextControl,
                wrapperClass: 'metric_names',
                required: true,
                controlOptions: {
                    model: this.model,
                    modelAttribute: 'metric_names',
                    placeholder: _('Enter Metric Names').t()
                }});

            this.children.metric_dimensions = new ControlWrapper({
                label: _('Dimension Names').t(),
                controlType: TextControl,
                wrapperClass: 'metric_dimensions',
                required: true,
                controlOptions: {
                    model: this.model,
                    modelAttribute: 'metric_dimensions',
                    placeholder: _('Enter Metric Dimension Names').t()
                }});

            this.children.statistics = new ControlWrapper({
                label: _('Metric Statistics').t(),
                wrapperClass: 'statistics_lst',
                required: true,
                controlType: MultiSelectInputControl,
                controlOptions: {
                    model: this.model,
                    modelAttribute: 'statistics_lst',
                    placeholder: _('Select Metric Statistic').t(),
                    // id: "metricsSelectControl",
                    items: _.map([
                        'Average',
                        'Sum',
                        'SampleCount',
                        'Maximum',
                        'Minimum'
                    ], function (name) {
                        return {
                            value: name,
                            label: name
                        };
                    })
                }
            });

            this.children.period = new ControlWrapper({
                label: _('Metric Granularity').t(),
                controlType: TextControl,
                wrapperClass: 'period',
                required: true,
                controlOptions: {
                    model: this.model,
                    modelAttribute: 'period',
                    placeholder: _('Enter Metric Granularity').t()
                }});

            this.children.polling_interval = new ControlWrapper({
                label: _('Minimum Polling Interval').t(),
                controlType: TextControl,
                wrapperClass: 'polling_interval',
                required: true,
                controlOptions: {
                    model: this.model,
                    modelAttribute: 'polling_interval',
                    placeholder: _('Enter Minimum Polling Interval').t()
                }});


            this.loadDataSyncUI();
            this.model.on("change:aws_account", this.loadDataSyncUI.bind(this));
            this.model.on("change:aws_region", this.loadDataSyncUI.bind(this));
            this.model.on("change:metric_namespace", this.loadSettings.bind(this));
        },

        // just called once
        loadDataSyncUI: function () {
            var aws_account = this.model.get("aws_account");
            var aws_region = this.model.get("aws_region");
            var attr = 'metric_namespace';
            this.children.aws_metric_namespace.control
                .setAutoCompleteFields(this.getDefaultDropdownlistItem(attr), true);
            if (!aws_account || !aws_region) {
                return;
            }

            var aws_regions = String(this.model.get("aws_region")).split(",");
            var metric_namespaces = [];
            _.each(aws_regions, function (region) {
                var metricNamespaces = new MetricNamespaces([], {
                    appData: { app: appData.get("app"), owner: appData.get("owner") },
                    targetApp: "-",
                    targetOwner: "nobody"
                });
                metricNamespaces.deferred = metricNamespaces.fetch({
                    data: {
                        aws_account: this.model.get("aws_account"),
                        aws_region: region
                    }
                });

                var job = this.startLoadingForModel(this.real_model, attr);

                metricNamespaces.deferred.done(function () {
                    // load AWS Account into list
                    metric_namespaces = _.uniq(_.union(metric_namespaces, metricNamespaces.models[0].entry.content.attributes.metric_namespace));
                    var data = _.sortBy(_.map(metric_namespaces,
                        function (namespace) {
                            return {value: namespace, label: namespace};
                        }), 'label');

                    if (data.length >= 1)
                        this.showJobSuccess(job, this.real_model, attr);
                    else
                        this.showJobNoData(job, this.real_model, attr);

                    data = this.ensureModelValueInDropdownList(attr, data);

                    this.children.aws_metric_namespace.control.setAutoCompleteFields(data, true);

                }.bind(this)).fail(function(ajaxRet) {
                    this.showJobFailure(job, this.real_model, attr, ajaxRet);
                }.bind(this));
            }.bind(this));

        },
        loadSettings: function () {
            var namespace = this.model.get("metric_namespace");
            if (!namespace) {
                return;
            }
            var attr = "Namespace: " + namespace;
            var cloudwatchSettings = new CloudwatchSettings([], {
                appData: { app: appData.get("app"), owner: appData.get("owner") },
                targetApp: "-",
                targetOwner: "nobody"
            });
            cloudwatchSettings.deferred = cloudwatchSettings.fetch({
                data: {
                    namespace: namespace
                }
            });

            var job = this.startLoadingForModel(this.real_model, attr);

            cloudwatchSettings.deferred.done(function () {
                // load AWS Account into list
                var settings = cloudwatchSettings.models[0].entry.content;

                if (settings.get("dimensions") && settings.get("metrics")){
                    this.showJobSuccess(job, this.real_model, attr);
                    this.model.set({
                        metric_names: JSON.stringify(settings.get("metrics")),
                        metric_dimensions: settings.get("dimensions"),
                        statistics_lst: ['Average', 'Sum', 'SampleCount', 'Maximum', 'Minimum'].join(",")
                    });
                } else {
                    this._removeOngoingJob(job);
                }
                // this.children.aws_metric_namespace.control.setAutoCompleteFields(data, true);

            }.bind(this)).fail(function() {
                this.showJobFailure(job, null, null, null, "Server Error. Cannot find templates for namespace: " + namespace);
            }.bind(this));

        },
        renderContent: function ($body) {
            $body.append(this.children.aws_metric_namespace.render().$el);
            $body.append(this.children.metric_names.render().$el);
            $body.append(this.children.metric_dimensions.render().$el);
            $body.append(this.children.statistics.render().$el);
            $body.append(this.children.period.render().$el);
            $body.append(this.children.polling_interval.render().$el);

            return this;
        }
    });
});
