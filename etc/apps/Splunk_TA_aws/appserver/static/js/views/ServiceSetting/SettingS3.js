define([
    'jquery',
    'underscore',
    'backbone',
    'app/views/Models/BaseSection',
    'views/shared/controls/TextControl',
    'app/views/Models/SingleInputControl',
    'app/views/Models/ControlWrapper'
],function(
    $,
    _,
    Backbone,
    BaseSection,
    TextControl,
    SingleInputControl,
    ControlWrapper
){
    return BaseSection.extend({
      initialize: function () {
          BaseSection.prototype.initialize.apply(this, arguments);

          this.children.key_name = new ControlWrapper({
              label: _('S3 Key Prefix').t(),
              controlType: TextControl,
              wrapperClass: 'key_name',
              controlOptions: {
                  model: this.model,
                  modelAttribute: 'key_name'
              }});

          this.children.initial_scan_datetime = new ControlWrapper({
              label: _('Start Date/Time').t(),
              controlType: TextControl,
              required: true,
              disabled: this.options.edit_mode,
              wrapperClass: 'initial_scan_datetime',
              controlOptions: {
                  model: this.model,
                  modelAttribute: 'initial_scan_datetime'
              }});

          this.children.blacklist = new ControlWrapper({
              label: _('Blacklist').t(),
              controlType: TextControl,
              wrapperClass: 'blacklist',
              controlOptions: {
                  model: this.model,
                  modelAttribute: 'blacklist'
              }});

          this.children.whitelist = new ControlWrapper({
              label: _('Whitelist').t(),
              controlType: TextControl,
              wrapperClass: 'whitelist',
              controlOptions: {
                  model: this.model,
                  modelAttribute: 'whitelist'
              }});
      },

      renderContent: function($body){
          $body.append(this.children.key_name.render().$el);
          $body.append(this.children.initial_scan_datetime.render().$el);
          $body.append(this.children.blacklist.render().$el);
          $body.append(this.children.whitelist.render().$el);

          return this;
      }
    });
});
