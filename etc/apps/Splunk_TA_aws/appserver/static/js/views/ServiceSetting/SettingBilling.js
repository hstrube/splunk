define([
    'jquery',
    'underscore',
    'backbone',
    'app/views/Models/BaseSection',
    'views/shared/controls/TextControl',
    'app/views/Models/SingleInputControl',
    'app/views/Models/ControlWrapper'
],function(
    $,
    _,
    Backbone,
    BaseSection,
    TextControl,
    SingleInputControl,
    ControlWrapper
){
    return BaseSection.extend({
      initialize: function () {
          BaseSection.prototype.initialize.apply(this, arguments);

          var monthly_report = [
              {value:'0',label:'None'},
              {value:'1',label:'Monthly Report'},
              {value:'2',label:'Monthly Cost Allocation Report',default:true}
          ];

          this.children.monthlyReport = new ControlWrapper({
              label: _('Monthly Report').t(),
              controlType: SingleInputControl,
              wrapperClass: 'monthlyreport',
              controlOptions: {
                  model: this.model,
                  modelAttribute: 'monthly_report_type',
                  disableSearch: true,
                  autoCompleteFields: monthly_report,
                  placeholder: _("Select a Monthly Report Type").t()
              }
          });

          var detail_report = [
              {value:'0',label:'None'},
              {value:'1',label:'Detailed Billing Report'},
              {value:'2',label:'Detailed Billing Report with Resources and Tags',default:true}
          ];
          this.children.detailReport = new ControlWrapper({
              label: _('Detail Report').t(),
              controlType: SingleInputControl,
              wrapperClass: 'detailreport',
              controlOptions: {
                  model: this.model,
                  modelAttribute: 'detail_report_type',
                  disableSearch: true,
                  autoCompleteFields: detail_report,
                  placeholder: _("Select a Detail Report Type").t()
              }
          });

          this.children.regexForReport = new ControlWrapper({
              label: _('Regex for Report Selection').t(),
              controlType: TextControl,
              wrapperClass: 'regexforreport',
              controlOptions: {
                  model: this.model,
                  modelAttribute: 'report_file_match_reg'
              }
          });

          this.children.initial_scan_datetime = new ControlWrapper({
              label: _('Start Date/Time (UTC)').t(),
              controlType: TextControl,
              wrapperClass: 'initial_scan_datetime',
              disabled: this.options.edit_mode,
              controlOptions: {
                  model: this.model,
                  modelAttribute: 'initial_scan_datetime',
                  placeholder: _("UTC date time in format \"%Y-%m-%dT%M:%H:%SZ\". Default is 3 months ago.").t()
              }
          });
      },

      renderContent: function($body){
          $body.append(this.children.monthlyReport.render().$el);
          $body.append(this.children.detailReport.render().$el);
          $body.append(this.children.regexForReport.render().$el);
          $body.append(this.children.initial_scan_datetime.render().$el);
          return this;
      }
    });
});
