define([
    'jquery',
    'underscore',
    'backbone',
    'app/models/appData',
    'app/collections/Regions',
    'app/views/Models/BaseSection',
    'views/shared/controls/TextControl',
    'app/views/Models/SingleInputControl',
    'app/views/Models/ControlWrapper',
    'app/collections/Accounts',
    'app/collections/IAMRoles',
    'app/views/Models/MultiSelectInputControl',
    "app/util/AccountUtil",
    "app/util/RegionUtil"
], function(
    $, _,
    Backbone,
    appData,
    Regions,
    BaseSection,
    TextControl,
    SingleInputControl,
    ControlWrapper,
    Accounts,
    IAMRoles,
    MultiSelectInputControl,
    AccountUtil,
    RegionUtil
) {
    return BaseSection.extend({
        initialize: function(options) {
            BaseSection.prototype.initialize.apply(this, arguments);
            this.service_type_origin = options.service_type;
            this.service_type = this._awsServiceType(options.service_type);
            this.allRegions = null;
            this._customComponents = [];
            this.createUIComponents();
            this.loadDataSyncUI();
            this._name2CateMap = {};
        },

        _awsServiceType: function(service_type) {
            var service_type_map = {
                'logs': 's3'
            };
            return service_type_map[service_type] || service_type;
        },

        _awsServiceRegion: function () {
            if (this.service_type_origin === 'logs') {
                this.multiRegionType = false;
                return new ControlWrapper({
                    label: _('S3 Bucket Region').t(),
                    controlType: SingleInputControl,
                    wrapperClass: 's3_bucket_region',
                    required: true,
                    controlOptions: {
                        model: this.model,
                        modelAttribute: 's3_bucket_region',
                        placeholder: _('Select an AWS S3 Bucket Region').t(),
                        disableSearch: false,
                        autoCompleteFields: []
                    }
                });
            }

            this.multiRegionType = ["cloudwatch", "description", "inspector"].indexOf(this.service_type) > -1;
            var singleOptions = {
                model: this.model,
                modelAttribute: 'aws_region',
                placeholder: _("Select an AWS Region").t(),
                disableSearch: false,
                autoCompleteFields: []
            };
            var multiOptions = {
                model: this.model,
                modelAttribute: 'aws_region',
                placeholder: _("Select AWS Regions").t()
            };
            return new ControlWrapper({
                label: _('AWS Region').t(),
                controlType: this.multiRegionType ? MultiSelectInputControl : SingleInputControl,
                wrapperClass: 'aws_region',
                required: true,
                controlOptions: this.multiRegionType ? multiOptions : singleOptions
            });
        },

        _setCustomComponents: function () {
            if (this.service_type_origin === 'logs') {
                 var log_type = new ControlWrapper({
                    label: _('Log Type').t(),
                    controlType: SingleInputControl,
                    wrapperClass: 'log_type',
                    required: true,
                    controlOptions: {
                        model: this.model,
                        modelAttribute: 'log_type',
                        placeholder: _("Select an AWS Log Type").t(),
                        autoCompleteFields: [
                            {
                                label: 'CloudTrail',
                                value: 'CloudTrail',
                                selected: true
                            }
                        ]
                    }
                });
                this.children.log_type = log_type;
                this._customComponents.push(log_type);
            }
        },

        // just called once
        createUIComponents: function() {
            this.children.awsAccount = new ControlWrapper({
                label: _('AWS Account').t(),
                controlType: SingleInputControl,
                wrapperClass: 'aws_account',
                required: true,
                controlOptions: {
                    model: this.model,
                    modelAttribute: 'aws_account',
                    placeholder: _("Select an AWS Account").t(),
                    autoCompleteFields: []
                }
            });
            this.children.aws_region = this._awsServiceRegion();
            this.children.assumedRole = new ControlWrapper({
                label: _('Assume Role').t(),
                controlType: SingleInputControl,
                wrapperClass: 'aws_iam_role',
                required: false,
                controlOptions: {
                    model: this.model,
                    modelAttribute: 'aws_iam_role',
                    placeholder: _("Select an AWS IAM Role to Assume (Optional)").t(),
                    allowClear: true,
                    autoCompleteFields: []
                }
            });
            this._setCustomComponents();
        },
        
        _awsServiceRegionAttr: function () {
            if (this.service_type_origin === 'logs') {
                return 's3_bucket_region';
            }
            return 'aws_region';
        },

        _loadAWSAccount: function() {
            var accounts = new Accounts([], {
                appData: {
                    app: appData.get("app"),
                    owner: appData.get("owner")
                },
                targetApp: "-",
                targetOwner: "nobody"
            });
            accounts.deferred = accounts.fetch();
            var attr = 'aws_account';
            var job = this.startLoadingForModel(this.real_model, attr);

            accounts.deferred.done(function() {
                // load AWS Account into list
                var data = _.map(accounts.models, function(account) {
                    var content = account.entry.content;
                    var name = content.get('name');
                    this._name2CateMap[name] = content.get('category');
                    return {
                        value: name,
                        label: name,
                        disabled: [1, 2, 4].indexOf(+content.get('category')) < 0
                    };
                }, this);
                this.children.awsAccount.control.setAutoCompleteFields(data, true);

                if (data.length >= 1) {
                    this.handleUIChange();
                    this.showJobSuccess(job, this.real_model, attr);
                } else {
                    this.showJobNoData(job, this.real_model, attr);
                }
            }.bind(this)).fail(function(ajaxRet) {
                this.showJobFailure(job, this.real_model, attr, ajaxRet);
            }.bind(this));
        },

        _loadIAMRoles: function() {
            var iamRoles = new IAMRoles([], {
                appData: {
                    app: appData.get("app"),
                    owner: appData.get("owner")
                },
                targetApp: "-",
                targetOwner: "nobody"
            });
            iamRoles.deferred = iamRoles.fetch();
            var iamRoleJob = this.startLoadingForModel(this.real_model, 'IAM Roles');
            iamRoles.deferred.done(function() {
                var data = _.map(iamRoles.models, function(iamRole) {
                    var name = iamRole.entry.attributes.name;
                    return {
                        value: name,
                        label: name
                    };
                }, this);
                this.children.assumedRole.control.setAutoCompleteFields(data, true);

                if (data.length >= 1)
                    this.showJobSuccess(iamRoleJob, this.real_model, 'Assume Role');
                else
                    this._removeOngoingJob(iamRoleJob);
            }.bind(this)).fail(function(ajaxRet) {
                this.showJobFailure(iamRoleJob, this.real_model, 'Assume Role', ajaxRet);
            }.bind(this));
        },

        _loadAWSRegion: function() {

            var attr = this._awsServiceRegionAttr();

            var loadRegions = (function(job) {
                var cate = this._name2CateMap[this.model.get("aws_account")];
                var filteredRegions = AccountUtil.getAccessableRegionList(cate, this.allRegions);
                var aws_regions = _.map(filteredRegions, function(region) {
                    return {
                        label: RegionUtil.getDisplayName(region),
                        value: region
                    };
                }, this);

                var sorted_regions = _.sortBy(aws_regions, "label");

                if (!job) {
                    job = this.startLoadingForModel(this.real_model, attr);
                }
                if (sorted_regions.length) {
                    this.showJobSuccess(job, this.real_model, attr);
                } else {
                    this.showJobNoData(job, this.real_model, attr, [
                        "Incorrect key id and/or secret key of this account",
                        "This account is not supported by this service.",
                        "Internet connection is not stable.",
                        "AWS server is down."
                    ]);
                }
                if (this.multiRegionType) {
                    this.children.aws_region.control.setItems(sorted_regions, true);
                    if (this.children.aws_region.control.error_items.length>0){
                        var error_msg = "Some configured regions are not supported, please check and fix them: " + this.children.aws_region.control.error_items.join(", ");
                        this.options.newInput.addErrorMsg(error_msg);
                    }
                } else {
                    this.children.aws_region.control.setAutoCompleteFields(sorted_regions, true);
                }
            }).bind(this);

            if (!this.allRegions) {
                var regions = new Regions([], {
                    appData: {
                        app: appData.get("app"),
                        owner: appData.get("owner")
                    },
                    targetApp: "-",
                    targetOwner: "nobody"
                });

                regions.deferred = regions.fetch({
                    data: {
                        aws_service: this.service_type
                    }
                });
                var job = this.startLoadingForModel(this.real_model, attr);

                regions.deferred.done(function() {
                    this.allRegions = regions.models[0].entry.content.attributes.regions;
                    loadRegions(job);
                }.bind(this)).fail(function(ajaxRet) {
                    this.showJobFailure(job, this.real_model, attr, ajaxRet);
                }.bind(this));
            } else {
                loadRegions();
            }

        },

        handleUIChange: function() {
            this.model.on("change:aws_account", this._loadAWSRegion.bind(this));
            if (this.model.get("aws_account")) {
                this._loadAWSRegion();
            }
        },
        // just called once
        loadDataSyncUI: function() {
            this._loadAWSAccount();
            this._loadIAMRoles();
        },

        renderContent: function($body) {
            _.each(this._customComponents, function (cb) {
                $body.append(cb.render().$el);
            });
            $body.append(this.children.awsAccount.render().$el);
            if (['cloudwatch', 'description', 'kinesis'].indexOf(this.service_type) >= 0) {
                $body.append(this.children.assumedRole.render().$el);
            }
            $body.append(this.children.aws_region.render().$el);
            return this;
        }
    });
});
