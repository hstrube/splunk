define([
    'jquery',
    'underscore',
    'backbone',
    'app/views/Models/BaseSection',
    'views/shared/controls/TextControl',
    'app/views/Models/SingleInputControl',
    'app/views/Models/ControlWrapper'
], function ($, _, Backbone, BaseSection, TextControl, SingleInputControl, ControlWrapper) {
    return BaseSection.extend({
        initialize: function () {
            BaseSection.prototype.initialize.apply(this, arguments);

            this.children.groups = new ControlWrapper({
                label: _('Log Group').t(),
                controlType: TextControl,
                wrapperClass: 'groups',
                required: true,
                controlOptions: {
                    model: this.model,
                    modelAttribute: 'groups'
                }});

            this.children.only_after = new ControlWrapper({
                label: _('Only After').t(),
                controlType: TextControl,
                wrapperClass: 'only_after',
                controlOptions: {
                    model: this.model,
                    modelAttribute: 'only_after'
                }});

            this.children.stream_matcher = new ControlWrapper({
                label: _('Stream Matching Regex').t(),
                controlType: TextControl,
                wrapperClass: 'stream_matcher',
                controlOptions: {
                    model: this.model,
                    modelAttribute: 'stream_matcher'
                }});

        },

        renderContent: function ($body) {
            $body.append(this.children.groups.render().$el);
            $body.append(this.children.only_after.render().$el);
            $body.append(this.children.stream_matcher.render().$el);
            return this;
        }
    });
});
