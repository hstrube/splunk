define([
    'jquery',
    'underscore',
    'backbone',
    'app/views/Models/BaseSection',
    'app/views/Models/MultiSelectInputControlFieldEditable',
    'app/util/Service.Description.API.Config',
    'app/views/Models/ControlWrapper'
], function(
    $, _,
    Backbone,
    BaseSection,
    MultiSelectInputControlFieldEditable,
    DescriptionConfig,
    ControlWrapper
) {

    function getLabel(api) {
        return DescriptionConfig.API_TO_LABEL[api] || api;
    }

    function getDefaultValue(api) {
        var val = DescriptionConfig.INTERVAL_DEFAULT_VALUES[api];
        return val != null ? val : DescriptionConfig.INTERVAL_DEFAULT_VALUE;
    }

    function parseItems(str) {
        var items = [];
        var config;
        if (navigator.userAgent.indexOf("MSIE 9.0") > 0) {
            //IE 9
            config = {
                type: "text",
                pattern: "[0-9]+"
            };
        } else {
            config = {
                type: "number",
                min: 1
            };
        }
        _.each(DescriptionConfig.API_LIST, function(api) {
            var item = {
                mainField: {
                    id: api,
                    label: getLabel(api)
                },
                editableFields: [{
                    name: "interval",
                    defaultValue: getDefaultValue(api),
                    unit: "second",
                    type: "input",
                    config: config,
                    validator: function(val) {
                        return val !== "" && !isNaN(+val) && +val >= 1;
                    },
                    errormessage: "Value should greater than zero!"
                }]
            };
            //to match "ec2_instances/(3600)"
            var re = new RegExp(api + "\\s*\\/\\s*([^\\s,]+)(?:\\s|,|$)");
            var result = str.match(re);
            if (result && result[1]) {
                item.editableFields[0].value = result[1];
                item.mainField.initialSelected = true;
            }
            items.push(item);
        });
        console.log(items);
        return items;
    }

    return BaseSection.extend({
        initialize: function() {
            BaseSection.prototype.initialize.apply(this, arguments);

            var model = this.model;
            this.children.apis = new ControlWrapper({
                label: _('APIs/Interval(seconds)').t(),
                controlType: MultiSelectInputControlFieldEditable,
                wrapperClass: 'apis',
                required: true,
                controlOptions: {
                    model: model,
                    modelAttribute: 'apis'
                }
            });
            var control = this.children.apis.control;

            var items = parseItems(model.get("apis"));

            control.setItems(items);
            control.render();
        },

        renderContent: function($body) {
            $body.append(this.children.apis.render().$el);
            return this;
        }
    });
});
