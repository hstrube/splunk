define([
    'jquery',
    'underscore',
    'backbone',
    'app/models/appData',
    'app/views/Models/BaseSection',
    'views/shared/controls/TextControl',
    'app/util/Service.S3.Sourcetype.Config',
    'app/util/Service.Kinesis.Sourcetype.Config',
    'app/util/Service.Logs.LogType.Config',
    'app/views/Models/SingleInputControl',
    'app/views/Models/SingleInputControlEx',
    'app/views/Models/ControlWrapper',
    'app/collections/Indexes'
], function(
    $,
    _,
    Backbone,
    appData,
    BaseSection,
    TextControl,
    S3SourcetypeConfig,
    KinesisSourcetypeConfig,
    LogsLogTypeConfig,
    SingleInputControl,
    SingleInputControlEx,
    ControlWrapper,
    Indexes
) {
    return BaseSection.extend({
        initialize: function() {
            // accept { setting_options: {sourcetype: true, host: true, interval: true, index: true}}
            BaseSection.prototype.initialize.apply(this, arguments);

            var options = this.options.setting_options;
            var children = this.children;
            var service_type = this.options.service_type;
            this.logs_sourcetype = this.options.model.attributes.logs_sourcetype;
            this.log_type = this.options.model.attributes.log_type;

            if (options.interval) {
                children.interval = new ControlWrapper({
                    label: _('Interval').t(),
                    controlType: TextControl,
                    wrapperClass: 'interval',
                    required: true,
                    controlOptions: {
                        model: this.model,
                        modelAttribute: 'interval'
                    }
                });
            }

            if (options.sourcetype) {
                var isS3 = service_type === "s3";
                var isKinesis = service_type === "kinesis";
                children.sourcetype = new ControlWrapper({
                    label: _('Source Type').t(),
                    controlType: isS3 || isKinesis ? SingleInputControlEx : TextControl,
                    wrapperClass: 'sourcetype',
                    required: false,
                    controlOptions: {
                        model: this.model,
                        modelAttribute: 'sourcetype'
                    }
                });
                var modelSourcetype = this.model.get('sourcetype');
                var sourcetypeList = [];
                if (modelSourcetype) {
                    sourcetypeList.push(modelSourcetype);
                }
                var list;
                if (isS3) {
                    list = _.map(_.union(S3SourcetypeConfig.SOURCETYPE_LIST, sourcetypeList),
                        function(sourcetype) {
                            return {
                                label: sourcetype,
                                value: sourcetype
                            };
                        });
                    children.sourcetype.control.setAutoCompleteFields(list, false);
                    children.sourcetype.on("change", this.onSourceTypeChange.bind(this));
                }
                if (isKinesis){
                    list = _.map(_.union(KinesisSourcetypeConfig.SOURCETYPE_LIST, sourcetypeList),
                        function(sourcetype) {
                            return {
                                label: sourcetype,
                                value: sourcetype
                            };
                        });
                    children.sourcetype.control.setAutoCompleteFields(list, false);
                }
            }

            if (options.index) {
                children.index = new ControlWrapper({
                    label: _('Index').t(),
                    controlType: SingleInputControlEx,
                    wrapperClass: 'index',
                    required: true,
                    controlOptions: {
                        model: this.model,
                        modelAttribute: 'index',
                        disableSearch: false,
                        autoCompleteFields: [{
                            label: "default",
                            value: "default"
                        }]
                    }
                });

                this.loadDataSyncUI();
            }

            if (options.host) {
                children.host = new ControlWrapper({
                    label: _('Host').t(),
                    controlType: TextControl,
                    wrapperClass: 'host',
                    controlOptions: {
                        model: this.model,
                        modelAttribute: 'host'
                    }
                });
            }

            if (options.log_type) {
                children.log_type = new ControlWrapper({
                    label: _('Log Type').t(),
                    controlType: SingleInputControl,
                    wrapperClass: 'log_type',
                    required: true,
                    controlOptions: {
                        model: this.model,
                        modelAttribute: 'log_type',
                        placeholder: _("Select an AWS Log Type").t(),
                        autoCompleteFields: []
                    }
                });

                children.log_type.control.setAutoCompleteFields(LogsLogTypeConfig.LOG_TYPE_LIST, false);
                children.log_type.on("change", this.onLogTypeChange.bind(this));
            }

            if (options.logs_sourcetype) {
                children.logs_sourcetype = new ControlWrapper({
                    label: _('Source Type').t(),
                    controlType: TextControl,
                    wrapperClass: 'logs_sourcetype',
                    required: true,
                    controlOptions: {
                        model: this.model,
                        modelAttribute: 'logs_sourcetype'
                    }
                });
            }
        },

        _loadIndex: function() {
            var indexes = new Indexes([], {
                appData: {
                    app: appData.get("app"),
                    owner: appData.get("owner")
                },
                targetApp: "-",
                targetOwner: "nobody"
            });
            indexes.deferred = indexes.fetch();
            var attr = 'index';
            var job = this.startLoadingForModel(this.real_model, attr);

            indexes.deferred.done(function() {
                var ids = indexes.models[0].attributes.entry[0].content.indexes;
                var id_lst = _.map(ids, function(index) {
                    return {
                        label: index,
                        value: index
                    };
                });
                id_lst = this.ensureModelValueInDropdownList(attr, id_lst);

                var list = _.flatten([{
                        label: "default",
                        value: "default"
                    },
                    id_lst
                ]);
                this.children.index.control.setAutoCompleteFields(list, true);

                if (id_lst.length >= 1) {
                    this.showJobSuccess(job, this.real_model, attr);
                } else {
                    this.showJobNoData(job, this.real_model, attr);
                }

            }.bind(this)).fail(function(ajaxRet) {
                this.showJobFailure(job, this.real_model, attr, ajaxRet);
            }.bind(this));
        },

        // just called once
        loadDataSyncUI: function() {
            if (!this.children.index) {
                return;
            }

            this._loadIndex();
        },
        onSourceTypeChange: function(val){
            var sectionType = S3SourcetypeConfig.getSettingsBySourcetype(val);
            this.trigger("templateChange", sectionType);
        },
        onLogTypeChange: function (val) {
            if (val==this.log_type && this.logs_sourcetype) {
                this.children.logs_sourcetype.control.setValue(this.logs_sourcetype, true);
            } else {
                this.children.logs_sourcetype.control.setValue(LogsLogTypeConfig.getSourceType(val), true);
            }

            var logTypeSettings = LogsLogTypeConfig.getSettings(val);
            this.trigger("s3LogTypeChange", logTypeSettings);
        },
        renderContent: function($body) {
            if (this.children.log_type) {
                $body.append(this.children.log_type.render().$el);
            }
            if (this.children.logs_sourcetype) {
                $body.append(this.children.logs_sourcetype.render().$el);
            }
            if (this.children.interval) {
                $body.append(this.children.interval.render().$el);
            }
            if (this.children.sourcetype) {
                $body.append(this.children.sourcetype.render().$el);
            }
            if (this.children.index) {
                $body.append(this.children.index.render().$el);
            }
            if (this.children.host) {
                $body.append(this.children.host.render().$el);
            }
            return this;
        }
    });
});
