define([
    'jquery',
    'underscore',
    'backbone',
    'app/models/appData',
    'app/collections/Buckets',
    'app/views/Models/BaseSection',
    'views/shared/controls/TextControl',
    'app/views/Models/SingleInputControl',
    'app/views/Models/ControlWrapper',
    'app/collections/Accounts'

], function ($, _, Backbone, appData, Buckets, BaseSection, TextControl, SingleInputControl, ControlWrapper, Accounts) {
    return BaseSection.extend({
        initialize: function (options) {
            BaseSection.prototype.initialize.apply(this, arguments);

            var accounts = new Accounts([], {
                appData: { app: appData.get("app"), owner: appData.get("owner") },
                targetApp: "-",
                targetOwner: "nobody"
            });

            accounts.deferred = accounts.fetch();
            var job = this.startLoadingForModel(this.real_model, 'aws_account');

            this.children.awsAccount = new ControlWrapper({
                label: _('AWS Account').t(),
                controlType: SingleInputControl,
                wrapperClass: 'aws_account',
                required: true,
                controlOptions: {
                    model: this.model,
                    modelAttribute: 'aws_account',
                    placeholder: _("Select an AWS Account").t(),
                    autoCompleteFields: []
                }
            });
            accounts.deferred.done(function () {
                var data = _.map(accounts.models, function (account) {
                        var name = account.entry.content.get('name');
                        return {
                            value: name,
                            label: name,
                            disabled: [1, 2, 4].indexOf(+account.entry.content.get('category')) < 0
                        };
                    });
                this.children.awsAccount.control.setAutoCompleteFields(data,true);
                if (data.length >= 1)
                    this.showJobSuccess(job, this.real_model, 'aws_account');
                else
                    this.showJobNoData(job, this.real_model, 'aws_account');

            }.bind(this)).fail(function(ajaxRet) {
                this.showJobFailure(job, this.real_model, 'aws_account', ajaxRet);
            }.bind(this));
        },

        sectionReady: function () {
            return this.promise;
        },

        renderContent: function ($body) {
            $body.append(this.children.awsAccount.render().$el);
            return this;
        }

    });
});
