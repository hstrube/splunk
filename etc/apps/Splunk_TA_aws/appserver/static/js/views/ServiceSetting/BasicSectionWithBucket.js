define([
    'jquery',
    'underscore',
    'backbone',
    'app/models/appData',
    'app/collections/Buckets',
    'app/views/Models/BaseSection',
    'views/shared/controls/TextControl',
    'app/views/Models/SingleInputControl',
    'app/views/Models/SingleInputControlEx',
    'app/views/Models/ControlWrapper',
    'views/shared/controls/SyntheticCheckboxControl',
    'app/collections/Accounts',
    'app/collections/IAMRoles'
], function(
    $,
    _,
    Backbone,
    appData,
    Buckets,
    BaseSection,
    TextControl,
    SingleInputControl,
    SingleInputControlEx,
    ControlWrapper,
    SyntheticCheckboxControl,
    Accounts,
    IAMRoles
) {
    return BaseSection.extend({
        initialize: function(options) {
            BaseSection.prototype.initialize.apply(this, arguments);
            this.service_type = options.service_type;
            this.createUIComponents();
            this.loadDataSyncUI();
            this._bucketListDeferred = null;
            this._accountCategory = {};
        },

        // just called once
        createUIComponents: function() {
            this.children.awsAccount = new ControlWrapper({
                label: _('AWS Account').t(),
                controlType: SingleInputControl,
                wrapperClass: 'aws_account',
                required: true,
                controlOptions: {
                    model: this.model,
                    modelAttribute: 'aws_account',
                    placeholder: _("Select an AWS Account").t(),
                    autoCompleteFields: []
                }
            });
            this.children.awsAccount.on("change", this.onAccountChange.bind(this));
            this.children.assumedRole = new ControlWrapper({
                label: _('Assume Role').t(),
                controlType: SingleInputControl,
                wrapperClass: 'aws_iam_role',
                required: false,
                controlOptions: {
                    model: this.model,
                    modelAttribute: 'aws_iam_role',
                    placeholder: _("Select an AWS IAM Role to Assume (Optional)").t(),
                    allowClear: true,
                    autoCompleteFields: []
                }
            });
            this.children.assumedRole.on("change", this.onAccountChange.bind(this));
            var hostName = this.model.get('host_name');
            var hostNameList = [
                "s3.amazonaws.com",
                "s3.cn-north-1.amazonaws.com.cn"
            ];
            if (hostName && hostNameList.indexOf(hostName) < 0) {
                hostNameList.push(hostName);
            }
            this.children.s3HostName = new ControlWrapper({
                label: _('S3 Host Name').t(),
                controlType: SingleInputControlEx,
                wrapperClass: 'host_name',
                required: true,
                controlOptions: {
                    model: this.model,
                    modelAttribute: 'host_name',
                    placeholder: _("Select a Host Name").t(),
                    allowClear: true,
                    disableSearch: false,
                    autoCompleteFields: _.map(hostNameList, function(host){
                        return {
                            value: host,
                            label: host
                        };
                    })
                }
            });
            this.children.s3Bucket = new ControlWrapper({
                label: _('S3 Bucket').t(),
                controlType: SingleInputControlEx,
                wrapperClass: 'bucket_name',
                required: true,
                controlOptions: {
                    model: this.model,
                    modelAttribute: 'bucket_name',
                    placeholder: _("Select a Bucket").t(),
                    allowClear: true,
                    disableSearch: false,
                    autoCompleteFields: []
                }
            });

            this.children.incremental = new ControlWrapper({
                label: _('Incremental Log').t(),
                controlType: SyntheticCheckboxControl,
                wrapperClass: 'incremental',
                controlOptions: {
                    model: this.model,
                    modelAttribute: 'incremental'
                }
            });
            this.children.incremental.on("change", this.onIncrementalChange.bind(this));
        },
        onAccountChange: function(val) {
            if (+this._accountCategory[val] === 4) { //means this account only have permission to China regions
                this.children.s3HostName.control.setValue("s3.cn-north-1.amazonaws.com.cn");
            } else {
                this.children.s3HostName.control.setValue("s3.amazonaws.com");
            }
        },

        onIncrementalChange: function(val){
            this.trigger("s3IncrementalChange", val);
        },

        // just called once
        loadDataSyncUI: function() {
            var accounts = new Accounts([], {
                appData: {
                    app: appData.get("app"),
                    owner: appData.get("owner")
                },
                targetApp: "-",
                targetOwner: "nobody"
            });
            accounts.deferred = accounts.fetch();
            var job = this.startLoadingForModel(this.real_model, 'aws_account');
            accounts.deferred.done(function() {
                // load AWS Account into list
                var data = _.map(accounts.models, function(account) {
                    var name = account.entry.content.get('name');
                    this._accountCategory[name] = account.entry.content.get('category');
                    return {
                        value: name,
                        label: name,
                        disabled: [1, 2, 4].indexOf(+account.entry.content.get('category')) < 0
                    };
                }, this);
                this.children.awsAccount.control.setAutoCompleteFields(data, true);

                if (data.length >= 1)
                    this.showJobSuccess(job, this.real_model, 'aws_account');
                else
                    this.showJobNoData(job, this.real_model, 'aws_account');
                // start to sync AWS Account changing
                this.handleUIChange();
            }.bind(this)).fail(function(ajaxRet) {
                this.showJobFailure(job, this.real_model, 'aws_account', ajaxRet);
            }.bind(this));

            //Load IAM roles
            var iamRoles = new IAMRoles([], {
                appData: {
                    app: appData.get("app"),
                    owner: appData.get("owner")
                },
                targetApp: "-",
                targetOwner: "nobody"
            });
            iamRoles.deferred = iamRoles.fetch();
            var iamRoleJob = this.startLoadingForModel(this.real_model, 'IAM Roles');
            iamRoles.deferred.done(function() {
                var data = _.map(iamRoles.models, function(iamRole) {
                    var name = iamRole.entry.attributes.name;
                    return {
                        value: name,
                        label: name
                    };
                }, this);
                this.children.assumedRole.control.setAutoCompleteFields(data, true);

                if (data.length >= 1)
                    this.showJobSuccess(iamRoleJob, this.real_model, 'Assume Role');
                else
                    this._removeOngoingJob(iamRoleJob);
                this.handleUIChange();
            }.bind(this)).fail(function(ajaxRet) {
                this.showJobFailure(iamRoleJob, this.real_model, 'Assume Role', ajaxRet);
            }.bind(this));
        },

        refresh_bucket_list: function() {
            var aws_account = this.model.get("aws_account");
            var host_name = this.model.get("host_name");
            var attr = "bucket_name";
            this.children.s3Bucket.control.setAutoCompleteFields(this.getDefaultDropdownlistItem(attr), true);

            if (!aws_account || !host_name) {
                return;
            }

            var buckets = new Buckets([], {
                appData: {
                    app: appData.get("app"),
                    owner: appData.get("owner")
                },
                targetApp: "-",
                targetOwner: "nobody"
            });

            if (this._bucketListDeferred && this._bucketListDeferred.state() === "pending") {
                this._bucketListDeferred.abort("force");
            }
            this._bucketListDeferred = buckets.deferred = buckets.fetch({
                data: {
                    aws_account: this.model.get("aws_account"),
                    host_name: this.model.get("host_name"),
                    aws_iam_role: this.model.get("aws_iam_role")
                }
            });
            var job = this.startLoadingForModel(this.real_model, 'bucket_name');

            buckets.deferred.done(function() {
                var data = _.map(buckets.models[0].entry.content.attributes.buckets, function(bucket) {
                    return {
                        label: bucket,
                        value: bucket
                    };
                });

                if (data.length >= 1)
                    this.showJobSuccess(job, this.real_model, 'bucket_name');
                else
                    this.showJobNoData(job, this.real_model, 'bucket_name', [
                        "Incorrect key id and/or secret key of this account",
                        "This account is not supported by this service.",
                        "The host name does not match the account.",
                        "Internet connection is not stable.",
                        "AWS server is down."
                    ]);

                data = this.ensureModelValueInDropdownList(attr, data);
                this.children.s3Bucket.control.setAutoCompleteFields(data, true);
            }.bind(this)).fail(function(ajaxRet, response) {
                if (response === "force") {
                    //no response means manually aborted.
                    this._removeOngoingJob(job);
                    return;
                }
                this.showJobFailure(job, this.real_model, 'bucket_name', ajaxRet);
            }.bind(this));

        },

        // handle AWS Account and host name change to reresh bucket list
        handleUIChange: function() {
            this.model.on("change:aws_account", this.refresh_bucket_list.bind(this));
            this.model.on("change:aws_iam_role", this.refresh_bucket_list.bind(this));
            this.model.on("change:host_name", this.refresh_bucket_list.bind(this));

            // for edit mode, check if aws_account is ready, reload it accordingly
            if (this.model.get("aws_account") && this.model.get("host_name"))
                this.refresh_bucket_list();
        },

        renderContent: function($body) {
            $body.append(this.children.awsAccount.render().$el);
            if (['s3', 'billing'].indexOf(this.service_type) >= 0) {
                $body.append(this.children.assumedRole.render().$el);
            }
            $body.append(this.children.s3HostName.render().$el);
            $body.append(this.children.s3Bucket.render().$el);
            if (this.service_type == 's3') {
                var editMode = this.options.newInput.edit_mode;
                if (editMode){
                    this.children.incremental.control.disable();
                }
                $body.append(this.children.incremental.render().$el);
            }
            return this;
        }
    });
});
