define([
    'jquery',
    'underscore',
    'backbone',
    'app/models/appData',
    'app/views/Models/BaseSection',
    'views/shared/controls/TextControl',
    'app/views/Models/SingleInputControl',
    'app/views/Models/SingleInputControlEx',
    'app/views/Models/MultiSelectInputControlFieldEditable',
    'app/views/Models/ControlWrapper',
    'app/collections/SQSQueues',
    "contrib/text!app/templates/Models/LoadingMsg.html",
    "contrib/text!app/templates/Models/ErrorMsg.html"

], function (
    $,
    _,
    Backbone,
    appData,
    BaseSection,
    TextControl,
    SingleInputControl,
    SingleInputControlEx,
    MultiSelectInputControlFieldEditable,
    ControlWrapper,
    SQSQueues,
    LoadingMsg,
    ErrorMsg
) {
    return BaseSection.extend({
        initialize: function () {
            BaseSection.prototype.initialize.apply(this, arguments);

            this.children.aws_sqs_queues = new ControlWrapper({
                label: _('SQS Queues').t(),
                controlType: MultiSelectInputControlFieldEditable,
                wrapperClass: 'sqs_queues',
                placeholder: _('Select AWS SQS Queues').t(),
                required: true,
                controlOptions: {
                    model: this.model,
                    allowClear: true,
                    modelAttribute: 'sqs_queues',
                    disableSearch: false,
                    autoCompleteFields: []
                }
            });

            this.loadDataSyncUI();
            this.model.on("change:aws_account", this.loadDataSyncUI.bind(this));
            this.model.on("change:aws_region", this.loadDataSyncUI.bind(this));
        },

        // just called once
        loadDataSyncUI: function () {
            var aws_account = this.model.get("aws_account");
            var aws_region = this.model.get("aws_region");
            var attr = 'sqs_queues';
            if (!aws_account || !aws_region) {
                return;
            }

            var sqsQueues = new SQSQueues([], {
                appData: { app: appData.get("app"), owner: appData.get("owner") },
                targetApp: "-",
                targetOwner: "nobody"
            });
            sqsQueues.deferred = sqsQueues.fetch({
                data: {
                    aws_account: this.model.get("aws_account"),
                    aws_region: this.model.get("aws_region")
                }
            });
            var job = this.startLoadingForModel(this.real_model, attr);

            sqsQueues.deferred.done(function () {
                // Load All AWS SQS Queues
                var queues_all = [];
                _.each(sqsQueues.models[0].entry.content.attributes.sqs_queue_names, function (sqsQueue) {
                    queues_all[sqsQueue] = {
                        mainField: {
                            id: sqsQueue,
                            label: sqsQueue,
                            initialSelected: false
                        }
                    }
                });

                // Load Selected AWS Queues
                var queues_deleted = [];
                var queues_existing = [];
                var attr_val = this.model.get(attr);
                if (attr_val != undefined) {
                    _.each(this.model.get(attr).split(/\s*,\s*/), function (sqsQueue) {
                        if (sqsQueue in queues_all) {
                            queues_all[sqsQueue].mainField.initialSelected = true;
                            queues_existing.push(sqsQueue);
                        } else {
                            queues_deleted.push(sqsQueue);
                        }
                    });
                    this.model.attributes.sqs_queues = queues_existing.join(', ');
                }


                var queues_all_list = [];
                for(var sqsQueue in queues_all) {
                    if (queues_all.hasOwnProperty(sqsQueue)){
                        queues_all_list.push(queues_all[sqsQueue]);
                    }
                }

                if (queues_all_list.length >= 1)
                    this.showJobSuccess(job, this.real_model, attr);
                else
                    this.showJobNoData(job, this.real_model, attr);

                this.children.aws_sqs_queues.control.setItems(queues_all_list);
                this.children.aws_sqs_queues.control.render();
                if (queues_deleted.length>0){
                    var msg = 'Some SQS queues are not found in AWS, please update them: ' + queues_deleted.join(', ');
                    this.container.addErrorMsg(msg);
                }
            }.bind(this)).fail(function(ajaxRet) {
                this.showJobFailure(job, this.real_model, attr, ajaxRet);
            }.bind(this));
        },

        renderContent: function ($body) {
            $body.append(this.children.aws_sqs_queues.render().$el);
            return this;
        }
    });
});
