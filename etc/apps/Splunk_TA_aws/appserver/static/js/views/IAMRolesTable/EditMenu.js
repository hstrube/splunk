define(
    [
        'underscore',
        'jquery',
        'views/shared/PopTart',
        'views/shared/Sidebar',
        'app/views/Dialogs/DeleteIAMRole',
        'app/views/Dialogs/EditIAMRole'
    ],
    function(
        _,
        $,
        PopTartView,
        SideBar,
        DeleteIAMRole,
        EditIAMRole
    ){
        return PopTartView.extend({
            className: 'dropdown-menu',
            initialize: function(options) {
                _.bindAll(this, 'edit', 'delete');
                PopTartView.prototype.initialize.apply(this, arguments);
                this.collection = options.collection;
                this.model = options.model;
                this.url = options.url;
                this.iam = options.iam;
            },

            events: {
                'click a.edit': 'edit',
                'click a.delete': 'delete'
            },

            render: function(){
                var html = this.compiledTemplate({
                    iam: this.iam
                });
                this.el.innerHTML = PopTartView.prototype.template_menu;
                this.$el.append(html);
                this.$el.addClass('dropdown-menu-narrow');
                return this;
            },

            edit: function(e){
                var editIAMRole = new EditIAMRole({
                    el: $("#edit-iam-role-dialog"),
                    collection: this.collection,
                    model: this.model
                });
                editIAMRole.render().modal();
                this.hide();
                e.preventDefault();
            },

            delete: function(e) {
                var in_use = this.model.entry.content.attributes.inputs > 0 ? true: false;
                var deleteIAMRole = new DeleteIAMRole({
                    el: $("#delete-iam-role-dialog"),
                    collection: this.collection,
                    model: this.model,
                    in_use: in_use
                });
                deleteIAMRole.render().modal();
                this.hide();
                e.preventDefault();
            },

            template: [
                '<ul class="first-group">',
                '<li><a href="#" class="edit"><%- _("Edit").t() %></a></li>',
                '<li><a href="#" class="delete"><%- _("Delete").t() %></a></li>',
                '</ul>'
            ].join('')
        });
    }
);
