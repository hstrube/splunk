define([
    "jquery",
    'underscore',
    'backbone',
    'views/Base',
    'app/views/IAMRolesTable/EditMenu',
    'views/shared/controls/ControlGroup',
    'views/shared/controls/SyntheticCheckboxControl'
], function(
    $,
    _,
    Backbone,
    BaseView,
    EditMenu,
    ControlGroup,
    SyntheticCheckboxControl
) {
    return BaseView.extend({
        tagName: 'tr',
        className: 'apps-table-tablerow',
        events: {
            'click td.actions > a.dropdown-toggle': function(e) {
                this.openEdit(e);
            }
        },
        initialize: function(options) {
            BaseView.prototype.initialize.apply(this, arguments);
            this.$el.addClass((this.options.index % 2) ? 'even' : 'odd');

            this.collection = this.model.collection;
            this.allCollection = this.model.allCollection;
            this.enableBulkActions = this.model.enableBulkActions;
            this.showActions = this.model.showActions;

            if (options.dispatcher) {
                this.dispatcher = options.dispatcher;
            }

            if (this.enableBulkActions) {
                if (!this.model.checkbox) {
                    this.model.checkbox = new Backbone.Model();
                    this.model.checkbox.set("checked", 0);
                }
                this.bulkboxControl = new SyntheticCheckboxControl({
                    modelAttribute: 'checked',
                    model: this.model.checkbox
                });
                this.bulkbox = new ControlGroup({
                    controls: [this.bulkboxControl]
                });
            }

            this.activate();
        },

        openEdit: function(e) {
            var $target = $(e.currentTarget);
            if (this.editmenu && this.editmenu.shown) {
                this.editmenu.hide();
                e.preventDefault();
                return;
            }

            var iam = this.model.iam_role.entry.content.get("iam");
            this.editmenu = new EditMenu({
                collection: this.model.collection,
                model: this.model.iam_role,
                url: this.model.collectionURL,
                iam: iam
            });
            $('body').append(this.editmenu.render().el);
            this.editmenu.show($target);
            e.preventDefault();
        },

        render: function() {
            var name = this.model.iam_role.entry.attributes.name;
            var inputs = this.model.iam_role.entry.content.inputs;

            if (typeof inputs === 'undefined') {
                var count = 0;
                _.each(this.allCollection.models, function(model) {
                    if (name == model.entry.content.attributes.aws_iam_role) {
                        count += 1;
                    }
                }.bind(this));
                inputs = count;
                this.model.iam_role.entry.content.attributes.inputs = inputs;
            }
            this.$el.html(this.compiledTemplate({
                name: name,
                inputs: inputs
            }));
            if (this.enableBulkActions) {
                this.$('.box').append(this.bulkbox.render().el);
            }
            return this;
        },

        template: [
            '<% if (this.enableBulkActions) { %>',
            '<td class="box checkbox col-inline"></td>',
            '<% } %>',
            '<td class="name"><%- name %></td>',
            '<td>********</td>',
            '<td><%- inputs %></td>',
            '<td class="actions">',
            '<a class="dropdown-toggle" href="#"><%- _("Action").t() %><span class="caret"></span></a>',
            '</td>'
        ].join('')
    });
});
