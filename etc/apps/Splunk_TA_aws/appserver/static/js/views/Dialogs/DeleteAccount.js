define([
    "jquery",
    "underscore",
    "backbone",
    "bootstrap",
    "contrib/text!app/templates/Dialogs/DeleteAccount.html"
], function(
    $,
    _,
    Backbone,
    Bootstrap,
    DeleteAccount
){
    return Backbone.View.extend({
        template: _.template(DeleteAccount),

        events: {
            "submit form": "delete"
        },

        initialize: function(options) {
            this.collection = options.collection;
            this.model = options.model;
            this.in_use = options.in_use;
        },

        render: function() {
            this.$el.html(this.template({
                account_name: this.model.entry.content.attributes.name,
                in_use: this.in_use
            }));

            var dlg=this;
            this.$("[role=dialog]").on('hidden.bs.modal', function() {
                dlg.undelegateEvents();
            });

            return this;
        },

        delete: function() {
            var $submit = this.$("input[type=submit]");
            $submit.attr("disabled", "disabled");

            var delete_url = this.model.collection.url + '/' + this.model.entry.attributes.name + "?output_mode=json";
            $.ajax({
                url: delete_url,
                type: 'DELETE'
            }).done(function(){
                this.collection.remove(this.model);
                if(this.collection.length !== 0){
                    _.each(this.collection.models,function(model){
                        model.paging.set('total',this.collection.length);
                    }.bind(this));
                }
                this.collection.reset(this.collection.models);
                this.$("[role=dialog]").modal('hide');
            }.bind(this)).fail(function(model,response){
                this.$("[role=dialog]").modal('hide');
                var rsp = JSON.parse(model.responseText);
                var type = rsp.messages[0].type;
                var regx=/External handler failed.+and output:\s+'([\s\S]*)'\.\s+See splunkd\.log for stderr output\./;
                var msg = String(rsp.messages[0].text);
                var matches=regx.exec(msg);
                alert(type+':\n'+matches[1]);
            }.bind(this));
        },

        modal: function() {
            this.$("[role=dialog]").modal({backdrop: 'static', keyboard: false});
        }
    });
});
