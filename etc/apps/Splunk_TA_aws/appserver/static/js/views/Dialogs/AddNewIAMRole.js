define([
    "jquery",
    "underscore",
    "backbone",
    "bootstrap",
    "bootstrapValidator",
    "contrib/text!app/templates/Dialogs/AddNewIAMRole.html",
    "contrib/text!app/templates/Models/ErrorMsg.html",
    "contrib/text!app/templates/Models/LoadingMsg.html",
    "app/models/IAMRole",
    "app/util/CheckboxUtil"
], function(
    $,
    _,
    Backbone,
    Bootstrap,
    BootstrapValidator,
    AddNewIAMRole,
    ErrorMsg,
    LoadingMsg,
    IAMRole,
    CheckboxUtil
) {
    return Backbone.View.extend({
        template: _.template(AddNewIAMRole),

        events: {
            "submit form": "add",
            "click label.checkbox a.btn": CheckboxUtil.eventHandler,
            "change label.checkbox.ckb-auto-detect a.btn": "onFlagAutoDetectChange"
        },

        initialize: function(options) {
            this.collection = options.collection;
        },

        render: function() {
            this.$el.html(this.template());
            this.categoryModel = new Backbone.Model({
                category: 0
            });
            var dlg = this;
            this.$("[role=dialog]").on('hidden.bs.modal', function() {
                dlg.undelegateEvents();
            });

            return this;
        },

        add: function() {
            var new_iam_role_val = this.getInputs();

            //Check the name
            if (new_iam_role_val.name.indexOf("_") === 0) {
                this.addErrorMsg('Name starting with "_" underscore is not allowed.');
                return;
            }
            this.removeErrorMsg();
            this._addIAMRole(new_iam_role_val);
        },
        _addIAMRole: function(iamRoleVal) {
            var iamRole = new IAMRole({
                appData: this.appData,
                collection: this.collection
            });
            iamRole.on("invalid", this.displayValidationError.bind(this));
            iamRole.entry.content.set(iamRoleVal);

            var deferred = iamRole.save();
            if (!deferred) {
                return;
            }
            var $submit = this.$("input[type=submit]");
            $submit.attr("disabled", "disabled");
            deferred.done(function() {
                this.collection.add(iamRole);
                if (this.collection.length !== 0) {
                    _.each(this.collection.models, function(model) {
                        model.paging.set('total', this.collection.length);
                    }.bind(this));
                }
                this.collection.reset(this.collection.models);
                this.$("[role=dialog]").modal('hide');
            }.bind(this)).fail(function(model, response) {
                $submit.removeAttr("disabled");
                this.displayError(model, response);
            }.bind(this));
        },
        onFlagAutoDetectChange: function(e, data) {
            var funcName = data.checked ? "addClass" : "removeClass";
            this.$(".ckb-category").each(function() {
                var $el = $(this);
                $el[funcName]("disabled");
                $el.find("a.btn")[funcName]("disabled");
            });
        },
        addErrorMsg: function(text) {
            if (this.$('.msg-error').length) {
                this.$('.msg-error').remove();
            }
            this.$(".modal-body").prepend(_.template(ErrorMsg, {
                msg: text
            }));
        },

        removeErrorMsg: function() {
            console.log("removeErrorMsg");

            if (this.$('.msg-error').length) {
                this.$('.msg-error').remove();
            }
        },

        addLoadingMsg: function(text) {
            console.log("addLoadingMsg:" + text);
            if (this.$('.msg-loading').length) {
                this.$('.msg-loading > .msg-text').text(text);
            } else {
                this.$(".modal-body").prepend(_.template(LoadingMsg, {
                    msg: text
                }));
            }
        },

        removeLoadingMsg: function() {
            console.log("removeLoadingMsg");
            if (this.$('.msg-loading').length) {
                this.$('.msg-loading').remove();
            }
        },

        parseAjaxError: function(model) {
            var text;
            try {
                var rsp = JSON.parse(model.responseText);
                var regx = /External handler failed.+and output:\s+'([\s\S]*)'\.\s+See splunkd\.log for stderr output\./;
                var msg = String(rsp.messages[0].text);
                var matches = regx.exec(msg);
                if (!matches || !matches[1]) {
                    // try to extract another one
                    regx = /In handler[^:]+:\s+(.*)/;
                    matches = regx.exec(msg);
                    if (!matches || !matches[1]) {
                        return msg;
                    }
                }
                text = matches[1];
            } catch (e) {
                text = "Error happens while creating IAM Role. See splunkd.log for stderr output.";
            } finally {
                return text;
            }
        },

        displayValidationError: function(model, error) {
            this.addErrorMsg(error);
        },
        displayError: function(model, response) {
            this.addErrorMsg(this.parseAjaxError(model, response));
        },

        getInputs: function() {
            return {
                "name": this.$("[name=new_iam_role_dialog_name]").val(),
                "arn": this.$("[name=new_iam_role_dialog_arn]").val()
            };
        },
        modal: function() {
            this.$("[role=dialog]").modal({
                backdrop: 'static',
                keyboard: false
            });
        }
    });
});
