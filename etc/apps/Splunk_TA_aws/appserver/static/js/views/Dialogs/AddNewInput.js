define([
    'jquery',
    'underscore',
    'backbone',
    "bootstrap",
    "bootstrapValidator",
    "splunk.util",
    'app/models/appData',
    'app/views/Models/SingleInputControl',
    'views/shared/tablecaption/Master',
    'contrib/text!app/templates/Inputs/InputTitle.html',
    'contrib/text!app/templates/Models/BillingSetting.html',
    'app/collections/Accounts',
    'app/util/Util',
    'app/util/Service.Description.API.Config',
    'app/util/Service.S3.Sourcetype.Config',
    'app/util/Service.Logs.LogType.Config',

    // Setting Panel
    'app/views/ServiceSetting/MetadataSection',
    'app/views/ServiceSetting/BasicSection',
    'app/views/ServiceSetting/BasicSectionWithBucket',
    'app/views/ServiceSetting/BasicSectionWithRegion',
    'app/views/ServiceSetting/SettingBilling',
    'app/views/ServiceSetting/SettingCloudTrail',
    'app/views/ServiceSetting/SettingCloudWatch',
    'app/views/ServiceSetting/SettingCloudWatchLog',
    'app/views/ServiceSetting/SettingConfig',
    'app/views/ServiceSetting/SettingDescription',
    'app/views/ServiceSetting/SettingKinesis',
    'app/views/ServiceSetting/SettingConfigRule',
    'app/views/ServiceSetting/SettingSQS',

    // Model
    'app/models/Service.Billing.Model',
    'app/models/Service.CloudTrail.Model',
    'app/models/Service.CloudWatch.Model',
    'app/models/Service.CloudWatchLogs.Model',
    'app/models/Service.Config.Model',
    'app/models/Service.Description.Model',
    'app/models/Service.Kinesis.Model',
    'app/models/Service.ConfigRule.Model',
    'app/models/Service.Inspector.Model',
    'app/models/Service.S3.Model',
    'app/models/Service.S3Universal.Model',
    'app/models/Service.SQS.Model',
    'app/models/Service.Logs.Model',

    "contrib/text!app/templates/Dialogs/AddNewInput.html",
    "contrib/text!app/templates/Dialogs/AddNewInputV2.html",
    "contrib/text!app/templates/Dialogs/AddNewS3Input.html",
    "contrib/text!app/templates/Dialogs/AddNewInspectorInput.html",
    "contrib/text!app/templates/Dialogs/EditInput.html",
    "contrib/text!app/templates/Dialogs/EditInputV2.html",
    "contrib/text!app/templates/Dialogs/EditS3Input.html",
    "contrib/text!app/templates/Dialogs/EditInspectorInput.html",
    "contrib/text!app/templates/Models/ErrorMsg.html",
    "contrib/text!app/templates/Models/LoadingMsg.html",
    'views/shared/controls/TextControl',
    'app/views/Models/ControlWrapper',
    'views/shared/controls/ControlGroup'
], function(
    $,
    _,
    Backbone,
    Bootstrap,
    BootstrapValidator,
    splunkdUtils,
    appData,
    SingleInputControl,
    CaptionView,
    InputTitleTemplate,
    BillingSetting,
    Accounts,
    Util,
    DescriptionConfig,
    S3SourcetypeConfig,
    LogsLogTypeConfig,
    MetadataSection,
    BasicSection,
    BasicSectionWithBucket,
    BasicSectionWithRegion,
    SettingBilling,
    SettingCloudTrail,
    SettingCloudWatch,
    SettingCloudWatchLog,
    SettingConfig,
    SettingDescription,
    SettingKinesis,
    SettingConfigRule,
    SettingSQS,
    BillingInput,
    CloudTrailInput,
    CloudWatchInput,
    CloudWatchLogsInput,
    ConfigInput,
    DescriptionInput,
    KinesisInput,
    ConfigRuleInput,
    InspectorInput,
    S3Input,
    S3UniversalInput,
    SQSInput,
    LogsInput,
    AddNewInputDialogTemplate,
    AddNewInputV2DialogTemplate,
    AddNewS3InputDialogTemplate,
    AddNewInspectorInputDialogTemplate,
    EditInputDialogTemplate,
    EditInputV2DialogTemplate,
    EditS3InputDialogTemplate,
    EditInspectorInputDialogTemplate,
    ErrorMsg,
    LoadingMsg,
    TextControl,
    ControlWrapper,
    ControlGroup
) {

    const DEPRECATED_SERVICES = ['cloudwatchlog'];

    function formatTimeZone(tz){
        var isPositive = tz > 0;
        var abs = Math.abs(tz);
        var isLessThanTen = abs < 10;
        var ret = abs + "00";
        if (isLessThanTen) {
            ret = "0" + ret;
        }
        if (isPositive) {
            ret = "+" + ret;
        } else {
            ret = "-" + ret;
        }
        return ret;
    }

    function getToday(){
        var today = new Date();
        var tz = today.getTimezoneOffset() / -60;
        today.setTime(today.getTime() - (7 * 24 - tz) * 3600 * 1000);
        today.setMilliseconds(0);
        var str = today.toISOString().split(".000Z")[0] + formatTimeZone(tz);
        return str;
    }

    function getDaysDelta(days, date_only) {
        var date = new Date();
        date.setDate(date.getDate() + days);
        date.setTime(date.getTime());
        date.setMilliseconds(0);
        if(date_only){
            return date.toISOString().substring(0, 10);
        }else{
            return date.toISOString().substring(0, 19) + 'Z';
        }
    }

    function getMonthsDelta(months, date_only) {
        var date = new Date();
        date.setMonth(date.getMonth() + months);
        date.setTime(date.getTime());
        if(date_only){
            return date.toISOString().substring(0, 10);
        }else{
            return date.toISOString().substring(0, 19) + 'Z';
        }
    }

    return Backbone.View.extend({
        initialize: function(options) {
            this.service_type = options.service_type;
            this.collection = options.collection;
            this.dispatcher = options.dispatcher;
            this.sv_map = {
                "billing": {
                    name: "billing",
                    title: "AWS Billing",
                    targetType: BasicSectionWithBucket,
                    settingType: SettingBilling,
                    inputModel: BillingInput,
                    helpLink: "aws.billing",
                    default_value: {
                        host_name: 's3.amazonaws.com',
                        sourcetype: "aws:billing",
                        interval: 86400,
                        index: "default",
                        monthly_report_type: 2,
                        detail_report_type: 2,
                        initial_scan_datetime: getMonthsDelta(-3, false)
                    }
                },
                "s3": {
                    name: "s3",
                    title: "AWS S3",
                    targetType: BasicSectionWithBucket,
                    settingType: S3SourcetypeConfig.getSettingsBySourcetype,
                    inputModel: S3UniversalInput,
                    helpLink: "aws.s3",
                    default_value: {
                        incremental: 1,
                        host_name: 's3.amazonaws.com',
                        sourcetype: "aws:s3",
                        interval: 1800,
                        index: "default",
                        character_set: "auto",
                        initial_scan_datetime: getToday(),
                        log_start_date: getDaysDelta(-7, true)
                    }
                },
                "config": {
                    name: "config",
                    title: "AWS Config",
                    targetType: BasicSectionWithRegion,
                    settingType: SettingConfig,
                    inputModel: ConfigInput,
                    helpLink: "aws.config",
                    default_value: {
                        sourcetype: "aws:config",
                        interval: 30,
                        index: "default"
                    }
                },
                "cloudwatch": {
                    name: "cloudwatch",
                    title: "AWS CloudWatch",
                    targetType: BasicSectionWithRegion,
                    settingType: SettingCloudWatch,
                    inputModel: CloudWatchInput,
                    helpLink: "aws.cloudwatch",
                    default_value: {
                        sourcetype: "aws:cloudwatch",
                        index: "default"
                    }
                },

                "cloudtrail": {
                    name: "cloudtrail",
                    title: "AWS CloudTrail",
                    targetType: BasicSectionWithRegion,
                    settingType: SettingCloudTrail,
                    inputModel: CloudTrailInput,
                    helpLink: "aws.cloudtrail",
                    default_value: {
                        sourcetype: "aws:cloudtrail",
                        interval: 30,
                        index: "default",
                        exclude_describe_events: "1",
                        remove_files_when_done: "0"
                    }
                },

                "cloudwatchlog": {
                    name: "cloudwatchlog",
                    title: "AWS CloudWatch Logs",
                    targetType: BasicSectionWithRegion,
                    settingType: SettingCloudWatchLog,
                    inputModel: CloudWatchLogsInput,
                    helpLink: "aws.cloudwatchlogs",
                    default_value: {
                        sourcetype: "aws:cloudwatchlogs:vpcflow",
                        interval: 600,
                        index: "default",
                        delay: 1800,
                        stream_matcher: ".*",
                        only_after: "1970-01-01T00:00:00"
                    }
                },
                "description": {
                    name: "description",
                    title: "AWS Description",
                    targetType: BasicSectionWithRegion,
                    settingType: SettingDescription,
                    inputModel: DescriptionInput,
                    helpLink: "aws.description",
                    default_value: {
                        sourcetype: "aws:description",
                        index: "default",
                        apis: DescriptionConfig.API_INTERVAL_INPUT_DEFAULT_VALUE
                    }
                },
                "kinesis": {
                    name: "kinesis",
                    title: "AWS Kinesis",
                    targetType: BasicSectionWithRegion,
                    settingType: SettingKinesis,
                    inputModel: KinesisInput,
                    helpLink: "aws.kinesis",
                    default_value: {
                        sourcetype: "aws:kinesis",
                        index: "default",
                        init_stream_position: "LATEST",
                        encoding: "",
                        format: "",
                    }
                },
                "configrule": {
                    name: "configrule",
                    title: "AWS Config Rules Tasks",
                    targetType: BasicSectionWithRegion,
                    settingType: SettingConfigRule,
                    inputModel: ConfigRuleInput,
                    helpLink: "aws.configrules",
                    default_value: {
                        sourcetype: "aws:config:rule",
                        index: "default",
                        interval: 300
                    }
                },

                "inspector": {
                    name: "inspector",
                    title: "AWS Inspector",
                    targetType: BasicSectionWithRegion,
                    inputModel: InspectorInput,
                    helpLink: "aws.inspector",
                    default_value: {
                        sourcetype: "aws:inspector",
                        index: "default",
                        interval: 300
                    }
                },

                "sqs": {
                    name: "sqs",
                    title: "AWS SQS",
                    targetType: BasicSectionWithRegion,
                    inputModel: SQSInput,
                    settingType: SettingSQS,
                    helpLink: "aws.sqs",
                    default_value: {
                        sourcetype: "aws:sqs",
                        index: "default",
                        interval: 30
                    }
                }
            };

            this.edit_mode = options.model ? true : false;
            if (!this.edit_mode) {
                this.model = new Backbone.Model(this.sv_map[this.service_type].default_value);
                var InputType = this.sv_map[this.service_type].inputModel;
                this.real_model = new InputType({
                    appData: this.appData,
                    collection: this.collection
                });
            } else {
                this.model = options.model.entry.content.clone();
                this.model.set({
                    name: options.model.entry.get("name")
                });

                this.real_model = options.model;
            }

            this.real_model.on("invalid", this.displayValidationError.bind(this));
        },

        modal: function() {
            this.$("[role=dialog]").modal({
                backdrop: 'static',
                keyboard: false
            });
        },

        _hasInterval: function(service_type){
            var list = [
                this.sv_map.cloudwatch.name,
                this.sv_map.description.name,
                this.sv_map.kinesis.name
            ];
            return list.indexOf(service_type) < 0;
        },
        submitTask: function() {
            this.$("input[type=submit]").attr('disabled', 'disabled');

            var input = this.real_model;

            var new_json = this.model.toJSON();
            var origial_json = input.entry.content.toJSON();

            input.entry.content.set(new_json);

            try {
                var deffer = input.save();

                if (!deffer.done) {
                    input.entry.content.set(origial_json);
                    input.trigger('change');
                    console.log("validation is not passed.");

                    // re-enable when failed
                    this.$("input[type=submit]").removeAttr('disabled');
                } else {
                    deffer.done(function() {
                        if (!this.edit_mode) {
                            this.collection.add(input);
                            //trigger type change event
                            this.dispatcher.trigger('filter-change', this.service_type);
                        }

                        this.$("[role=dialog]").modal('hide');
                        this.undelegateEvents();

                    }.bind(this)).fail(function(model, response) {
                        input.entry.content.set(origial_json);
                        input.trigger('change');
                        // re-enable when failed
                        this.$("input[type=submit]").removeAttr('disabled');

                        this.displayError(model, response);
                    }.bind(this));
                }
            } catch (err) {
                console.log("error while saving");
                console.log(err);

                // enable button when exception happens
                this.$("input[type=submit]").removeAttr('disabled');
            }
        },

        render: function() {
            var editTemplate = EditInputDialogTemplate;
            var addNewTemplate = AddNewInputDialogTemplate;
            var isS3 = this.service_type === "s3";
            if (isS3) {
                editTemplate = EditS3InputDialogTemplate;
                addNewTemplate = AddNewS3InputDialogTemplate;
            }
            if (this.service_type === "inspector"){
                editTemplate = EditInspectorInputDialogTemplate;
                addNewTemplate = AddNewInspectorInputDialogTemplate;
            }
            if (["sqs", "logs"].indexOf(this.service_type) >= 0){
                editTemplate = EditInputV2DialogTemplate;
                addNewTemplate = AddNewInputV2DialogTemplate;
            }

            var template;
            var json_data = {
                title: this.sv_map[this.service_type].title
            };
            if (this.edit_mode) {
                template = _.template(editTemplate);
                json_data.name = this.model.get("name");
            } else {
                template = _.template(addNewTemplate);
            }

            if (isS3 || this.service_type === 'billing') {
                json_data.tabname = 'Buckets';
            } else {
                json_data.tabname = 'Regions';
            }

            this.$el.html(template(json_data));

            this.$("[role=dialog]").on('hidden.bs.modal', function() {
                this.undelegateEvents();
            }.bind(this));

            this.renderInputName();
            this.renderSetting();

            this.$("input[type=submit]").on("click", this.submitTask.bind(this));

            return this;
        },

        renderInputName: function() {
            this.$("#add_task_dialog_name").empty();

            var helpLink = splunkdUtils.make_url("help") + "?location=" + Util.getLinkPrefix() + this.sv_map[this.service_type].helpLink,
                helpText = "<a class='external' target='_blank' href='" + helpLink + "'>Learn more</a> about configuring this input.";

            if (DEPRECATED_SERVICES.indexOf(this.service_type) !== -1) {
                helpText = "<p class='deprecated_hint'>Input to be deprecated in upcoming releases.</p>" + helpText;
            }

            var inputNameControl = new ControlGroup({
                controlType: 'Text',
                required: !this.edit_mode,
                label: _("Name").t(),
                help: _(helpText).t(),
                enabled: !this.edit_mode,
                controlOptions: {
                    modelAttribute: 'name',
                    model: this.model,
                    placeholder: _("Input a name").t()
                }
            });

            this.$('#add_task_dialog_name').append(inputNameControl.render().$el);

        },

        renderSetting: function() {
            var settingType = this.sv_map[this.service_type].settingType;
            if (this.service_type === "s3") {
                if (this.model.get("incremental") == '1'){
                    settingType = LogsLogTypeConfig.getSettings(this.model.get("log_type"));
                }else{
                    settingType = settingType(this.model.get("sourcetype"));
                }
            }
            this.render_common_setting();
            if (settingType !== undefined) {
                this._renderSetting(settingType);
            }
        },

        _renderSetting: function(settingType) {
            this._currentSettingType = settingType;
            var settingSection = new settingType({
                allowExpand: false,
                model: this.model,
                real_model: this.real_model,
                container: this,
                edit_mode: this.edit_mode
            });

            this.$('#new_task_dialog_config_template').empty();
            this.$('#new_task_dialog_config_template').append(settingSection.render().el);
        },

        renderMetaSection: function (metaSection) {
            metaSection.on("templateChange", this.onTemplateSettingChange.bind(this));
            metaSection.on("s3LogTypeChange", this.onS3LogTypeChange.bind(this));
            this.$('#new_task_dialog_config_setting').empty();
            this.$('#new_task_dialog_config_setting').append(metaSection.render().el);
        },

        onTemplateSettingChange: function(settingType) {
            if (this._currentSettingType === settingType) {
                return;
            }
            this._renderSetting(settingType);
        },

        onS3LogTypeChange: function (logTypeSettings) {
            this._renderSetting(logTypeSettings);
        },

        onS3IncrementalChange: function (val) {
            if (val == '1') {
                var metaSection = new MetadataSection({
                    model: this.model,
                    real_model: this.real_model,
                    container: this,
                    service_type: this.service_type,
                    setting_options: {
                        logs_sourcetype: true,
                        host: false,
                        interval: true,
                        index: true,
                        log_type: true
                    }
                });
                this.renderMetaSection(metaSection);
                this._renderSetting(LogsLogTypeConfig.getSettings());
            } else {
                this.renderMetaSection(this.getMetaSection());
                this.renderSetting();
            }
        },
        render_common_setting: function() {
            var service_type = this.service_type;
            var targetType = this.sv_map[service_type].targetType;
            var basic_section_setting = {
                service_type: service_type,
                model: this.model,
                real_model: this.real_model,
                container: this
            };

            basic_section_setting.newInput = this;
            var basicSection = new targetType(basic_section_setting);

            this.$('#new_task_dialog_config_service').empty();
            this.$('#new_task_dialog_config_service').append(basicSection.render().el);
            basicSection.on('s3IncrementalChange', this.onS3IncrementalChange.bind(this));

            this.renderMetaSection(this.getMetaSection());
        },

        getMetaSection: function () {
            var metaSection;
            if (this.service_type=='s3' && this.model.get("incremental") == '1'){
                metaSection = new MetadataSection({
                    model: this.model,
                    real_model: this.real_model,
                    container: this,
                    service_type: this.service_type,
                    setting_options: {
                        log_type: true,
                        logs_sourcetype: true,
                        host: false,
                        interval: true,
                        index: true
                    }
                });
            }else{
                metaSection = new MetadataSection({
                    model: this.model,
                    real_model: this.real_model,
                    container: this,
                    service_type: this.service_type,
                    setting_options: {
                        sourcetype: ["sqs"].indexOf(this.service_type) < 0,
                        host: false, // hide host for all inputs
                        interval: this._hasInterval(this.service_type),
                        index: true,
                        ct_blacklist: this.service_type === "s3"
                    }
                });
            }
            return metaSection;
        },

        displayValidationError: function(model, error) {
            this.addErrorMsg(error);
        },

        addErrorMsg: function(text) {
            console.log("addErrorMsg:" + text);
            if (this.$('.msg-error').length) {
                this.$('.msg-error').remove();
            }
            this.$(".modal-body").prepend(_.template(ErrorMsg, {
                msg: text
            }));
        },

        removeErrorMsg: function() {
            console.log("removeErrorMsg");

            if (this.$('.msg-error').length) {
                this.$('.msg-error').remove();
            }
        },

        addLoadingMsg: function(text) {
            console.log("addLoadingMsg:" + text);
            if (this.$('.msg-loading').length) {
                this.$('.msg-loading > .msg-text').text(text);
            } else {
                this.$(".modal-body").prepend(_.template(LoadingMsg, {
                    msg: text
                }));
            }
        },

        removeLoadingMsg: function() {
            console.log("removeLoadingMsg");
            if (this.$('.msg-loading').length) {
                this.$('.msg-loading').remove();
            }
        },

        parseAjaxError: function(model, response) {
            var rsp = JSON.parse(model.responseText);
            var regx = /External handler failed.+and output:\s+'([\s\S]*)'\.\s+See splunkd\.log for stderr output\./;
            var msg = String(rsp.messages[0].text);
            var matches = regx.exec(msg);
            if (!matches || !matches[1]) {
                // try to extract another one
                regx = /In handler[^:]+:\s+(.*)/;
                matches = regx.exec(msg);
                if (!matches || !matches[1]) {
                    return msg;
                }
            }

            return matches[1];
        },

        displayError: function(model, response) {
            this.addErrorMsg(this.parseAjaxError(model, response));
        }

    });
});
