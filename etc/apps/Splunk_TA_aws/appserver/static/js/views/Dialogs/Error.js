define([
    "jquery",
    "underscore",
    "backbone",
    "bootstrap",
    "contrib/text!app/templates/Dialogs/Error.html"
], function(
    $,
    _,
    Backbone,
    Bootstrap,
    Error
){
    return Backbone.View.extend({
        template: _.template(Error),

        initialize: function(options) {
            this.msg = options.msg;
        },

        render: function() {
            this.$el.html(this.template({
                msg: this.msg
            }));

            var dlg=this;
            this.$("[role=dialog]").on('hidden.bs.modal', function() {
                dlg.undelegateEvents();
            });

            return this;
        },

        modal: function() {
            this.$("[role=dialog]").modal({backdrop: 'static', keyboard: false});
        }
    });
});
