define([
    "jquery",
    "underscore",
    "backbone",
    "bootstrap",
    'app/util/Util',
    "contrib/text!app/templates/Dialogs/DeleteInput.html",
    "contrib/text!app/templates/Models/ErrorMsg.html"
], function(
    $,
    _,
    Backbone,
    Bootstrap,
    Util,
    DeleteInput,
    ErrorMsg
){
    return Backbone.View.extend({
        template: _.template(DeleteInput),

        events: {
            "submit form": "delete"
        },

        initialize: function(options) {
            this.collection = options.collection;
            this.model = options.model;
            this.service_type = options.service_type;
            this.stateModel = options.stateModel;
            this.dispatcher = options.dispatcher;
        },

        render: function() {
            this.$el.html(this.template({
                input_name: this.model.entry.attributes.name,
                service_type: this.service_type
            }));

            var dlg=this;
            this.$("[role=dialog]").on('hidden.bs.modal', function() {
                dlg.undelegateEvents();
            });

            return this;
        },

        delete: function() {
            var $submit = this.$("input[type=submit]");
            $submit.attr("disabled", "disabled");

            var collection = this.model.collection;
            if (!collection){
                collection = this.collection;
            }
            var url = this.model._url;
            if (url == null) {
                url = collection._url;
            }
            var delete_url = [
                collection.proxyUrl,
                url,
                Util.encodeUrl(this.model.entry.attributes.name)
            ].join("/") + '?output_mode=json';
            $.ajax({
                url: delete_url,
                type: 'DELETE'
            }).done(function(){
                var collection = this.collection;
                collection.remove(this.model);

                if(collection.length > 0){
                    _.each(collection.models,function(model){
                        model.paging.set('total',model.paging.get('total') -1);
                    }.bind(this));
                    collection.reset(collection.models);
                }else{
                    var offset = this.stateModel.get('offset');
                    var count = this.stateModel.get('count');
                    collection.paging.set('offset',offset - count);
                    collection.paging.set('perPage',count);
                    collection.paging.set('total',offset);

                    _.each(collection.models,function(model){
                        model.paging.set('offset',offset - count);
                        model.paging.set('perPage',count);
                        model.paging.set('total',offset);
                    }.bind(this));

                    this.stateModel.set('offset',offset - count);
                }

                if(collection._url == null){
                    this.dispatcher.trigger('delete-input');
                }
                this.$("[role=dialog]").modal('hide');
            }.bind(this)).fail(function(model,response){
                $submit.removeAttr("disabled");
                var rsp = JSON.parse(model.responseText);
                var regx = /External handler failed.+and output:\s+'([\s\S]*)'\.\s+See splunkd\.log for stderr output\./;
                var msg = String(rsp.messages[0].text);
                var matches=regx.exec(msg);
                if(this.$('.msg-text').length){
                    this.$('.msg-text').text(matches[1]);
                }else{
                    this.$(".modal-body").prepend(_.template(ErrorMsg,{msg:matches[1]}));
                }
            }.bind(this));
        },

        modal: function() {
            this.$("[role=dialog]").modal({backdrop: 'static', keyboard: false});
        }
    });
});
