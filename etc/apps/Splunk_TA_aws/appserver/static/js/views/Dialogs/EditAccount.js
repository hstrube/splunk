define([
    "jquery",
    "underscore",
    "backbone",
    "bootstrap",
    "bootstrapValidator",
    "contrib/text!app/templates/Dialogs/EditAccount.html",
    "contrib/text!app/templates/Models/ErrorMsg.html",
    "app/views/Models/SingleInputControl",
    "app/util/AccountUtil",
    "app/util/CheckboxUtil"
], function(
    $,
    _,
    Backbone,
    Bootstrap,
    BootstrapValidator,
    EditAccount,
    ErrorMsg,
    SingleInputControl,
    AccountUtil,
    CheckboxUtil
) {
    return Backbone.View.extend({
        template: _.template(EditAccount),

        events: {
            "submit form": "edit",
            "click label.checkbox a.btn": CheckboxUtil.eventHandler
        },

        initialize: function(options) {
            this.collection = options.collection;
            this.model = options.model;
            this.model.on("invalid", this.displayValidationError.bind(this));
            this.prompt_val = '********';
            this._secretKeyEdited = false;
            this.old_key_id = this.model.entry.content.attributes.key_id;
            this.categoryModel = new Backbone.Model({
                category: +this.model.entry.content.attributes.category
            });
            this.iam = this.model.entry.content.attributes.iam;
        },

        render: function() {
            var account_name = this.model.entry.content.attributes.name;
            if(this.iam){
                this.$el.html(this.template({
                    name: account_name,
                    key_id: '',
                    secret_key: ''
                }));
                this.$('[name=edit_account_dialog_key_id]').prop('readonly', true);
                this.$('[name=edit_account_dialog_secret_key]').prop('readonly', true);
            } else {
                this.$el.html(this.template({
                    name: account_name,
                    key_id: this.old_key_id,
                    secret_key: this.prompt_val
                }));

                var self = this;
                this.$('[name=edit_account_dialog_secret_key]').focus(function() {
                    if (!self._secretKeyEdited) {
                        $(this).val('');
                    }
                }).blur(function() {
                    if (!self._secretKeyEdited &&
                        self.$('[name=edit_account_dialog_key_id]').val() === self.old_key_id) {
                        $(this).val(self.prompt_val);
                    }
                }).change(function(){
                    self._secretKeyEdited = true;
                });
                this.$('[name=edit_account_dialog_key_id]').keydown(function() {
                    this.$('[name=edit_account_dialog_secret_key]').val('');
                }.bind(this));
            }
            this.$('[name=edit_account_dialog_name]').prop('readonly', true);
            this.regionSelect = new SingleInputControl({
                model: this.categoryModel,
                modelAttribute: 'category',
                placeholder: _("Select a region category").t(),
                disableSearch: true,
                autoCompleteFields: [{
                    label: _('Global').t(),
                    value: 1
                }, {
                    label: _('GovCloud').t(),
                    value: 2
                }, {
                    label: _('China').t(),
                    value: 4
                }]
            });
            this.$('.region-category-select').append(this.regionSelect.render().$el);
            if ([1, 2, 4].indexOf(this.categoryModel.get('category')) < 0) {
                this.categoryModel.set('category', 0);
            }
            this.listenTo(this.categoryModel, 'change:category', this.removeErrorMsg.bind(this));
            this.$("[role=dialog]").on('hidden.bs.modal', function() {
                this.undelegateEvents();
            }.bind(this));

            return this;
        },

        edit: function() {
            var $submit = this.$(".submit-btn");
            var new_account_val = this.getInputs();

            if (new_account_val.category === 0) {
                this.addErrorMsg('Region Category is required.');
                return;
            }

            if(!this.iam){
                if (!this._secretKeyEdited){
                    this.addErrorMsg("Please re-enter secret key.");
                    return;
                }
            }

            if (new_account_val.key_id === '' &&
                new_account_val.secret_key === '' &&
                new_account_val.category === this.old_category) {
                this.$("[role=dialog]").modal('hide');
                return;
            }
            var oldValue = {
                key_id: this.old_key_id,
                category: this.old_category,
                iam: this.iam
            };

            this.model.set("name", this.model.entry.content.get("name"));
            var deferred = this.model.save(new_account_val, {
                wait: true
            });
            if (!deferred) {
                return;
            }
            $submit.attr("disabled", "disabled");
            deferred.done(function() {
                this.model.collection.trigger("change");
                this.$("[role=dialog]").modal('hide');
            }.bind(this)).fail(function(model, response) {
                $submit.removeAttr("disabled");
                this.model.entry.content.set(oldValue);
                this.displayError(model, response);
            }.bind(this));
        },
        addErrorMsg: function(text) {
            if (this.$('.msg-error').length) {
                this.$('.msg-error').remove();
            }
            this.$(".modal-body").prepend(_.template(ErrorMsg, {
                msg: text
            }));
        },
        removeErrorMsg: function() {
            if (this.$('.msg-error').length) {
                this.$('.msg-error').remove();
            }
        },
        parseAjaxError: function(model) {
            var text;
            try {
                var rsp = JSON.parse(model.responseText);
                var regx = /External handler failed.+and output:\s+'([\s\S]*)'\.\s+See splunkd\.log for stderr output\./;
                var msg = String(rsp.messages[0].text);
                var matches = regx.exec(msg);
                if (!matches || !matches[1]) {
                    // try to extract another one
                    regx = /In handler[^:]+:\s+(.*)/;
                    matches = regx.exec(msg);
                    if (!matches || !matches[1]) {
                        return msg;
                    }
                }
                text = matches[1];
            } catch (e) {
                text = "Error happens while editing account. See splunkd.log for stderr output.";
            } finally {
                return text;
            }
        },
        getInputs: function() {
            var val = {
                "name": this.$("[name=edit_account_dialog_name]").val(),
                "key_id": this.$("[name=edit_account_dialog_key_id]").val(),
                "secret_key": this.$("[name=edit_account_dialog_secret_key]").val(),
                "category": this._getCategory(),
                "iam": this.iam
            };
            if ( !this.iam && val.secret_key === this.prompt_val) {
                val.secret_key = '';
            }
            return val;
        },
        _getCategory: function() {
            return +this.categoryModel.get('category');
        },
        displayValidationError: function(model, error) {
            this.addErrorMsg(error);
        },
        displayError: function(model, response) {
            this.addErrorMsg(this.parseAjaxError(model, response));
        },
        modal: function() {
            this.$("[role=dialog]").modal({
                backdrop: 'static',
                keyboard: false
            });
        }
    });
});
