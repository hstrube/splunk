define([
    "jquery",
    "underscore",
    "backbone",
    "bootstrap",
    "bootstrapValidator",
    "contrib/text!app/templates/Dialogs/EditIAMRole.html",
    "contrib/text!app/templates/Models/ErrorMsg.html",
    "app/views/Models/SingleInputControl",
    "app/util/CheckboxUtil"
], function(
    $,
    _,
    Backbone,
    Bootstrap,
    BootstrapValidator,
    EditIAMRole,
    ErrorMsg,
    SingleInputControl,
    CheckboxUtil
) {
    return Backbone.View.extend({
        template: _.template(EditIAMRole),

        events: {
            "submit form": "edit",
            "click label.checkbox a.btn": CheckboxUtil.eventHandler
        },

        initialize: function(options) {
            this.collection = options.collection;
            this.model = options.model;
            this.model.on("invalid", this.displayValidationError.bind(this));
            this.prompt_val = '********';
            this._secretKeyEdited = false;
            this.iam = this.model.entry.content.attributes.iam;
        },

        render: function() {
            var name = this.model.entry.attributes.name;
            this.$el.html(this.template({
                name: name,
                arn: this.prompt_val
            }));

            var self = this;
            this.$('[name=edit_iam_role_dialog_arn]').focus(function() {
                if ($(this).val() === self.prompt_val) {
                    $(this).val('');
                }
            }).blur(function() {
                if (!$(this).val()) {
                    $(this).val(self.prompt_val);
                }
            });

            this.$('[name=edit_iam_role_dialog_name]').prop('readonly', true);
            this.$("[role=dialog]").on('hidden.bs.modal', function() {
                this.undelegateEvents();
            }.bind(this));

            return this;
        },

        edit: function() {
            var $submit = this.$(".submit-btn");
            var new_iam_role_val = this.getInputs();
            if (!new_iam_role_val) {
                this.$("[role=dialog]").modal('hide');
                return;
            }

            this.model.set("name", this.model.entry.attributes.name);
            var deferred = this.model.save(new_iam_role_val, {
                wait: true
            });
            if (!deferred) {
                return;
            }
            $submit.attr("disabled", "disabled");
            deferred.done(function() {
                this.model.collection.trigger("change");
                this.$("[role=dialog]").modal('hide');
            }.bind(this)).fail(function(model, response) {
                $submit.removeAttr("disabled");
                this.displayError(model, response);
            }.bind(this));
        },
        addErrorMsg: function(text) {
            if (this.$('.msg-error').length) {
                this.$('.msg-error').remove();
            }
            this.$(".modal-body").prepend(_.template(ErrorMsg, {
                msg: text
            }));
        },
        removeErrorMsg: function() {
            if (this.$('.msg-error').length) {
                this.$('.msg-error').remove();
            }
        },
        parseAjaxError: function(model) {
            var text;
            try {
                var rsp = JSON.parse(model.responseText);
                var regx = /External handler failed.+and output:\s+'([\s\S]*)'\.\s+See splunkd\.log for stderr output\./;
                var msg = String(rsp.messages[0].text);
                var matches = regx.exec(msg);
                if (!matches || !matches[1]) {
                    // try to extract another one
                    regx = /In handler[^:]+:\s+(.*)/;
                    matches = regx.exec(msg);
                    if (!matches || !matches[1]) {
                        return msg;
                    }
                }
                text = matches[1];
            } catch (e) {
                text = "Error happens while editing account. See splunkd.log for stderr output.";
            } finally {
                return text;
            }
        },
        getInputs: function() {
            var arn = this.$("[name=edit_iam_role_dialog_arn]").val();
            var data = {};
            if (arn != this.prompt_val){
                data['arn'] = arn;
            }
            return data;
        },
        displayValidationError: function(model, error) {
            this.addErrorMsg(error);
        },
        displayError: function(model, response) {
            this.addErrorMsg(this.parseAjaxError(model, response));
        },
        modal: function() {
            this.$("[role=dialog]").modal({
                backdrop: 'static',
                keyboard: false
            });
        }
    });
});
