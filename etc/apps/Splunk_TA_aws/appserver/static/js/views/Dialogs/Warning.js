define([
    "jquery",
    "underscore",
    "backbone",
    "bootstrap",
    "app/util/CheckboxUtil",
    "contrib/text!app/templates/Dialogs/Warning.html",
    'jquery.cookie'
], function(
    $,
    _,
    Backbone,
    Bootstrap,
    CheckboxUtil,
    Warning
) {
    return Backbone.View.extend({
        template: _.template(Warning),

        initialize: function(options) {
            this.msg = options.msg;
            this.detail = options.detail;
            this.id = options.id;
            this.closeButton = _.defaults(options.closeButtonConfig || {}, {
                text: "OK",
                className: "btn btn-primary"
            });
            this.extraButton = options.extraButtonConfig || null;
        },

        render: function() {
            this.$el.html(this.template({
                msg: this.msg,
                detail: this.detail,
                closeButton: this.closeButton,
                extraButton: this.extraButton
            }));

            if (this.detail) {
                this.$(".msg-detail-text").html(this.detail);
            }
            var dlg = this;
            this.$("[role=dialog]").on('hidden.bs.modal', function() {
                dlg.undelegateEvents();
                if (dlg.id != null && dlg._isDontShowChecked()){
                    $.cookie(dlg.id, true);
                }
            });

            return this;
        },
        events: {
            "click label.checkbox a.btn": CheckboxUtil.eventHandler,
            "click .extra-button": function (event){
                if (this.extraButton && _.isFunction(this.extraButton.onclick)){
                    this.extraButton.onclick.call(this, event);
                }
            }
        },
        _isDontShowChecked:function(){
            return !!this.$("[name=flag_dont_show]").attr("checked");
        },
        modal: function() {
            this.$("[role=dialog]").modal({
                backdrop: 'static',
                keyboard: false
            });
        }
    });
});
