define([
    "jquery",
    "underscore",
    "backbone",
    "bootstrap",
    "bootstrapValidator",
    "contrib/text!app/templates/Dialogs/AddNewAccount.html",
    "contrib/text!app/templates/Models/ErrorMsg.html",
    "contrib/text!app/templates/Models/LoadingMsg.html",
    "app/models/Account",
    "app/views/Models/SingleInputControl",
    "app/util/AccountUtil",
    "app/util/CheckboxUtil"
], function(
    $,
    _,
    Backbone,
    Bootstrap,
    BootstrapValidator,
    AddNewAccount,
    ErrorMsg,
    LoadingMsg,
    Account,
    SingleInputControl,
    AccountUtil,
    CheckboxUtil
) {
    return Backbone.View.extend({
        encrypted_field: "secret_key",
        template: _.template(AddNewAccount),

        events: {
            "submit form": "add",
            "click label.checkbox a.btn": CheckboxUtil.eventHandler,
            "change label.checkbox.ckb-auto-detect a.btn": "onFlagAutoDetectChange"
        },

        initialize: function(options) {
            this.collection = options.collection;
        },

        render: function() {
            this.$el.html(this.template());
            this.categoryModel = new Backbone.Model({
                category: 0
            });
            this.regionSelect = new SingleInputControl({
                model: this.categoryModel,
                modelAttribute: 'category',
                placeholder: _("Select a region category").t(),
                disableSearch: true,
                autoCompleteFields: [{
                    label: _('Global').t(),
                    value: 1
                }, {
                    label: _('GovCloud').t(),
                    value: 2
                }, {
                    label: _('China').t(),
                    value: 4
                }]
            });
            this.$('.region-category-select').append(this.regionSelect.render().$el);
            var dlg = this;
            this.$("[role=dialog]").on('hidden.bs.modal', function() {
                dlg.undelegateEvents();
            });

            return this;
        },

        add: function() {
            var new_account_val = this.getInputs();

            //Check the name
            if (new_account_val.name.indexOf("_") === 0) {
                this.addErrorMsg('Name starting with "_" underscore is not allowed.');
                return;
            }
            if (new_account_val.category === 0) {
                this.addErrorMsg('Region Category is required.');
                return;
            }
            this.removeErrorMsg();
            this._addAccount(new_account_val);
        },
        // _isAutoDetectChecked: function() {
        //     return !!this.$("[name=flag_auto_detect]").attr("checked");
        // },
        _addAccount: function(accountVal) {
            var account = new Account({
                appData: this.appData,
                collection: this.collection
            });
            account.on("invalid", this.displayValidationError.bind(this));
            account.entry.content.set(accountVal);

            var deferred = account.save();
            if (!deferred) {
                return;
            }
            var $submit = this.$("input[type=submit]");
            $submit.attr("disabled", "disabled");
            deferred.done(function() {
                delete account.entry.content.attributes[this.encrypted_field];
                this.collection.add(account);
                if (this.collection.length !== 0) {
                    _.each(this.collection.models, function(model) {
                        model.paging.set('total', this.collection.length);
                    }.bind(this));
                }
                this.collection.reset(this.collection.models);
                this.$("[role=dialog]").modal('hide');
            }.bind(this)).fail(function(model, response) {
                $submit.removeAttr("disabled");
                this.displayError(model, response);
            }.bind(this));
        },
        onFlagAutoDetectChange: function(e, data) {
            var funcName = data.checked ? "addClass" : "removeClass";
            this.$(".ckb-category").each(function() {
                var $el = $(this);
                $el[funcName]("disabled");
                $el.find("a.btn")[funcName]("disabled");
            });
        },
        addErrorMsg: function(text) {
            if (this.$('.msg-error').length) {
                this.$('.msg-error').remove();
            }
            this.$(".modal-body").prepend(_.template(ErrorMsg, {
                msg: text
            }));
        },

        removeErrorMsg: function() {
            console.log("removeErrorMsg");

            if (this.$('.msg-error').length) {
                this.$('.msg-error').remove();
            }
        },

        addLoadingMsg: function(text) {
            console.log("addLoadingMsg:" + text);
            if (this.$('.msg-loading').length) {
                this.$('.msg-loading > .msg-text').text(text);
            } else {
                this.$(".modal-body").prepend(_.template(LoadingMsg, {
                    msg: text
                }));
            }
        },

        removeLoadingMsg: function() {
            console.log("removeLoadingMsg");
            if (this.$('.msg-loading').length) {
                this.$('.msg-loading').remove();
            }
        },

        parseAjaxError: function(model) {
            var text;
            try {
                var rsp = JSON.parse(model.responseText);
                var regx = /External handler failed.+and output:\s+'([\s\S]*)'\.\s+See splunkd\.log for stderr output\./;
                var msg = String(rsp.messages[0].text);
                var matches = regx.exec(msg);
                if (!matches || !matches[1]) {
                    // try to extract another one
                    regx = /In handler[^:]+:\s+(.*)/;
                    matches = regx.exec(msg);
                    if (!matches || !matches[1]) {
                        return msg;
                    }
                }
                text = matches[1];
            } catch (e) {
                text = "Error happens while creating account. See splunkd.log for stderr output.";
            } finally {
                return text;
            }
        },

        displayValidationError: function(model, error) {
            this.addErrorMsg(error);
        },
        displayError: function(model, response) {
            this.addErrorMsg(this.parseAjaxError(model, response));
        },

        getInputs: function() {
            return {
                "name": this.$("[name=new_account_dialog_name]").val(),
                "key_id": this.$("[name=new_account_dialog_key_id]").val(),
                "secret_key": this.$("[name=new_account_dialog_secret_key]").val(),
                "category": this._getCategory()
            };
        },
        _getCategory: function() {
            return +this.categoryModel.get('category');
        },
        modal: function() {
            this.$("[role=dialog]").modal({
                backdrop: 'static',
                keyboard: false
            });
        }
    });
});
