define([
    "jquery",
    "underscore",
    "backbone",
    "app/views/InputsTable/TableRow",
    "app/views/InputsTable/MoreInfo",
    'views/Base',
    'views/shared/TableHead',
    'views/shared/delegates/TableRowToggle'
], function(
    $,
    _,
    Backbone,
    TableRow,
    MoreInfo,
    BaseView,
    TableHeadView,
    TableRowToggleView
) {
    return BaseView.extend({
        initialize: function(options) {
            this.model = options.model;
            this.collection = options.collection;
            this.enableBulkActions = options.enableBulkActions;
            this.showActions = options.showActions;
            this.dispatcher = options.dispatcher;

            this.children.tableRowToggle = new TableRowToggleView({
                el: this.el,
                collapseOthers: true
            });

            /** A simple object to send events from any row to the table */
            // this.dispatcher = _.extend({}, Backbone.Events);

            //Table Header
            var tableHeaders = [];
            tableHeaders.push({
                label: 'i',
                className: 'col-info',
                html: '<i class="icon-info"></i>'
            });
            // if(this.enableBulkActions){
            //     tableHeaders.push({ className: 'bulk-checkbox col-checkbox col-inline'});
            // }
            tableHeaders.push({
                label: _('Name').t(),
                sortKey: 'name'
            });
            tableHeaders.push({
                label: _('Service').t(),
                sortKey: 'service'
            });
            tableHeaders.push({
                label: _('Account').t(),
                sortKey: 'aws_account'
            });
            tableHeaders.push({
                label: _('Index').t(),
                sortKey: 'index'
            });
            tableHeaders.push({
                label: _('Sourcetype').t(),
                sortKey: 'sourcetype'
            });
            tableHeaders.push({
                label: _('Status').t(),
                sortKey: 'status'
            });

            if (this.showActions) {
                tableHeaders.push({
                    label: _('Actions').t()
                });
            }

            var TableHead = TableHeadView;

            this.children.head = new TableHead({
                model: this.model,
                columns: tableHeaders
            });
            this.children.rows = this.rowsFromCollection();
            this.activate();
        },

        startListening: function() {
            this.listenTo(this.collection, 'remove', this.renderRows);
            this.listenTo(this.collection, 'reset', this.renderRows);
            this.listenTo(this.collection, 'change', this.renderRows);
            this.listenTo(this.collection, 'add', this.renderRows);
        },
        rowsFromCollection: function() {
            return _.flatten(
                this.collection.map(function(model, i) {
                    var row_dispatcher = _.extend({}, Backbone.Events);

                    row_dispatcher.on('delete-input', function() {
                        this.dispatcher.trigger('delete-input');
                    }.bind(this));

                    return [
                        new TableRow({
                            dispatcher: row_dispatcher,
                            model: {
                                input: model,
                                state: this.model,
                                enableBulkActions: false,
                                showActions: true,
                                collection: this.collection
                            },
                            index: i,
                        }),
                        new MoreInfo({
                            dispatcher: row_dispatcher,
                            model: {
                                input: model,
                                state: this.model
                            },
                            index: i
                        })
                    ];
                }, this)
            );
        },
        _render: function() {
            _(this.children.rows).each(function(row) {
                // console.log(row.render().el);
                this.$el.find('tbody:first').append(row.render().$el);
                // row.render().$el.appendTo(this.$('.app-listings'));
                // console.log(this.$el);
            }, this);
            // console.log(this.el.innerHTML);
            //this.children.tableDock.update();
        },
        renderRows: function() {
            _(this.children.rows).each(function(row) {
                row.remove();
            }, this);
            this.children.rows = this.rowsFromCollection();
            this._render();
        },
        render: function() {
            if (!this.el.innerHTML) {
                this.$el.append(this.compiledTemplate({}));
                this.children.head.render().prependTo(this.$('> .table-chrome'));
            }
            this._render();
            return this;
        },
        template: [
            '<table class="table table-chrome table-striped table-row-expanding table-listing">',
            '<tbody class="app-listings"></tbody>',
            '</table>'
        ].join('')

    });

});