define(
    [
        'underscore',
        'backbone',
        'views/Base',
        'views/shared/controls/ControlGroup',
        'views/shared/controls/SyntheticCheckboxControl',
        'app/views/InputsTable/EditMenu'
    ],
    function(
        _,
        Backbone,
        BaseView,
        ControlGroup,
        SyntheticCheckboxControl,
        EditMenu
    )
    {
        return BaseView.extend({
            tagName: 'tr',
            className: 'apps-table-tablerow',
            events: {
                'click td.actions > a.dropdown-toggle': function(e) {
                    this.openEdit(e);
                }
            },
            initialize: function(options) {
                BaseView.prototype.initialize.apply(this, arguments);
                this.$el.addClass((this.options.index % 2) ? 'even' : 'odd');

                this.collection = this.model.collection;
                this.enableBulkActions = this.model.enableBulkActions;
                this.showActions = this.model.showActions;
                this.stateModel  = this.model.state;
                this.dispatcher = options.dispatcher;

                this.activate();
            },

            convertToBoolean: function(arg) {
                return $.parseJSON(String(arg).toLowerCase());
            },

            render: function() {
                var name = this.model.input.entry.attributes.name;
                var service = '';
                if(this.model.input.attributes.id.indexOf('inputs/billing/') > -1){
                    service = "Billing";
                } else if(this.model.input.attributes.id.indexOf('inputs/config/') > -1){
                    service = "Config";
                } else if(this.model.input.attributes.id.indexOf('inputs/cloudtrail/') > -1){
                    service = "CloudTrail";
                }else if(this.model.input.attributes.id.indexOf('inputs/cloudwatch/') > -1){
                    service = "CloudWatch";
                } else if(this.model.input.attributes.id.indexOf('inputs/splunk_ta_aws_inputs_s3_universal') > -1){
                    service = "S3";
                } else if(this.model.input.attributes.id.indexOf('inputs/description/') > -1){
                    service = "Description";
                } else if(this.model.input.attributes.id.indexOf('inputs/cloudwatch-logs/') > -1){
                    service = "CloudWatch Logs";
                } else if(this.model.input.attributes.id.indexOf('inputs/kinesis/') > -1){
                    service = "Kinesis";
                } else if(this.model.input.attributes.id.indexOf('inputs/config-rule/') > -1){
                    service = "Config Rules";
                } else if(this.model.input.attributes.id.indexOf('inputs/inspector/') > -1){
                    service = "Inspector";
                } else if(this.model.input.attributes.id.indexOf('inputs/sqs/') > -1){
                    service = "SQS";
                }

                this.model.input.entry.content.attributes.service = service;
                var disabled = this.model.input.entry.content.attributes.disabled == false ? 'Enabled': 'Disabled';
                var index = this.model.input.entry.content.attributes.index;
                var account = this.model.input.entry.content.attributes.aws_account;
                // var interval = this.model.input.entry.content.attributes.interval;
                var sourcetype = this.model.input.entry.content.attributes.sourcetype;

                //Set the service type of model
                if (service === 'CloudWatch Logs') {
                    this.service_type = 'cloudwatchlog'
                } else if (service === 'Config Rules') {
                    this.service_type = 'configrule'
                } else {
                    this.service_type = service.toLowerCase();
                }

                this.$el.html(this.compiledTemplate({
                    name: name,
                    service: service,
                    disabled: disabled,
                    index: index,
                    account: account,
                    sourcetype: sourcetype
                }));
                if(this.enableBulkActions){
                    this.$('.box').append(this.bulkbox.render().el);
                }
                return this;
            },

            openEdit: function (e) {
                e.preventDefault();
                var $target = $(e.currentTarget);
                if (this.editmenu && this.editmenu.shown) {
                    this.editmenu.hide();
                    e.preventDefault();
                    return;
                }

                this.dispatcher.on('disable-input', function() {
                    this.model.input.entry.content.attributes.disabled = true;
                    this.$('td.disabled').text('Disabled');
                }.bind(this));

                this.dispatcher.on('enable-input', function() {
                    this.model.input.entry.content.attributes.disabled = false;
                    this.$('td.disabled').text('Enabled');
                }.bind(this));

                this.editmenu = new EditMenu({
                    collection: this.collection,
                    model: this.model.input,
                    dispatcher: this.dispatcher,
                    service_type: this.service_type,
                    stateModel: this.stateModel
                });

                $('body').append(this.editmenu.render().el);
                this.editmenu.show($target);
                e.preventDefault();
            },

            template:'\
                <td class="expands">\
                    <a href="#"><i class="icon-triangle-right-small"></i></a>\
                </td>\
                <% if (this.enableBulkActions) { %>\
                    <td class="box checkbox col-inline"></td>\
                <% } %>\
                <td class="name"><%- name %></td>\
                <td class="name"><%- service %></td>\
                <td class="account"><%- account %></td>\
                <td class="index"><%- index %></td>\
                <td class="sourcetype"><%- sourcetype %></td>\
                <td class="disabled">\
                    <%- disabled %>\
                </td>\
                <% if (this.showActions) { %>\
                  <td class="actions">\
                    <a class="dropdown-toggle" href="#"><%- _("Action").t() %><span class="caret"></span></a>\
                  </td>\
                <% } %>\
                '
        });
    }
);
