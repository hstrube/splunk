define([
    // 'module',module,
    "jquery",
    'underscore',
    'views/Base'
], function(
    $,
    _,
    BaseView
) {
    return BaseView.extend({
        // moduleId: module.id,
        tagName: 'tr',
        className: 'more-info',
        /**
         * @param {Object} options {
         *     model: {
         *         state: <models.State>
         *         scheduledView: <models.services.ScheduledView>
         *         dashboard: <models.services.data.ui.View>,
         *         application: <models.Application>,
         *         user: <models.service.admin.user>,
         *         serverInfo: <models.services.server.serverinfo>
         *     },
         *     collection: roles: <collections.services.authorization.Roles>
         *     index: <index_of_the_row>
         * }
         */
        initialize: function(options) {
            BaseView.prototype.initialize.apply(this, arguments);
            this.$el.addClass((this.options.index % 2) ? 'even' : 'odd').css('display', 'none');
            this.model = options.model.input;
            // this.detail = this.model.input.entry.content.attributes;
            this.detail = this.model.entry.content.attributes;
            this.dispatcher = options.dispatcher;

            this.dispatcher.on('disable-input', function() {
                this.model.entry.content.attributes.disabled = true;
                this.changeDisabled('true');
                // this.$('td.disabled').text('Disabled');
            }.bind(this));

            this.dispatcher.on('enable-input', function() {
                this.model.entry.content.attributes.disabled = false;
                this.changeDisabled('false');
                // this.$('td.disabled').text('Enabled');
            }.bind(this));


            this.detail_report_type_dict = {
                '0': 'none',
                '1': 'detail billing report',
                '2': 'detail billing report with resources and tags'
            };
            this.monthly_report_type_dict = {
                '0': 'none',
                '1': 'Monthly Report',
                '2': 'monthly Cost Allocation Report'
            };
            this.readable_label = {
                'detail_report_type': this.detail_report_type_dict,
                'monthly_report_type': this.monthly_report_type_dict
            };
            this.service_dict = {
                'Billing': {
                    'name': 'Name',
                    'aws_account': 'AWS Account',
                    'aws_iam_role': 'Assume Role',
                    'interval': 'Interval',
                    'bucket_name': 'S3 Bucket',
                    'monthly_report_type': 'Monthly Report',
                    'detail_report_type': 'Detail Report',
                    'report_file_match_reg': 'Regex for Report Selection',
                    'sourcetype': 'Source Type',
                    'index': 'Index',
                    'host_name': 'S3 Host Name',
                    'host': 'Host',
                    'service': 'Service',
                    'disabled': 'Disabled'
                },
                'Config': {
                    'name': 'Name',
                    'aws_account': 'AWS Account',
                    'aws_region': 'AWS Region',
                    'sqs_queue': 'SQS Queue Name',
                    'enable_additional_notifications': 'Additional Notifications',
                    'interval': 'Interval',
                    'sourcetype': 'Source Type',
                    'index': 'Index',
                    'host': 'Host',
                    'service': 'Service',
                    'disabled': 'Disabled'
                },
                'CloudTrail': {
                    'name': 'Name',
                    'aws_account': 'AWS Account',
                    'aws_region': 'AWS Region',
                    'sqs_queue': 'SQS Queue Name',
                    'exclude_describe_events': 'Exclude Events',
                    'remove_files_when_done': 'Remove Log Files When Done',
                    'blacklist': 'Blacklist for Excluded Events',
                    'interval': 'Interval',
                    'sourcetype': 'Source Type',
                    'index': 'Index',
                    'host': 'Host',
                    'service': 'Service',
                    'disabled': 'Disabled'
                },
                'CloudWatch': {
                    'name': 'Name',
                    'aws_account': 'AWS Account',
                    'aws_iam_role': 'Assume Role',
                    'aws_regions': 'AWS Region',
                    'metric_namespace': 'Metric Namespace',
                    'metric_names': 'Metric Names',
                    'metric_dimensions': 'Dimension Names',
                    'statistics': 'Metric Statistics Being Requested',
                    'period': 'Metric Granularity',
                    'polling_interval': 'Minimum Polling Interval',
                    'sourcetype': 'Source Type',
                    'index': 'Index',
                    'host': 'Host',
                    'service': 'Service',
                    'disabled': 'Disabled'
                },
                'Description': {
                    'name': 'Name',
                    'aws_account': 'AWS Account',
                    'aws_iam_role': 'Assume Role',
                    'aws_regions': 'AWS Region',
                    'apis': 'APIs',
                    'interval': 'Interval',
                    'sourcetype': 'Source Type',
                    'index': 'Index',
                    'host': 'Host',
                    'service': 'Service',
                    'disabled': 'Disabled'
                },
                'CloudWatch Logs': {
                    'name': 'Name',
                    'aws_account': 'AWS Account',
                    'aws_region': 'AWS Region',
                    'groups': 'Log Group',
                    'only_after': 'Only After',
                    'stream_matcher': 'Stream Matching Regex',
                    'interval': 'Interval',
                    'sourcetype': 'Source Type',
                    'index': 'Index',
                    'host': 'Host',
                    'service': 'Service',
                    'disabled': 'Disabled'
                },
                'Kinesis': {
                    'name': 'Name',
                    'aws_account': 'AWS Account',
                    'aws_iam_role': 'Assume Role',
                    'aws_region': 'AWS Region',
                    'sourcetype': 'Source Type',
                    'stream_names': 'Stream Name',
                    'init_stream_position': 'Initial Stream Position',
                    'encoding': 'Encoding with',
                    'format': 'Record Format',
                    'index': 'Index',
                    'host': 'Host',
                    'service': 'Service',
                    'disabled': 'Disabled'
                },
                'Config Rules': {
                    'name': 'Name',
                    'aws_account': 'AWS Account',
                    'aws_region': 'AWS Region',
                    'sourcetype': 'Source Type',
                    'rule_names': 'Rule Names',
                    'index': 'Index',
                    'host': 'Host',
                    'service': 'Service',
                    'disabled': 'Disabled'
                },
                'Inspector': {
                    'name': 'Name',
                    'aws_account': 'AWS Account',
                    'aws_regions': 'AWS Regions',
                    'sourcetype': 'Source Type',
                    'index': 'Index',
                    'host': 'Host',
                    'service': 'Service',
                    'disabled': 'Disabled'
                },
                'SQS': {
                    'name': 'Name',
                    'aws_account': 'AWS Account',
                    'aws_region': 'AWS Region',
                    'sqs_queues': 'SQS Queues',
                    'sourcetype': 'Source Type',
                    'index': 'Index',
                    'host': 'Host',
                    'service': 'Service',
                    'disabled': 'Disabled'
                }
            };
            this.blacklist = ['statistics_lst'];

            this.s3SourcetypeDictDefault = {
                'name': 'Name',
                'incremental': 'Incremental Log',
                'aws_account': 'AWS Account',
                'aws_iam_role': 'Assume Role',
                'bucket_name': 'S3 Bucket',
                'key_name': 'S3 Key Prefix',
                'initial_scan_datetime': 'Start Date/Time',
                'blacklist': 'Blacklist',
                'whitelist': 'Whitelist',
                'interval': 'Interval',
                'sourcetype': 'Source Type',
                'index': 'Index',
                'host_name': 'S3 Host Name',
                'host': 'Host',
                'service': 'Service',
                'disabled': 'Disabled',
            };
            this.s3SourcetypeDict = {
                'aws:s3': this.s3SourcetypeDictDefault,
                'aws:cloudtrail': {
                    'name': 'Name',
                    'incremental': 'Incremental Log',
                    'aws_account': 'AWS Account',
                    'aws_iam_role': 'Assume Role',
                    'bucket_name': 'S3 Bucket',
                    'key_name': 'S3 Key Prefix',
                    'interval': 'Interval',
                    'sourcetype': 'Source Type',
                    'index': 'Index',
                    'host_name': 'S3 Host Name',
                    'host': 'Host',
                    'service': 'Service',
                    'disabled': 'Disabled',
                    'initial_scan_datetime': 'Start Date/Time',
                    'ct_blacklist': 'Blacklist for Excluded Cloudtrail Events'
                },
                'aws:s3:accesslogs': {
                    'name': 'Name',
                    'incremental': 'Incremental Log',
                    'aws_account': 'AWS Account',
                    'aws_iam_role': 'Assume Role',
                    'bucket_name': 'S3 Bucket',
                    'key_name': 'S3 Key Prefix',
                    'interval': 'Interval',
                    'sourcetype': 'Source Type',
                    'index': 'Index',
                    'host_name': 'S3 Host Name',
                    'host': 'Host',
                    'service': 'Service',
                    'disabled': 'Disabled',
                    'initial_scan_datetime': 'Start Date/Time',
                },
                'aws:cloudfront:accesslogs': {
                    'name': 'Name',
                    'incremental': 'Incremental Log',
                    'aws_account': 'AWS Account',
                    'aws_iam_role': 'Assume Role',
                    'bucket_name': 'S3 Bucket',
                    'key_name': 'S3 Key Prefix',
                    'interval': 'Interval',
                    'sourcetype': 'Source Type',
                    'index': 'Index',
                    'host_name': 'S3 Host Name',
                    'host': 'Host',
                    'service': 'Service',
                    'disabled': 'Disabled',
                    'initial_scan_datetime': 'Start Date/Time',
                },
                'aws:elb:accesslogs': {
                    'name': 'Name',
                    'incremental': 'Incremental Log',
                    'aws_account': 'AWS Account',
                    'aws_iam_role': 'Assume Role',
                    'bucket_name': 'S3 Bucket',
                    'key_name': 'S3 Key Prefix',
                    'interval': 'Interval',
                    'sourcetype': 'Source Type',
                    'index': 'Index',
                    'host_name': 'S3 Host Name',
                    'host': 'Host',
                    'service': 'Service',
                    'disabled': 'Disabled',
                    'initial_scan_datetime': 'Start Date/Time',
                },
            };
        },

        render: function() {
            var template;
            if (this.detail.service === "S3") {
                template = this.renderS3();
            } else {
                template = this.renderNormal();
            }
            this.$el.html(template);
            return this;
        },
        renderNormal: function() {

            var template = '';
            var aws_region_rendered = false;
            var detail = this.detail;
            var serviceDict = this.service_dict[detail.service];
            for (var name in detail) {
                if (!detail.hasOwnProperty(name)) {
                    continue;
                }
                var value = detail[name];
                if (value === '' || value == null) {
                    continue;
                }
                if (name.indexOf('eai:') === 0 || (this.blacklist.indexOf(name) > -1)) {
                    continue;
                }
                if (name.indexOf('aws_region') >= 0) {
                    if (aws_region_rendered)
                        continue;
                    aws_region_rendered = true;
                }

                if (serviceDict.hasOwnProperty(name)) {
                    template += '<dt>' + serviceDict[name] + '</dt><dd>';
                } else {
                    template += '<dt>' + name + '</dt><dd>';
                }
                if (this.readable_label.hasOwnProperty(name)) {
                    template += this.readable_label[name][value] + '</dd>';
                } else {
                    template += value + '</dd>';
                }
            }
            template = '<td class="details" colspan="7"><dl class="list-dotted">' + template + '</dl></td>';
            return template;
        },
        renderS3: function(){
            var template = '';
            var aws_region_rendered = false;

            var detail = this.detail,
                incremental = detail.incremental,
                sourcetype = detail.sourcetype,
                serviceDict = this.s3SourcetypeDict[sourcetype] || this.s3SourcetypeDictDefault;

            for (var name in detail) {
                if (!detail.hasOwnProperty(name)) {
                    continue;
                }
                if (!serviceDict.hasOwnProperty(name)) {
                    continue;
                }
                var value = detail[name];
                if (value === '' || value == null) {
                    continue;
                }
                if (name.indexOf('eai:') === 0 || (this.blacklist.indexOf(name) > -1)) {
                    continue;
                }
                if (name.indexOf('aws_region') >= 0) {
                    if (aws_region_rendered){
                        continue;
                    }
                    aws_region_rendered = true;
                }

                template += '<dt>' + serviceDict[name] + '</dt><dd>' + value + '</dd>';
            }
            template = '<td class="details" colspan="7"><dl class="list-dotted">' + template + '</dl></td>';
            return template;
        },
        changeDisabled: function(value) {
            var existed = false;
            _.each(this.$el.find('dt'), function(element) {
                if ($(element).text() === 'Disabled') {
                    existed = true;
                    $(element).next('dd').text(value);
                }
            }.bind(this));
            if (!existed) {
                this.$el.find('dl').append('<dt>Disabled</dt><dd>' + value + '</dd>');
            }
        }
    });
});
