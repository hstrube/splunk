define([
    'underscore',
    'jquery',
    'views/shared/PopTart',
    'app/util/Util',
    'app/views/Dialogs/AddNewInput',
    'app/views/Dialogs/DeleteInput',
    'app/views/Dialogs/Error'
], function(
    _,
    $,
    PopTartView,
    Util,
    EditInputDialog,
    DeleteInputDialog,
    ErrorDialog
) {
    return PopTartView.extend({
        className: 'dropdown-menu',
        initialize: function(options) {
            _.bindAll(this, 'edit', 'delete');
            PopTartView.prototype.initialize.apply(this, arguments);
            this.collection = options.collection;
            this.model = options.model;
            this.service_type = options.service_type;
            this.dispatcher = options.dispatcher;
            this.stateModel = options.stateModel;
        },

        events: {
            'click a.edit': 'edit',
            'click a.delete': 'delete'
        },

        render: function() {
            var html = this.compiledTemplate({});
            this.el.innerHTML = PopTartView.prototype.template_menu;
            this.$el.append(html);

            if (this.model.entry.content.attributes.disabled) {
                this.$('.second-group').html('<li class="enable-li"><a href="#" class="enable">Enable</a></li>');
            } else {
                this.$('.second-group').html('<li class="disable-li"><a href="#" class="disable">Disable</a></li>');
            }
            this.$el.addClass('dropdown-menu-narrow');
            this.$('a.enable').on('click', this.enableInput.bind(this));

            this.$('a.disable').on('click', this.disableInput.bind(this));

            return this;
        },

        enableInput: function(e) {
            this.hide();
            var url = this.model._url == null ? this.model.collection._url : this.model._url;
            var enable_url = this.model.collection.proxyUrl + '/' + url + '/' + Util.encodeUrl(this.model.entry.attributes.name) + '/enable?output_mode=json';
            $.post(enable_url).done(function() {
                this.dispatcher.trigger('enable-input');
            }.bind(this)).fail(function(model, response) {
                this._displayError(model, response);
            }.bind(this));
            e.preventDefault();
        },

        disableInput: function(e) {
            this.hide();
            var url = this.model._url == null ? this.model.collection._url : this.model._url;
            var disable_url = this.model.collection.proxyUrl + '/' + url + '/' + Util.encodeUrl(this.model.entry.attributes.name) + '/disable?output_mode=json';
            $.post(disable_url).done(function() {
                this.dispatcher.trigger('disable-input');
            }.bind(this)).fail(function(model, response) {
                this._displayError(model, response);
            }.bind(this));
            e.preventDefault();
        },

        edit: function(e) {
            var dlg = new EditInputDialog({
                el: $("#addon_task_edit_dialog"),
                collection: this.collection,
                model: this.model,
                service_type: this.service_type
            }).render();
            dlg.modal();
            this.hide();
            e.preventDefault();
        },

        delete: function(e) {
            var deleteInputDialog = new DeleteInputDialog({
                el: $("#addon_task_delete_dialog"),
                collection: this.collection,
                model: this.model,
                service_type: this.service_type,
                stateModel: this.stateModel,
                dispatcher: this.dispatcher
            });
            deleteInputDialog.render().modal();
            this.hide();
            e.preventDefault();
        },

        _displayError: function(model, response) {
            var error_msg = '';
            try {
                var rsp = JSON.parse(model.responseText);
                var type = rsp.messages[0].type;
                var regx = /External handler failed.+and output:\s+'([\s\S]*)'\.\s+See splunkd\.log for stderr output\./;
                var msg = String(rsp.messages[0].text);
                var matches = regx.exec(msg);
                if (!matches || !matches[1]) {
                    // try to extract another one
                    regx = /In handler[^:]+:\s+(.*)/;
                    matches = regx.exec(msg);
                    if (!matches || !matches[1]) {
                        matches = [msg];
                    }
                }
                error_msg = matches[1];
            } catch (err) {
                error_msg = "ERROR in processing the request";
            } finally {
                var errorDialog = new ErrorDialog({
                    el: $('#addon_task_error_dialog'),
                    msg: error_msg
                });
                errorDialog.render().modal();
            }
        },

        template: [
            '<ul class="first-group">',
            '<li><a href="#" class="edit"><%- _("Edit").t() %></a></li>',
            '<li><a href="#" class="delete"><%- _("Delete").t() %></a></li>',
            '</ul>',
            '<ul class="second-group">',
            '</ul>',
        ].join('')
    });
});
