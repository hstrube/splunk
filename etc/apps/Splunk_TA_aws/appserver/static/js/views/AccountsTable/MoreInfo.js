define([
    // 'module',module,
    "jquery",
    'underscore',
    'views/Base',
    'views/shared/delegates/TableRowToggle',
    'views/dashboards/table/controls/DetailView'
],function(
    $,
    _,
    BaseView,
    TableRowToggleView,
    DetailView
){
    return BaseView.extend({
      // moduleId: module.id,
      tagName: 'tr',
      className: 'more-info',
      /**
       * @param {Object} options {
       *     model: {
       *         state: <models.State>
       *         scheduledView: <models.services.ScheduledView>
       *         dashboard: <models.services.data.ui.View>,
       *         application: <models.Application>,
       *         user: <models.service.admin.user>,
       *         serverInfo: <models.services.server.serverinfo>
       *     },
       *     collection: roles: <collections.services.authorization.Roles>
       *     index: <index_of_the_row>
       * }
       */
      initialize: function() {
          BaseView.prototype.initialize.apply(this, arguments);
          this.$el.addClass((this.options.index % 2) ? 'even' : 'odd').css('display', 'none');
          // this.model.dashboard.meta.on('change:description', this.updateDesc, this);
      },
      events: {
          'expand': function(){
              // if(!this.children.details) {
              //     var scheduledView = this.model.scheduledView;
              //     if(scheduledView.isNew()) {
              //         scheduledView.findByName(this.model.dashboard.entry.get('name'),
              //             this.model.application.get('app'), this.model.application.get('owner'));
              //     }
              //     this.children.details = new DetailView({
              //         model: {
              //             state: this.model.state,
              //             scheduledView: scheduledView,
              //             dashboard: this.model.dashboard,
              //             application: this.model.application,
              //             user: this.model.user,
              //             appLocal: this.model.appLocal,
              //             serverInfo: this.model.serverInfo
              //         },
              //         collection: this.collection
              //     });
              //     this.children.details.render().appendTo(this.$('.details'));
              // } else {
              //     this.children.details.render();
              // }
          }
      },
      // updateDesc: function() {
      //     var $descriptionElement = this.$('p.dashboard-description'),
      //         descriptionText = this.model.dashboard.meta.get('description');
      //     if ($descriptionElement.length === 0 && descriptionText) {
      //         this.$('td.details').prepend('<p class="dashboard-description">' + _.escape(descriptionText) + '</p>');
      //         return;
      //     }
      //     if ($descriptionElement.length !== 0 && !descriptionText) {
      //        $descriptionElement.remove();
      //        return;
      //     }
      //     $descriptionElement.text(descriptionText);
      // },
      render: function() {
          var name = this.model.server.entry.content.attributes.account_name;
          var description = this.model.server.entry.content.attributes.description;
          var connection = this.model.server.entry.content.attributes.server_url;
          var account = this.model.server.entry.content.attributes.account_password;
          var templateData = {
              name:name,
              description:description,
              connection:connection,
              account:account
          }
          // var html = _.template(templateData);
          //
          // this.setElement($(html));
          this.$el.html(this.compiledTemplate(templateData));
          return this;

          // this.$el.html(this.compiledTemplate({
          //     cols: 4 + ((this.model.user.canUseApps()) ? 1 : 0)
          // }));
          // this.updateDesc();
          // if(this.children.details) {
          //     this.children.details.render().appendTo(this.$('.details'));
          // }
          // return this;
      },
      // template: '<td class="details" colspan="<%= cols %>"></td>'
      template: '\
              <td class="details" colspan="5">\
                  <dl class="list-dotted">\
                      <dt class="name"><%- _("Name").t() %></dt>\
                          <dd>\
                              <%= name %>\
                          </dd>\
                      <dt class="description"><%- _("Description").t() %></dt>\
                          <dd>\
                              <%= description %>\
                          </dd>\
                      <dt class="connection"><%- _("Connection").t() %></dt>\
                          <dd>\
                              <%= connection %>\
                          </dd>\
                      <dt class="account"><%- _("Account").t() %></dt>\
                          <dd>\
                              <%= account %>\
                          </dd>\
                  </dl>\
              </td>\
          '
    });
});
