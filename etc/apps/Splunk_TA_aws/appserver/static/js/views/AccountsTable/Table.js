define([
    "jquery",
    "underscore",
    "backbone",
    "app/views/AccountsTable/TableRow",
    'views/Base',
    'views/shared/TableHead',
    'views/shared/delegates/TableRowToggle'
], function(
    $,
    _,
    Backbone,
    TableRow,
    BaseView,
    TableHeadView,
    TableRowToggleView
) {
    return BaseView.extend({
        initialize: function(options) {
            this.model = options.model;
            this.collection = options.collection;
            this.allCollection = options.allCollection;
            this.enableBulkActions = options.enableBulkActions;

            this.children.tableRowToggle = new TableRowToggleView({
                el: this.el,
                collapseOthers: true
            });
            //Table Header
            var tableHeaders = [];
            tableHeaders.push({
                label: _('Name').t(),
                sortKey: 'name'
            });
            tableHeaders.push({
                label: _('Key ID').t(),
                sortKey: 'key_id'
            });
            tableHeaders.push({
                label: _('Autodiscovered IAM Role').t(),
                sortKey: 'iam'
            });
            tableHeaders.push({
                label: _('Region Category').t()
            });
            tableHeaders.push({
                label: _('Inputs').t()
            });
            tableHeaders.push({
                label: _('Actions').t()
            });

            var TableHead = TableHeadView;

            this.children.head = new TableHead({
                model: this.model,
                columns: tableHeaders
            });
            this.children.rows = this.rowsFromCollection();
            this.activate();
        },

        startListening: function() {
            this.listenTo(this.collection, 'remove', this.renderRows);
            this.listenTo(this.collection, 'reset', this.renderRows);
            this.listenTo(this.collection, 'change', this.renderRows);
            this.listenTo(this.collection, 'add', this.renderRows);
        },

        rowsFromCollection: function() {
            return _.flatten(
                this.collection.map(function(model, i) {
                    return [
                        new TableRow({
                            dispatcher: this.dispatcher,
                            model: {
                                collection: this.collection,
                                allCollection: this.allCollection,
                                account: model,
                                state: this.model,
                                enableBulkActions: this.enableBulkActions,
                                showActions: this.showActions,
                                collectionURL: this.collection.url
                            },
                            index: i,
                        })
                    ];
                }, this)
            );
        },
        _render: function() {
            _(this.children.rows).each(function(row) {
                this.$el.find('tbody:first').append(row.render().$el);
            }, this);
        },
        renderRows: function() {
            _(this.children.rows).each(function(row) {
                row.remove();
            }, this);
            this.children.rows = this.rowsFromCollection();
            this._render();
        },
        render: function() {
            if (!this.el.innerHTML) {
                this.$el.append(this.compiledTemplate({}));
                this.children.head.render().prependTo(this.$('> .table-chrome'));
            }
            this._render();
            return this;
        },
        template: [
            '<table class="table table-chrome table-striped table-row-expanding table-listing">',
            '<tbody class="app-listings"></tbody>',
            '</table>'
        ].join('')
    });

});