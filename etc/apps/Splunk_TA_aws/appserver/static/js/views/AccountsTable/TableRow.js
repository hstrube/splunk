define([
    "jquery",
    'underscore',
    'backbone',
    'views/Base',
    'app/views/AccountsTable/EditMenu',
    'views/shared/controls/ControlGroup',
    'views/shared/controls/SyntheticCheckboxControl',
    "app/util/AccountUtil"
], function(
    $,
    _,
    Backbone,
    BaseView,
    EditMenu,
    ControlGroup,
    SyntheticCheckboxControl,
    AccountUtil
) {
    var CATEGORY_LABEL_MAP={
        "Commercial": "Global",
        "Government": "GovCloud",
        "China": "China"
    };

    return BaseView.extend({
        tagName: 'tr',
        className: 'apps-table-tablerow',
        events: {
            'click td.actions > a.dropdown-toggle': function(e) {
                this.openEdit(e);
            }
        },
        initialize: function(options) {
            BaseView.prototype.initialize.apply(this, arguments);
            this.$el.addClass((this.options.index % 2) ? 'even' : 'odd');

            this.collection = this.model.collection;
            this.allCollection = this.model.allCollection;
            this.enableBulkActions = this.model.enableBulkActions;
            this.showActions = this.model.showActions;

            if (options.dispatcher) {
                this.dispatcher = options.dispatcher;
            }

            if (this.enableBulkActions) {
                if (!this.model.checkbox) {
                    this.model.checkbox = new Backbone.Model();
                    this.model.checkbox.set("checked", 0);
                }
                this.bulkboxControl = new SyntheticCheckboxControl({
                    modelAttribute: 'checked',
                    model: this.model.checkbox
                });
                this.bulkbox = new ControlGroup({
                    controls: [this.bulkboxControl]
                });
            }

            this.activate();
        },

        openEdit: function(e) {
            var $target = $(e.currentTarget);
            if (this.editmenu && this.editmenu.shown) {
                this.editmenu.hide();
                e.preventDefault();
                return;
            }

            var iam = this.model.account.entry.content.get("iam");
            this.editmenu = new EditMenu({
                collection: this.model.collection,
                model: this.model.account,
                url: this.model.collectionURL,
                iam: iam
            });
            $('body').append(this.editmenu.render().el);
            this.editmenu.show($target);
            e.preventDefault();
        },

        render: function() {
            var content = this.model.account.entry.content;
            var account_name = content.get("name");
            var key_id = content.get("key_id");
            var iam_bin = content.get("iam");
            var iam = "No";
            if (iam_bin == "1" || iam_bin == 1){
                iam = "Yes";
            }
            var inputs = content.get("inputs");
            var flags = AccountUtil.parseCategoryCode(content.get("category"));
            var category = [
                "Commercial",
                "Government",
                "China"
            ];
            category = _.filter(category, function(cate){
                return cate && cate.toLowerCase? flags[cate.toLowerCase()]: false;
            });
            if (category.length === 1) {
                category = _.escape(_(CATEGORY_LABEL_MAP[category[0]]).t());
            } else {
                category = '<i class="icon-error" style="color:#d6563c;font-size:16px;"></i> '+_.escape(_('Not set. Please edit this account.').t());
            }

            if (typeof inputs === 'undefined') {
                var count = 0;
                _.each(this.allCollection.models, function(model) {
                    if (account_name == model.entry.content.attributes.aws_account) {
                        count += 1;
                    }
                }.bind(this));
                inputs = count;
                this.model.account.entry.content.attributes.inputs = inputs;
            }
            this.$el.html(this.compiledTemplate({
                account_name: account_name,
                key_id: key_id,
                iam: iam,
                inputs: inputs,
                category: category
            }));
            if (this.enableBulkActions) {
                this.$('.box').append(this.bulkbox.render().el);
            }
            return this;
        },

        template: [
            '<% if (this.enableBulkActions) { %>',
            '<td class="box checkbox col-inline"></td>',
            '<% } %>',
            '<td class="name"><%- account_name %></td>',
            '<td><%- key_id %></td>',
            '<td><%- iam %></td>',
            '<td><%= category %></td>',
            '<td><%- inputs %></td>',
            '<td class="actions">',
            '<a class="dropdown-toggle" href="#"><%- _("Action").t() %><span class="caret"></span></a>',
            '</td>'
        ].join('')
    });
});
