define(
    [
        'underscore',
        'jquery',
        'views/shared/PopTart',
        'views/shared/Sidebar',
        'app/views/Dialogs/DeleteAccount',
        'app/views/Dialogs/EditAccount'
    ],
    function(
        _,
        $,
        PopTartView,
        SideBar,
        DeleteAccount,
        EditAccount
    ){
        return PopTartView.extend({
            className: 'dropdown-menu',
            initialize: function(options) {
                _.bindAll(this, 'edit', 'delete');
                PopTartView.prototype.initialize.apply(this, arguments);
                this.collection = options.collection;
                this.model = options.model;
                this.url = options.url;
                this.iam = options.iam;
            },

            events: {
                'click a.edit': 'edit',
                'click a.delete': 'delete'
            },

            render: function(){
                var html = this.compiledTemplate({
                    iam: this.iam
                });
                this.el.innerHTML = PopTartView.prototype.template_menu;
                this.$el.append(html);
                this.$el.addClass('dropdown-menu-narrow');
                return this;
            },

            edit: function(e){
                var editAccount = new EditAccount({
                    el: $("#edit-account-dialog"),
                    collection: this.collection,
                    model: this.model
                });
                editAccount.render().modal();
                this.hide();
                e.preventDefault();
            },

            delete: function(e) {
                var in_use = this.model.entry.content.attributes.inputs > 0 ? true: false;
                var deleteAccount = new DeleteAccount({
                    el: $("#delete-account-dialog"),
                    collection: this.collection,
                    model: this.model,
                    in_use: in_use
                });
                deleteAccount.render().modal();
                this.hide();
                e.preventDefault();
            },

            template: [
                '<ul class="first-group">',
                '<li><a href="#" class="edit"><%- _("Edit").t() %></a></li>',
                '<% if (!iam) { %>',
                '<li><a href="#" class="delete"><%- _("Delete").t() %></a></li>',
                '<% } %>',
                '</ul>'
            ].join('')
        });
    }
);
