define([
    'jquery',
    'underscore',
    'splunkjs/mvc/protections',
    'splunkjs/mvc/headerview',
    'splunkjs/mvc/utils',
    'app/models/Server',
    'app/views/Dialogs/Warning',
    'jquery.cookie'
], function($, _, protections, HeaderView, utils, Server, WarningDialog) {

    protections.enableCSRFProtection($);

    // render header
    var headerView = new HeaderView({
        el: $('#app-bar'),
        acceleratedAppNav: true
    });
    headerView.render();

    // require stylesheet
    $('#main-section-body').hide();

    require([`app/pcss/pages/${utils.getPageInfo().page}.pcss`], function() {
        $('#main-section-body').show();
    });


    // check search head
    var WARNING_MSG = [
        "Configuring this add-on on a search head is not best practice."
    ];

    var DETAIL_MSG = "You are accessing the configuration for this add-on on a search head. As a best practice, you should configure the accounts and inputs for this add-on on a data collection node such as a heavy forwarder. <span class=\"help-block \"><a class=\"external\" target=\"_blank\" href=\"http://docs.splunk.com/Documentation/AddOns/latest/AWS/Distributeddeployment\">Learn more</a></span>";
    var COOKIE_ID = "dontShow";

    function isSearchHead(roles) {
        var ret = false;
        ret = _.some(roles, function(role) {
            return role.indexOf("search") > -1;
        });

        return ret;
    }

    if (!$.cookie(COOKIE_ID)) {
        var si = new Server({
            name: "info"
        });
        var deferred = si.fetch();
        deferred.done(function(data) {
            var server_roles = data.entry[0].content.server_roles;
            if (isSearchHead(server_roles)) {
                var warningDialog = new WarningDialog({
                    el: $('#addon_task_warning_dialog'),
                    msg: WARNING_MSG,
                    detail: DETAIL_MSG,
                    id: COOKIE_ID,
                    closeButtonConfig: {
                        text: "Continue",
                        className: "btn"
                    },
                    extraButtonConfig: {
                        text: "Leave the App",
                        className: "btn btn-primary",
                        onclick: function(){
                            location.href = "../search/search";
                        }
                    }
                });
                warningDialog.render().modal();
            }
        });
    }
});