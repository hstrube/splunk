require([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap',
    'splunk.util',
    'app/collections/ProxyBase.Collection',
    'app/collections/Accounts',
    'app/collections/Billings',
    'app/collections/Configs',
    'app/collections/CloudTrails',
    'app/collections/CloudWatchs',
    'app/collections/S3s',
    'app/collections/S3Universals',
    'app/collections/Descriptions',
    'app/collections/CloudWatchLogs',
    'app/collections/Kinesises',
    'app/collections/ConfigRules',
    'app/collections/Inspectors',
    'app/collections/SQSs',
    'app/collections/Logs',
    'app/views/InputsTable/Table',
    'app/models/appData',
    'contrib/text!app/templates/Inputs/InputTitle.html',
    'models/Base',
    'views/shared/tablecaption/Master',
    'app/views/Models/InputFilter',
    'app/views/Models/Filter',
    'app/views/Dialogs/AddNewInput',
    'app/views/Models/EditMenu',
    'app/models/Service.Billing.Model',
    'app/models/Service.CloudTrail.Model',
    'app/models/Service.CloudWatch.Model',
    'app/models/Service.CloudWatchLogs.Model',
    'app/models/Service.Config.Model',
    'app/models/Service.Description.Model',
    'app/models/Service.S3.Model',
    'app/models/Service.S3Universal.Model',
    'app/models/Service.Kinesis.Model',
    'app/models/Service.ConfigRule.Model',
    'app/models/Service.Inspector.Model',
    'app/models/Service.SQS.Model',
    'app/models/Service.Logs.Model',
    'contrib/text!app/templates/InputsPageTemplate.html',
    'app/pages/common'
], function(
    $,
    _,
    Backbone,
    Bootstrap,
    splunkdUtils,
    ProxyBase,
    Accounts,
    Billings,
    Configs,
    CloudTrails,
    CloudWatchs,
    S3s,
    S3Universals,
    Descriptions,
    CloudWatchLogs,
    Kinesises,
    ConfigRules,
    Inspectors,
    SQSs,
    Logs,
    InputsTable,
    appData,
    InputTitleTemplate,
    BaseModel,
    CaptionView,
    InputFilter,
    Filter,
    AddNewInputDialog,
    EditMenu,
    BillingInput,
    CloudTrailInput,
    CloudWatchInput,
    CloudWatchLogsInput,
    ConfigInput,
    DescriptionInput,
    S3Input,
    S3UniversalInput,
    KinesisInput,
    ConfigRuleInput,
    InspectorInput,
    SQSInput,
    LogsInput,
    PageTemplate
) {
    var MODEL_MAP = {
        'billing': BillingInput,
        'cloudtrail': CloudTrailInput,
        'cloudwatch': CloudWatchInput,
        'config': ConfigInput,
        's3': S3UniversalInput,
        'description': DescriptionInput,
        'cloudwatchlog': CloudWatchLogsInput,
        'kinesis': KinesisInput,
        'configrule': ConfigRuleInput,
        'inspector': InspectorInput,
        'sqs': SQSInput
    };

    function createCollection(claz) {
        return new claz([], {
            appData: {
                app: appData.get("app"),
                owner: appData.get("owner")
            },
            targetApp: "Splunk_TA_aws",
            targetOwner: "nobody"
        });
    }

    function createStateModel(options) {
        options = options || {};
        _.defaults(options, {
            sortKey: 'name',
            sortDirection: 'asc',
            count: 15,
            offset: 0,
            fetching: true
        });
        var model = new BaseModel();
        model.set(options);
        return model;
    }

    function resetInputs(inputs, stateModel, totalModels){
        var offset = stateModel.get('offset');
        var count = stateModel.get('count');
        var total = totalModels.length;
        setPagingSettings(inputs, offset, count, total);
        var models = totalModels.models.slice(offset, offset + count);
        setPagingSettings(models, offset, count, total);
        inputs.reset(models);
    }

    function setPagingSettings(models, offset, count, total){
        if (!_.isArray(models)){
            models = [models];
        }
        _.each(models, function(model) {
            model.paging.set('offset', offset);
            model.paging.set('perPage', count);
            model.paging.set('total', total);
        });
    }

    var inputs_view = Backbone.View.extend({
        initialize: function() {
            //state model
            this.stateModel = createStateModel();

            //collections
            this.billings = createCollection(Billings);
            this.cloudtrails = createCollection(CloudTrails);
            this.cloudwatchs = createCollection(CloudWatchs);
            this.configs = createCollection(Configs);
            this.s3s = createCollection(S3Universals);
            this.descriptions = createCollection(Descriptions);
            this.cloudwatchlogs = createCollection(CloudWatchLogs);
            this.kinesises = createCollection(Kinesises);
            this.configrules = createCollection(ConfigRules);
            this.inspectors = createCollection(Inspectors);
            this.sqss = createCollection(SQSs);

            this.accounts = createCollection(Accounts);

            this._typesMap = {
                'billing': this.billings,
                'cloudtrail': this.cloudtrails,
                'cloudwatch': this.cloudwatchs,
                'config': this.configs,
                's3': this.s3s,
                'description': this.descriptions,
                'cloudwatchlog': this.cloudwatchlogs,
                'kinesis': this.kinesises,
                'configrule': this.configrules,
                'inspector': this.inspectors,
                'sqs': this.sqss
            };

            this.dispatcher = _.extend({}, Backbone.Events);

            //Change filter
            this.listenTo(this.dispatcher, 'filter-change', function(type) {
                this.filterChange(type);
            }.bind(this));

            //Delete input
            this.listenTo(this.dispatcher, 'delete-input', function() {
                var all_deferred = this.fetchAllCollection();
                all_deferred.done(function() {
                    this.cachedInputs = this.combineCollection();
                    this.cachedSearchInputs = this.combineCollection();
                    this.inputs._url = undefined;
                    this.inputs._type = undefined;
                    resetInputs(this.inputs, this.stateModel, this.cachedSearchInputs);
                }.bind(this));
            }.bind(this));

            //Change sort
            this.listenTo(this.stateModel,
                'change:sortDirection change:sortKey', _.debounce(function() {
                    this.sortCollection();
                }.bind(this), 0));

            //Change search
            this.listenTo(this.stateModel, 'change:search', _.debounce(function() {
                this.searchCollection();
            }.bind(this), 0));

            //Change offset
            this.listenTo(this.stateModel, 'change:offset', _.debounce(function() {
                this.pageCollection(this.stateModel);
            }.bind(this), 0));

            this.accounts_deferred = this.fetchListCollection(this.accounts,
                this.stateModel);
            this.deferred = this.fetchAllCollection();

            this.filter = new Filter({
                dispatcher: this.dispatcher
            });
        },

        filterChange: function(type) {
            this.stateModel.set('offset', 0);

            if (type === 'all') {
                var search = this.stateModel.get('search');
                if (typeof search !== 'undefined' && search !==
                    'name=* OR sourcetype=*') {
                    this.searchCollection();
                    this.inputs._url = undefined;
                    this.inputs._type = undefined;
                } else {
                    var all_deferred = this.fetchAllCollection();
                    all_deferred.done(function() {
                        this.cachedInputs = this.combineCollection();
                        this.cachedSearchInputs = this.combineCollection();
                        this.inputs._url = undefined;
                        this.inputs._type = undefined;
                        resetInputs(this.inputs, this.stateModel, this.cachedSearchInputs);
                        this.sortCollection();
                    }.bind(this));
                }
            } else {
                var typeModel = this._typesMap[type];
                var singleStateModel = createStateModel({
                    count: 1000
                });
                var deferred = this.fetchListCollection(typeModel,
                    singleStateModel);
                deferred.done(function() {
                    this.cachedInputs = this.singleCollection(typeModel);
                    this.cachedSearchInputs = this.singleCollection(typeModel);
                    this.inputs.model = MODEL_MAP[type];
                    this.inputs._url = typeModel._url;
                    this.inputs._type = type;
                    resetInputs(this.inputs, this.stateModel, this.cachedSearchInputs);
                    this.sortCollection();
                }.bind(this));
            }
        },

        render: function() {
            // render template
            $('#main-section-body').append(PageTemplate);
            document.title = 'Manage AWS Inputs';

            this.accounts_deferred.done(function() {
                if (this.accounts.models.length === 0) {
                    var no_account_html =
                        '<p>No inputs. <a href="./configuration">Create an account</a> before creating an input.</p>';
                    $('#addon_no_account').append($(no_account_html));

                    var inputs_template_data = {
                        title: 'Inputs',
                        description: 'Create data inputs to collect data from AWS',
                        enableButton: false
                    };
                    var title_template = _.template(InputTitleTemplate);
                    $("#addon_input_title").html(title_template(
                        inputs_template_data));
                } else {
                    this.deferred.done(function() {
                        var inputs_template_data = {
                            title: 'Inputs',
                            description: 'Create data inputs to collect data from AWS',
                            enableButton: true,
                            button_id: "addInputBtn",
                            button_value: "Create New Input"
                        };

                        var title_template = _.template(
                            InputTitleTemplate);

                        this.cachedInputs = this.combineCollection();
                        this.cachedSearchInputs = this.combineCollection();

                        //Display the first page
                        this.inputs = this.combineCollection();
                        this.inputs.models = this.cachedInputs
                            .models.slice(0, this.stateModel.get(
                                'count'));

                        var total = this.inputs.length;
                        if (total) {
                            _.each(this.inputs.models, function(model) {
                                model.paging.set('total', total);
                            });
                            this.inputs.paging.set('total', total);
                        }

                        this.caption = new CaptionView({
                            countLabel: _('Inputs').t(),
                            model: {
                                state: this.stateModel
                            },
                            collection: this.inputs,
                            noFilterButtons: true,
                            filterKey: [
                                'name',
                                'sourcetype'
                            ]
                        });

                        //Create view
                        this.input_list = new InputsTable({
                            model: this.stateModel,
                            collection: this.inputs,
                            dispatcher: this.dispatcher,
                            showActions: true
                        });

                        $("#addon_input_title").html(
                            title_template(inputs_template_data));
                        $('#addon_input_caption').append(this.caption.render().$el);
                        //Render input type filter
                        $('.table-caption-inner').append(this.filter.render().$el);
                        $('#addon_input_list').append(this.input_list.render().$el);

                        $("#addInputBtn").on("click", function(e) {
                            var $target = $(e.currentTarget);
                            if (this.editmenu && this.editmenu.shown) {
                                this.editmenu.hide();
                                e.preventDefault();
                                return;
                            }

                            this.editmenu = new EditMenu({
                                collection: this.inputs,
                                dispatcher: this.dispatcher
                            });

                            $('body').append(this.editmenu.render().el);
                            this.editmenu.show($target);
                        }.bind(this));

                    }.bind(this));
                }
            }.bind(this));
        },

        fetchAllCollection: function() {
            var singleStateModel = createStateModel({
                count: 1000
            });

            return $.when(
                this.fetchListCollection(this.billings, singleStateModel),
                this.fetchListCollection(this.cloudtrails, singleStateModel),
                this.fetchListCollection(this.cloudwatchs, singleStateModel),
                this.fetchListCollection(this.configs, singleStateModel),
                this.fetchListCollection(this.s3s, singleStateModel),
                this.fetchListCollection(this.descriptions, singleStateModel),
                this.fetchListCollection(this.cloudwatchlogs, singleStateModel),
                this.fetchListCollection(this.kinesises, singleStateModel),
                this.fetchListCollection(this.configrules, singleStateModel),
                this.fetchListCollection(this.inspectors, singleStateModel),
                this.fetchListCollection(this.sqss, singleStateModel)
            );
        },

        combineCollection: function() {
            var collection = createCollection(ProxyBase);

            _.each(this._typesMap, function(subCollection){
                collection.add(subCollection.models, {
                    silent: true
                });
            });
            return collection;
        },

        singleCollection: function(model) {
            var collection = createCollection(ProxyBase);

            collection.add(model.models, {
                silent: true
            });
            return collection;

        },
        fetchListCollection: function(collection, stateModel) {
            var search = stateModel.get('search') || '';

            stateModel.set('fetching', true);
            return collection.fetch({
                data: {
                    sort_dir: stateModel.get('sortDirection'),
                    sort_key: stateModel.get('sortKey'),
                    search: search,
                    count: stateModel.get('count'),
                    offset: stateModel.get('offset')
                },
                success: function() {
                    stateModel.set('fetching', false);
                }.bind(this)
            });
        },

        searchCollection: function() {
            var search = this.stateModel.get('search');
            var result = [];
            if (search !== 'name=* OR sourcetype=*') {
                var a = this.stateModel.get('search');
                search = a.substring(a.indexOf('*') + 1, a.indexOf('*', a.indexOf(
                    '*') + 1));
                _.each(this.cachedInputs.models, function(model) {
                    if (model.entry.get('name').indexOf(search) > -1 ||
                        model.entry.content.get('sourcetype').indexOf(
                            search) > -1) {
                        result.push(model);
                    }
                });

                var offset = this.stateModel.get('offset');
                var count = this.stateModel.get('count');
                var total = result.length;
                setPagingSettings(this.inputs, offset, count, total);
                setPagingSettings(result, offset, count, total);
                this.cachedSearchInputs.reset(result);

                var tempStateModel = createStateModel();

                this.pageCollection(tempStateModel);

            } else {
                var type = this.inputs._type;
                if (type == null) {

                    var all_deferred = this.fetchAllCollection();
                    all_deferred.done(function() {
                        this.cachedInputs = this.combineCollection();
                        this.cachedSearchInputs = this.combineCollection();
                        this.inputs._url = undefined;
                        this.inputs._type = undefined;
                        resetInputs(this.inputs, this.stateModel, this.cachedSearchInputs);

                        if (this.stateModel.get('search') !== 'name=* OR sourcetype=*') {
                            this.searchCollection();
                        }
                    }.bind(this));
                } else {
                    var typeModel = this._typesMap[type];
                    var singleStateModel = createStateModel({
                        count: 1000
                    });
                    var deferred = this.fetchListCollection(typeModel,
                        singleStateModel);
                    deferred.done(function() {
                        this.cachedInputs = this.singleCollection(typeModel);
                        this.cachedSearchInputs = this.singleCollection(typeModel);
                        this.inputs.model = MODEL_MAP[type];
                        this.inputs._url = typeModel._url;
                        this.inputs._type = type;
                        resetInputs(this.inputs, this.stateModel, this.cachedSearchInputs);
                        this.sortCollection();

                        if (this.stateModel.get('search') !=='name=* OR sourcetype=*') {
                            this.searchCollection();
                        }

                    }.bind(this));
                }

            }
        },

        pageCollection: function(stateModel) {
            resetInputs(this.inputs, stateModel, this.cachedSearchInputs);
        },

        sortCollection: function() {
            var stateModel = this.stateModel;
            var sort_dir = stateModel.get('sortDirection');
            var sort_key = stateModel.get('sortKey');
            var sortable = this.inputs.models;

            var sortAlphabetical = function(a, b) {
                var textA = a.entry.content.get(sort_key).toUpperCase();
                var textB = b.entry.content.get(sort_key).toUpperCase();
                if (sort_dir === 'asc') {
                    return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                } else {
                    return (textA > textB) ? -1 : (textA < textB) ? 1 : 0;
                }
            };

            var handler = {
                'name': function(a,b){
                    var textA = a.entry.get(sort_key).toUpperCase();
                    var textB = b.entry.get(sort_key).toUpperCase();

                    if(sort_dir === 'asc'){
                        return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                    }else{
                        return (textA > textB) ? -1 : (textA < textB) ? 1 : 0;
                    }
                },
                'index': sortAlphabetical,
                'sourcetype': sortAlphabetical,
                'aws_account': sortAlphabetical,
                'status': function(a, b) {
                    var textA = a.entry.content.get('disabled') ? 1 : 0;
                    var textB = b.entry.content.get('disabled') ? 1 : 0;
                    if (sort_dir === 'asc') {
                        return (textA < textB) ? -1 : (textA > textB) ?
                            1 : 0;
                    } else {
                        return (textA > textB) ? -1 : (textA < textB) ?
                            1 : 0;
                    }
                },
                'service': sortAlphabetical
            };
            sortable.sort(handler[sort_key]);
            this.inputs.reset(sortable);
        }
    });
    var inputs = new inputs_view();
    inputs.render();
});
