require([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap',
    'splunk.util',
    'models/Base',
    'app/collections/Accounts',
    'app/collections/IAMRoles',
    'app/collections/Billings',
    'app/collections/Configs',
    'app/collections/CloudTrails',
    'app/collections/CloudWatchs',
    'app/collections/S3s',
    'app/collections/Descriptions',
    'app/collections/CloudWatchLogs',
    'app/collections/Kinesises',
    'app/collections/ConfigRules',
    'app/collections/Inspectors',
    'app/collections/SQSs',
    'app/collections/Logs',
    'app/collections/ProxyBase.Collection',
    'app/views/AccountsTable/Table',
    'app/views/IAMRolesTable/Table',
    'app/models/Base.Model',
    'app/models/appData',
    'views/shared/tablecaption/Master',
    'views/shared/controls/SyntheticRadioControl',
    'views/shared/controls/SyntheticSelectControl',
    'contrib/text!app/templates/Configuration/AWSProxy.html',
    'contrib/text!app/templates/Configuration/AWSLogging.html',
    'app/views/Dialogs/AddNewAccount',
    'app/views/Dialogs/AddNewIAMRole',
    "contrib/text!app/templates/Models/SavingMsg.html",
    "contrib/text!app/templates/Models/ErrorMsg.html",
    'contrib/text!app/templates/ConfigurationPageTemplate.html',
    'app/pages/common'
], function(
    $,
    _,
    Backbone,
    Bootstrap,
    splunkdUtils,
    BaseModel,
    Accounts,
    IAMRoles,
    Billings,
    Configs,
    CloudTrails,
    CloudWatchs,
    S3s,
    Descriptions,
    CloudWatchLogs,
    Kinesises,
    ConfigRules,
    Inspectors,
    SQSs,
    Logs,
    ProxyBase,
    AccountsTable,
    IAMRolesTable,
    Base_Model,
    appData,
    CaptionView,
    SyntheticRadioControl,
    SyntheticSelectControl,
    AWSProxyTemplate,
    AWSLoggingTemplate,
    AddNewAccount,
    AddNewIAMRole,
    SavingMsgView,
    ErrorMsgView,
    PageTemplate
) {
    // render template
    $('#main-section-body').append(PageTemplate);
    document.title = 'Configurations';

    $('.nav li').on('click', function() {
        var num = $(this).nextAll().length,
            max = $(".nav li").length - 1;
        if (num === max) {
            //$(this).removeClass('prev')
            $(this).nextAll().removeClass('prev');

        } else {
            $(this).prevAll().addClass('prev');
            $(this).nextAll().removeClass('prev');
        }
    });

    var configuration_view = Backbone.View.extend({
        initialize: function() {
            _.bindAll(this, 'renderAccount', 'renderIAMRole', 'renderProxy', 'saveProxy', 'getLoggingUrl', 'setLogging', 'renderLogging', 'getLogging', 'saveLogging', 'addAccount');
            //collections
            var appDataForConfig = {
                app: appData.get("app"),
                owner: appData.get("owner")
            };
            var viewConfig =  {
                appData: appDataForConfig,
                targetApp: "Splunk_TA_aws",
                targetOwner: "nobody"
            };
            this.billings = new Billings([], viewConfig);
            this.cloudtrails = new CloudTrails([], viewConfig);
            this.cloudwatchs = new CloudWatchs([], viewConfig);
            this.configs = new Configs([], viewConfig);
            this.s3s = new S3s([], viewConfig);
            this.descriptions = new Descriptions([], viewConfig);
            this.cloudwatchlogs = new CloudWatchLogs([], viewConfig);
            this.kinesises = new Kinesises([], viewConfig);
            this.configRules = new ConfigRules([], viewConfig);
            this.inspectors = new Inspectors([], viewConfig);
            this.sqss = new SQSs([], viewConfig);
            this.logs = new Logs([], viewConfig);

            //state model
            this.stateModel = new BaseModel();
            this.stateModel.set({
                sortKey: 'name',
                sortDirection: 'asc',
                count: 0,
                offset: 0,
                fetching: true
            });
            //accounts collection
            this.accounts = new Accounts([], viewConfig);
            this.iam_roles = new IAMRoles([], viewConfig);
            //stateModel event listener
            this.stateModel.on('change:sortDirection change:sortKey change:search change:offset', _.debounce(function() {
                var state_deferred = this.fetchListCollection(this.accounts, this.stateModel);
                state_deferred.done(function() {
                    _.each(this.accounts.models, function(account) {
                        var count = 0;
                        _.each(this.combineCollection().models, function(model) {
                            if (account.entry.content.attributes.name === model.entry.content.attributes.aws_account) {
                                count += 1;
                            }
                        }.bind(this));
                        account.entry.content.attributes.inputs = count;
                        count = 0;
                    }.bind(this));
                    // this.stateModel.set('fetching', false);
                }.bind(this));
            }.bind(this), 0), this);

            this.accountRendered = false;
            this.iamroleRendered = false;
            this.proxyRendered = false;
            this.loggingRendered = false;

            this.renderAccount();

            $('#account-li').on('click', this.renderAccount);
            $('#iam-role-li').on('click', this.renderIAMRole);

            //AWS Proxy
            var AWSProxyModel = Base_Model.extend({
                url: 'splunk_ta_aws/settings/proxy4ui'
            });
            var awsProxy = new AWSProxyModel();
            awsProxy.set('name', 'aws_proxy');
            this.awsProxyUrl = awsProxy._getFullUrl() + '?output_mode=json';
            this.renderProxy();


            //AWS Logging
            this.awsLoggingBillingUrl = this.getLoggingUrl('billing');
            this.awsLoggingConfigUrl = this.getLoggingUrl('config');
            this.awsLoggingCloudTrailUrl = this.getLoggingUrl('cloudtrail');
            this.awsLoggingCloudWatchUrl = this.getLoggingUrl('cloudwatch');
            this.awsLoggingS3Url = this.getLoggingUrl('s3');
            this.awsLoggingDescriptionUrl = this.getLoggingUrl('description');
            this.awsLoggingCloudWatchLogsUrl = this.getLoggingUrl('cloudwatch-logs');
            this.awsLoggingKinesisUrl = this.getLoggingUrl('kinesis');
            this.awsLoggingInspectorUrl = this.getLoggingUrl('inspector');
            this.awsLoggingConfigRuleUrl = this.getLoggingUrl('config-rule');
            this.awsLoggingSQSUrl = this.getLoggingUrl('sqs');
            this.awsLoggingLogsUrl = this.getLoggingUrl('logs');

            this.renderLogging(0);
        },

        renderAccount: function() {
            if (!this.accountRendered) {
                //render add server button
                var add_button_data = {
                    button_id: "addAccountBtn",
                    button_value: "Create New Account"
                };

                var account_deferred = this.fetchListCollection(this.accounts, this.stateModel);
                var all_deferred = this.fetchAllCollection(new BaseModel({
                    sortKey: 'name',
                    sortDirection: 'asc',
                    count: 0,
                    offset: 0,
                    fetching: true
                }));
                account_deferred.done(function() {
                    all_deferred.done(function() {
                        _.each(this.accounts.models, function(account) {
                            var count = 0;
                            _.each(this.combineCollection().models, function(model) {
                                if (account.entry.content.attributes.name === model.entry.content.attributes.aws_account) {
                                    count += 1;
                                }
                            }.bind(this));
                            account.entry.content.attributes.inputs = count;
                            count = 0;
                        }.bind(this));


                        //Caption
                        this.caption = new CaptionView({
                            countLabel: _('Accounts').t(),
                            model: {
                                state: this.stateModel
                            },
                            collection: this.accounts,
                            noFilterButtons: true,
                            filterKey: ['name', 'key_id']
                        });

                        //Create view
                        this.account_list = new AccountsTable({
                            model: this.stateModel,
                            collection: this.accounts,
                            allCollection: this.combineCollection(),
                            showActions: true
                        });

                        $('#account-tab').append(this.caption.render().$el);
                        $('#account-tab .table-caption-inner').prepend($(_.template(this.buttonTemplate, add_button_data)));
                        $('#account-tab').append(this.account_list.render().$el);

                        $('#addAccountBtn').on('click', function() {
                            var dlg = new AddNewAccount({
                                el: $("#new-account-dialog"),
                                collection: this.accounts
                            }).render();
                            dlg.modal();
                        }.bind(this));
                    }.bind(this));

                }.bind(this));
                this.accountRendered = true;
            }
        },

        renderIAMRole: function () {
            if (!this.iamroleRendered) {
                //render add server button
                var add_button_data = {
                    button_id: "addIAMRoleBtn",
                    button_value: "Create New IAM Role"
                };

                var iam_role_deferred = this.fetchListCollection(this.iam_roles, this.stateModel);
                var all_deferred = this.fetchAllCollection(new BaseModel({
                    sortKey: 'name',
                    sortDirection: 'asc',
                    count: 0,
                    offset: 0,
                    fetching: true
                }));
                iam_role_deferred.done(function() {
                    all_deferred.done(function() {
                        _.each(this.iam_roles.models, function(iam_role) {
                            var count = 0;
                            _.each(this.combineCollection().models, function(model) {
                                if (iam_role.entry.attributes.name === model.entry.content.attributes.aws_iam_role) {
                                    count += 1;
                                }
                            }.bind(this));
                            iam_role.entry.content.attributes.inputs = count;
                            count = 0;
                        }.bind(this));


                        //Caption
                        this.caption = new CaptionView({
                            countLabel: _('IAM Roles').t(),
                            model: {
                                state: this.stateModel
                            },
                            collection: this.iam_roles,
                            noFilterButtons: true,
                            filterKey: ['name']
                        });

                        //Create view
                        this.iam_role_list = new IAMRolesTable({
                            model: this.stateModel,
                            collection: this.iam_roles,
                            allCollection: this.combineCollection(),
                            showActions: true
                        });

                        $('#iam-role-tab').append(this.caption.render().$el);
                        $('#iam-role-tab .table-caption-inner').prepend($(_.template(this.buttonTemplate, add_button_data)));
                        $('#iam-role-tab').append(this.iam_role_list.render().$el);

                        $('#addIAMRoleBtn').on('click', function() {
                            var dlg = new AddNewIAMRole({
                                el: $("#new-iam-role-dialog"),
                                collection: this.iam_roles
                            }).render();
                            dlg.modal();
                        }.bind(this));
                    }.bind(this));

                }.bind(this));
                this.iamroleRendered = true;
            }
        },

        renderProxy: function() {
            if (!this.proxyRendered) {
                var main = this;
                $.ajax({
                    type: 'GET',
                    url: this.awsProxyUrl,
                    success: function(result) {
                        main.proxyRendered = true;
                        var awsProxyData = result.entry[0].content;

                        var $div = $(_.template(AWSProxyTemplate, {}));
                        $('#proxy-tab').html($div);

                        var password = '';
                        if (awsProxyData.username) {
                            password = '********';
                        }

                        $('#aws-proxy-host').val(awsProxyData.host);
                        $('#aws-proxy-port').val(awsProxyData.port);
                        $('#aws-proxy-user').val(awsProxyData.username);
                        $('#aws-proxy-user-old').val(awsProxyData.username);
                        $('#aws-proxy-pass').val(password);
                        $('#aws-proxy-pass').prop('default_val', password);
                        if (awsProxyData.disabled === '0' || awsProxyData.disabled === false) {
                            $('#aws-proxy-enable').prop('checked', true);
                        }
                        $('#aws-proxy-save').on('click', main.saveProxy);

                        $('#aws-proxy-pass').focus(function() {
                            if ($(this).val() === $(this).prop('default_val')) {
                                $(this).val('');
                            }
                        }).blur(function() {
                            if ($(this).val() === '' && $('#aws-proxy-user').val() === $('#aws-proxy-user-old').val()) {
                                $(this).val($(this).prop('default_val'));
                            }
                        });

                        $('#aws-proxy-user').change(function() {
                            if ($(this).val() === $('#aws-proxy-user-old').val()) {
                                $('#aws-proxy-pass').val($('#aws-proxy-pass').prop('default_val'));
                            } else {
                                $('#aws-proxy-pass').val('');
                            }
                        });
                    },
                    error: function( /*xhr, status, error*/ ) {

                    }
                });
            }
        },

        parseErrorMsg: function(data) {
            var error_msg = '';
            try {
                var rsp = JSON.parse(data.responseText);
                var regx = /External handler failed.+and output:\s+'([\s\S]*)'\.\s+See splunkd\.log for stderr output\./;
                var msg = String(rsp.messages[0].text);
                var matches = regx.exec(msg);
                if (!matches || !matches[1]) {
                    // try to extract another one
                    regx = /In handler[^:]+:\s+(.*)/;
                    matches = regx.exec(msg);
                    if (!matches || !matches[1]) {
                        matches = [msg];
                    }
                }
                error_msg = matches[1];
            } catch (err) {
                error_msg = "ERROR in processing the request";
            }
            return error_msg.replace(/Splunk Add-on REST Handler ERROR\[\d{1,6}\]\: /, '');
        },

        getProxyData: function() {
            var host = $('#aws-proxy-host').val();
            var port = $('#aws-proxy-port').val();
            var username = $('#aws-proxy-user').val();
            var password = $('#aws-proxy-pass').val();
            if (username === $('#aws-proxy-user-old').val() && password === $('#aws-proxy-pass').prop('default_val')) {
                password = '';
            }
            var disabled = '1';
            if ($('#aws-proxy-enable').is(':checked')) {
                disabled = '0';
            }

            if (disabled === '0' && !host) {
                this.addErrorMsg('#aws-proxy-modal-body', 'Host is required for an enabled proxy.');
                return null;
            }

            var matches = /^\d*$/.exec(port);
            if (!matches) {
                this.addErrorMsg('#aws-proxy-modal-body', 'Invalid port.');
                return null;
            }
            var intPort = parseInt(port);
            if (_.isNaN(intPort) || intPort < 1 || intPort > 65535) {
                this.addErrorMsg('#aws-proxy-modal-body', 'Invalid port.');
                return null;
            }

            if (host && !port) {
                this.addErrorMsg('#aws-proxy-modal-body', 'Port is required for non-empty host.');
                return null;
            }

            if (port && !host) {
                this.addErrorMsg('#aws-proxy-modal-body', 'Host is required for non-empty port.');
                return null;
            }

            if (username && !host) {
                this.addErrorMsg('#aws-proxy-modal-body', 'Host is required for non-empty username.');
                return null;
            }

            if (password && !username) {
                this.addErrorMsg('#aws-proxy-modal-body', 'Username is required for non-empty password.');
                return null;
            }

            if (username && username !== $('#aws-proxy-user-old').val() && !password) {
                this.addErrorMsg('#aws-proxy-modal-body', 'Password is required for a non-empty modified/new username');
                return null;
            }


            return {
                host: host,
                port: port,
                username: username,
                password: password,
                disabled: disabled
            };
        },

        saveProxy: function() {
            var data = this.getProxyData();
            if (data == null) {
                return;
            }
            var main = this;
            $('#aws-proxy-save').prop('disabled', true);
            this.addSavingMsg('#aws-proxy-modal-body', 'AWS Proxy Infomation');
            main.removeErrorMsg('#aws-proxy-modal-body');
            $.ajax({
                type: 'POST',
                url: this.awsProxyUrl,
                data: {
                    host: data.host,
                    port: data.port,
                    username: data.username,
                    password: data.password,
                    disabled: data.disabled
                },
                success: function( /*result, status, xhr*/ ) {
                    main.proxyRendered = false;
                    main.renderProxy();
                },
                error: function(xhr) {
                    main.removeSavingMsg('#aws-proxy-modal-body');
                    main.addErrorMsg('#aws-proxy-modal-body', main.parseErrorMsg(xhr));
                    $('#aws-proxy-save').prop('disabled', false);
                }
            });
        },


        getLoggingUrl: function(product) {
            if (product === 'logs') {
                var AWSLoggingModel = Base_Model.extend({
                    url: 'splunk_ta_aws/settings/ta_aws_settings/'
                });
                var awsLogging = new AWSLoggingModel();
                awsLogging.set('name', 'splunk_ta_aws_' + product);
                return awsLogging._getFullUrl() + '?output_mode=json';
            }

            var AWSLoggingModel = Base_Model.extend({
                url: 'splunk_ta_aws/settings/' + product + '_setting'
            });
            var awsLogging = new AWSLoggingModel();
            awsLogging.set('name', 'logging');
            return awsLogging._getFullUrl() + '?output_mode=json';
        },

        renderLogging: function(refresh) {
            if (!this.loggingRendered) {
                var main = this;
                if (!refresh) {
                    var $div = $(_.template(AWSLoggingTemplate, {}));
                    $('#logging-tab').html($div);
                }

                $.when(
                    this.getLogging(this.awsLoggingBillingUrl, "#aws-logging-billing"),
                    this.getLogging(this.awsLoggingConfigUrl, "#aws-logging-config"),
                    this.getLogging(this.awsLoggingCloudTrailUrl, "#aws-logging-cloudtrail"),
                    this.getLogging(this.awsLoggingCloudWatchUrl, "#aws-logging-cloudwatch"),
                    this.getLogging(this.awsLoggingS3Url, "#aws-logging-s3"),
                    this.getLogging(this.awsLoggingDescriptionUrl, "#aws-logging-description"),
                    this.getLogging(this.awsLoggingCloudWatchLogsUrl, "#aws-logging-cloudwatch-logs"),
                    this.getLogging(this.awsLoggingKinesisUrl, "#aws-logging-kinesis"),
                    this.getLogging(this.awsLoggingInspectorUrl, "#aws-logging-inspector"),
                    this.getLogging(this.awsLoggingConfigRuleUrl, "#aws-logging-config-rule"),
                    this.getLogging(this.awsLoggingSQSUrl, "#aws-logging-sqs"),
                    this.getLogging(this.awsLoggingLogsUrl, "#aws-logging-logs")
                ).done(function() {
                    main.loggingRendered = true;
                    $('#aws-logging-save').on('click', main.saveLogging);
                    $('#aws-logging-save').prop('disabled', false);
                }).fail(function() {
                    main.addErrorMsg('#aws-logging-modal-body', 'Fail to get logging infomation. Please refresh it.');
                });
            }
        },

        getLogging: function(url, selectId) {
            return $.ajax({
                type: 'GET',
                url: url,
                success: function(result) {
                    var awsLoggingData = result.entry[0].content;
                    $(selectId + ' option').filter(function() {
                        return $(this).text() === awsLoggingData.level;
                    }).prop('selected', true);
                }
            });
        },

        saveLogging: function() {
            var main = this;
            $('#aws-logging-save').prop('disabled', true);
            this.addSavingMsg('#aws-logging-modal-body', 'AWS Add-on Logging Information');
            main.removeErrorMsg('#aws-logging-modal-body');
            $.when(
                this.setLogging(this.awsLoggingBillingUrl, "#aws-logging-billing"),
                this.setLogging(this.awsLoggingConfigUrl, "#aws-logging-config"),
                this.setLogging(this.awsLoggingCloudTrailUrl, "#aws-logging-cloudtrail"),
                this.setLogging(this.awsLoggingCloudWatchUrl, "#aws-logging-cloudwatch"),
                this.setLogging(this.awsLoggingS3Url, "#aws-logging-s3"),
                this.setLogging(this.awsLoggingDescriptionUrl, "#aws-logging-description"),
                this.setLogging(this.awsLoggingCloudWatchLogsUrl, "#aws-logging-cloudwatch-logs"),
                this.setLogging(this.awsLoggingKinesisUrl, "#aws-logging-kinesis"),
                this.setLogging(this.awsLoggingInspectorUrl, "#aws-logging-inspector"),
                this.setLogging(this.awsLoggingConfigRuleUrl, "#aws-logging-config-rule"),
                this.setLogging(this.awsLoggingSQSUrl, "#aws-logging-sqs"),
                this.setLogging(this.awsLoggingLogsUrl, "#aws-logging-logs")
            ).done(function() {
                main.loggingRendered = false;
                main.renderLogging(1);
                main.removeSavingMsg('#aws-logging-modal-body');
            }).fail(function() {
                main.addErrorMsg('#aws-logging-modal-body', 'Fail to update logging infomation. Please try it again.');
                $('#aws-logging-save').prop('disabled', false);
                main.removeSavingMsg('#aws-logging-modal-body');
            });
        },

        setLogging: function(url, selectId) {
            return $.ajax({
                type: 'POST',
                url: url,
                data: {
                    level: $(selectId + ' option:selected').text()
                }
            });
        },

        addErrorMsg: function(container, text) {
            if ($(container + ' .msg-error').length) {
                $(container + ' .msg-text').text(text);
            } else {
                $(container + ".modal-body").prepend(_.template(ErrorMsgView, {
                    msg: text
                }));
            }
        },

        removeErrorMsg: function(container) {
            if ($(container + ' .msg-error').length) {
                $(container + ' .msg-error').remove();
            }
        },

        addSavingMsg: function(container, text) {
            if ($(container + ' .msg-loading').length) {
                $(container + ' .msg-text').text('Saving ' + text);
            } else {
                $(container + '.modal-body').prepend(_.template(SavingMsgView, {
                    msg: text
                }));
            }
        },

        removeSavingMsg: function(container) {
            if ($(container + ' .msg-loading').length) {
                $(container + ' .msg-loading').remove();
            }
        },


        addAccount: function() {

        },

        fetchAllCollection: function(stateModel) {
            return $.when(
                this.fetchListCollection(this.billings, stateModel),
                this.fetchListCollection(this.cloudtrails, stateModel),
                this.fetchListCollection(this.cloudwatchs, stateModel),
                this.fetchListCollection(this.configs, stateModel),
                this.fetchListCollection(this.s3s, stateModel),
                this.fetchListCollection(this.descriptions, stateModel),
                this.fetchListCollection(this.cloudwatchlogs, stateModel),
                this.fetchListCollection(this.kinesises, stateModel),
                this.fetchListCollection(this.configRules, stateModel),
                this.fetchListCollection(this.inspectors, stateModel),
                this.fetchListCollection(this.sqss, stateModel),
                this.fetchListCollection(this.logs, stateModel)
            );
        },

        combineCollection: function() {
            var temp_collection = new ProxyBase([], {
                appData: {
                    app: appData.get("app"),
                    owner: appData.get("owner")
                },
                targetApp: "Splunk_TA_aws",
                targetOwner: "nobody"
            });
            temp_collection.add(this.billings.models, {
                silent: true
            });
            temp_collection.add(this.cloudtrails.models, {
                silent: true
            });
            temp_collection.add(this.cloudwatchs.models, {
                silent: true
            });
            temp_collection.add(this.configs.models, {
                silent: true
            });
            temp_collection.add(this.s3s.models, {
                silent: true
            });
            temp_collection.add(this.descriptions.models, {
                silent: true
            });
            temp_collection.add(this.cloudwatchlogs.models, {
                silent: true
            });
            temp_collection.add(this.kinesises.models, {
                silent: true
            });
            temp_collection.add(this.configRules.models, {
                silent: true
            });
            temp_collection.add(this.inspectors.models, {
                silent: true
            });
            temp_collection.add(this.sqss.models, {
                silent: true
            });
            temp_collection.add(this.logs.models, {
                silent: true
            });
            return temp_collection;
        },

        fetchListCollection: function(collection, stateModel) {
            var search = '';
            if (stateModel.get('search')) {
                search = stateModel.get('search');
            }

            stateModel.set('fetching', true);
            return collection.fetch({
                data: {
                    sort_dir: stateModel.get('sortDirection'),
                    sort_key: stateModel.get('sortKey').split(','),
                    search: search,
                    count: stateModel.get('count'),
                    offset: stateModel.get('offset')
                },
                success: function() {
                    stateModel.set('fetching', false);
                }.bind(this)
            });
        },

        buttonTemplate: [
            '<div class = "add-button">',
            '<div class="btn-group btn-group-sm">',
            '<button type="button" class="btn add-button btn-primary" id="<%= button_id%>">',
            '<%= button_value%>',
            '</button>',
            '</div>',
            '</div>'
        ].join('')
    });

    new configuration_view();
});
