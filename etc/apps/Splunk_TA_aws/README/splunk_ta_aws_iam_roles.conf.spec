[<name>]
arn = ARN of AWS IAM Role. Configuring roles by directly editing splunk_ta_aws_iam_roles.conf is not supported. Use the IAM role tab under the add-on's Configuration menu in Splunk Web to manage IAM roles.
