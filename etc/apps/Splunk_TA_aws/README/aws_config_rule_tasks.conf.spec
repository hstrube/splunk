[<name>]
account = AWS account used to connect to AWS
region = AWS region
rule_names = Config Rule Names in a comma-separated list. Leave empty to collect all rules.
sourcetype = Sourcetype
polling_interval = Data collection interval in seconds. Default is 300 seconds
index = The index you want to put data in.
